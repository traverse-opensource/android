package ch.mobilethinking.traverse;

import android.content.Context;
import android.test.InstrumentationTestRunner;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.api.RemoteHelper;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
/*import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;*/

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest extends InstrumentationTestRunner{

    //private final OkHttpClient client = new OkHttpClient();
    private Context instrumentationCtx;

    @Before
    public void setup() {
        instrumentationCtx = getContext();
    }


    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void path_checkPathApi() throws Exception {
        String remoteRootPath = RemoteHelper.getRootRemotePath();
        assertEquals("http://192.168.0.249:3000", remoteRootPath);
    }

    @Test
    public void api_checkFicheApi() throws Exception {
        String allFiches = RemoteHelper.getRootRemotePath() + "/fiches";

        APIManager.getFicheManager(instrumentationCtx.getApplicationContext()).getFiches().onSuccess(new Continuation<Fiche.FicheList, Void>() {
            @Override
            public Void then(final Task<Fiche.FicheList> task) throws Exception {
                final Fiche.FicheList itemList = task.getResult();

                assertTrue("size> " + itemList.getId(), 0 == 0);

                //intent.putExtra(ZoneActivity.EXTRA_ZONE_ITEMS_KEY, itemList);
                //context.startActivity(intent);

                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {
                //TODO handle error!
                return null;
            }
        });
    }
}