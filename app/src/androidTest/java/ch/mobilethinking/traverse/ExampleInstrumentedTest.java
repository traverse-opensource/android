package ch.mobilethinking.traverse;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Locale;
import java.util.concurrent.CountDownLatch;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.api.dao.ApiDAO;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest{

    private String TAG = "Traverse:IntrumentTest";

    private Context appContext;

    @Before
    public void setup() {
        appContext = InstrumentationRegistry.getTargetContext();
    }

    @Test
    public void useAppContext() throws Exception {

        assertEquals("ch.mobilethinking.traverse.debug", appContext.getPackageName());
    }

    //:{"lat":46.332343, "lng": 6.381772}}
    @Test
    public void testGeolocBaseOnLocationInsteadOfUserId() throws Exception {
        ApiDAO.getMixinDAO(appContext).getSuggestion(46.332343, 6.381772, null).onSuccessTask(new Continuation<MixinItem[], Task<Void>>() {
            @Override
            public Task<Void> then(Task<MixinItem[]> task) throws Exception {

                assertTrue(task.getResult().length > 0);

                return null;
            }
        });
    }

    @Test
    public void testGetHearStrokesFromMixin(){
        ApiDAO.getMixinDAO(appContext).getHeartStrokes().onSuccessTask(new Continuation<MixinItem[], Task<Void>>() {
            @Override
            public Task<Void> then(Task<MixinItem[]> task) throws Exception {

                if (task.isFaulted() || task.isCancelled()) {
                    assertFalse("Something was wrong in the request", true);
                }

                MixinItem[] result = task.getResult();

                assertTrue("not empty fiche heart strokes", (ListCombinator.arrayToList(result, Fiche.class)).size() > 0);
                assertTrue("not empty playlist heart strokes", (ListCombinator.arrayToList(result, Playlist.class)).size() > 0);

                return null;
            }
        });
    }

    @Test
    public void testTags() {
        System.out.println("ok");
        ApiDAO.getFilterDAO(appContext).getTags().onSuccessTask(new Continuation<Tag[], Task<Tag[]>>() {
            @Override
            public Task<Tag[]> then(Task<Tag[]> task) throws Exception {

                if (task.isFaulted() || task.isCancelled()) {
                    assertFalse("Something was wrong in the request," + task.getError().getMessage(), true);
                }

                Tag[] result = task.getResult();

                Log.wtf("testTags", result.toString());

                assertTrue(result.toString(), result.toString().length() > 0);

                System.out.println(result.toString());

                return task;
            }
        });
    }

    @Test
    public void testFiltersNoFilters() {

        assertTrue("Starting without filters", true);

        final CountDownLatch signal = new CountDownLatch(1);

        ApiDAO.getFilterDAO(appContext).getPaginatedResults(6.0, 1.0, 1, 10, null).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {

                if (task.isCancelled() || task.isFaulted()) {
                    assertFalse("Something is wrong here> " + task.getError().getMessage(), true);
                }

                MixinItem[] items = task.getResult();

                int itemSize = task.getResult().length;

                String toDebug = "";

                for (MixinItem item: items) {
                    toDebug += String.format(Locale.ENGLISH, "<%s: %s>",
                            item.getType(),
                            item.getName()) + "\n";
                }

                assertTrue("Should not be empty> " + itemSize + toDebug, itemSize > 0);

                signal.countDown();

                return task;
            }
        });

        try{
            signal.await();
        }catch(Exception e) {
            assertFalse("Cannot use signal mechanism", true);
        }
    }

    @Test
    public void testFiltersUsingPlaylists() {

        assertTrue("Starting with filter", true);

        final CountDownLatch signal = new CountDownLatch(1);

        //Creating the arguments params
        final FilterCreator filter = new FilterCreator()
                .addThemeName("Sites naturels ou paysagers")
                .addTagId("591ea33efda54c285ef9a0cf")
                .addCategoryId("58d54043a071ddd0d1129a65")
                .addFicheType("Places")
                .usesPlaylist(true);

        ApiDAO.getFilterDAO(appContext).getPaginatedResults(6.0, 1.0, 1, 10, filter).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {

                if (task.isCancelled() || task.isFaulted()) {
                    assertTrue("Something is wrong here> " + task.getError().getMessage(), true);
                }

                MixinItem[] items = task.getResult();

                int itemSize = task.getResult().length;

                String toDebug = "";

                for (MixinItem item: items) {
                    toDebug += String.format(Locale.ENGLISH, "<%s: %s>",
                            item.getType(),
                            item.getName()) + "\n";
                }

                assertTrue("result> "+ filter.build() + "######" + itemSize + "######" + toDebug, itemSize > 0);

                signal.countDown();

                return task;
            }
        });

        try{
            signal.await();
        }catch(Exception e) {
            assertFalse("Cannot use signal mechanism", true);
        }
    }
}
