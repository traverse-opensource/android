package ch.mobilethinking.traverse.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.model.User;

/**
 * Created by Carlos Ballester on 31-May-17.
 */

public class PreferenceManager {

    public static void setViewedFiches(Context context, HashMap<String, Boolean> viewedFiches) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(viewedFiches);
        sharedPreferencesEditor.putString("viewedFiches", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static HashMap<String, Boolean> getViewedFiches(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        if (sharedPreferences.contains("viewedFiches")) {
            final Gson gson = new Gson();
            return gson.fromJson(sharedPreferences.getString("viewedFiches", ""), HashMap.class);
        }
        return new HashMap<String, Boolean>();
    }

    public static void setSavedPlaylists(Context context, HashMap<String, Boolean> savedPlaylists) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(savedPlaylists);
        sharedPreferencesEditor.putString("savedPlaylists", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static HashMap<String, Boolean> getSavedPlaylists(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        if (sharedPreferences.contains("savedPlaylists")) {
            final Gson gson = new Gson();
            return gson.fromJson(sharedPreferences.getString("savedPlaylists", ""), HashMap.class);
        }
        return new HashMap<String, Boolean>();
    }

    public static void addPlaylistToFavourites (Context context, String playlistId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        HashMap<String, Boolean> savedPlaylists = new HashMap<>();
        final Gson gson = new Gson();
        if (sharedPreferences.contains("savedPlaylists")) {
            savedPlaylists = gson.fromJson(sharedPreferences.getString("savedPlaylists", ""), HashMap.class);
        }
        savedPlaylists.put(playlistId, true);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        String serializedObject = gson.toJson(savedPlaylists);
        sharedPreferencesEditor.putString("savedPlaylists", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static void removePlaylistFromFavourites (Context context, String playlistId) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        HashMap<String, Boolean> savedPlaylists = new HashMap<>();
        final Gson gson = new Gson();
        if (sharedPreferences.contains("savedPlaylists")) {
            savedPlaylists = gson.fromJson(sharedPreferences.getString("savedPlaylists", ""), HashMap.class);
        }
        savedPlaylists.remove(playlistId);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        String serializedObject = gson.toJson(savedPlaylists);
        sharedPreferencesEditor.putString("savedPlaylists", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static Integer getSearchRadius(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        if (sharedPreferences.contains("searchRadius")) {
            return sharedPreferences.getInt("searchRadius", 30000);
        }
        return 30000;
    }

    public static void setSearchRadius(Context context, Integer radius) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putInt("searchRadius", radius);
        sharedPreferencesEditor.apply();
    }

    public static void setSavedFilters(Context context, ArrayList<FilterCreator> savedFilters) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(savedFilters);
        sharedPreferencesEditor.putString("savedFilters", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static ArrayList<FilterCreator> getSavedFilters(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        if (sharedPreferences.contains("savedFilters")) {
            final Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<FilterCreator>>(){}.getType();
            return gson.fromJson(sharedPreferences.getString("savedFilters", ""), type);
        }
        return new ArrayList<FilterCreator>();
    }

    public static User getLogedUser(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        if (sharedPreferences.contains("logedUser")) {
            final Gson gson = new Gson();
            return gson.fromJson(sharedPreferences.getString("logedUser", ""), User.class);
        }
        return null;
    }

    public static void setLogedUser(Context context, User user) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        final Gson gson = new Gson();
        String serializedObject = gson.toJson(user);
        sharedPreferencesEditor.putString("logedUser", serializedObject);
        sharedPreferencesEditor.apply();
    }

    public static void logOut(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.remove("logedUser");
        sharedPreferencesEditor.apply();
    }

    public static boolean getTutorialDone(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        if (sharedPreferences.contains("tutorialDone")) {
            return sharedPreferences.getBoolean("tutorialDone", false);
        }
        return false;
    }

    public static void setTutorialDone(Context context, boolean tutorialDone) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("Traverse", 0);
        SharedPreferences.Editor sharedPreferencesEditor = sharedPreferences.edit();
        sharedPreferencesEditor.putBoolean("tutorialDone", tutorialDone);
        sharedPreferencesEditor.apply();
    }

}
