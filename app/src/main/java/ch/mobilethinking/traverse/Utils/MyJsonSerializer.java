package ch.mobilethinking.traverse.Utils;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import java.io.IOException;

/**
 * Created by Carlos Ballester on 27-Sep-17.
 */

public class MyJsonSerializer<T, X>  implements CacheSerializer<T> {
    private final ObjectMapper mapper;
    private final Class<T> castTo;
    private final Class<X> clazz;

    public MyJsonSerializer(Class<X> clazz, Class<T> castTo) {
        this.clazz = clazz;
        this.castTo = castTo;
        mapper = new ObjectMapper();
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        mapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
    }

    @Override
    public T fromString(String data) {
        try {
            return castTo.cast(mapper.readValue(data, clazz));
        } catch (IOException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException();
    }

    @Override
    public String toString(T object) {
        try {
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        throw new IllegalStateException();
    }

}
