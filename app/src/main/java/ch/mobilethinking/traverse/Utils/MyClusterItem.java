package ch.mobilethinking.traverse.Utils;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Carlos Ballester on 01-Jun-17.
 */

public class MyClusterItem implements ClusterItem, Parcelable {
    private LatLng mPosition;
    private String mTitle;
    private String mSnippet;
    private int mIcon;
    private String mThemeColor;
    private String mId;

    public MyClusterItem(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public MyClusterItem(double lat, double lng, String title, String snippet) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
    }

    public MyClusterItem(double lat, double lng, String title, String snippet, int icon, String color, String id) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
        mIcon = icon;
        mThemeColor = color;
        mId = id;
    }

    protected MyClusterItem(Parcel in) {
        mTitle = in.readString();
        mSnippet = in.readString();
        mIcon = in.readInt();
        mThemeColor = in.readString();
        mId = in.readString();
        mPosition = in.readParcelable(LatLng.class.getClassLoader());
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }

    public int getIcon() {
        return mIcon;
    }

    public String getThemeColor() {
        return mThemeColor;
    }

    public String getId() {
        return mId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mSnippet);
        dest.writeInt(mIcon);
        dest.writeString(mThemeColor);
        dest.writeString(mId);
        mPosition.writeToParcel(dest, flags);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<MyClusterItem> CREATOR = new Parcelable.Creator<MyClusterItem>() {
        @Override
        public MyClusterItem createFromParcel(Parcel in) {
            return new MyClusterItem(in);
        }

        @Override
        public MyClusterItem[] newArray(int size) {
            return new MyClusterItem[size];
        }
    };

}
