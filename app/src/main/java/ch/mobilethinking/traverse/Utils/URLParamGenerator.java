package ch.mobilethinking.traverse.Utils;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import ch.mobilethinking.traverse.BuildConfig;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;

/**
 * Created by Carlos Ballester on 19-May-17.
 */

public class URLParamGenerator {

    public static String generate(@Nullable HashMap<String, ArrayList<Object>> arguments){
        String params = "";
        if(arguments != null && arguments.size() > 0){
            for (String s: arguments.keySet()
                    ) {
                for (Object o: arguments.get(s)
                        ) {
                    params += "&" + s + "=" + String.valueOf(o);
                }
            }
        }
        return params;
    }

    public static String generateShareUrl(MixinItem i) {
        return BuildConfig.API_HOST + (TextUtils.isEmpty(BuildConfig.API_PORT ) ? "" : ":" + BuildConfig.API_PORT) + "/public/" + (i instanceof Fiche ? "fiche/" : "playlist/") + i.getId();
    }

}
