package ch.mobilethinking.traverse.Utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.style.URLSpan;
import android.view.View;

import ch.mobilethinking.traverse.Views.Activities.WebViewActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;

/**
 * Created by Carlos Ballester on 29-May-17.
 */

@SuppressLint("ParcelCreator")
public class LinkSpan extends URLSpan {

    private Activity activity;

    public LinkSpan(Activity activity, String url) {
        super(url);
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        String url = getURL();
        Intent i = new Intent(activity, WebViewActivity.class);
        i.putExtra("url", url);
        activity.startActivity(i);
    }
}
