package ch.mobilethinking.traverse.Utils;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import ch.mobilethinking.traverse.R;

/**
 * Created by Carlos Ballester on 17-Aug-17.
 */

public class MessageUtil {

    public static void genericError (Activity activity) {
        if(!checkConnection(activity)) {
            Toast.makeText(activity, activity.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(activity, activity.getString(R.string.generic_error), Toast.LENGTH_LONG).show();
        }
    }

    public static void connectivityError (Activity activity) {
        if(!checkConnection(activity)) {
            Toast.makeText(activity, activity.getString(R.string.no_internet), Toast.LENGTH_LONG).show();
        }
    }

    public static void showMessage (Activity activity, String message) {
        Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
    }

    public static void showMessage (Activity activity, int message) {
        Toast.makeText(activity, activity.getString(message), Toast.LENGTH_LONG).show();
    }

    public static boolean checkConnection (Activity activity) {
        ConnectivityManager cm =
                (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public static boolean checkConnection (Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

}
