package ch.mobilethinking.traverse.Utils;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;

/**
 * Created by Carlos Ballester on 19-May-17.
 */

public class ListCombinator {

    public static List<Object> combineLists(Object[] firstList, Object[] secondList){
        List<Object> combined = new ArrayList<>();
        if(firstList != null && firstList.length > 0) {
            for (Object o1 : firstList
                    ) {
                combined.add(o1);
            }
        }
        if(secondList != null && secondList.length > 0) {
            for (Object o2 : secondList
                    ) {
                combined.add(o2);
            }
        }
        return combined;
    }

    //maybe we can use the same method, but since I don't know the impact on the code, prefer to let it in comment
    public static List<Object> arrayToList(Object[] list) {
        return arrayToList(list, Object.class);
        /*List<Object> objectList = new ArrayList<>();
        if(list != null && list.length > 0) {
            for (Object o1 : list
                    ) {
                objectList.add(o1);
            }
        }
        return objectList;*/
    }

    //added generic type return for this static method to filter only by klazz type instances
    public static <T> List<T> arrayToList(Object[] list, Class<T> klazz) {
        List<T> objectList = new ArrayList<>();
        if(list != null && list.length > 0) {
            for (Object o1 : list
                    ) {
                if (klazz.isInstance(o1)){
                    objectList.add(klazz.cast(o1));
                }
            }
        }
        return objectList;
    }

    public static List<MyClusterItem> generateClusterItems (Fiche[] list) {
        List<MyClusterItem> clusterList = new ArrayList<>();
        if(list != null && list.length > 0) {
            for (Fiche f : list
                    ) {
                int icon = getTypeIcon(f);
                String color = f.getMainTheme().getHexColor();
                MyClusterItem item = new MyClusterItem(f.getLatitude(), f.getLongitude(), f.getName(), f.getCity(), icon, color, f.getId());
                clusterList.add(item);
            }
        }
        return clusterList;
    }

    public static List<MyClusterItem> generateLinkedClusterItems (LinkedFiche[] list) {
        List<MyClusterItem> clusterList = new ArrayList<>();
        if(list != null && list.length > 0) {
            for (LinkedFiche lf : list
                    ) {
                Fiche f = lf.getFiche();
                if(f.getLatitude() != null && f.getLongitude() != null) {
                    int icon = getTypeIcon(f);
                    String color = f.getMainTheme().getHexColor();
                    MyClusterItem item = new MyClusterItem(f.getLatitude(), f.getLongitude(), f.getName(), f.getCity(), icon, color, f.getId());
                    clusterList.add(item);
                }
            }
        }
        return clusterList;
    }

    public static int getTypeIcon (Fiche f) {
        if (f instanceof FichePlace){
            return R.drawable.ic_site;
        } else if (f instanceof FichePeople){
            return R.drawable.ic_people;
        } else if (f instanceof FicheObject){
            return R.drawable.ic_object;
        } else if (f instanceof FicheEvent){
            return R.drawable.ic_events;
        } else if (f instanceof FicheMedia){
            return R.drawable.ic_image;
        } else {
            return R.drawable.ic_site;
        }
    }

    public static String getThemeColor(Theme[] themes) {
        if(themes == null || themes.length == 0) {
            return "";
        } else {
            int ponderation = 0;
            Theme finalTheme = new Theme();
            for (Theme t: themes
                    ) {
                if(t.getPonderation() >= ponderation){
                    finalTheme = t;
                }
            }
            return finalTheme.getHexColor();
        }
    }

}
