package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Activities.FilterActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.Theme;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class ThemeAdapter extends RecyclerView.Adapter<ThemeAdapter.DataObjectHolder> {
    private LayoutInflater inflator;
    private FilterActivity activity;
    private List<Theme> list;
    private List<Boolean> selected;

    public ThemeAdapter(FilterActivity activity, List<Theme> list) {
        this.activity = activity;
        this.list = list;
        selected = new ArrayList<Boolean>();
        for (Theme t: list
                ) {
            if(activity.getFilter().getThemeNames().contains(t.getName())) {
                selected.add(true);
            } else {
                selected.add(false);
            }
        }
        activity.setCounts(activity.getThemeCountView(), R.plurals.theme_count,activity.getFilter().getThemeNames().size(), activity.getFilter().getThemeNames().size());
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public ThemeAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.category_theme_list_item, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }


    @Override
    public void onBindViewHolder(final ThemeAdapter.DataObjectHolder holder, final int position) {
        final Theme t = list.get(position);
        holder.title.setText(t.getName());
        holder.themeLine.setBackgroundColor(Color.parseColor(t.getHexColor()));

        if(selected.get(position)) {
            holder.checkbox.setChecked(true);
        } else {
            holder.checkbox.setChecked(false);
        }

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.checkbox.isChecked()) {
                    holder.checkbox.setChecked(false);
                    selected.set(position, false);
                    FilterCreator f = activity.getFilter();
                    f.removeThemeName(t.getName());
                    activity.setFilter(f);
                } else {
                    holder.checkbox.setChecked(true);
                    selected.set(position, true);
                    FilterCreator f = activity.getFilter();
                    f.addThemeName(t.getName());
                    activity.setFilter(f);
                }
                activity.setCounts(activity.getThemeCountView(), R.plurals.theme_count,activity.getFilter().getThemeNames().size(), activity.getFilter().getThemeNames().size());
            }
        });
        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.checkbox.isChecked()) {
                    holder.checkbox.setChecked(false);
                    selected.set(position, false);
                } else {
                    holder.checkbox.setChecked(true);
                    selected.set(position, true);
                }
                activity.setCounts(activity.getThemeCountView(), R.plurals.theme_count,activity.getFilter().getThemeNames().size(), activity.getFilter().getThemeNames().size());
            }
        });

    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private final CardView card;
        private final TextView title;
        private final AppCompatCheckBox checkbox;
        private final View themeLine;

        public DataObjectHolder(View itemView) {
            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card_item);
            title = (TextView) itemView.findViewById(R.id.title);
            checkbox = (AppCompatCheckBox) itemView.findViewById(R.id.checkbox);
            themeLine = (View) itemView.findViewById(R.id.theme_line);
        }
    }

}
