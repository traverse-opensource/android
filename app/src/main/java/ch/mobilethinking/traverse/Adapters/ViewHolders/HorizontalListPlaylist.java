package ch.mobilethinking.traverse.Adapters.ViewHolders;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.Views.Fragments.PlaylistFragment;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by Carlos Ballester on 16-May-17.
 */

public class HorizontalListPlaylist extends RecyclerView.ViewHolder implements View.OnClickListener {

    public static final int HORIZONTAL_LIST_PLAYLIST = 1;
    public CardView card;
    public ImageView coverImage;
    public View themeLine;
    public TextView itemNumber;
    public CardView themeIcon;
    public LinearLayout cardContentLayout;
    public TextView themeName;
    public TextView title;
    private Activity activity;
    private BaseFragment fragment;
    private Playlist playlist;

    public HorizontalListPlaylist(View itemView) {
        super(itemView);
        card = (CardView) itemView.findViewById(R.id.card_playlist_horizontal_list);
        coverImage = (ImageView) itemView.findViewById(R.id.cover_image);
        themeLine = (View) itemView.findViewById(R.id.theme_line);
        themeIcon = (CardView) itemView.findViewById(R.id.playlist_info);
        itemNumber = (TextView) itemView.findViewById(R.id.item_number);
        cardContentLayout = (LinearLayout) itemView.findViewById(R.id.card_content_layout);
        themeName = (TextView) itemView.findViewById(R.id.theme_name);
        title = (TextView) itemView.findViewById(R.id.title);
    }

    public CardView getCard() {
        return card;
    }

    public void setCard(CardView card) {
        this.card = card;
    }

    public ImageView getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(Activity activity, Object resource) {
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Glide.with(activity).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        }
    }

    public View getThemeLine() {
        return themeLine;
    }

    public void setThemeLine(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            this.themeLine.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
        }
    }

    public void setThemeIcon(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIcon.setCardBackgroundColor(Color.parseColor(color));
        } else {
            this.themeIcon.setCardBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
        }
    }

    public TextView getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(Activity activity, int items) {
        this.itemNumber.setText(activity.getResources().getQuantityString(R.plurals.playlist_number_fiches, items, items));
    }

    public LinearLayout getCardContentLayout() {
        return cardContentLayout;
    }

    public void setCardContentLayout(LinearLayout cardContentLayout) {
        this.cardContentLayout = cardContentLayout;
    }

    public TextView getThemeName() {
        return themeName;
    }

    public void setThemeName(Activity activity, String name) {
        if(!TextUtils.isEmpty(name)) {
            this.themeName.setText(name);
        }
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }

    public void setThemeSchema(Activity activity, Theme theme) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setThemeLine(activity, null);
            setThemeName(activity, " ");
            setThemeIcon(activity, null);
            this.themeName.setTextColor(activity.getResources().getColor(R.color.colorAccent));
        } else {
            setThemeLine(activity, theme.getHexColor());
            setThemeIcon(activity, theme.getHexColor());
            setThemeName(activity, theme.getName());
            this.themeName.setTextColor(Color.parseColor(theme.getHexColor()));
        }
    }

    public void setPlaylist (Activity activity, BaseFragment fragment, Playlist playlist) {
        this.activity = activity;
        this.fragment = fragment;
        this.playlist = playlist;
        setCoverImage(activity, playlist.getCoverPath());
        setThemeSchema(activity, playlist.getMainTheme());
        setItemNumber(activity, playlist.getLinkedFiches().length);
        setTitle(playlist.getName());
        if(this.fragment != null) {
            card.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View anchor) {
        // TODO Auto-generated method stub
        switch (anchor.getId()) {
            case R.id.card_playlist_horizontal_list:
                Bundle b = new Bundle();
                b.putSerializable("playlist", this.playlist);
                BaseFragment playlistFragment = PlaylistFragment.newInstance(b);
                playlistFragment.setTab(fragment.getTab());
                fragment.switchFragment(playlistFragment, PlaylistFragment.TAG, true);
                break;
        }
    }

}
