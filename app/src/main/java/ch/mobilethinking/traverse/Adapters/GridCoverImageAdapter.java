package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

import ch.mobilethinking.traverse.Adapters.ViewHolders.HorizontalListPlaylist;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by Carlos Ballester on 03-Aug-17.
 */

public class GridCoverImageAdapter extends BaseAdapter {

    private LayoutInflater inflator;
    private Activity activity;
    private List<String> list;

    public GridCoverImageAdapter(Activity activity, List<String> list) {
        this.activity = activity;
        this.list = list;
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item = inflator
                    .inflate(R.layout.cover_image_grid_item, parent, false);

        ImageView coverImage = (ImageView) item.findViewById(R.id.cover_image);
        final ImageView coverImageSelected = (ImageView) item.findViewById(R.id.cover_image_selected);

        if(!TextUtils.isEmpty(list.get(position)) && !list.get(position).contains("default")) {
            String imageUrl = "";
            if(list.get(position).split("http").length > 2){
                imageUrl = "http" + list.get(position).split("http")[2];
            } else {
                imageUrl = list.get(position);
            }
            Glide.with(activity).load(imageUrl).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(coverImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(coverImage);
        }

        coverImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(coverImageSelected.getVisibility() == View.GONE){
                    coverImageSelected.setVisibility(View.VISIBLE);
                } else {
                    coverImageSelected.setVisibility(View.GONE);
                }
            }
        });

        return item;
    }

}
