package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import org.json.JSONObject;

import java.io.File;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Views.Activities.FilterActivity;
import ch.mobilethinking.traverse.Views.Activities.LoginActivity;
import ch.mobilethinking.traverse.Views.Activities.WebViewActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.SocialFeed;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class SocialFeedAdapter extends RecyclerView.Adapter<SocialFeedAdapter.DataObjectHolder> {
    private final static String TAG = SocialFeedAdapter.class.getSimpleName();
    private LayoutInflater inflator;
    private Activity activity;
    private List<SocialFeed> list;

    public SocialFeedAdapter(Activity activity, List<SocialFeed> list) {
        this.activity = activity;
        this.list = list;
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public SocialFeedAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.card_social_feed, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }


    @Override
    public void onBindViewHolder(final SocialFeedAdapter.DataObjectHolder holder, final int position) {

        holder.cover.setVisibility(View.VISIBLE);
        holder.title.setVisibility(View.VISIBLE);
        holder.description.setVisibility(View.VISIBLE);
        final SocialFeed sf = list.get(position);

        if(sf.getKind().equalsIgnoreCase("traverse")) {
            holder.flagLayout.setVisibility(View.VISIBLE);
            holder.descriptionSeparator.setVisibility(View.VISIBLE);
            holder.flagLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flagFeedDialog(sf.getId());
                }
            });
        } else {
            holder.flagLayout.setVisibility(View.GONE);
            holder.descriptionSeparator.setVisibility(View.GONE);
        }

        Glide.with(activity).load(getTypeIcon(sf.getKind())).into(holder.network);
        if(!TextUtils.isEmpty(sf.getCreated_by().getFull_name())){
            holder.contributor.setText(sf.getCreated_by().getFull_name());
        }
        if(sf.getCover().getBig() != null && !TextUtils.isEmpty(sf.getCover().getBig())) {
            Glide.with(activity).load(sf.getCover().getBig()).into(holder.cover);
            holder.cover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(sf.getKind().equalsIgnoreCase("traverse")) {
                        showImageDialog(sf.getCover().getBig());
                    } else if (!TextUtils.isEmpty(sf.getPermalink())) {
                        Intent i = new Intent(activity, WebViewActivity.class);
                        i.putExtra("url", sf.getPermalink());
                        activity.startActivity(i);
                    } else {
                        showImageDialog(sf.getCover().getBig());
                    }
                }
            });
        } else {
            holder.cover.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(sf.getName())){
            holder.title.setText(sf.getName());
        } else {
            holder.title.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(sf.getDescription())){
            holder.description.setText(sf.getDescription());
        } else {
            holder.description.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private final CardView card;
        private final TextView title;
        private final TextView description;
        private final ImageView cover;
        private final CircleImageView network;
        private final TextView contributor;
        private final RelativeLayout flagLayout;
        private final View descriptionSeparator;

        public DataObjectHolder(View itemView) {
            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card_social_feed);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            contributor = (TextView) itemView.findViewById(R.id.contributor_name);
            cover = (ImageView) itemView.findViewById(R.id.cover_image);
            network = (CircleImageView) itemView.findViewById(R.id.social_image);
            flagLayout = (RelativeLayout) itemView.findViewById(R.id.action_flag_layout);
            descriptionSeparator = itemView.findViewById(R.id.description_separator);
        }
    }

    private int getTypeIcon (String kind) {
        switch (kind) {
            case "instagram":
                return R.drawable.instagram;
            case "twitter":
                return R.drawable.twitter;
            case "facebook":
                return R.drawable.facebook;
            default:
                return R.drawable.traverse_512_512;
        }
    }

    /**
     * Shows a dialog to allow password recovery
     */
    private void  showImageDialog(String imageUrl) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity, R.style.MyDialogTheme);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.dialog_image_overlay, null);
        builder.setView(dialog_layout);
        //ImageView close = (ImageView) dialog_layout.findViewById(R.id.dialog_close);
        ImageView image = (ImageView) dialog_layout.findViewById(R.id.dialog_image);
        final ProgressBar progress = (ProgressBar) dialog_layout.findViewById(R.id.dialog_image_progress);
        Glide.with(activity).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progress.setVisibility(View.GONE);
                return false;
            }
        }).into(image);
        final AlertDialog d = builder.create();
        d.show();
        /*close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });*/
    }

    /**
     * Shows a dialog to allow log in
     */
    private void flagFeedDialog(final String feedId) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.app_name));
        builder.setMessage(activity.getString(R.string.flag_dialog_text));
        builder.setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                flagFeed(feedId);
            }
        });
        builder.setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void flagFeed (final String feedId) {
        APIManager.getFicheManager(activity.getApplicationContext()).flagFeed(feedId).onSuccess(new Continuation<String, Void>() {

            @Override
            public Void then(final Task<String> task) throws Exception {
                final String feed = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# INDIVIDUAL FEED #################\n";
                        toWrite += feed;
                        Log.d(TAG, toWrite);
                        MessageUtil.showMessage(activity, R.string.flag_thanks);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(activity);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

}
