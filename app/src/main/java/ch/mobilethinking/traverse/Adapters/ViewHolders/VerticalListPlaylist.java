package ch.mobilethinking.traverse.Adapters.ViewHolders;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.bumptech.glide.Glide;

import java.util.Arrays;
import java.util.HashMap;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.GenericVerticalAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Utils.URLParamGenerator;
import ch.mobilethinking.traverse.Views.Activities.LoginActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.Views.Fragments.PlaylistFragment;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Carlos Ballester on 16-May-17.
 */

public class VerticalListPlaylist extends RecyclerView.ViewHolder implements View.OnClickListener {

    public static final int VERTICAL_LIST_PLAYLIST = 3;
    private static final String TAG = VerticalListPlaylist.class.getSimpleName();
    public CardView card;
    public LinearLayout cardHeaderLayout;
    public CircleImageView contributorImage;
    public TextView contributorName;
    public ImageView otherActions;
    public ImageView coverImage;
    public View themeLine;
    public CardView themeIcon;
    public TextView itemNumber;
    public LinearLayout cardContentLayout;
    public TextView themeName;
    public TextView title;
    public HorizontalScrollView scrollHashtags;
    public LinearLayout layoutHashtags;
    public TextView description;
    public ImageView likeAction;
    public TextView likeNumber;
    public RelativeLayout likeActionLayout;
    public ImageView shareAction;
    public RelativeLayout shareActionLayout;
    public RelativeLayout favouriteActionLayout;
    public ImageView favouriteAction;
    public TextView labelsTitle;
    public ChipCloud labelGrid;
    private PopupMenu popupMenu;
    private Activity activity;
    private BaseFragment fragment;
    private Playlist playlist;
    private int position;
    private GenericVerticalAdapter adapter;
    private boolean isFavourite = false;
    private boolean liked = false;

    public VerticalListPlaylist(View itemView) {
        super(itemView);
        card = (CardView) itemView.findViewById(R.id.card_playlist_vertical_list);
        cardHeaderLayout = (LinearLayout) itemView.findViewById(R.id.card_header_content);
        contributorImage = (CircleImageView) itemView.findViewById(R.id.contributor_image);
        contributorName = (TextView) itemView.findViewById(R.id.contributor_name);
        //otherActions = (ImageView) itemView.findViewById(R.id.other_actions);
        coverImage = (ImageView) itemView.findViewById(R.id.cover_image);
        themeLine = (View) itemView.findViewById(R.id.theme_line);
        themeIcon = (CardView) itemView.findViewById(R.id.theme_icon);
        itemNumber = (TextView) itemView.findViewById(R.id.item_number);
        cardContentLayout = (LinearLayout) itemView.findViewById(R.id.card_content_layout);
        themeName = (TextView) itemView.findViewById(R.id.theme_name);
        title = (TextView) itemView.findViewById(R.id.title);
        scrollHashtags = (HorizontalScrollView) itemView.findViewById(R.id.scroll_hashtags);
        layoutHashtags = (LinearLayout) itemView.findViewById(R.id.layout_hashtags);
        description = (TextView) itemView.findViewById(R.id.description);
        likeAction = (ImageView) itemView.findViewById(R.id.action_like_icon);
        likeNumber = (TextView) itemView.findViewById(R.id.action_like_label);
        likeActionLayout = (RelativeLayout) itemView.findViewById(R.id.action_like_layout);
        shareAction = (ImageView) itemView.findViewById(R.id.action_share_icon);
        shareActionLayout = (RelativeLayout) itemView.findViewById(R.id.action_share_layout);
        favouriteActionLayout = (RelativeLayout) itemView.findViewById(R.id.action_favourite_layout);
        favouriteAction = (ImageView) itemView.findViewById(R.id.action_favourite_icon);
        labelsTitle = (TextView) itemView.findViewById(R.id.labels_title);
        labelGrid = (ChipCloud) itemView.findViewById(R.id.labels_grid);
    }

    public CardView getCard() {
        return card;
    }

    public void setCard(CardView card) {
        this.card = card;
    }

    public LinearLayout getCardHeaderLayout() {
        return cardHeaderLayout;
    }

    public void setCardHeaderLayout(LinearLayout cardHeaderLayout) {
        this.cardHeaderLayout = cardHeaderLayout;
    }

    public CircleImageView getContributorImage() {
        return contributorImage;
    }

    public void setContributorImage(Activity activity, Object resource) {
        if(resource != null && !TextUtils.isEmpty((String)resource)) {
            Glide.with(activity).load(resource).into(this.contributorImage);
        } else {
            Glide.with(activity).load(R.drawable.default_contributor).into(this.contributorImage);
        }
    }

    public TextView getContributorName() {
        return contributorName;
    }

    public void setContributorName(String name) {
        if(!TextUtils.isEmpty(name)) {
            this.contributorName.setText(name);
        }
    }

    public ImageView getOtherActions() {
        return otherActions;
    }

    public void setOtherActions(ImageView otherActions) {
        this.otherActions = otherActions;
    }

    public ImageView getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(Activity activity, Object resource) {
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Glide.with(activity).load(imageUrl).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        }
    }

    public View getThemeLine() {
        return themeLine;
    }

    public void setThemeLine(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            this.themeLine.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
        }
    }

    public void setThemeIcon(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIcon.setCardBackgroundColor(Color.parseColor(color));
        } else {
            this.themeIcon.setCardBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
        }
    }

    public TextView getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(Activity activity, int items) {
        this.itemNumber.setText(activity.getResources().getQuantityString(R.plurals.playlist_number_fiches, items, items));
    }

    public LinearLayout getCardContentLayout() {
        return cardContentLayout;
    }

    public void setCardContentLayout(LinearLayout cardContentLayout) {
        this.cardContentLayout = cardContentLayout;
    }

    public TextView getThemeName() {
        return themeName;
    }

    public void setThemeName(Activity activity, String name) {
        if(!TextUtils.isEmpty(name)) {
            this.themeName.setText(name);
        }
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }

    public void setHashtags(Activity activity, Tag[] tags) {
        layoutHashtags.removeAllViews();
        if(tags == null || tags.length < 1){
            scrollHashtags.setVisibility(View.GONE);
            layoutHashtags.setVisibility(View.GONE);
        } else {
            scrollHashtags.setVisibility(View.VISIBLE);
            layoutHashtags.setVisibility(View.VISIBLE);
            for (Tag t : tags
                    ) {
                TextView tv = new TextView(activity);
                tv.setText(activity.getString(R.string.hashtag, t.getName()));
                tv.setTextColor(activity.getResources().getColor(R.color.colorHashtag));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0,0,16,0);
                tv.setLayoutParams(lp);
                layoutHashtags.addView(tv);
            }
        }
    }

    public TextView getDescription() {
        return description;
    }

    public void setDescription(Activity activity, String description) {
        if(!TextUtils.isEmpty(description)) {
            this.description.setText(Html.fromHtml(description));
        } else {
            this.description.setVisibility(View.GONE);
        }
    }

    public ImageView getLikeAction() {
        return likeAction;
    }

    public void setLikeAction(ImageView likeAction) {
        this.likeAction = likeAction;
    }

    public TextView getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber.setText(String.valueOf(likeNumber));
        if(PreferenceManager.getLogedUser(activity) != null && Arrays.asList(playlist.getUserIds()).contains(PreferenceManager.getLogedUser(activity).getId())){
            liked = true;
            likeAction.setImageResource(R.drawable.ic_like_selected);
        } else {
            liked = false;
            likeAction.setImageResource(R.drawable.ic_like);
        }
    }

    public ImageView getShareAction() {
        return shareAction;
    }

    public void setShareAction(ImageView shareAction) {
        this.shareAction = shareAction;
    }

    public ImageView getFavouriteAction() {
        return favouriteAction;
    }

    public void setFavouriteAction(ImageView favouriteAction) {
        this.favouriteAction = favouriteAction;
    }

    public ChipCloud getLabelGrid() {
        return labelGrid;
    }

    public String setPlaylistThemeSchema(Activity activity, Theme theme) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setThemeLine(activity, null);
            setThemeName(activity, " ");
            setThemeIcon(activity, null);
            this.themeName.setTextColor(activity.getResources().getColor(R.color.colorAccent));
            return null;
        } else {
            setThemeLine(activity, theme.getHexColor());
            setThemeIcon(activity, theme.getHexColor());
            setThemeName(activity, theme.getName());
            this.themeName.setTextColor(Color.parseColor(theme.getHexColor()));
            return theme.getHexColor();
        }
    }

    public void setLabelGrid(Activity activity, Category[] categories, String[] subcategories, String color) {
        labelGrid.removeAllViews();
        if(categories != null && categories.length > 0) {
            labelsTitle.setVisibility(View.VISIBLE);
            labelGrid.setVisibility(View.VISIBLE);
            if(!TextUtils.isEmpty(color)){
                String[] s = new String[0];
                new ChipCloud.Configure()
                        .chipCloud(labelGrid)
                        .deselectedColor(Color.parseColor(color))
                        .deselectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .selectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .labels(s)
                        .build();
            } else {
                String[] s = new String[0];
                new ChipCloud.Configure()
                        .chipCloud(labelGrid)
                        .deselectedColor(activity.getResources().getColor(R.color.colorAccent))
                        .deselectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .selectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .labels(s)
                        .build();
            }
            for (Category c : categories
                    ) {
                labelGrid.addChip(c.getName());
            }
            if(subcategories != null && subcategories.length > 0) {
                for (String s : subcategories
                        ) {
                    labelGrid.addChip(s);
                }
            }
        } else{
            labelsTitle.setVisibility(View.GONE);
            labelGrid.setVisibility(View.GONE);
        }
    }

    public void setPlaylist(Activity activity, BaseFragment fragment, Playlist playlist, GenericVerticalAdapter adapter, int position) {
        this.activity = activity;
        this.fragment = fragment;
        this.playlist = playlist;
        this.adapter = adapter;
        this.position = position;
        setContributorName(this.playlist.getCreated_by().getName());
        setContributorImage(activity, this.playlist.getCreated_by().getProfile().getPicture());
        setCoverImage(this.activity, this.playlist.getCoverPath());
        //TODO compute theme color
        String color = setPlaylistThemeSchema(this.activity, this.playlist.getMainTheme());
        setItemNumber(this.activity, this.playlist.getLinkedFiches().length);
        setTitle(playlist.getName());
        setHashtags(this.activity, this.playlist.getLabels().getTags());
        setDescription(this.activity, this.playlist.getDescription());
        setLabelGrid(this.activity, this.playlist.getLabels().getCategories(), playlist.getLabels().getSousCategories(), color);
        setLikeNumber(playlist.getUserIds().length);
        card.setOnClickListener(this);
        //otherActions.setOnClickListener(this);
        favouriteActionLayout.setOnClickListener(this);
        likeActionLayout.setOnClickListener(this);
        shareActionLayout.setOnClickListener(this);
        if(PreferenceManager.getSavedPlaylists(activity).containsKey(playlist.getId())){
            isFavourite = true;
            favouriteAction.setImageResource(R.drawable.ic_favourites_selected);
        } else {
            isFavourite = false;
            favouriteAction.setImageResource(R.drawable.ic_favourites);
        }
    }

    @Override
    public void onClick(View anchor) {
        // TODO Auto-generated method stub
        switch (anchor.getId()) {
            /*case R.id.other_actions:
                popupMenu = new PopupMenu(activity, anchor);
                popupMenu.setOnDismissListener(new VerticalListPlaylist.OnDismissListener());
                popupMenu.setOnMenuItemClickListener(new VerticalListPlaylist.OnMenuItemClickListener());
                popupMenu.inflate(R.menu.menu_card_other_actions);
                popupMenu.show();
                break;*/
            case R.id.card_playlist_vertical_list:
                Bundle b = new Bundle();
                b.putSerializable("playlist", this.playlist);
                BaseFragment playlistFragment = PlaylistFragment.newInstance(b);
                playlistFragment.setTab(fragment.getTab());
                fragment.switchFragment(playlistFragment, PlaylistFragment.TAG, true);
                break;
            case R.id.action_favourite_layout:
                if(isFavourite) {
                    PreferenceManager.removePlaylistFromFavourites(activity, playlist.getId());
                    isFavourite = false;
                    favouriteAction.setImageResource(R.drawable.ic_favourites);
                } else {
                    PreferenceManager.addPlaylistToFavourites(activity, playlist.getId());
                    isFavourite = true;
                    favouriteAction.setImageResource(R.drawable.ic_favourites_selected);
                    APIManager.getPlaylistManager(activity.getApplicationContext()).savePlaylist(playlist.getId());
                }
                break;
            case R.id.action_like_layout:
                if(PreferenceManager.getLogedUser(activity) != null) {
                    if (liked) {
                        dislike();
                        liked = false;
                        likeAction.setImageResource(R.drawable.ic_like);
                    } else {
                        like();
                        liked = true;
                        likeAction.setImageResource(R.drawable.ic_like_selected);
                    }
                } else {
                   showLoginDialog();
                }
                break;
            case R.id.action_share_layout:
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.app_name));
                myIntent.putExtra(Intent.EXTRA_TEXT, URLParamGenerator.generateShareUrl(playlist));
                activity.startActivity(Intent.createChooser(myIntent, activity.getString(R.string.share)));
                break;
        }
    }

    private class OnDismissListener implements PopupMenu.OnDismissListener {

        @Override
        public void onDismiss(PopupMenu menu) {

        }

    }

    private class OnMenuItemClickListener implements
            PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.media_submit:
                    Toast.makeText(activity.getApplicationContext(), activity.getString(R.string.coming_soon),
                            Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.problem_submit:
                    Toast.makeText(activity.getApplicationContext(), activity.getString(R.string.coming_soon),
                            Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    }

    public void like() {
        APIManager.getPlaylistManager(activity.getApplicationContext()).likePlaylist(playlist.getId(), PreferenceManager.getLogedUser(activity.getApplicationContext()).getId()).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                Playlist p = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        playlist = p;
                        setLikeNumber(p.getUserIds().length);
                        adapter.setListItem(position, p);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(activity);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void dislike() {
        APIManager.getPlaylistManager(activity.getApplicationContext()).dislikePlaylist(playlist.getId(), PreferenceManager.getLogedUser(activity.getApplicationContext()).getId()).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                Playlist p = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        playlist = p;
                        setLikeNumber(p.getUserIds().length);
                        adapter.setListItem(position, p);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(activity);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    /**
     * Shows a dialog to allow log in
     */
    private void showLoginDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.app_name));
        builder.setMessage(activity.getString(R.string.login_dialog_text));
        builder.setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                activity.startActivity(new Intent(activity, LoginActivity.class));
            }
        });
        builder.setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}
