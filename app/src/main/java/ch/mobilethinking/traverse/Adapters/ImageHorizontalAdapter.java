package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Activities.DiaporamaActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class ImageHorizontalAdapter extends RecyclerView.Adapter<ImageHorizontalAdapter.DataObjectHolder> {
    private LayoutInflater inflator;
    private Activity activity;
    private BaseFragment fragment;
    private List<Object> list;

    public ImageHorizontalAdapter(Activity activity, BaseFragment fragment, List<Object> list) {
        this.activity = activity;
        this.fragment = fragment;
        this.list = list;
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public ImageHorizontalAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.image_horizontal_list_item, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }


    @Override
    public void onBindViewHolder(ImageHorizontalAdapter.DataObjectHolder holder, final int position) {
        FicheMedia fm = (FicheMedia) list.get(position);
        if(!TextUtils.isEmpty(fm.getCoverPath()) && !fm.getCoverPath().contains("defaultImage")) {
            String imageUrl = "";
            if(fm.getCoverPath().split("http").length > 2){
                imageUrl = "http" + fm.getCoverPath().split("http")[2];
            } else {
                imageUrl = fm.getCoverPath();
            }
            Glide.with(activity).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(holder.coverImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(holder.coverImage);
        }

        holder.coverImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, DiaporamaActivity.class);
                i.putExtra("images", (Serializable)list);
                i.putExtra("position", position);
                activity.startActivity(i);
            }
        });

    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private final ImageView coverImage;

        public DataObjectHolder(View itemView) {
            super(itemView);
            coverImage = (ImageView) itemView.findViewById(R.id.cover_image);
        }
    }

}
