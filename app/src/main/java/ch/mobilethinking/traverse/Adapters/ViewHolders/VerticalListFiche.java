package ch.mobilethinking.traverse.Adapters.ViewHolders;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.text.style.TtsSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.GenericVerticalAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Utils.URLParamGenerator;
import ch.mobilethinking.traverse.Views.Activities.AddToPlaylistActivity;
import ch.mobilethinking.traverse.Views.Activities.LoginActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.Views.Fragments.FicheFragment;
import ch.mobilethinking.traverse.Views.Fragments.ProfileFragment;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Carlos Ballester on 16-May-17.
 */

public class VerticalListFiche extends RecyclerView.ViewHolder implements View.OnClickListener {

    public static final int VERTICAL_LIST_FICHE = 2;
    private static final String TAG = VerticalListFiche.class.getSimpleName();
    public CardView card;
    public LinearLayout cardHeaderLayout;
    public CircleImageView contributorImage;
    public TextView contributorName;
    public ImageView otherActions;
    public ImageView coverImage;
    public View themeLine;
    public CircleImageView themeIconColor;
    public ImageView themeIcon;
    public LinearLayout cardContentLayout;
    public TextView themeName;
    public TextView title;
    public HorizontalScrollView scrollHashtags;
    public LinearLayout layoutHashtags;
    public TextView eventTime;
    public View eventTimeSeparator;
    public TextView description;
    public ImageView likeAction;
    public TextView likeNumber;
    public RelativeLayout likeActionLayout;
    public ImageView shareAction;
    public RelativeLayout shareActionLayout;
    public RelativeLayout favouriteActionLayout;
    public ImageView favouriteAction;
    public TextView labelsTitle;
    public ChipCloud labelGrid;
    private PopupMenu popupMenu;
    private Activity activity;
    private BaseFragment fragment;
    private Fiche fiche;
    private int position;
    private GenericVerticalAdapter adapter;
    private boolean liked = false;

    public VerticalListFiche(View itemView) {
        super(itemView);
        card = (CardView) itemView.findViewById(R.id.card_fiche_vertical_list);
        cardHeaderLayout = (LinearLayout) itemView.findViewById(R.id.card_header_content);
        contributorImage = (CircleImageView) itemView.findViewById(R.id.contributor_image);
        contributorName = (TextView) itemView.findViewById(R.id.contributor_name);
        //otherActions = (ImageView) itemView.findViewById(R.id.other_actions);
        coverImage = (ImageView) itemView.findViewById(R.id.cover_image);
        themeLine = (View) itemView.findViewById(R.id.theme_line);
        themeIconColor = (CircleImageView) itemView.findViewById(R.id.theme_icon_color);
        themeIcon = (ImageView) itemView.findViewById(R.id.theme_icon);
        cardContentLayout = (LinearLayout) itemView.findViewById(R.id.card_content_layout);
        themeName = (TextView) itemView.findViewById(R.id.theme_name);
        title = (TextView) itemView.findViewById(R.id.title);
        scrollHashtags = (HorizontalScrollView) itemView.findViewById(R.id.scroll_hashtags);
        layoutHashtags = (LinearLayout) itemView.findViewById(R.id.layout_hashtags);
        eventTime = (TextView) itemView.findViewById(R.id.event_time);
        eventTimeSeparator = (View) itemView.findViewById(R.id.event_time_separator);
        description = (TextView) itemView.findViewById(R.id.description);
        likeAction = (ImageView) itemView.findViewById(R.id.action_like_icon);
        likeNumber = (TextView) itemView.findViewById(R.id.action_like_label);
        likeActionLayout = (RelativeLayout) itemView.findViewById(R.id.action_like_layout);
        shareAction = (ImageView) itemView.findViewById(R.id.action_share_icon);
        shareActionLayout = (RelativeLayout) itemView.findViewById(R.id.action_share_layout);
        favouriteActionLayout = (RelativeLayout) itemView.findViewById(R.id.action_favourite_layout);
        favouriteAction = (ImageView) itemView.findViewById(R.id.action_favourite_icon);
        labelsTitle = (TextView) itemView.findViewById(R.id.labels_title);
        labelGrid = (ChipCloud) itemView.findViewById(R.id.labels_grid);
    }

    public CardView getCard() {
        return card;
    }

    public void setCard(CardView card) {
        this.card = card;
    }

    public LinearLayout getCardHeaderLayout() {
        return cardHeaderLayout;
    }

    public void setCardHeaderLayout(LinearLayout cardHeaderLayout) {
        this.cardHeaderLayout = cardHeaderLayout;
    }

    public CircleImageView getContributorImage() {
        return contributorImage;
    }

    public void setContributorImage(Activity activity, Object resource) {
        if(resource != null && !TextUtils.isEmpty((String)resource)) {
            Glide.with(activity).load(resource).into(this.contributorImage);
        } else {
            Glide.with(activity).load(R.drawable.default_contributor).into(this.contributorImage);
        }
    }

    public TextView getContributorName() {
        return contributorName;
    }

    public void setContributorName(String name) {
        if(!TextUtils.isEmpty(name)) {
            this.contributorName.setText(name);
        }
    }

    public ImageView getOtherActions() {
        return otherActions;
    }

    public void setOtherActions(ImageView otherActions) {
        this.otherActions = otherActions;
    }

    public ImageView getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(Activity activity, Object resource) {
        //the thumbnails of youtube usually comes with default.jpg at the end of the string
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Glide.with(activity).load(imageUrl).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        }
    }

    public View getThemeLine() {
        return themeLine;
    }

    public void setThemeLine(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            this.themeLine.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
        }
    }

    public CircleImageView getThemeIconColor() {
        return themeIconColor;
    }

    public void setThemeIconColor(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIconColor.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
        } else {
            this.themeIconColor.setImageDrawable(new ColorDrawable(activity.getResources().getColor(R.color.colorAccent)));
        }
    }

    public LinearLayout getCardContentLayout() {
        return cardContentLayout;
    }

    public void setCardContentLayout(LinearLayout cardContentLayout) {
        this.cardContentLayout = cardContentLayout;
    }

    public TextView getThemeName() {
        return themeName;
    }

    public void setThemeName(Activity activity, String name) {
        if(!TextUtils.isEmpty(name)) {
            this.themeName.setText(name);
        }
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }

    public HorizontalScrollView getScrollHashtags() {
        return scrollHashtags;
    }

    public void setScrollHashtags(HorizontalScrollView scrollHashtags) {
        this.scrollHashtags = scrollHashtags;
    }

    public LinearLayout getLayoutHashtags() {
        return layoutHashtags;
    }

    public void setLayoutHashtags(LinearLayout layoutHashtags) {
        this.layoutHashtags = layoutHashtags;
    }

    public void setHashtags(Activity activity, Tag[] tags) {
        layoutHashtags.removeAllViews();
        if(tags == null || tags.length < 1){
            scrollHashtags.setVisibility(View.GONE);
            layoutHashtags.setVisibility(View.GONE);
        } else {
            scrollHashtags.setVisibility(View.VISIBLE);
            layoutHashtags.setVisibility(View.VISIBLE);
            for (Tag t : tags
                 ) {
                TextView tv = new TextView(activity);
                tv.setText(activity.getString(R.string.hashtag, t.getName()));
                tv.setTextColor(activity.getResources().getColor(R.color.colorHashtag));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0,0,16,0);
                tv.setLayoutParams(lp);
                layoutHashtags.addView(tv);
            }
        }
    }

    public TextView getDescription() {
        return description;
    }

    public void setDescription(Activity activity, Fiche fiche) {
        if(!TextUtils.isEmpty(fiche.getShort_description())) {
            this.description.setText(Html.fromHtml(fiche.getShort_description()));
        } else {
            this.description.setVisibility(View.GONE);
        }
    }

    public ImageView getLikeAction() {
        return likeAction;
    }

    public void setLikeAction(ImageView likeAction) {
        this.likeAction = likeAction;
    }

    public TextView getLikeNumber() {
        return likeNumber;
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber.setText(String.valueOf(likeNumber));
        if(PreferenceManager.getLogedUser(activity) != null && Arrays.asList(fiche.getUserIds()).contains(PreferenceManager.getLogedUser(activity).getId())){
            liked = true;
            likeAction.setImageResource(R.drawable.ic_like_selected);
        } else {
            liked = false;
            likeAction.setImageResource(R.drawable.ic_like);
        }
    }

    public ImageView getShareAction() {
        return shareAction;
    }

    public void setShareAction(ImageView shareAction) {
        this.shareAction = shareAction;
    }

    public ImageView getFavouriteAction() {
        return favouriteAction;
    }

    public void setFavouriteAction(ImageView favouriteAction) {
        this.favouriteAction = favouriteAction;
    }

    public ChipCloud getLabelGrid() {
        return labelGrid;
    }

    public void setLabelGrid(Activity activity, Category[] categories, String[] subcategories, String color) {
        labelGrid.removeAllViews();
        if(categories != null && categories.length > 0) {
            labelsTitle.setVisibility(View.VISIBLE);
            labelGrid.setVisibility(View.VISIBLE);
            if(!TextUtils.isEmpty(color)){
                String[] s = new String[0];
                new ChipCloud.Configure()
                        .chipCloud(labelGrid)
                        .deselectedColor(Color.parseColor(color))
                        .deselectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .selectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .labels(s)
                        .build();
            }else {
                String[] s = new String[0];
                new ChipCloud.Configure()
                        .chipCloud(labelGrid)
                        .deselectedColor(activity.getResources().getColor(R.color.colorAccent))
                        .deselectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .selectedFontColor(activity.getResources().getColor(R.color.colorLightWhite))
                        .labels(s)
                        .build();
            }
            for (Category c : categories
                    ) {
                labelGrid.addChip(c.getName());
            }
            if(subcategories != null && subcategories.length > 0) {
                for (String s : subcategories
                        ) {
                    labelGrid.addChip(s);
                }
            }
        } else{
            labelsTitle.setVisibility(View.GONE);
            labelGrid.setVisibility(View.GONE);
        }
    }

    public String setThemeSchema(Activity activity, Theme theme) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setThemeLine(activity, null);
            setThemeIconColor(activity, null);
            setThemeName(activity, " ");
            this.themeName.setTextColor(activity.getResources().getColor(R.color.colorAccent));
            return null;
        } else {
            setThemeLine(activity, theme.getHexColor());
            setThemeIconColor(activity, theme.getHexColor());
            setThemeName(activity, theme.getName());
            this.themeName.setTextColor(Color.parseColor(theme.getHexColor()));
            return theme.getHexColor();
        }
    }

    public void setTypeIcon (Activity activity, Fiche fiche){
        if (fiche instanceof FichePlace){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_site));
        } else if (fiche instanceof FichePeople){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_people));
        } else if (fiche instanceof FicheObject){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_object));
        } else if (fiche instanceof FicheEvent){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_events));
        } else if (fiche instanceof FicheMedia){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_image));
        }
    }

    public void setFiche (Activity activity, BaseFragment fragment, Fiche fiche, GenericVerticalAdapter adapter, int position) {
        this.activity = activity;
        this.fragment = fragment;
        this.fiche = fiche;
        this.adapter = adapter;
        this.position = position;
        setContributorName(this.fiche.getCreated_by().getName());
        setContributorImage(activity, this.fiche.getCreated_by().getProfile().getPicture());
        setCoverImage(this.activity, this.fiche.getCoverPath());
        String color = setThemeSchema(this.activity, this.fiche.getMainTheme());
        setTypeIcon(this.activity, this.fiche);
        setTitle(this.fiche.getName());
        setHashtags(this.activity, this.fiche.getTags());
        if(fiche instanceof FicheEvent) {
            eventTime.setVisibility(View.VISIBLE);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'", Locale.getDefault());
            Calendar start = Calendar.getInstance(Locale.getDefault());
            Calendar end = Calendar.getInstance(Locale.getDefault());
            try {
                if(!TextUtils.isEmpty(((FicheEvent) fiche).getStart_date())) {
                    start.setTime(sdf.parse(((FicheEvent) fiche).getStart_date()));
                }
                if (!TextUtils.isEmpty(((FicheEvent) fiche).getEnd_date())) {
                    end.setTime(sdf.parse(((FicheEvent) fiche).getEnd_date()));
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String startDate = start.get(Calendar.DAY_OF_MONTH) + "/" + (start.get(Calendar.MONTH) + 1) + "/" + start.get(Calendar.YEAR);
            String endDate = end.get(Calendar.DAY_OF_MONTH) + "/" + (end.get(Calendar.MONTH) + 1) + "/" + end.get(Calendar.YEAR);
            eventTime.setText(startDate + " - " + endDate);
        } else {
            eventTime.setVisibility(View.GONE);
        }
        setDescription(this.activity, this.fiche);
        setLabelGrid(this.activity, this.fiche.getCategories(), fiche.getSubCategories(), color);
        setLikeNumber(this.fiche.getUserIds().length);
        //otherActions.setOnClickListener(this);
        card.setOnClickListener(this);
        likeActionLayout.setOnClickListener(this);
        shareActionLayout.setOnClickListener(this);
        favouriteActionLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View anchor) {
        // TODO Auto-generated method stub
        switch (anchor.getId()) {
            case R.id.card_fiche_vertical_list:
                Bundle b = new Bundle();
                b.putSerializable("fiche", this.fiche);
                BaseFragment ficheFragment = FicheFragment.newInstance(b);
                ficheFragment.setTab(fragment.getTab());
                fragment.switchFragment(ficheFragment, FicheFragment.TAG, true);
                break;
            /*case R.id.other_actions:
                popupMenu = new PopupMenu(activity, anchor);
                popupMenu.setOnDismissListener(new OnDismissListener());
                popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener());
                popupMenu.inflate(R.menu.menu_card_other_actions);
                popupMenu.show();
                break;*/
            case R.id.action_favourite_layout:
                if(PreferenceManager.getLogedUser(activity) == null) {
                    showLoginDialog();
                } else {
                    Intent i = new Intent(activity, AddToPlaylistActivity.class);
                    i.putExtra("fiche", fiche);
                    activity.startActivity(i);
                }
                break;
            case R.id.action_like_layout:
                if(PreferenceManager.getLogedUser(activity) != null) {
                    if (liked) {
                        dislike();
                        liked = false;
                        likeAction.setImageResource(R.drawable.ic_like);
                    } else {
                        like();
                        liked = true;
                        likeAction.setImageResource(R.drawable.ic_like_selected);
                    }
                } else {
                    showLoginDialog();
                }
                break;
            case R.id.action_share_layout:
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.putExtra(Intent.EXTRA_SUBJECT, activity.getString(R.string.app_name));
                myIntent.putExtra(Intent.EXTRA_TEXT, URLParamGenerator.generateShareUrl(fiche));
                activity.startActivity(Intent.createChooser(myIntent, activity.getString(R.string.share)));
                break;
        }
    }

    private class OnDismissListener implements PopupMenu.OnDismissListener {

        @Override
        public void onDismiss(PopupMenu menu) {

        }

    }

    private class OnMenuItemClickListener implements
            PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.media_submit:
                    Toast.makeText(activity.getApplicationContext(), activity.getString(R.string.coming_soon),
                            Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.problem_submit:
                    Toast.makeText(activity.getApplicationContext(), activity.getString(R.string.coming_soon),
                            Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    }

    public void like() {
        APIManager.getFicheManager(activity.getApplicationContext()).likeFiche(fiche.getId(), PreferenceManager.getLogedUser(activity.getApplicationContext()).getId()).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                Fiche f = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        fiche = f;
                        setLikeNumber(f.getUserIds().length);
                        adapter.setListItem(position, f);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(activity);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void dislike() {
        APIManager.getFicheManager(activity.getApplicationContext()).dislikeFiche(fiche.getId(), PreferenceManager.getLogedUser(activity.getApplicationContext()).getId()).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                Fiche f = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        fiche = f;
                        setLikeNumber(f.getUserIds().length);
                        adapter.setListItem(position, f);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(activity);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    /**
     * Shows a dialog to allow log in
     */
    private void showLoginDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.app_name));
        builder.setMessage(activity.getString(R.string.login_dialog_text));
        builder.setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                activity.startActivity(new Intent(activity, LoginActivity.class));
            }
        });
        builder.setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }


}
