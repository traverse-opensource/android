package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import ch.mobilethinking.traverse.Adapters.ViewHolders.VerticalListFiche;
import ch.mobilethinking.traverse.Adapters.ViewHolders.VerticalListPlaylist;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class DiscoveryVerticalAdapter extends GenericVerticalAdapter {
    private LayoutInflater inflator;
    private Activity activity;
    private BaseFragment fragment;

    public DiscoveryVerticalAdapter(Activity activity, BaseFragment fragment, List<Object> list) {
        super(list);
        this.activity = activity;
        this.fragment = fragment;
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder dataObjectHolder;
        if(viewType == VerticalListFiche.VERTICAL_LIST_FICHE) {
            View view = inflator
                    .inflate(R.layout.card_fiche_vertical_list, parent, false);
            dataObjectHolder = new VerticalListFiche(view);
        } else{
            View view = inflator
                    .inflate(R.layout.card_playlist_vertical_list, parent, false);
            dataObjectHolder = new VerticalListPlaylist(view);
        }
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if(holder instanceof VerticalListFiche) {
            VerticalListFiche ficheHolder = (VerticalListFiche) holder;
            ficheHolder.setFiche(activity, fragment, (Fiche)list.get(position), this, position);
        } else if(holder instanceof VerticalListPlaylist) {
            VerticalListPlaylist playlistHolder = (VerticalListPlaylist) holder;
            playlistHolder.setPlaylist(activity, fragment, (Playlist)list.get(position), this, position);
        }

    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {

        if(list.get(position) instanceof Fiche){
            return VerticalListFiche.VERTICAL_LIST_FICHE;
        }else if(list.get(position) instanceof Playlist){
            return VerticalListPlaylist.VERTICAL_LIST_PLAYLIST;
        } else {
            return VerticalListFiche.VERTICAL_LIST_FICHE;
        }
    }

}
