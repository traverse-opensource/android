package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import ch.mobilethinking.traverse.Adapters.ViewHolders.HorizontalListPlaylist;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by Carlos Ballester on 03-Aug-17.
 */

public class GridPlaylistAdapter extends BaseAdapter {

    private LayoutInflater inflator;
    private Activity activity;
    private List<Playlist> list;

    public GridPlaylistAdapter(Activity activity, List<Playlist> list) {
        this.activity = activity;
        this.list = list;
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View item;
        if(position == 0) {
            item = inflator
                    .inflate(R.layout.card_new_playlist, parent, false);
        } else {
            item = inflator
                    .inflate(R.layout.card_playlist_horizontal_list, parent, false);
            HorizontalListPlaylist playlistView = new HorizontalListPlaylist(item);
            playlistView.setPlaylist(activity, null, list.get(position));
        }
        return item;
    }

}
