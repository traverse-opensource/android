package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Activities.WebViewActivity;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;

/**
 * Created by Carlos Ballester on 01-Jun-17.
 */

public class DiaporamaAdapter extends PagerAdapter {

    private List<Object> images;
    private LayoutInflater inflater;
    private Activity activity;

    public DiaporamaAdapter (Activity activity, List<Object> images) {
        this.activity = activity;
        this.images=images;
        inflater = LayoutInflater.from(activity);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.diaporama_slide, view, false);
        final ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.image);
        final ProgressBar imageProgress = (ProgressBar) myImageLayout.findViewById(R.id.image_progress);

        FicheMedia fm = (FicheMedia) images.get(position);

        if(!TextUtils.isEmpty(fm.getName())) {
            ((TextView) myImageLayout.findViewById(R.id.title)).setText(fm.getName());
        } else {
            ((TextView) myImageLayout.findViewById(R.id.title)).setVisibility(View.GONE);
        }

        if(fm.getCover().containsKey("cover_credit") && !TextUtils.isEmpty(fm.getCover().get("cover_credit"))) {
            ((TextView) myImageLayout.findViewById(R.id.photographer)).setText(fm.getCover().get("cover_credit"));
        } else {
            ((TextView) myImageLayout.findViewById(R.id.photographer)).setText(activity.getString(R.string.no_credit));
        }

        /*if(!TextUtils.isEmpty(fm.getReferences())) {
            ((TextView) myImageLayout.findViewById(R.id.source)).setText(Html.fromHtml(fm.getReferences()));
        } else {
            ((TextView) myImageLayout.findViewById(R.id.source)).setVisibility(View.GONE);
        }*/

        if(!TextUtils.isEmpty(fm.getCoverPath()) && !fm.getCoverPath().contains("defaultImage")) {
            String imageUrl = "";
            if(fm.getCoverPath().split("http").length > 2){
                imageUrl = "http" + fm.getCoverPath().split("http")[2];
            } else {
                imageUrl = fm.getCoverPath();
            }

            final String finalImageUrl = imageUrl;
            final Intent i = new Intent(activity, WebViewActivity.class);
            i.putExtra("url", imageUrl);
            Glide.with(activity).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    imageProgress.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    imageProgress.setVisibility(View.GONE);
                    myImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showImageDialog(finalImageUrl);
                        }
                    });
                    return false;
                }
            }).into(myImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(myImage);
        }

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    /**
     * Shows a dialog to allow password recovery
     */
    private void  showImageDialog(String imageUrl) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity, R.style.MyDialogTheme);
        LayoutInflater inflater = activity.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.dialog_image_overlay, null);
        builder.setView(dialog_layout);
        //ImageView close = (ImageView) dialog_layout.findViewById(R.id.dialog_close);
        ImageView image = (ImageView) dialog_layout.findViewById(R.id.dialog_image);
        final ProgressBar progress = (ProgressBar) dialog_layout.findViewById(R.id.dialog_image_progress);
        Glide.with(activity).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                progress.setVisibility(View.GONE);
                return false;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                progress.setVisibility(View.GONE);
                return false;
            }
        }).into(image);
        final AlertDialog d = builder.create();
        d.show();
        /*close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });*/
    }

}
