package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Activities.DiaporamaActivity;
import ch.mobilethinking.traverse.Views.Activities.EditPlaylistActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class CoverHorizontalAdapter extends RecyclerView.Adapter<CoverHorizontalAdapter.DataObjectHolder> {
    private LayoutInflater inflator;
    private Activity activity;
    private List<String> list;
    private List<Boolean> clicked;
    private int previousSelected = -1;

    public CoverHorizontalAdapter(Activity activity, List<String> list, List<Boolean> clicked) {
        this.activity = activity;
        this.list = list;
        this.clicked = clicked;
        for (int i = 0; i < this.clicked.size(); i++
             ) {
            if(this.clicked.get(i)) {
                previousSelected = i;
            }
        }
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public CoverHorizontalAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.cover_image_grid_item, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }


    @Override
    public void onBindViewHolder(final CoverHorizontalAdapter.DataObjectHolder holder, final int position) {
        if(!TextUtils.isEmpty(list.get(position)) && !list.get(position).contains("default")) {
            String imageUrl = "";
            if(list.get(position).split("http").length > 2){
                imageUrl = "http" + list.get(position).split("http")[2];
            } else {
                imageUrl = list.get(position);
            }
            Glide.with(activity).load(imageUrl).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(holder.coverImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).skipMemoryCache(true).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(holder.coverImage);
        }

        if(clicked.get(position)) {
            holder.coverImageSelected.setVisibility(View.VISIBLE);
        } else {
            holder.coverImageSelected.setVisibility(View.GONE);
        }

        holder.coverImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.coverImageSelected.getVisibility() == View.GONE){
                    holder.coverImageSelected.setVisibility(View.VISIBLE);
                    clicked.set(position, true);
                    if(previousSelected != -1){
                        clicked.set(previousSelected, false);
                    }
                    previousSelected = position;
                    ((EditPlaylistActivity)activity).setCover(list.get(position));
                } else {
                    holder.coverImageSelected.setVisibility(View.GONE);
                    clicked.set(position, false);
                    previousSelected = -1;
                    ((EditPlaylistActivity)activity).setCover(null);
                }
                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private final ImageView coverImage;
        private final ImageView coverImageSelected;

        public DataObjectHolder(View itemView) {
            super(itemView);
            coverImage = (ImageView) itemView.findViewById(R.id.cover_image);
            coverImageSelected = (ImageView) itemView.findViewById(R.id.cover_image_selected);
        }
    }

}
