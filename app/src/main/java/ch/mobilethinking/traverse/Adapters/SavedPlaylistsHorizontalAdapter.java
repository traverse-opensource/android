package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.HashMap;

import ch.mobilethinking.traverse.Adapters.ViewHolders.HorizontalListPlaylist;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class SavedPlaylistsHorizontalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private LayoutInflater inflator;
    private Activity activity;
    private BaseFragment fragment;
    private ArrayList<Playlist> list;
    private int cardWidth;

    public SavedPlaylistsHorizontalAdapter(Activity activity, BaseFragment fragment, ArrayList<Playlist> list) {
        this.activity = activity;
        this.fragment = fragment;
        this.list = list;
        cardWidth = defineViewWidth(activity);
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder dataObjectHolder;
        View view = inflator
                .inflate(R.layout.card_playlist_horizontal_list, parent, false);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = cardWidth;
        view.setLayoutParams(params);
        dataObjectHolder = new HorizontalListPlaylist(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        HorizontalListPlaylist playlistHolder = (HorizontalListPlaylist) holder;
        playlistHolder.setPlaylist(activity, fragment, list.get(position));
    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    private int defineViewWidth(Context context) {
        DisplayMetrics dm = new DisplayMetrics();
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(dm);
        int deviceWidth = dm.widthPixels;
        return deviceWidth/2;
    }

    @Override
    public int getItemViewType(int position) {
        return HorizontalListPlaylist.HORIZONTAL_LIST_PLAYLIST;
    }

}
