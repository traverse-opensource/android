package ch.mobilethinking.traverse.Adapters;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.Helpers.ItemTouchHelperAdapter;
import ch.mobilethinking.traverse.Adapters.Helpers.ItemTouchHelperViewHolder;
import ch.mobilethinking.traverse.Adapters.Helpers.OnStartDragListener;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Activities.FilterActivity;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class ReorderPlaylistAdapter extends RecyclerView.Adapter<ReorderPlaylistAdapter.DataObjectHolder>
        implements ItemTouchHelperAdapter {
    private final static String TAG = ReorderPlaylistAdapter.class.getSimpleName();
    private LayoutInflater inflator;
    private Activity activity;
    private String playlistId;
    private List<LinkedFiche> list;
    private final OnStartDragListener dragStartListener;

    public ReorderPlaylistAdapter(Activity activity, OnStartDragListener dragStartListener, String playlistId, List<LinkedFiche> list) {
        this.activity = activity;
        this.dragStartListener = dragStartListener;
        this.playlistId = playlistId;
        this.list = list;
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public ReorderPlaylistAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.order_playlist_item, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(final ReorderPlaylistAdapter.DataObjectHolder holder, final int position) {
        final Fiche f = list.get(position).getFiche();
        holder.title.setText(f.getName());
        holder.ficheNumber.setText(String.valueOf(position+1));

        if (f instanceof FichePlace){
            holder.themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_site));
        } else if (f instanceof FichePeople){
            holder.themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_people));
        } else if (f instanceof FicheObject){
            holder.themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_object));
        } else if (f instanceof FicheEvent){
            holder.themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_events));
        } else if (f instanceof FicheMedia){
            holder.themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_image));
        }

        String color = f.getMainTheme().getHexColor();
        if(!TextUtils.isEmpty(color)){
            holder.themeIconCircle.setBorderColor(Color.parseColor(color));
            holder.themeIcon.setColorFilter(Color.parseColor(color));
            holder.ficheNumberCircle.setBorderColor(Color.parseColor(color));
            holder.ficheNumber.setTextColor(Color.parseColor(color));
        } else {
            holder.themeIconCircle.setBorderColor(activity.getResources().getColor(R.color.colorAccent));
            holder.themeIcon.setColorFilter(activity.getResources().getColor(R.color.colorAccent));
            holder.ficheNumberCircle.setBorderColor(activity.getResources().getColor(R.color.colorAccent));
            holder.ficheNumber.setTextColor(activity.getResources().getColor(R.color.colorAccent));
        }

        HashMap<String, Boolean> viewedFiches = PreferenceManager.getViewedFiches(activity);
        if(viewedFiches.containsKey(f.getId())){
            if(!TextUtils.isEmpty(color)) {
                holder.themeIconCircle.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
                holder.themeIcon.setColorFilter(activity.getResources().getColor(R.color.colorPrimary));
            } else {
                holder.themeIconCircle.setImageDrawable(new ColorDrawable(activity.getResources().getColor(R.color.colorAccent)));
                holder.themeIcon.setColorFilter(activity.getResources().getColor(R.color.colorPrimary));
            }
        }

        // Start a drag whenever the handle view its touched
        holder.dragHandle.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                    dragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });

        holder.deleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDeleteDialog(position);
            }
        });

    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(list, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        return true;
    }

    @Override
    public boolean onItemDrop() {
        notifyDataSetChanged();
        return true;
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder
            implements ItemTouchHelperViewHolder {
        private final CardView card;
        private final TextView title;
        private final TextView ficheNumber;
        private final ImageView dragHandle;
        private final ImageView deleteIcon;
        CircleImageView themeIconCircle;
        ImageView themeIcon;
        CircleImageView ficheNumberCircle;

        public DataObjectHolder(View itemView) {
            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card_item);
            title = (TextView) itemView.findViewById(R.id.linkedfiche_title);
            ficheNumber = (TextView) itemView.findViewById(R.id.linkedfiche_number);
            dragHandle = (ImageView) itemView.findViewById(R.id.linkedfiche_reorder);
            deleteIcon = (ImageView) itemView.findViewById(R.id.linkedfiche_delete);
            themeIconCircle = (CircleImageView) itemView.findViewById(R.id.theme_icon_circle);
            themeIcon = (ImageView) itemView.findViewById(R.id.theme_icon);
            ficheNumberCircle = (CircleImageView) itemView.findViewById(R.id.fiche_number_circle);
        }

        @Override
        public void onItemSelected() {
            ((CardView)itemView).setCardBackgroundColor(Color.LTGRAY);
        }

        @Override
        public void onItemClear() {
            ((CardView)itemView).setCardBackgroundColor(Color.WHITE);
        }

    }

    /**
     * Shows a dialog to confirm playlist delete
     */
    private void showDeleteDialog(final int position) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        builder.setTitle(activity.getString(R.string.app_name));
        builder.setMessage(activity.getString(R.string.delete_linkedfiche_dialog_text));
        builder.setPositiveButton(activity.getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                deleteLinkedFiche(position);
            }
        });
        builder.setNegativeButton(activity.getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void deleteLinkedFiche (final int position) {
            APIManager.getPlaylistManager(activity.getApplicationContext()).deleteLinkedFiche(playlistId, list.get(position).getId()).onSuccess(new Continuation<Playlist, Void>() {

                @Override
                public Void then(final Task<Playlist> task) throws Exception {
                    final Playlist playlist = task.getResult();
                    try {

                        if (task.isCancelled()) {
                            Log.d(TAG, "onSuccess cancelled");
                        } else if (task.isFaulted()) {
                            Log.d(TAG, "onSuccess faulted");
                            String stackTrace = "";

                            for (StackTraceElement trace : task.getError().getStackTrace()) {
                                stackTrace += trace.toString() + "\n";
                            }

                            String toWrite =
                                    String.format(
                                            "%s\n\n%s\n\n%s",
                                            task.getError().getMessage(),
                                            task.getError().getCause(),
                                            stackTrace);
                            Log.d(TAG, toWrite);
                        } else {
                            Toast.makeText(activity, activity.getString(R.string.deleted_from_playlist), Toast.LENGTH_LONG).show();
                            APIManager.getUserManager(activity.getApplicationContext()).saveUserPlaylists(PreferenceManager.getLogedUser(activity.getApplicationContext()).getId());
                            list.remove(position);
                            notifyDataSetChanged();
                        }

                    } catch (Exception e) {
                        Log.d(TAG, "Exception: " + e.getMessage());
                    }
                    return null;
                }
            }).continueWith(new Continuation<Void, Void>() {
                @Override
                public Void then(final Task<Void> task) throws Exception {

                    if (task.isCancelled()) {
                        Log.d(TAG, "Continuation cancelled");
                    } else if (task.isFaulted()) {
                        Log.d(TAG, "Continuation faulted");
                        Log.d(TAG, "Error: " + task.getError().getMessage());
                        MessageUtil.genericError(activity);
                    } else {
                        Log.d(TAG, "Continuation OK");
                    }

                    return null;
                }
            });
    }

    public String[] getLinkedFiches() {
        ArrayList<String> linkedFiches = new ArrayList<>();
        for (LinkedFiche ln: list
             ) {
            linkedFiches.add(ln.getFiche().getId());
        }
        return linkedFiches.toArray(new String[linkedFiches.size()]);
    }

}
