package ch.mobilethinking.traverse.Adapters;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Traverse;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Activities.FilterActivity;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.model.Theme;

/**
 * Created by Carlos Ballester on 09-May-17.
 */

public class FilterHistoryAdapter extends RecyclerView.Adapter<FilterHistoryAdapter.DataObjectHolder> {
    private LayoutInflater inflator;
    private FilterActivity activity;
    private ArrayList<FilterCreator> list;

    public FilterHistoryAdapter(FilterActivity activity, ArrayList<FilterCreator> list) {
        this.activity = activity;
        this.list = list;
        this.inflator = LayoutInflater.from(activity);
    }

    @Override
    public FilterHistoryAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflator.inflate(R.layout.filter_history_item, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
        return dataObjectHolder;
    }


    @Override
    public void onBindViewHolder(final FilterHistoryAdapter.DataObjectHolder holder, final int position) {

        Calendar c = Calendar.getInstance(Locale.getDefault());
        FilterCreator filter = list.get(position);
        c.setTimeInMillis(filter.getDate());
        holder.filterDate.setText(activity.getString(R.string.filter_date, c.get(Calendar.DAY_OF_MONTH)+"."+(c.get(Calendar.MONTH)+1)+"."+c.get(Calendar.YEAR),
                c.get(Calendar.HOUR_OF_DAY)+":"+(String.valueOf(c.get(Calendar.MINUTE)).length() == 1 ? "0"+c.get(Calendar.MINUTE) : c.get(Calendar.MINUTE))));
        holder.filterContent.setText(activity.getString(R.string.filter_content, filter.getFicheTypes().size(), filter.getThemeNames().size(), filter.getCategoryIds().size(), filter.getTagIds().size()));
        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Traverse.setFilter(list.get(position));
                activity.onBackPressed();
            }
        });
        holder.card.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showOptionsDialog(position);
                return false;
            }
        });
    }

    private void showOptionsDialog(final int position) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
        Calendar c = Calendar.getInstance(Locale.getDefault());
        FilterCreator filter = list.get(position);
        c.setTimeInMillis(filter.getDate());
        builder.setTitle(activity.getString(R.string.filter_options_title, c.get(Calendar.DAY_OF_MONTH)+"."+(c.get(Calendar.MONTH)+1)+"."+c.get(Calendar.YEAR),
                c.get(Calendar.HOUR_OF_DAY)+":"+(String.valueOf(c.get(Calendar.MINUTE)).length() == 1 ? "0"+c.get(Calendar.MINUTE) : c.get(Calendar.MINUTE))));
        builder.setItems(activity.getResources().getStringArray(R.array.filter_options), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        Traverse.setFilter(list.get(position));
                        activity.onBackPressed();
                        break;
                    case 1:
                        Traverse.setFilter(list.get(position));
                        Intent i = new Intent(activity, FilterActivity.class);
                        activity.startActivity(i);
                        activity.finish();
                        break;
                    case 2:
                        ArrayList<FilterCreator> temp = PreferenceManager.getSavedFilters(activity);
                        temp.remove(position);
                        PreferenceManager.setSavedFilters(activity, temp);
                        list.remove(position);
                        notifyDataSetChanged();
                        break;
                    default:
                        break;
                }
            }
        });
        builder.show();
    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class DataObjectHolder extends RecyclerView.ViewHolder {
        private final CardView card;
        private final TextView filterDate;
        private final TextView filterContent;

        public DataObjectHolder(View itemView) {
            super(itemView);
            card = (CardView) itemView.findViewById(R.id.card_item);
            filterDate = (TextView) itemView.findViewById(R.id.filter_date);
            filterContent = (TextView) itemView.findViewById(R.id.filter_content);
        }
    }

}
