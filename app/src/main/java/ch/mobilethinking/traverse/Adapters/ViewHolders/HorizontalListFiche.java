package ch.mobilethinking.traverse.Adapters.ViewHolders;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Activities.MainActivity;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.Views.Fragments.FicheFragment;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Carlos Ballester on 16-May-17.
 */

public class HorizontalListFiche extends RecyclerView.ViewHolder implements View.OnClickListener {

    public static final int HORIZONTAL_LIST_FICHE = 0;
    public CardView card;
    public ImageView coverImage;
    public View themeLine;
    public CircleImageView themeIconColor;
    public ImageView themeIcon;
    public LinearLayout cardContentLayout;
    public TextView themeName;
    public TextView title;
    private Activity activity;
    private BaseFragment fragment;
    private Fiche fiche;

    public HorizontalListFiche(View itemView) {
        super(itemView);
        card = (CardView) itemView.findViewById(R.id.card_fiche_horizontal_list);
        coverImage = (ImageView) itemView.findViewById(R.id.cover_image);
        themeLine = (View) itemView.findViewById(R.id.theme_line);
        themeIconColor = (CircleImageView) itemView.findViewById(R.id.theme_icon_color);
        themeIcon = (ImageView) itemView.findViewById(R.id.theme_icon);
        cardContentLayout = (LinearLayout) itemView.findViewById(R.id.card_content_layout);
        themeName = (TextView) itemView.findViewById(R.id.theme_name);
        title = (TextView) itemView.findViewById(R.id.title);
    }

    public CardView getCard() {
        return card;
    }

    public void setCard(CardView card) {
        this.card = card;
    }

    public ImageView getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(Activity activity, Object resource) {
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Glide.with(activity).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        } else {
            Glide.with(activity).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        }
    }

    public View getThemeLine() {
        return themeLine;
    }

    public void setThemeLine(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            this.themeLine.setBackgroundColor(activity.getResources().getColor(R.color.colorAccent));
        }
    }

    public CircleImageView getThemeIconColor() {
        return themeIconColor;
    }

    public void setThemeIconColor(Activity activity, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIconColor.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
        } else {
            this.themeIconColor.setImageDrawable(new ColorDrawable(activity.getResources().getColor(R.color.colorAccent)));
        }
    }

    public LinearLayout getCardContentLayout() {
        return cardContentLayout;
    }

    public void setCardContentLayout(LinearLayout cardContentLayout) {
        this.cardContentLayout = cardContentLayout;
    }

    public TextView getThemeName() {
        return themeName;
    }

    public void setThemeName(Activity activity, String name) {
        if(!TextUtils.isEmpty(name)) {
            this.themeName.setText(name);
        }
    }

    public TextView getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }

    public void setThemeSchema(Activity activity, Theme theme) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setThemeLine(activity, null);
            setThemeIconColor(activity, null);
            setThemeName(activity, activity.getString(R.string.generic_fiche_theme, ""));
            this.themeName.setTextColor(activity.getResources().getColor(R.color.colorAccent));
        } else {
            setThemeLine(activity, theme.getHexColor());
            setThemeIconColor(activity, theme.getHexColor());
            setThemeName(activity, theme.getName());
            this.themeName.setTextColor(Color.parseColor(theme.getHexColor()));
        }
    }

    public void setTypeIcon (Activity activity, Fiche fiche){
        if (fiche instanceof FichePlace){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_site));
        } else if (fiche instanceof FichePeople){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_people));
        } else if (fiche instanceof FicheObject){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_object));
        } else if (fiche instanceof FicheEvent){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_events));
        } else if (fiche instanceof FicheMedia){
            themeIcon.setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_image));
        }
    }

    public void setFiche (Activity activity, BaseFragment fragment, Fiche fiche) {
        this.activity = activity;
        this.fragment = fragment;
        this.fiche = fiche;
        setCoverImage(activity, fiche.getCoverPath());
        setThemeSchema(activity, fiche.getMainTheme());
        setTypeIcon(activity, fiche);
        setTitle(fiche.getName());
        card.setOnClickListener(this);
    }

    @Override
    public void onClick(View anchor) {
        // TODO Auto-generated method stub
        switch (anchor.getId()) {
            case R.id.card_fiche_horizontal_list:
                Bundle b = new Bundle();
                b.putSerializable("fiche", this.fiche);
                BaseFragment ficheFragment = FicheFragment.newInstance(b);
                ficheFragment.setTab(fragment.getTab());
                fragment.switchFragment(ficheFragment, FicheFragment.TAG, true);
                break;
        }
    }

}
