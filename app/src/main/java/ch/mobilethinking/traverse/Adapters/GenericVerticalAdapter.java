package ch.mobilethinking.traverse.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.List;

import ch.mobilethinking.traverse.Adapters.ViewHolders.VerticalListFiche;
import ch.mobilethinking.traverse.Adapters.ViewHolders.VerticalListPlaylist;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by Carlos Ballester on 11-Aug-17.
 */

public class GenericVerticalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public List<Object> list;
    public GenericVerticalAdapter(List<Object> list) {
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

    }


    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(list.get(position) instanceof Fiche){
            return VerticalListFiche.VERTICAL_LIST_FICHE;
        }else if(list.get(position) instanceof Playlist){
            return VerticalListPlaylist.VERTICAL_LIST_PLAYLIST;
        } else {
            return VerticalListFiche.VERTICAL_LIST_FICHE;
        }
    }

    public List<Object> getList() {
        return list;
    }

    public void setList(List<Object> list) {
        this.list = list;
    }

    public void setListItem(int position, Object item) {
        list.set(position, item);
    }

}
