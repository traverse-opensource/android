package ch.mobilethinking.traverse.Views.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.fasterxml.jackson.annotation.JsonCreator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.CoverHorizontalAdapter;
import ch.mobilethinking.traverse.Adapters.GridCoverImageAdapter;
import ch.mobilethinking.traverse.Adapters.GridPlaylistAdapter;
import ch.mobilethinking.traverse.Adapters.ImageHorizontalAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.playlist.Label;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class EditPlaylistActivity extends AppCompatActivity {

    private final static String TAG = EditPlaylistActivity.class.getSimpleName();
    private Toolbar toolbar;
    private Playlist playlist;
    private EditText playlistTitle;
    private EditText playlistDescription;
    private GridView coverImages;
    private RecyclerView coverList;
    private ChipCloud categoryGrid;
    private ChipCloud tagGrid;
    private SwitchCompat publish;
    private TextView saveButton;

    private String cover = null;

    private ArrayList<String> covers;
    private ArrayList<Boolean> clickedCovers;
    private LinkedHashMap<String, Category> categories;
    private LinkedHashMap<String, String> subcategories;
    private LinkedHashMap<String, Tag> tags;

    private LinkedHashMap<String, Category> playlistCategories;
    private LinkedHashMap<String, String> playlistSubcategories;
    private LinkedHashMap<String, Tag> playlistTags;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_playlist);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.edit_playlist));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        playlistTitle = (EditText) findViewById(R.id.edit_playlist_title_hint);
        playlistDescription = (EditText) findViewById(R.id.edit_playlist_description_hint);
        //coverImages = (GridView) findViewById(R.id.playlist_image_grid);
        coverList = (RecyclerView) findViewById(R.id.playlist_cover_list);
        categoryGrid = (ChipCloud) findViewById(R.id.category_grid);
        tagGrid = (ChipCloud) findViewById(R.id.tag_grid);
        publish = (SwitchCompat) findViewById(R.id.edit_playlist_publish_switch);
        saveButton = (TextView) findViewById(R.id.save_playlist_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    createPlaylistInfo();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        if(getIntent().hasExtra("playlist")) {
            playlist = (Playlist) getIntent().getSerializableExtra("playlist");
        }

        covers = new ArrayList<String>();
        clickedCovers = new ArrayList<Boolean>();
        categories = new LinkedHashMap<String, Category>();
        subcategories = new LinkedHashMap<String, String>();
        tags = new LinkedHashMap<String, Tag>();

        playlistCategories = new LinkedHashMap<String, Category>();
        playlistSubcategories = new LinkedHashMap<String, String>();
        playlistTags = new LinkedHashMap<String, Tag>();

        for (LinkedFiche ln: playlist.getLinkedFiches()
             ) {
            covers.add(ln.getFiche().getCoverPath());
            if(!TextUtils.isEmpty(playlist.getCoverPath()) && playlist.getCoverPath().equals(ln.getFiche().getCoverPath())) {
                clickedCovers.add(true);
                cover = playlist.getCoverPath();
            } else {
                clickedCovers.add(false);
            }
            for (Category c: ln.getFiche().getCategories()
                 ) {
                categories.put(c.getId(), c);
            }
            for (Tag t: ln.getFiche().getTags()
                    ) {
                tags.put(t.getId(), t);
            }
            for (String s: ln.getFiche().getSubCategories()
                    ) {
                subcategories.put(s, s);
            }
        }

        if(playlist.getLabels() != null) {
            for (Category c : playlist.getLabels().getCategories()
                    ) {
                playlistCategories.put(c.getId(), c);
            }

            for (String s : playlist.getLabels().getSousCategories()
                    ) {
                playlistSubcategories.put(s, s);
            }

            for (Tag t : playlist.getLabels().getTags()
                    ) {
                playlistTags.put(t.getId(), t);
            }
        }

        if(playlist.getStatus() == 1){
            publish.setChecked(true);
        }

        setTitle(playlist.getName());
        setDescription(playlist.getDescription());
        setCoverImages(covers, clickedCovers);
        //setCovers(covers);
        setCategories(categories);
        setTags(tags);

    }

    private void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            playlistTitle.setText(title);
        }
    }

    private void setDescription(String description) {
        if(!TextUtils.isEmpty(description)) {
            playlistDescription.setText(description);
        }
    }

    private void setCovers(ArrayList<String> covers) {
        final GridCoverImageAdapter adapter = new GridCoverImageAdapter(this, covers);
        coverImages.setAdapter(adapter);
    }

    public void setCoverImages (List<String> covers, List<Boolean> clickedCovers) {
        CoverHorizontalAdapter coverImagesAdapter = new CoverHorizontalAdapter(this, covers, clickedCovers);
        coverList.setAdapter(coverImagesAdapter);
    }

    private void setCategories(final LinkedHashMap<String, Category> categories) {
        int i = 0;
        String[] empty = new String[0];
        new ChipCloud.Configure()
                .chipCloud(categoryGrid)
                .labels(empty)
                .build();
        for (Category c: categories.values()
             ) {
            categoryGrid.addChip(c.getName());
            if(playlistCategories.containsKey(c.getId())){
                categoryGrid.setSelectedChip(i);
            }
            i++;
        }
        categoryGrid.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int index) {
                Category c = (Category) categories.values().toArray()[index];
                playlistCategories.put(c.getId(), c);
                Log.d(TAG, playlistCategories.values().toString());
            }
            @Override
            public void chipDeselected(int index) {
                Category c = (Category) categories.values().toArray()[index];
                playlistCategories.remove(c.getId());
                Log.d(TAG, playlistCategories.values().toString());
            }
        });
    }

    private void setTags(final LinkedHashMap<String, Tag> tags) {
        int i = 0;
        String[] empty = new String[0];
        new ChipCloud.Configure()
                .chipCloud(tagGrid)
                .labels(empty)
                .build();
        for (Tag t: tags.values()
                ) {
            tagGrid.addChip(t.getName());
            if(playlistTags.containsKey(t.getId())){
                tagGrid.setSelectedChip(i);
            }
            i++;
        }
        tagGrid.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int index) {
                Tag t = (Tag) tags.values().toArray()[index];
                playlistTags.put(t.getId(), t);
                Log.d(TAG, playlistTags.values().toString());
            }
            @Override
            public void chipDeselected(int index) {
                Tag t = (Tag) tags.values().toArray()[index];
                playlistTags.remove(t.getId());
                Log.d(TAG, playlistTags.values().toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void addPlaylist (JSONObject playlistInfo) {
        APIManager.getPlaylistManager(getApplicationContext()).addPlaylist(playlistInfo).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                final Playlist playlist = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        Toast.makeText(EditPlaylistActivity.this, getString(R.string.playlist_added), Toast.LENGTH_LONG).show();
                        APIManager.getUserManager(getApplicationContext()).saveUserPlaylists(PreferenceManager.getLogedUser(getApplicationContext()).getId());
                        finish();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(EditPlaylistActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void updatePlaylist (JSONObject playlistInfo) {
        APIManager.getPlaylistManager(getApplicationContext()).updatePlaylist(playlistInfo, playlist.getId()).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                playlist = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        Toast.makeText(EditPlaylistActivity.this, getString(R.string.playlist_updated), Toast.LENGTH_LONG).show();
                        APIManager.getUserManager(getApplicationContext()).saveUserPlaylists(PreferenceManager.getLogedUser(getApplicationContext()).getId());
                        Intent i = new Intent(EditPlaylistActivity.this, PlaylistOptionsActivity.class);
                        i.putExtra("playlist", playlist);
                        startActivity(i);
                        finish();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(EditPlaylistActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void setCover(@Nullable String cover) {
        this.cover = cover;
    }

    public String getCover() {
        return this.cover;
    }

    private boolean createPlaylistInfo() throws JSONException {
        //HashMap<String, Object> playlistInfo = new HashMap<String, Object>();
        JSONObject playlistInfo = new JSONObject();

        //name
        if(!TextUtils.isEmpty(playlistTitle.getText().toString())) {
            playlistInfo.put("name", playlistTitle.getText().toString());
        } else {
            showError(R.string.title_empty);
            return false;
        }

        //description
        if(!TextUtils.isEmpty(playlistDescription.getText().toString())) {
            playlistInfo.put("description", playlistDescription.getText().toString());
        } else {
            showError(R.string.description_empty);
            return false;
        }

        //add user and linked fiche if it is a new playlist (id == null)
        if(playlist.getId() == null) {
            playlistInfo.put("created_by", PreferenceManager.getLogedUser(this).getId());
            playlistInfo.put("ficheId", playlist.getLinkedFiches()[0].getFiche().getId());
        }

        //cover
        if(!TextUtils.isEmpty(cover)) {
            playlistInfo.put("photoUrl", cover);
        } else {
            showError(R.string.cover_empty);
            return false;
        }

        //status
        playlistInfo.put("status", publish.isChecked() ? "1" : "0");

        //labels
        JSONObject labels = new JSONObject();

        JSONArray categories = new JSONArray();
        for (String s: playlistCategories.keySet()
             ) {
            categories.put(s);
        }

        JSONArray sousCategories = new JSONArray();
        for (String s: playlistSubcategories.keySet()
                ) {
            sousCategories.put(s);
        }

        JSONArray tags = new JSONArray();
        for (String s: playlistTags.keySet()
                ) {
            tags.put(s);
        }

        labels.put("categories", categories);
        labels.put("sousCategories", sousCategories);
        labels.put("tags", tags);
        playlistInfo.put("labels", labels);

        try {
            Log.d(TAG, playlistInfo.toString(4));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if(playlist.getId() != null) {
            updatePlaylist(playlistInfo);
        } else {
            addPlaylist(playlistInfo);
        }

        return true;

    }

    private void showError(int errorString) {
        Toast.makeText(this, getString(errorString), Toast.LENGTH_LONG).show();
    }

}
