package ch.mobilethinking.traverse.Views.Activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.LinkSpan;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.User;

/**
 * Created by Carlos Ballester on 09-Aug-17.
 */

public class RegisterActivity extends AppCompatActivity {

    private final static String TAG = RegisterActivity.class.getSimpleName();
    private Toolbar toolbar;
    HashMap<String, String> user = new HashMap<String, String>();

    private EditText editName;
    private ImageView checkName;
    private EditText editEmail;
    private ImageView checkEmail;
    private EditText editPassword;
    private ImageView checkPassword;
    private EditText editRepeatPassword;
    private ImageView checkRepeatPassword;
    private CheckBox newsletterCheck;
    private CheckBox termsCheck;
    private TextView termsText;
    private TextView buttonRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.register_activity));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        editName = (EditText) findViewById(R.id.edit_name);
        checkName = (ImageView) findViewById(R.id.check_name);

        TextWatcher nameTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0) {
                    checkName.setVisibility(View.GONE);
                } else {
                    editName.setError(null);
                    checkName.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        editName.addTextChangedListener(nameTextWatcher);

        editEmail = (EditText) findViewById(R.id.edit_email);
        checkEmail = (ImageView) findViewById(R.id.check_email);

        TextWatcher emailTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() == 0 || !isEmailValid(s.toString())) {
                    checkEmail.setVisibility(View.GONE);
                } else {
                    editEmail.setError(null);
                    checkEmail.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        editEmail.addTextChangedListener(emailTextWatcher);

        editPassword = (EditText) findViewById(R.id.edit_password);
        checkPassword = (ImageView) findViewById(R.id.check_password);

        TextWatcher passwordTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!isPasswordValid(s.toString())) {
                    checkPassword.setVisibility(View.GONE);
                } else {
                    editPassword.setError(null);
                    checkPassword.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        editPassword.addTextChangedListener(passwordTextWatcher);

        editRepeatPassword = (EditText) findViewById(R.id.edit_repeatpassword);
        checkRepeatPassword = (ImageView) findViewById(R.id.check_repeatpassword);

        TextWatcher repeatPasswordTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!isPasswordValid(editPassword.getText().toString()) || !isMatchingPassword(editPassword.getText().toString(), s.toString())) {
                    checkRepeatPassword.setVisibility(View.GONE);
                } else {
                    editRepeatPassword.setError(null);
                    checkRepeatPassword.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        editRepeatPassword.addTextChangedListener(repeatPasswordTextWatcher);

        newsletterCheck = (CheckBox) findViewById(R.id.newsletter_checkbox);

        termsCheck = (CheckBox) findViewById(R.id.terms_checkbox);
        termsCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    termsText.setError(null);
                }
            }
        });

        termsText = (TextView) findViewById(R.id.terms_text);

        Spannable spannableDescription = new SpannableString(Html.fromHtml(getString(R.string.terms_html)));
        // Replace each URLSpan by a LinkSpan
        URLSpan[] spans = spannableDescription.getSpans(0, spannableDescription.length(), URLSpan.class);
        for (URLSpan urlSpan : spans) {
            Log.d(TAG, urlSpan.getURL());
            LinkSpan linkSpan = new LinkSpan(this, urlSpan.getURL());
            int spanStart = spannableDescription.getSpanStart(urlSpan);
            int spanEnd = spannableDescription.getSpanEnd(urlSpan);
            spannableDescription.setSpan(linkSpan, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableDescription.removeSpan(urlSpan);
        }

        // Make sure the TextView supports clicking on Links
        this.termsText.setMovementMethod(LinkMovementMethod.getInstance());
        this.termsText.setText(spannableDescription, TextView.BufferType.SPANNABLE);

        buttonRegister = (TextView) findViewById(R.id.button_register);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void signUp () {
        APIManager.getUserManager(getApplicationContext()).signUp(new JSONObject(user)).onSuccess(new Continuation<User, Void>() {

            @Override
            public Void then(final Task<User> task) throws Exception {
                final User user = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# USER #################\n";
                        toWrite += user.toString();
                        Log.d(TAG, toWrite);
                        PreferenceManager.setLogedUser(getBaseContext(), user);
                        onBackPressed();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(RegisterActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void userHashmap (String name, String email, String password) {
        user.put("name", name);
        user.put("email", email);
        user.put("password", password);
        signUp();
    }

    /**
     * Attempts to register the account specified by the register form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        editName.setError(null);
        editEmail.setError(null);
        editPassword.setError(null);
        editRepeatPassword.setError(null);
        termsText.setError(null);

        // Store values at the time of the login attempt.
        String name = editName.getText().toString();
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        String repeatPassword = editRepeatPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        //Check for terms checkbox
        if(!termsCheck.isChecked()) {
            termsText.setError(getString(R.string.accept_terms));
            focusView = termsText;
            cancel = true;
        }

        //Check for a valid password
        if (TextUtils.isEmpty(password)) {
            editPassword.setError(getString(R.string.error_empty_password));
            focusView = editPassword;
            cancel = true;
        } else if (!isPasswordValid(password)) {
            editPassword.setError(getString(R.string.error_short_password));
            focusView = editPassword;
            cancel = true;
        } else if (!isMatchingPassword(password, repeatPassword)) {
            editRepeatPassword.setError(getString(R.string.error_match_password));
            focusView = editRepeatPassword;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email) || !isEmailValid(email)) {
            editEmail.setError(getString(R.string.error_invalid_email));
            focusView = editEmail;
            cancel = true;
        }

        // Check for a valid name, if the user entered one.
        if (TextUtils.isEmpty(name)) {
            editName.setError(getString(R.string.error_empty_name));
            focusView = editName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            userHashmap(name, email, password);
        }
    }

    private boolean isEmailValid(String email) {
        Matcher matcher = Patterns.EMAIL_ADDRESS.matcher(email);
        return matcher.matches();
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 7;
    }

    private boolean isMatchingPassword(String password, String repeatPassword) {
        return password.equals(repeatPassword);
    }

}
