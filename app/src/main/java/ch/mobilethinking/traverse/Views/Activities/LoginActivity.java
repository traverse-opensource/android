package ch.mobilethinking.traverse.Views.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.User;
import retrofit2.Call;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private final static String TAG = LoginActivity.class.getSimpleName();
    private final static String FACEBOOK = "facebook";
    private final static String TWITTER = "twitter";
    private final static String GOOGLE = "google";
    private final static int GOOGLE_SIGNIN = 1337;
    private Toolbar toolbar;
    private CallbackManager callbackManager;
    TwitterAuthClient twitterAuthClient;
    private GoogleApiClient mGoogleApiClient;
    private ProfileTracker profileTracker;
    private TextInputEditText username;
    private TextInputEditText password;
    private TextView loginButton;
    private TextView registerButton;
    private TextView forgotPassword;
    private RelativeLayout facebookButton;
    private RelativeLayout twitterButton;
    private RelativeLayout googleButton;
    HashMap<String, String> user = new HashMap<String, String>();
    private boolean revealPass = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel);

        username = (TextInputEditText) findViewById(R.id.username_edit);
        password = (TextInputEditText) findViewById(R.id.password_edit);
        loginButton = (TextView) findViewById(R.id.button_login);
        registerButton = (TextView) findViewById(R.id.button_register);
        forgotPassword = (TextView) findViewById(R.id.forgotten);
        facebookButton = (RelativeLayout) findViewById(R.id.facebook_button);
        twitterButton = (RelativeLayout) findViewById(R.id.twitter_button);
        googleButton = (RelativeLayout) findViewById(R.id.google_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });

        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPasswordDialog();
            }
        });

        facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
            }
        });

        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "Access token: " + AccessToken.getCurrentAccessToken().getToken());
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                // Application code
                                try {
                                    Log.d(TAG, "Profile: " + object.getString("email") + " - " + Profile.getCurrentProfile().getId() + "  - " + Profile.getCurrentProfile().getName() + "  - " + Profile.getCurrentProfile().getProfilePictureUri(200, 200));
                                    userHashmap(Profile.getCurrentProfile().getId(), Profile.getCurrentProfile().getName(), object.getString("email"), FACEBOOK, null);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,picture");
                request.setParameters(parameters);
                request.executeAsync();

            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        twitterAuthClient= new TwitterAuthClient();
        twitterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                twitterAuthClient.authorize(LoginActivity.this, new com.twitter.sdk.android.core.Callback<TwitterSession>() {
                    @Override
                    public void success(Result<TwitterSession> twitterSessionResult) {
                        // Success
                        Call<com.twitter.sdk.android.core.models.User> user = TwitterCore.getInstance().getApiClient().getAccountService().verifyCredentials(false, false, true);
                        user.enqueue(new Callback<com.twitter.sdk.android.core.models.User>() {
                            @Override
                            public void success(Result<com.twitter.sdk.android.core.models.User> userResult) {
                                userHashmap(userResult.data.idStr, userResult.data.name, userResult.data.email, TWITTER, userResult.data.profileImageUrl.replace("_normal", "_bigger"));
                            }

                            @Override
                            public void failure(TwitterException exc) {
                                Log.d("TwitterKit", "Verify Credentials Failure", exc);
                            }
                        });
                    }
                    @Override
                    public void failure(TwitterException e) {
                        e.printStackTrace();
                    }
                });
            }
        });

        // Configure sign-in to request the user's ID, email address, and basic profile. ID and
        // basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to GoogleSignIn.API and the options above.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        googleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, GOOGLE_SIGNIN);
            }
        });

    }

    /**
     * Alerts when the Google Play services client is successfully connected.
     */
    @Override
    public void onConnected(Bundle connectionHint) {

    }

    /**
     * Handles failure to connect to the Google Play services client.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // Refer to the reference doc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    /**
     * Handles suspension of the connection to the Google Play services client.
     */
    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Play services connection suspended");
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(FacebookSdk.isFacebookRequestCode(requestCode)) {
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
        } else if(requestCode == TwitterAuthConfig.DEFAULT_AUTH_REQUEST_CODE) {
            super.onActivityResult(requestCode, resultCode, data);
            twitterAuthClient.onActivityResult(requestCode, resultCode, data);
        } else if(requestCode == GOOGLE_SIGNIN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                // Get account information
                userHashmap(acct.getId(), acct.getDisplayName(), acct.getEmail(), GOOGLE, acct.getPhotoUrl().toString());
            }
        }
    }

    private void login (JSONObject user) {
        APIManager.getUserManager(getApplicationContext()).login(user).onSuccess(new Continuation<User, Void>() {

            @Override
            public Void then(final Task<User> task) throws Exception {
                final User user = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# USER #################\n";
                        toWrite += user.toString();
                        Log.d(TAG, toWrite);
                        PreferenceManager.setLogedUser(getBaseContext(), user);
                        onBackPressed();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.showMessage(LoginActivity.this, R.string.wrong_login);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void signUp () {
        APIManager.getUserManager(getApplicationContext()).signUp(new JSONObject(user)).onSuccess(new Continuation<User, Void>() {

            @Override
            public Void then(final Task<User> task) throws Exception {
                final User user = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# USER #################\n";
                        toWrite += user.toString();
                        Log.d(TAG, toWrite);
                        PreferenceManager.setLogedUser(getBaseContext(), user);
                        onBackPressed();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(LoginActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void userHashmap (String id, String name, String email, String kind, @Nullable String picture) {
        user.put("email", email);
        user.put("name", name);
        user.put("kind", kind);
        user.put("userId", id);
        user.put("userName", name);
        if(kind.equals(FACEBOOK)) {
            user.put("picture", "https://graph.facebook.com/" + id + "/picture?type=large");
        } else {
            user.put("picture", picture);
        }
        signUp();
    }

    /**
     * Attempts to sign in the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        username.setError(null);
        password.setError(null);

        // Store values at the time of the login attempt.
        String userString = username.getText().toString();
        String passwordString = password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(passwordString)) {
            password.setError(getString(R.string.error_empty_password));
            focusView = password;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(userString)) {
            username.setError(getString(R.string.error_user_required));
            focusView = username;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            HashMap<String, String> user = new HashMap<>();
            user.put("email", username.getText().toString());
            user.put("password", password.getText().toString());
            login(new JSONObject(user));
        }
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows a dialog to allow password recovery
     */
    private void  showPasswordDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.dialog_custom_text_input, null);
        builder.setView(dialog_layout);
        final EditText email = (EditText) dialog_layout.findViewById(R.id.edit_email);
        email.setError(null);
        builder.setPositiveButton(R.string.yes, null);
        builder.setNegativeButton(R.string.no, null);
        final AlertDialog d = builder.create();
        d.show();
        Button yes = d.getButton(AlertDialog.BUTTON_POSITIVE);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email.setError(null);
                if (TextUtils.isEmpty(email.getText().toString())) {
                    email.setError(getString(R.string.error_invalid_email));
                    email.requestFocus();
                } else {
                    recoverPassword(email.getText().toString());
                    d.dismiss();
                }
            }
        });
        Button no = d.getButton(AlertDialog.BUTTON_NEGATIVE);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
    }

    private void recoverPassword (String email) {
        APIManager.getUserManager(getApplicationContext()).resetPassword(email).onSuccess(new Continuation<SimpleResponse, Void>() {

            @Override
            public Void then(final Task<SimpleResponse> task) throws Exception {
                final String message = task.getResult().getMessage();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        MessageUtil.showMessage(LoginActivity.this, message);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.showMessage(LoginActivity.this, R.string.invalid_user);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }


}
