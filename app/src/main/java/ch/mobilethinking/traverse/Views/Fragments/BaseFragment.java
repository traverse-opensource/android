package ch.mobilethinking.traverse.Views.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ScrollingView;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.model.LatLng;

import ch.mobilethinking.traverse.Traverse;
import ch.mobilethinking.traverse.Views.Activities.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public abstract class BaseFragment extends Fragment {

    public RelativeLayout loadingLayout;
    public View mainLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((MainActivity)getActivity()).setSelectedTab(getTab());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    public void switchFragment (BaseFragment fragment, String tag, boolean addToBackStack) {
        ((MainActivity)getActivity()).switchFragment(fragment, tag, addToBackStack, null);
    }

    public Fragment getCurrentFragment() {
        return ((MainActivity)getActivity()).getCurrentFragment();
    }

    public void setCurrentFragment(Fragment currentFragment) {
        ((MainActivity)getActivity()).setCurrentFragment(currentFragment);
    }

    public void setSelectedTab (int tab) {
        ((MainActivity)getActivity()).setSelectedTab(tab);
    }

    public void showProgress () {
        if(loadingLayout != null) {
            loadingLayout.setVisibility(View.VISIBLE);
        }
        if(mainLayout != null){
            mainLayout.setVisibility(View.GONE);
        }
    }

    public void hideProgress () {
        if(loadingLayout != null) {
            loadingLayout.setVisibility(View.GONE);
        }
        if(mainLayout != null){
            mainLayout.setVisibility(View.VISIBLE);
        }
    }

    public void showLoading () {
        ((MainActivity)getActivity()).showLoading();
    }

    public void hideLoading () {
        ((MainActivity)getActivity()).hideLoading();
    }

    public LatLng getLocation () {
        return Traverse.getLastKnownLocation();
    }

    public GoogleApiClient getGoogleApiClient () {
        return ((MainActivity)getActivity()).getGoogleApiClient();
    }

    public void setActionBar (Toolbar toolbar) {
        ((MainActivity)getActivity()).setSupportActionBar(toolbar);
    }

    public ActionBar getActionBar () {
        return ((MainActivity)getActivity()).getSupportActionBar();
    }



    public abstract int getTab ();
    public abstract void setTab (int tab);

}
