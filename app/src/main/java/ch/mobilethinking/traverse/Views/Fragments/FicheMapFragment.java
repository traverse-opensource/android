package ch.mobilethinking.traverse.Views.Fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.florent37.fiftyshadesof.FiftyShadesOf;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.List;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.MyClusterItem;
import ch.mobilethinking.traverse.Views.Activities.MainActivity;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import de.hdodenhof.circleimageview.CircleImageView;

public class FicheMapFragment extends BaseFragment implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<MyClusterItem>, ClusterManager.OnClusterInfoWindowClickListener<MyClusterItem>, ClusterManager.OnClusterItemClickListener<MyClusterItem>, ClusterManager.OnClusterItemInfoWindowClickListener<MyClusterItem> {

    public static final String TAG = FicheMapFragment.class.getSimpleName();
    //The tab this fragment is linked to
    public static int TAB = R.id.tab_discovery;
    private static final int DEFAULT_ZOOM = 13;
    private GoogleMap mMap;
    private ClusterManager<MyClusterItem> clusterManager;
    private List<MyClusterItem> fiche;
    private ImageView zoomIn;
    private ImageView zoomOut;
    private ImageView mapSearch;
    private ImageView myLocation;
    private CardView infoCard;
    private ImageView coverImage;
    private View themeLine;
    private CircleImageView themeIconColor;
    private ImageView themeIcon;
    private LinearLayout cardContentLayout;
    private TextView title;
    private TextView address;
    private TextView distance;
    private FiftyShadesOf fiftyShadesOf;
    private SupportMapFragment mapFragment;

    public FicheMapFragment() {
        // Required empty public constructor
    }

    public static FicheMapFragment newInstance(Bundle args) {
        FicheMapFragment fragment = new FicheMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        loadingLayout = (RelativeLayout) rootView.findViewById(R.id.loading_layout);
        zoomIn = (ImageView)  rootView.findViewById(R.id.zoom_in);
        zoomOut = (ImageView)  rootView.findViewById(R.id.zoom_out);
        mapSearch = (ImageView)  rootView.findViewById(R.id.map_search_button);
        mapSearch.setVisibility(View.GONE);
        myLocation = (ImageView)  rootView.findViewById(R.id.my_location);
        infoCard = (CardView) rootView.findViewById(R.id.map_info_card);
        coverImage = (ImageView) rootView.findViewById(R.id.cover_image);
        themeLine = (View) rootView.findViewById(R.id.theme_line);
        themeIconColor = (CircleImageView) rootView.findViewById(R.id.theme_icon_color);
        themeIcon = (ImageView) rootView.findViewById(R.id.theme_icon);
        cardContentLayout = (LinearLayout) rootView.findViewById(R.id.card_content_layout);
        title = (TextView) rootView.findViewById(R.id.title);
        address = (TextView) rootView.findViewById(R.id.address);
        distance= (TextView) rootView.findViewById(R.id.distance);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);

        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        fiche = (List<MyClusterItem>) getArguments().getSerializable("fiche");

        showProgress();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentByTag("ficheMapFragment");
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.map_container, mapFragment, "ficheMapFragment");
            ft.commit();
            fm.executePendingTransactions();
        }

        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.clear();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            } else {
                Log.d(TAG, "Permissions OK");
            }
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);

        //Reposition google map toolbar on top-left
        if (mapFragment != null && mapFragment.getView() != null) {
            View toolbar = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).
                    getParent()).findViewById(Integer.parseInt("4"));
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) toolbar.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            rlp.setMargins(60, 30, 0, 0);
            Log.d(TAG, "Modified map toolbar");
        }

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        hideProgress();

        // Get the last known location and move the camera
        if(fiche != null && fiche.get(0) != null && fiche.get(0).getPosition() != null) {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(getLocation());
            builder.include(fiche.get(0).getPosition());
            LatLngBounds bounds = builder.build();
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                    bounds, 100));
        } else {
            LatLng location = ((MainActivity) getActivity()).getLastLocation();
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    location, DEFAULT_ZOOM));
        }

        clusterManager = new ClusterManager<MyClusterItem>(getContext(), mMap);
        clusterManager.setRenderer(new MyClusterRenderer());

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        mMap.setOnCameraIdleListener(clusterManager);
        mMap.setOnMarkerClickListener(clusterManager);
        mMap.setOnInfoWindowClickListener(clusterManager);

        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterInfoWindowClickListener(this);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setOnClusterItemInfoWindowClickListener(this);

        for (MyClusterItem item: fiche
             ) {
                clusterManager.addItem(item);
            }

        clusterManager.cluster();

            myLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LatLng location = ((MainActivity)getActivity()).getLastLocation();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            location, DEFAULT_ZOOM));
                }
            });

        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float currentZoom = mMap.getCameraPosition().zoom;
                mMap.animateCamera(CameraUpdateFactory.zoomTo(currentZoom+1));
            }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float currentZoom = mMap.getCameraPosition().zoom;
                mMap.animateCamera(CameraUpdateFactory.zoomTo(currentZoom-1));
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                infoCard.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    @Override
    public boolean onClusterClick(Cluster<MyClusterItem> cluster) {
        // Show a toast with some info when the cluster is clicked.

        // Zoom in the cluster. Need to create LatLngBounds and including all the cluster items
        // inside of bounds, then animate to center of the bounds.

        // Create the builder to collect all essential cluster items for the bounds.
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (ClusterItem item : cluster.getItems()) {
            builder.include(item.getPosition());
        }
        // Get the LatLngBounds
        final LatLngBounds bounds = builder.build();

        // Animate camera to the bounds
        try {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return true;
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MyClusterItem> cluster) {
        // Does nothing, but you could go to a list of the users.
    }

    @Override
    public boolean onClusterItemClick(MyClusterItem item) {
        // Loads the info card for a clicked marker
        fiftyShadesOf = FiftyShadesOf.with(getContext())
                .on(title, address, distance)
                .start();
        infoCard.setVisibility(View.VISIBLE);
        getFiche(item.getId());
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(MyClusterItem item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    private class MyClusterRenderer extends DefaultClusterRenderer<MyClusterItem> {

        private final IconGenerator iconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final IconGenerator clusterIconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final ImageView itemImage;

        public MyClusterRenderer() {
            super(getActivity().getApplicationContext(), mMap, clusterManager);
            View clusterView = getActivity().getLayoutInflater().inflate(R.layout.cluster_layout, null);
            FrameLayout clusterFrame = (FrameLayout) clusterView.findViewById(R.id.cluster_frame);
            Drawable background = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_cluster, null);
            clusterFrame.setBackground(background);
            clusterIconGenerator.setContentView(clusterView);

            View itemView = getActivity().getLayoutInflater().inflate(R.layout.fiche_map_marker, null);
            iconGenerator.setContentView(itemView);
            itemImage = (ImageView) itemView.findViewById(R.id.type_icon);

        }

        @Override
        protected void onBeforeClusterItemRendered(MyClusterItem item, MarkerOptions markerOptions) {
            Drawable background = ContextCompat.getDrawable(getContext(), R.drawable.fiche_map_marker_shape);
            Log.d("itemcolor", item.getThemeColor());
            if(!TextUtils.isEmpty(item.getThemeColor())) {
                background.setColorFilter(Color.parseColor(item.getThemeColor()), PorterDuff.Mode.SRC_OVER);
            }
            iconGenerator.setBackground(
                    background);
            Log.d("itemicon", "Icon: " + item.getIcon());
            itemImage.setImageDrawable(getResources().getDrawable(item.getIcon()));
            Bitmap icon = iconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MyClusterItem> cluster, MarkerOptions markerOptions) {
            clusterIconGenerator.setBackground(
                    ContextCompat.getDrawable(getContext(), R.drawable.ic_cluster));
            int size = cluster.getSize();
            Bitmap icon = clusterIconGenerator.makeIcon(String.valueOf(size));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    private void getFiche (String id) {
        APIManager.getFicheManager(getActivity().getApplicationContext()).getFiche(id).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                final Fiche fiche = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# FICHE #################\n";
                        toWrite += fiche.toString();
                        Log.d(TAG, toWrite);

                        fiftyShadesOf.stop();

                        setCoverImage(fiche.getCoverPath());
                        setThemeSchema(fiche.getMainTheme());
                        setTypeIcon(fiche);
                        setTitle(fiche.getName());
                        setDescription(fiche);
                        setDistance(new LatLng(fiche.getLatitude(), fiche.getLongitude()), ((MainActivity)getActivity()).getLastLocation());
                        infoCard.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle b = new Bundle();
                                fiche.setRelatedFiches(null);
                                fiche.setLong_description(null);
                                fiche.setCategories(null);
                                fiche.setTags(null);
                                fiche.setSubCategories(null);
                                b.putSerializable("fiche", fiche);
                                BaseFragment ficheFragment = FicheFragment.newInstance(b);
                                ficheFragment.setTab(getTab());
                                switchFragment(ficheFragment, FicheFragment.TAG, true);
                            }
                        });

                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void setCoverImage(Object resource) {
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Log.d("Image URL", imageUrl);
            Glide.with(this).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        } else {
            Glide.with(this).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        }
    }

    public void setThemeLine(String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            this.themeLine.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    public void setThemeIconColor(String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIconColor.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
        } else {
            this.themeIconColor.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));
        }
    }

    public void setThemeSchema(Theme theme) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setThemeLine(null);
            setThemeIconColor(null);
        } else {
            setThemeLine(theme.getHexColor());
            setThemeIconColor(theme.getHexColor());
        }
    }

    public void setTypeIcon (Fiche fiche){
        if (fiche instanceof FichePlace){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_site));
        } else if (fiche instanceof FichePeople){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_people));
        } else if (fiche instanceof FicheObject){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_object));
        } else if (fiche instanceof FicheEvent){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_events));
        } else if (fiche instanceof FicheMedia){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
        }
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }

    public void setDescription(Fiche fiche) {
        if(!TextUtils.isEmpty(fiche.getShort_description())) {
            this.address.setText(Html.fromHtml(fiche.getShort_description()));
        } else {
            this.address.setVisibility(View.GONE);
        }
    }

    public void setAddress(String address) {

        if(!TextUtils.isEmpty(address)) {
            this.address.setText(address);
        }
    }

    public void setDistance(LatLng fiche, LatLng me) {
        double distance = SphericalUtil.computeDistanceBetween(fiche, me);
        if(!TextUtils.isEmpty(String.valueOf(distance))) {
            this.distance.setText("À " + String.format(Locale.getDefault(), "%.2f", distance/1000) + " km");
        }
    }

}
