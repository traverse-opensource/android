package ch.mobilethinking.traverse.Views.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.FichePlaylistsAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class FichePlaylistsFragment extends BaseFragment {

    public static final String TAG = FichePlaylistsFragment.class.getSimpleName();
    public static int TAB = R.id.tab_discovery;
    private Fiche fiche;
    private RecyclerView playlistsRecycler;
    private FichePlaylistsAdapter playlistAdapter;
    private TextView noPlaylists;

    public FichePlaylistsFragment() {
        // Required empty public constructor
    }

    public static FichePlaylistsFragment newInstance(Bundle args) {
        FichePlaylistsFragment fragment = new FichePlaylistsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_fiche_playlists, container, false);

        playlistsRecycler = (RecyclerView) rootView.findViewById(R.id.fiche_related_playlists);
        noPlaylists = (TextView) rootView.findViewById(R.id.text_no_playlists_related);
        playlistsRecycler.setVisibility(View.VISIBLE);
        noPlaylists.setVisibility(View.GONE);
        fiche = (Fiche) getArguments().getSerializable("fiche");

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getPlaylists(this);
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    private void getPlaylists (final BaseFragment fragment) {
        APIManager.getFicheManager(getActivity().getApplicationContext()).getFichePlaylists(fiche.getId()).onSuccess(new Continuation<Playlist.PlaylistList, Void>() {

            @Override
            public Void then(final Task<Playlist.PlaylistList> task) throws Exception {
                final List<Playlist> playlists = task.getResult().getPlaylists();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# PLAYLISTS #################\n";
                        toWrite += playlists.toString();
                        Log.d(TAG, toWrite);

                        if(playlists != null && !playlists.isEmpty()) {
                            playlistAdapter = new FichePlaylistsAdapter(getActivity(), fragment, ListCombinator.arrayToList(playlists.toArray()));
                            playlistsRecycler.setAdapter(playlistAdapter);
                        } else {
                            playlistsRecycler.setVisibility(View.GONE);
                            noPlaylists.setVisibility(View.VISIBLE);
                        }

                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

}
