package ch.mobilethinking.traverse.Views.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ch.mobilethinking.traverse.Adapters.DiaporamaAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;

public class GridActivity extends AppCompatActivity {

    private static final String TAG = GridActivity.class.getSimpleName();
    private Toolbar toolbar;
    private ImageView diaporama;
    private GridLayout imageGrid;
    private int position = 0;
    private List<Object> images = new ArrayList<Object>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        imageGrid = (GridLayout) findViewById(R.id.image_grid);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        diaporama = (ImageView) toolbar.findViewById(R.id.diaporama);

        images = (List<Object>) getIntent().getSerializableExtra("images");

        diaporama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(GridActivity.this, DiaporamaActivity.class);
                i.putExtra("images", (Serializable)images);
                i.putExtra("position", 0);
                startActivity(i);
                finish();
            }
        });

        for (Object o: images
             ) {
            FicheMedia fm = (FicheMedia) o;
            LayoutInflater inflator = LayoutInflater.from(this);
            View gridImage = inflator
                    .inflate(R.layout.grid_image, imageGrid, false);
            ImageView image = (ImageView) gridImage.findViewById(R.id.image);

            if(!TextUtils.isEmpty(fm.getCoverPath()) && !fm.getCoverPath().contains("defaultImage")) {
                String imageUrl = "";
                if(fm.getCoverPath().split("http").length > 2){
                    imageUrl = "http" + fm.getCoverPath().split("http")[2];
                } else {
                    imageUrl = fm.getCoverPath();
                }
                Glide.with(this).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(image);
            } else {
                Glide.with(this).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(image);
            }

            gridImage.setTag(position);
            gridImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(GridActivity.this, DiaporamaActivity.class);
                    i.putExtra("images", (Serializable)images);
                    i.putExtra("position", (int)v.getTag());
                    startActivity(i);
                    finish();
                }
            });

            imageGrid.addView(gridImage);
            position++;
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
