package ch.mobilethinking.traverse.Views.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ch.mobilethinking.traverse.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TutorialOneFragment extends Fragment {


    public TutorialOneFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_tutorial_1, container, false);
        return rootView;
    }

}
