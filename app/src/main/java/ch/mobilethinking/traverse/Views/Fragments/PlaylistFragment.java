package ch.mobilethinking.traverse.Views.Fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.URLSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.florent37.fiftyshadesof.FiftyShadesOf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.DiscoveryHorizontalAdapter;
import ch.mobilethinking.traverse.Adapters.ImageHorizontalAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.LinkSpan;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Utils.URLParamGenerator;
import ch.mobilethinking.traverse.Views.Activities.AddToPlaylistActivity;
import ch.mobilethinking.traverse.Views.Activities.EditPlaylistActivity;
import ch.mobilethinking.traverse.Views.Activities.LoginActivity;
import ch.mobilethinking.traverse.Views.Activities.PlaylistOptionsActivity;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import de.hdodenhof.circleimageview.CircleImageView;

public class PlaylistFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = PlaylistFragment.class.getSimpleName();
    public static int TAB = R.id.tab_discovery;
    private Playlist playlist;
    String color;
    public ImageView edit;
    public ImageView otherActions;
    public ImageView coverImage;
    private ProgressBar imageProgress;
    public View themeLine;
    private CardView themeIcon;
    public TextView itemNumber;
    public LinearLayout cardContentLayout;
    private CircleImageView contributorImage;
    private TextView contributorName;
    private TextView themeName;
    private TextView title;
    private HorizontalScrollView scrollHashtags;
    private LinearLayout layoutHashtags;
    private TextView description;
    private LinearLayout layoutLinkedFiches;
    private ImageView likeAction;
    private TextView likeNumber;
    private RelativeLayout likeActionLayout;
    private ImageView shareAction;
    private RelativeLayout shareActionLayout;
    public RelativeLayout favouriteActionLayout;
    private ImageView favouriteAction;
    private TextView labelsTitle;
    private ChipCloud labelGrid;
    private PopupMenu popupMenu;
    private FiftyShadesOf fiftyShadesOf;
    private boolean isFavourite;
    private boolean liked = false;

    public PlaylistFragment() {
        // Required empty public constructor
    }

    public static PlaylistFragment newInstance(Bundle args) {
        PlaylistFragment fragment = new PlaylistFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_playlist, container, false);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);

        if(getArguments().containsKey("deeplink")){
            getActionBar().setTitle(getString(R.string.playlist_title));
        } else {
            getActionBar().setHomeButtonEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }

        edit = (ImageView) rootView.findViewById(R.id.edit_playlist);
        //otherActions = (ImageView) rootView.findViewById(R.id.other_actions);
        coverImage = (ImageView) rootView.findViewById(R.id.cover_image);
        imageProgress = (ProgressBar) rootView.findViewById(R.id.image_progress);
        themeLine = (View) rootView.findViewById(R.id.theme_line);
        themeIcon = (CardView) rootView.findViewById(R.id.theme_icon);
        itemNumber = (TextView) rootView.findViewById(R.id.item_number);
        cardContentLayout = (LinearLayout) rootView.findViewById(R.id.card_content_layout);
        contributorImage = (CircleImageView) rootView.findViewById(R.id.contributor_image);
        contributorName = (TextView) rootView.findViewById(R.id.contributor_name);
        cardContentLayout = (LinearLayout) rootView.findViewById(R.id.card_content_layout);
        themeName = (TextView) rootView.findViewById(R.id.theme_name);
        title = (TextView) rootView.findViewById(R.id.title);
        scrollHashtags = (HorizontalScrollView) rootView.findViewById(R.id.scroll_hashtags);
        layoutHashtags = (LinearLayout) rootView.findViewById(R.id.layout_hashtags);
        description = (TextView) rootView.findViewById(R.id.description);
        layoutLinkedFiches = (LinearLayout) rootView.findViewById(R.id.playlist_fiches);
        likeAction = (ImageView) rootView.findViewById(R.id.action_like_icon);
        likeNumber = (TextView) rootView.findViewById(R.id.action_like_label);
        likeActionLayout = (RelativeLayout) rootView.findViewById(R.id.action_like_layout);
        shareAction = (ImageView) rootView.findViewById(R.id.action_share_icon);
        shareActionLayout = (RelativeLayout) rootView.findViewById(R.id.action_share_layout);
        favouriteActionLayout = (RelativeLayout) rootView.findViewById(R.id.action_favourite_layout);
        favouriteAction = (ImageView) rootView.findViewById(R.id.action_favourite_icon);
        labelsTitle = (TextView) rootView.findViewById(R.id.labels_title);
        labelGrid = (ChipCloud) rootView.findViewById(R.id.labels_grid);

        fiftyShadesOf = FiftyShadesOf.with(getContext())
                .on(title, layoutHashtags, description)
                .start();

        setThemeName(" ");

        playlist = (Playlist) getArguments().getSerializable("playlist");

        if(PreferenceManager.getLogedUser(getContext()) != null && PreferenceManager.getLogedUser(getContext()).getId().equals(playlist.getCreated_by().getId())){
            edit.setVisibility(View.VISIBLE);
        }

        edit.setOnClickListener(this);
        //otherActions.setOnClickListener(this);
        favouriteActionLayout.setOnClickListener(this);
        likeActionLayout.setOnClickListener(this);
        shareActionLayout.setOnClickListener(this);
        if(PreferenceManager.getSavedPlaylists(getContext()).containsKey(playlist.getId())){
            isFavourite = true;
            favouriteAction.setImageResource(R.drawable.ic_favourites_selected);
        } else {
            isFavourite = false;
            favouriteAction.setImageResource(R.drawable.ic_favourites);
        }
        setCoverImage(playlist.getCoverPath());
        setItemNumber(playlist.getLinkedFiches().length);
        setTitle(playlist.getName());

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getPlaylist();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_playlist_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
                Bundle b = new Bundle();
                b.putSerializable("fiches", (Serializable) ListCombinator.generateLinkedClusterItems(playlist.getLinkedFiches()));
                b.putSerializable("playlist", playlist);
                BaseFragment mapFragment = PlaylistMapFragment.newInstance(b);
                mapFragment.setTab(getTab());
                switchFragment(mapFragment, PlaylistMapFragment.TAG, true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    private void getPlaylist () {

        APIManager.getPlaylistManager(getActivity().getApplicationContext()).getPlaylist(playlist.getId()).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                Playlist p = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# PLAYLIST #################\n";
                        toWrite += playlist.toString();
                        Log.d(TAG, toWrite);

                        playlist = p;
                        setCoverImage(p.getCoverPath());
                        setItemNumber(p.getLinkedFiches().length);
                        setTitle(p.getName());
                        color = setPlaylistThemeSchema(p.getMainTheme());
                        setContributorName(p.getCreated_by().getName());
                        setLikeNumber(p.getUserIds().length);
                        setContributorImage(p.getCreated_by().getProfile().getPicture());
                        setHashtags(p.getLabels().getTags());
                        setLinkedFiches(p.getLinkedFiches());
                        setDescription(p.getDescription());
                        setLabelGrid(p.getLabels().getCategories(), p.getLabels().getSousCategories(), color);
                        fiftyShadesOf.stop();

                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    getActivity().onBackPressed();
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void setCoverImage(Object resource) {
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Glide.with(getActivity().getApplicationContext()).load(imageUrl).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    imageProgress.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    imageProgress.setVisibility(View.GONE);
                    return false;
                }
            }).into(this.coverImage);
        } else {
            Glide.with(getActivity().getApplicationContext()).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
            imageProgress.setVisibility(View.GONE);
        }
    }

    public void setContributorName(String name) {
        if(!TextUtils.isEmpty(name)) {
            this.contributorName.setText(name);
        }
    }

    public void setContributorImage(String imageUrl) {
        if(!TextUtils.isEmpty(imageUrl)){
            Glide.with(this).load(imageUrl).dontAnimate().into(contributorImage);
        } else {
            Glide.with(this).load(R.drawable.default_contributor).dontAnimate().into(contributorImage);
        }
    }

    public String setThemeSchema(Theme[] themes) {
        if(themes == null || themes.length == 0) {
            setThemeLine(null);
            setThemeName(" ");
            this.themeName.setTextColor(getResources().getColor(R.color.colorAccent));
            return null;
        } else {
            int ponderation = 0;
            Theme finalTheme = new Theme();
            for (Theme t: themes
                    ) {
                if(t.getPonderation() >= ponderation){
                    finalTheme = t;
                }
            }
            setThemeLine(finalTheme.getHexColor());
            setThemeName(": " + finalTheme.getName());
            this.themeName.setTextColor(Color.parseColor(finalTheme.getHexColor()));
            return finalTheme.getHexColor();
        }
    }

    public String setPlaylistThemeSchema(Theme theme) {
            if(theme.getName().equalsIgnoreCase("default")) {
                setThemeLine(null);
                setThemeName(" ");
                setThemeIcon(null);
                this.themeName.setTextColor(getResources().getColor(R.color.colorAccent));
                return null;
            } else {
                setThemeLine(theme.getHexColor());
                setThemeIcon(theme.getHexColor());
                setThemeName(theme.getName());
                this.themeName.setTextColor(Color.parseColor(theme.getHexColor()));
                return theme.getHexColor();
            }
    }

    public void setThemeLine(String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            this.themeLine.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    public void setThemeIcon(String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIcon.setCardBackgroundColor(Color.parseColor(color));
        } else {
            this.themeIcon.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    public void setThemeName(String name) {
        if(!TextUtils.isEmpty(name)) {
            this.themeName.setText(name);
        }
    }

    public void setItemNumber(int items) {
        this.itemNumber.setText(getResources().getQuantityString(R.plurals.playlist_number_fiches, items, items));
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }

    public void setHashtags(Tag[] tags) {
        if(tags == null || tags.length < 1){
            scrollHashtags.setVisibility(View.GONE);
            layoutHashtags.setVisibility(View.GONE);
        } else {
            layoutHashtags.removeAllViews();
            for (Tag t : tags
                    ) {
                TextView tv = new TextView(getContext());
                tv.setText(getString(R.string.hashtag, t.getName()));
                tv.setTextColor(getResources().getColor(R.color.colorHashtag));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0,0,16,0);
                tv.setLayoutParams(lp);
                layoutHashtags.addView(tv);
            }
        }
    }

    public void setDescription(String description) {
        if(!TextUtils.isEmpty(description)) {
            this.description.setText(Html.fromHtml(description));
        } else {
            this.description.setVisibility(View.GONE);
        }
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber.setText(String.valueOf(likeNumber));
        if(PreferenceManager.getLogedUser(getContext()) != null && Arrays.asList(playlist.getUserIds()).contains(PreferenceManager.getLogedUser(getContext()).getId())){
            liked = true;
            likeAction.setImageResource(R.drawable.ic_like_selected);
        } else {
            liked = false;
            likeAction.setImageResource(R.drawable.ic_like);
        }
    }

    public void setLinkedFiches (LinkedFiche[] linkedFiches) {
        layoutLinkedFiches.removeAllViews();
        int i = 1;
        for (LinkedFiche lf: linkedFiches
             ) {
            final Fiche f = lf.getFiche();
            LayoutInflater inflator = LayoutInflater.from(getContext());
            View linkedFicheView = inflator
                    .inflate(R.layout.linked_fiche_view, layoutLinkedFiches, false);
            ((TextView)linkedFicheView.findViewById(R.id.linkedfiche_title)).setText(f.getName());
            if(i == 1){
                linkedFicheView.findViewById(R.id.timeline_hide_top).setVisibility(View.VISIBLE);
            }
            if(i == linkedFiches.length){
                linkedFicheView.findViewById(R.id.timeline_hide_bottom).setVisibility(View.VISIBLE);
                linkedFicheView.findViewById(R.id.separator).setVisibility(View.GONE);
            }
            CircleImageView themeIconCircle = (CircleImageView) linkedFicheView.findViewById(R.id.theme_icon_circle);
            ImageView themeIcon = (ImageView) linkedFicheView.findViewById(R.id.theme_icon);
            CircleImageView ficheNumberCircle = (CircleImageView) linkedFicheView.findViewById(R.id.fiche_number_circle);
            TextView linkedFicheNumber = (TextView) linkedFicheView.findViewById(R.id.linkedfiche_number);
            linkedFicheNumber.setText(String.valueOf(i));

            if (f instanceof FichePlace){
                themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_site));
            } else if (f instanceof FichePeople){
                themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_people));
            } else if (f instanceof FicheObject){
                themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_object));
            } else if (f instanceof FicheEvent){
                themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_events));
            } else if (f instanceof FicheMedia){
                themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
            }

            String color = f.getMainTheme().getHexColor();
            if(!TextUtils.isEmpty(color)){
                themeIconCircle.setBorderColor(Color.parseColor(color));
                themeIcon.setColorFilter(Color.parseColor(color));
                ficheNumberCircle.setBorderColor(Color.parseColor(color));
                linkedFicheNumber.setTextColor(Color.parseColor(color));
            } else {
                themeIconCircle.setBorderColor(getResources().getColor(R.color.colorAccent));
                themeIcon.setColorFilter(getResources().getColor(R.color.colorAccent));
                ficheNumberCircle.setBorderColor(getResources().getColor(R.color.colorAccent));
                linkedFicheNumber.setTextColor(getResources().getColor(R.color.colorAccent));
            }

            HashMap<String, Boolean> viewedFiches = PreferenceManager.getViewedFiches(getContext());
            if(viewedFiches.containsKey(f.getId())){
                if(!TextUtils.isEmpty(color)) {
                    themeIconCircle.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
                    themeIcon.setColorFilter(getResources().getColor(R.color.colorPrimary));
                } else {
                    themeIconCircle.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));
                    themeIcon.setColorFilter(getResources().getColor(R.color.colorPrimary));
                }
            }

            linkedFicheView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle b = new Bundle();
                    b.putSerializable("fiche", f);
                    b.putString("playlistId", playlist.getId());
                    BaseFragment playlistFicheFragment = PlaylistFicheFragment.newInstance(b);
                    playlistFicheFragment.setTab(getTab());
                    switchFragment(playlistFicheFragment, PlaylistFicheFragment.TAG, true);
                }
            });

            layoutLinkedFiches.addView(linkedFicheView);
            i++;
        }
    }

    public void setLabelGrid(Category[] categories, String[] subcategories, String color) {
        labelGrid.removeAllViews();
        if(categories != null && categories.length > 0) {
            if(!TextUtils.isEmpty(color)){
                String[] s = new String[0];
                new ChipCloud.Configure()
                        .chipCloud(labelGrid)
                        .deselectedColor(Color.parseColor(color))
                        .deselectedFontColor(getResources().getColor(R.color.colorLightWhite))
                        .selectedFontColor(getResources().getColor(R.color.colorLightWhite))
                        .labels(s)
                        .build();
            }
            for (Category c : categories
                    ) {
                labelGrid.addChip(c.getName());
            }
            if(subcategories != null && subcategories.length > 0) {
                for (String s : subcategories
                        ) {
                    labelGrid.addChip(s);
                }
            }
        } else{
            labelsTitle.setVisibility(View.GONE);
            labelGrid.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View anchor) {
        // TODO Auto-generated method stub
        switch (anchor.getId()) {
            /*case R.id.other_actions:
                popupMenu = new PopupMenu(this.getContext(), anchor);
                popupMenu.setOnDismissListener(new PlaylistFragment.OnDismissListener());
                popupMenu.setOnMenuItemClickListener(new PlaylistFragment.OnMenuItemClickListener());
                popupMenu.inflate(R.menu.menu_card_other_actions);
                popupMenu.show();
                break;*/
            case R.id.action_favourite_layout:
                if(isFavourite) {
                    PreferenceManager.removePlaylistFromFavourites(getContext(), playlist.getId());
                    isFavourite = false;
                    favouriteAction.setImageResource(R.drawable.ic_favourites);
                } else {
                    PreferenceManager.addPlaylistToFavourites(getContext(), playlist.getId());
                    isFavourite = true;
                    favouriteAction.setImageResource(R.drawable.ic_favourites_selected);
                    APIManager.getPlaylistManager(getActivity().getApplicationContext()).savePlaylist(playlist.getId());
                }
                break;
            case R.id.edit_playlist:
                if(PreferenceManager.getLogedUser(getContext()) != null && PreferenceManager.getLogedUser(getContext()).getId().equals(playlist.getCreated_by().getId())){
                    Intent i= new Intent(getActivity(), PlaylistOptionsActivity.class);
                    i.putExtra("playlist", playlist);
                    startActivity(i);
                }
                break;
            case R.id.action_like_layout:
                if(PreferenceManager.getLogedUser(getContext()) != null) {
                    if (liked) {
                        dislike();
                        liked = false;
                        likeAction.setImageResource(R.drawable.ic_like);
                    } else {
                        like();
                        liked = true;
                        likeAction.setImageResource(R.drawable.ic_like_selected);
                    }
                } else {
                    showLoginDialog();
                }
                break;
            case R.id.action_share_layout:
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                myIntent.putExtra(Intent.EXTRA_TEXT, URLParamGenerator.generateShareUrl(playlist));
                startActivity(Intent.createChooser(myIntent, getString(R.string.share)));
                break;
        }
    }

    private class OnDismissListener implements PopupMenu.OnDismissListener {

        @Override
        public void onDismiss(PopupMenu menu) {

        }

    }

    private class OnMenuItemClickListener implements
            PopupMenu.OnMenuItemClickListener {

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // TODO Auto-generated method stub
            switch (item.getItemId()) {
                case R.id.media_submit:
                    Toast.makeText(getContext(), getString(R.string.coming_soon),
                            Toast.LENGTH_SHORT).show();
                    return true;
                case R.id.problem_submit:
                    Toast.makeText(getContext(), getString(R.string.coming_soon),
                            Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    }

    public void like() {
        APIManager.getPlaylistManager(getActivity().getApplicationContext()).likePlaylist(playlist.getId(), PreferenceManager.getLogedUser(getActivity().getApplicationContext()).getId()).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                Playlist p = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        playlist = p;
                        setLikeNumber(p.getUserIds().length);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void dislike() {
        APIManager.getPlaylistManager(getActivity().getApplicationContext()).dislikePlaylist(playlist.getId(), PreferenceManager.getLogedUser(getActivity().getApplicationContext()).getId()).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                Playlist p = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        playlist = p;
                        setLikeNumber(p.getUserIds().length);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    /**
     * Shows a dialog to allow log in
     */
    private void showLoginDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.login_dialog_text));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}
