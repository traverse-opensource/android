package ch.mobilethinking.traverse.Views.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.FilterHistoryAdapter;
import ch.mobilethinking.traverse.Adapters.ThemeAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Traverse;
import ch.mobilethinking.traverse.Utils.AnimationUtil;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;

public class FilterActivity extends AppCompatActivity {

    private static final String TAG = FilterActivity.class.getSimpleName();
    private Toolbar toolbar;
    private RelativeLayout loadingLayout;
    private View mainLayout;
    private FilterCreator filter;

    private List<Tag> tags;
    private List<Tag> selectedTags;
    private List<Category> categories;
    private List<Category> selectedCategories;
    private List<Theme> themes;
    private ArrayList<FilterCreator> savedFilters;

    //Tag filtering elements
    private LinearLayout tagHeader;
    private TextView tagHeaderCount;
    private LinearLayout tagContent;
    private ImageView tagOpen;
    private AutoCompleteTextView autocompleteTags;
    private ProgressBar progressSearch;
    private ChipCloud tagGrid;
    private int chipCount = 0;

    //Category filtering elements
    private LinearLayout categoryHeader;
    private TextView categoryHeaderCount;
    private LinearLayout categoryContent;
    private ImageView categoryOpen;
    private ChipCloud categoryGrid;
    private int categoryCount = 0;

    //Theme filtering elements
    private LinearLayout themeHeader;
    private TextView themeHeaderCount;
    private LinearLayout themeContent;
    private ImageView themeOpen;
    private RecyclerView themeRecycler;

    //Type filtering elements
    private LinearLayout typeHeader;
    private TextView typeHeaderCount;
    private LinearLayout typeContent;
    private ImageView typeOpen;
    private CardView typeFilterEvents;
    private AppCompatCheckBox typeFilterEventsCheckbox;
    private CardView typeFilterObjects;
    private AppCompatCheckBox typeFilterObjectsCheckbox;
    private CardView typeFilterMedias;
    private AppCompatCheckBox typeFilterMediasCheckbox;
    private CardView typeFilterPeople;
    private AppCompatCheckBox typeFilterPeopleCheckbox;
    private CardView typeFilterPlaces;
    private AppCompatCheckBox typeFilterPlacesCheckbox;
    private CardView typeFilterPlaylists;
    private AppCompatCheckBox typeFilterPlaylistsCheckbox;

    private Button filterApply;
    private Button filterReset;
    private RecyclerView filterHistory;
    private FilterHistoryAdapter filterAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.filters));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        //TODO recover filter if there is already one set in Traverse class
        if(Traverse.getFilter() != null){
            filter = Traverse.getFilter();
        } else {
            filter = new FilterCreator().usesPlaylist(false);
        }

        loadingLayout = (RelativeLayout) findViewById(R.id.loading_layout);
        mainLayout = (ScrollView) findViewById(R.id.main_layout);

        //Tag filtering assignations
        tagHeader = (LinearLayout) findViewById(R.id.tag_filter_header);
        tagHeaderCount = (TextView) findViewById(R.id.tag_filter_header_count);
        tagOpen = (ImageView) findViewById(R.id.tag_open);
        tagContent = (LinearLayout) findViewById(R.id.tag_filter_content);
        autocompleteTags = (AutoCompleteTextView) findViewById(R.id.autocomplete_tags);
        progressSearch = (ProgressBar) findViewById(R.id.progress_search_tags);
        tagGrid = (ChipCloud) findViewById(R.id.tag_grid);
        tagHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tagContent.getVisibility() == View.VISIBLE){
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                    if(inputMethodManager.isActive()) {
                        inputMethodManager.hideSoftInputFromWindow(autocompleteTags.getWindowToken(), 0);
                    }
                    tagOpen.setRotation(180);
                    AnimationUtil.slideUp(getBaseContext(), tagContent);
                } else {
                    if(categoryContent.getVisibility() == View.VISIBLE) {
                        categoryOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), categoryContent);
                    }
                    if(themeContent.getVisibility() == View.VISIBLE) {
                        themeOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), themeContent);
                    }
                    if(typeContent.getVisibility() == View.VISIBLE) {
                        typeOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), typeContent);
                    }
                    tagOpen.setRotation(270);
                    AnimationUtil.slideDown(getBaseContext(), tagContent);
                }
            }
        });
        tagContent.setVisibility(View.GONE);

        //Category filtering assignations
        categoryHeader = (LinearLayout) findViewById(R.id.category_filter_header);
        categoryHeaderCount = (TextView) findViewById(R.id.category_filter_header_count);
        categoryOpen = (ImageView) findViewById(R.id.category_open);
        categoryContent = (LinearLayout) findViewById(R.id.category_filter_content);
        categoryGrid = (ChipCloud) findViewById(R.id.category_grid);
        categoryHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if(inputMethodManager.isActive()) {
                    inputMethodManager.hideSoftInputFromWindow(autocompleteTags.getWindowToken(), 0);
                }
                if(categoryContent.getVisibility() == View.VISIBLE){
                    categoryOpen.setRotation(180);
                    AnimationUtil.slideUp(getBaseContext(), categoryContent);
                } else {
                    if(tagContent.getVisibility() == View.VISIBLE){
                        tagOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), tagContent);
                    }

                    if(themeContent.getVisibility() == View.VISIBLE){
                        themeOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), themeContent);
                    }

                    if(typeContent.getVisibility() == View.VISIBLE){
                        typeOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), typeContent);
                    }
                    categoryOpen.setRotation(270);
                    AnimationUtil.slideDown(getBaseContext(), categoryContent);
                }
            }
        });
        categoryContent.setVisibility(View.GONE);

        //Theme filtering assignations
        themeHeader = (LinearLayout) findViewById(R.id.theme_filter_header);
        themeHeaderCount = (TextView) findViewById(R.id.theme_filter_header_count);
        themeOpen = (ImageView) findViewById(R.id.theme_open);
        themeContent = (LinearLayout) findViewById(R.id.theme_filter_content);
        themeRecycler = (RecyclerView) findViewById(R.id.theme_list);
        themeHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if(inputMethodManager.isActive()) {
                    inputMethodManager.hideSoftInputFromWindow(autocompleteTags.getWindowToken(), 0);
                }
                if(themeContent.getVisibility() == View.VISIBLE){
                    themeOpen.setRotation(180);
                    AnimationUtil.slideUp(getBaseContext(), themeContent);
                } else {
                    if(tagContent.getVisibility() == View.VISIBLE){
                        tagOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), tagContent);
                    }

                    if(categoryContent.getVisibility() == View.VISIBLE){
                        categoryOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), categoryContent);
                    }

                    if(typeContent.getVisibility() == View.VISIBLE){
                        typeOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), typeContent);
                    }
                    themeOpen.setRotation(270);
                    AnimationUtil.slideDown(getBaseContext(), themeContent);
                }
            }
        });
        themeContent.setVisibility(View.GONE);

        //Type filtering assignations
        typeHeader = (LinearLayout) findViewById(R.id.type_filter_header);
        typeHeaderCount = (TextView) findViewById(R.id.type_filter_header_count);
        typeOpen = (ImageView) findViewById(R.id.type_open);
        typeContent = (LinearLayout) findViewById(R.id.type_filter_content);

        typeFilterEvents = (CardView) findViewById(R.id.type_filter_events);
        typeFilterEventsCheckbox = (AppCompatCheckBox) findViewById(R.id.type_filter_events_checkbox);
        typeFilterEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterEventsCheckbox.isChecked()) {
                    typeFilterEventsCheckbox.setChecked(false);
                    filter.removeFicheType("Events");
                } else {
                    typeFilterEventsCheckbox.setChecked(true);
                    filter.addFicheType("Events");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        typeFilterEventsCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterEventsCheckbox.isChecked()) {
                    typeFilterEventsCheckbox.setChecked(false);
                    filter.removeFicheType("Events");
                } else {
                    typeFilterEventsCheckbox.setChecked(true);
                    filter.addFicheType("Events");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        if(filter.getFicheTypes().contains("Events")) {
            typeFilterEventsCheckbox.setChecked(true);
            setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
        }

        typeFilterObjects = (CardView) findViewById(R.id.type_filter_objects);
        typeFilterObjectsCheckbox = (AppCompatCheckBox) findViewById(R.id.type_filter_objects_checkbox);
        typeFilterObjects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterObjectsCheckbox.isChecked()) {
                    typeFilterObjectsCheckbox.setChecked(false);
                    filter.removeFicheType("Objects");
                } else {
                    typeFilterObjectsCheckbox.setChecked(true);
                    filter.addFicheType("Objects");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        typeFilterObjectsCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterObjectsCheckbox.isChecked()) {
                    typeFilterObjectsCheckbox.setChecked(false);
                    filter.removeFicheType("Objects");
                } else {
                    typeFilterObjectsCheckbox.setChecked(true);
                    filter.addFicheType("Objects");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        if(filter.getFicheTypes().contains("Objects")) {
            typeFilterObjectsCheckbox.setChecked(true);
            setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
        }

        typeFilterMedias = (CardView) findViewById(R.id.type_filter_medias);
        typeFilterMediasCheckbox = (AppCompatCheckBox) findViewById(R.id.type_filter_medias_checkbox);
        typeFilterMedias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterMediasCheckbox.isChecked()) {
                    typeFilterMediasCheckbox.setChecked(false);
                    filter.removeFicheType("Media");
                } else {
                    typeFilterMediasCheckbox.setChecked(true);
                    filter.addFicheType("Media");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        typeFilterMediasCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterMediasCheckbox.isChecked()) {
                    typeFilterMediasCheckbox.setChecked(false);
                    filter.removeFicheType("Media");
                } else {
                    typeFilterMediasCheckbox.setChecked(true);
                    filter.addFicheType("Media");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        if(filter.getFicheTypes().contains("Media")) {
            typeFilterMediasCheckbox.setChecked(true);
            setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
        }

        typeFilterPeople = (CardView) findViewById(R.id.type_filter_people);
        typeFilterPeopleCheckbox = (AppCompatCheckBox) findViewById(R.id.type_filter_people_checkbox);
        typeFilterPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterPeopleCheckbox.isChecked()) {
                    typeFilterPeopleCheckbox.setChecked(false);
                    filter.removeFicheType("People");
                } else {
                    typeFilterPeopleCheckbox.setChecked(true);
                    filter.addFicheType("People");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        typeFilterPeopleCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterPeopleCheckbox.isChecked()) {
                    typeFilterPeopleCheckbox.setChecked(false);
                    filter.removeFicheType("People");
                } else {
                    typeFilterPeopleCheckbox.setChecked(true);
                    filter.addFicheType("People");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        if(filter.getFicheTypes().contains("People")) {
            typeFilterPeopleCheckbox.setChecked(true);
            setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
        }

        typeFilterPlaces = (CardView) findViewById(R.id.type_filter_places);
        typeFilterPlacesCheckbox = (AppCompatCheckBox) findViewById(R.id.type_filter_places_checkbox);
        typeFilterPlaces.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterPlacesCheckbox.isChecked()) {
                    typeFilterPlacesCheckbox.setChecked(false);
                    filter.removeFicheType("Places");
                } else {
                    typeFilterPlacesCheckbox.setChecked(true);
                    filter.addFicheType("Places");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        typeFilterPlacesCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterPlacesCheckbox.isChecked()) {
                    typeFilterPlacesCheckbox.setChecked(false);
                    filter.removeFicheType("Places");
                } else {
                    typeFilterPlacesCheckbox.setChecked(true);
                    filter.addFicheType("Places");
                }
                setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
            }
        });
        if(filter.getFicheTypes().contains("Places")) {
            typeFilterPlacesCheckbox.setChecked(true);
            setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
        }

        typeFilterPlaylists = (CardView) findViewById(R.id.type_filter_playlists);
        typeFilterPlaylistsCheckbox = (AppCompatCheckBox) findViewById(R.id.type_filter_playlists_checkbox);
        typeFilterPlaylists.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterPlaylistsCheckbox.isChecked()) {
                    typeFilterPlaylistsCheckbox.setChecked(false);
                    filter.usesPlaylist(false);
                    setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
                } else {
                    typeFilterPlaylistsCheckbox.setChecked(true);
                    filter.usesPlaylist(true);
                    setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
                }
            }
        });
        typeFilterPlaylistsCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(typeFilterPlaylistsCheckbox.isChecked()) {
                    typeFilterPlaylistsCheckbox.setChecked(false);
                    filter.usesPlaylist(false);
                    setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
                } else {
                    typeFilterPlaylistsCheckbox.setChecked(true);
                    filter.usesPlaylist(true);
                    setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
                }
            }
        });
        if(filter.getUsesPlaylist()) {
            typeFilterPlaylistsCheckbox.setChecked(true);
            setCounts(typeHeaderCount, R.plurals.type_count, filter.getFicheTypes().size(), filter.getFicheTypes().size());
        }

        typeHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                if(inputMethodManager.isActive()) {
                    inputMethodManager.hideSoftInputFromWindow(autocompleteTags.getWindowToken(), 0);
                }
                if(typeContent.getVisibility() == View.VISIBLE){
                    typeOpen.setRotation(180);
                    AnimationUtil.slideUp(getBaseContext(), typeContent);
                } else {
                    if(tagContent.getVisibility() == View.VISIBLE){
                        tagOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), tagContent);
                    }

                    if(categoryContent.getVisibility() == View.VISIBLE){
                        categoryOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), categoryContent);
                    }

                    if(themeContent.getVisibility() == View.VISIBLE){
                        themeOpen.setRotation(180);
                        AnimationUtil.slideUp(getBaseContext(), themeContent);
                    }

                    typeOpen.setRotation(270);
                    AnimationUtil.slideDown(getBaseContext(), typeContent);
                }
            }
        });
        typeContent.setVisibility(View.GONE);

        filterApply = (Button) findViewById(R.id.filter_apply_button);
        filterApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Filter is: " + filter.build());
                Traverse.setFilter(filter);
                onBackPressed();
            }
        });

        filterReset = (Button) findViewById(R.id.filter_reset_button);
        filterReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Traverse.setFilter(null);
                onBackPressed();
            }
        });

        filterHistory = (RecyclerView) findViewById(R.id.filter_history);
        savedFilters = PreferenceManager.getSavedFilters(this);
        filterAdapter = new FilterHistoryAdapter(this, savedFilters);
        filterHistory.setAdapter(filterAdapter);

        if (savedInstanceState != null) {
            //probably orientation change
            tags = (List<Tag>) savedInstanceState.getSerializable("tags");
            selectedTags = (List<Tag>) savedInstanceState.getSerializable("selected_tags");
            categories = (List<Category>) savedInstanceState.getSerializable("categories");
            selectedCategories = (List<Category>) savedInstanceState.getSerializable("selected_categories");
            themes = (List<Theme>) savedInstanceState.getSerializable("themes");
        } else {
            if (tags != null && categories != null && themes != null) {
                //returning from backstack, data is fine, do nothing
            } else {
                showProgress();
                if(tags == null) {
                    tags = new ArrayList<>();
                    selectedTags = new ArrayList<>();
                    getTags();
                }
                if(categories == null) {
                    categories = new ArrayList<>();
                    selectedCategories = new ArrayList<>();
                    getCategories();
                }
                if(themes == null) {
                    themes = new ArrayList<>();
                    getThemes();
                }
            }
        }

    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("tags", (Serializable) tags);
        outState.putSerializable("selected_tags", (Serializable) selectedTags);
        outState.putSerializable("categories", (Serializable) categories);
        outState.putSerializable("selected_categories", (Serializable) selectedCategories);
        outState.putSerializable("themes", (Serializable) themes);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_filter_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                if(filter.isEmpty()){
                    Toast.makeText(this, getString(R.string.filter_empty), Toast.LENGTH_LONG).show();
                } else {
                    ArrayList<FilterCreator> filters = PreferenceManager.getSavedFilters(this);
                    Calendar c = Calendar.getInstance();
                    filter.setDate(c.getTimeInMillis());
                    filters.add(0, filter);
                    savedFilters.add(0, filter);
                    PreferenceManager.setSavedFilters(this, filters);
                    filterAdapter.notifyDataSetChanged();
                    Toast.makeText(this, getString(R.string.filter_saved), Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("unchecked")
    private void getTags () {
        APIManager.getFilterManager(getApplicationContext()).getTags().onSuccess(new Continuation<Tag[], Void>() {

            @Override
            public Void then(final Task<Tag[]> task) throws Exception {
                final Tag[] tagList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        String[] empty = new String[0];
                        new ChipCloud.Configure()
                                .chipCloud(tagGrid)
                                .labels(empty)
                                .build();

                        tags = (List<Tag>)(Object) ListCombinator.arrayToList(tagList);
                        final ArrayAdapter<Tag> adapter = new ArrayAdapter<Tag>(getBaseContext(),
                                android.R.layout.simple_dropdown_item_1line, tags);
                        autocompleteTags.setAdapter(adapter);
                        TextWatcher completeWatcher = new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if (s.length() > 0){
                                    progressSearch.setVisibility(View.VISIBLE);
                                } else if (s.length() == 0) {
                                    progressSearch.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void afterTextChanged(Editable s) {

                            }
                        };
                        autocompleteTags.addTextChangedListener(completeWatcher);
                        autocompleteTags.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                autocompleteTags.setText("");
                                Tag t = adapter.getItem(position);
                                tagGrid.addChip(t.getName());
                                selectedTags.add(t);
                                tagGrid.setSelectedChip(chipCount);
                                chipCount++;
                            }
                        });
                        if(filter.getTagIds().size() > 0){
                          restoreTags();
                        } else {
                            tagGrid.setChipListener(new ChipListener() {
                                @Override
                                public void chipSelected(int index) {
                                    filter.addTagId(selectedTags.get(index).getId());
                                    setCounts(tagHeaderCount, R.plurals.tag_count, filter.getTagIds().size(), filter.getTagIds().size());
                                }
                                @Override
                                public void chipDeselected(int index) {
                                    filter.removeTagId(selectedTags.get(index).getId());
                                    setCounts(tagHeaderCount, R.plurals.tag_count, filter.getTagIds().size(), filter.getTagIds().size());
                                }
                            });
                        }
                        hideProgress();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(FilterActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void restoreTags () {
        for (String s: filter.getTagIds()
             ) {
            for (Tag t : tags
                 ) {
                if(s.equalsIgnoreCase(t.getId())){
                    Log.d(TAG, s+":-:"+t.getId());
                    tagGrid.addChip(t.getName());
                    selectedTags.add(t);
                    tagGrid.setSelectedChip(chipCount);
                    chipCount++;
                }
            }
        }
        tagGrid.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int index) {
                filter.addTagId(selectedTags.get(index).getId());
                setCounts(categoryHeaderCount, R.plurals.category_count, filter.getCategoryIds().size(), filter.getCategoryIds().size());
            }
            @Override
            public void chipDeselected(int index) {
                filter.removeTagId(selectedTags.get(index).getId());
                setCounts(categoryHeaderCount, R.plurals.category_count, filter.getCategoryIds().size(), filter.getCategoryIds().size());
            }
        });
        setCounts(tagHeaderCount, R.plurals.tag_count, filter.getTagIds().size(), filter.getTagIds().size());
    }

    @SuppressWarnings("unchecked")
    private void getCategories () {
        APIManager.getFilterManager(getApplicationContext()).getCategories().onSuccess(new Continuation<Category[], Void>() {

            @Override
            public Void then(final Task<Category[]> task) throws Exception {
                final Category[] cats = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        categories = (List<Category>)(Object)ListCombinator.arrayToList(cats);

                        String[] empty = new String[0];
                        new ChipCloud.Configure()
                                .chipCloud(categoryGrid)
                                .labels(empty)
                                .build();

                        for (Category category: cats) {
                            categoryGrid.addChip(category.getName());
                        }

                        if(filter.getCategoryIds().size() > 0){
                            restoreCategories();
                        } else {
                            categoryGrid.setChipListener(new ChipListener() {
                                @Override
                                public void chipSelected(int index) {
                                    selectedCategories.add(categories.get(index));
                                    filter.addCategoryId(categories.get(index).getId());
                                    setCounts(categoryHeaderCount, R.plurals.category_count, filter.getCategoryIds().size(), filter.getCategoryIds().size());
                                }
                                @Override
                                public void chipDeselected(int index) {
                                    selectedCategories.remove(categories.get(index));
                                    filter.removeCategoryId(categories.get(index).getId());
                                    setCounts(categoryHeaderCount, R.plurals.category_count, filter.getCategoryIds().size(), filter.getCategoryIds().size());
                                }
                            });
                        }
                        hideProgress();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(FilterActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void restoreCategories () {
        for (String s: filter.getCategoryIds()
                ) {
            int pos = 0;
            for (Category c : categories
                    ) {
                if(s.equalsIgnoreCase(c.getId())){
                    Log.d(TAG, s+":-:"+c.getId());
                    selectedCategories.add(c);
                    categoryGrid.setSelectedChip(pos);
                }
                pos++;
            }
        }
        categoryGrid.setChipListener(new ChipListener() {
            @Override
            public void chipSelected(int index) {
                selectedCategories.add(categories.get(index));
                filter.addCategoryId(categories.get(index).getId());
                setCounts(categoryHeaderCount, R.plurals.category_count, filter.getCategoryIds().size(), filter.getCategoryIds().size());
            }
            @Override
            public void chipDeselected(int index) {
                selectedCategories.remove(categories.get(index));
                filter.removeCategoryId(categories.get(index).getId());
                setCounts(categoryHeaderCount, R.plurals.category_count, filter.getCategoryIds().size(), filter.getCategoryIds().size());
            }
        });
        setCounts(categoryHeaderCount, R.plurals.category_count, filter.getCategoryIds().size(), filter.getCategoryIds().size());
    }

    @SuppressWarnings("unchecked")
    private void getThemes() {
        APIManager.getFilterManager(getApplicationContext()).getThemes().onSuccess(new Continuation<Theme[], Void>() {

            @Override
            public Void then(final Task<Theme[]> task) throws Exception {
                final Theme[] themeList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        themes = (List<Theme>)(Object)ListCombinator.arrayToList(themeList);
                        ThemeAdapter adapter = new ThemeAdapter(FilterActivity.this, themes);
                        themeRecycler.setAdapter(adapter);
                        hideProgress();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(FilterActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void showProgress () {
        if(loadingLayout != null) {
            loadingLayout.setVisibility(View.VISIBLE);
        }
        if(mainLayout != null){
            mainLayout.setVisibility(View.GONE);
        }
    }

    public void hideProgress () {
        if(loadingLayout != null) {
            loadingLayout.setVisibility(View.GONE);
        }
        if(mainLayout != null){
            mainLayout.setVisibility(View.VISIBLE);
        }
    }

    public FilterCreator getFilter () {
        return filter;
    }

    public void setFilter (FilterCreator filter) {
        this.filter = filter;
    }

    public TextView getThemeCountView () {
        return themeHeaderCount;
    }

    public void setCounts (TextView view, int plurals, int count, int number) {
        if (count <= 0) {
           view.setVisibility(View.INVISIBLE);
        } else {
            view.setVisibility(View.VISIBLE);
            if(view.getId() == R.id.type_filter_header_count && filter.getUsesPlaylist()) {
                view.setText(getResources().getQuantityString(plurals, count+1, number+1));
            } else {
                view.setText(getResources().getQuantityString(plurals, count, number));
            }
        }
    }

}
