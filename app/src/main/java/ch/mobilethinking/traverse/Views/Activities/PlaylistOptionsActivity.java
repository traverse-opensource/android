package ch.mobilethinking.traverse.Views.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.GridPlaylistAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class PlaylistOptionsActivity extends AppCompatActivity {

    private final static String TAG = PlaylistOptionsActivity.class.getSimpleName();
    private Toolbar toolbar;
    private GridView playlistGrid;
    private Playlist playlist;
    private TextView editPlaylist;
    private TextView reorderPlaylist;
    private TextView deletePlaylist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_options);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.options_playlist));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel);

        editPlaylist = (TextView) findViewById(R.id.playlist_parameters);
        reorderPlaylist = (TextView) findViewById(R.id.playlist_order);
        deletePlaylist = (TextView) findViewById(R.id.playlist_delete);

        editPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PreferenceManager.getLogedUser(getBaseContext()) != null && PreferenceManager.getLogedUser(getBaseContext()).getId().equals(playlist.getCreated_by().getId())){
                    Intent i= new Intent(PlaylistOptionsActivity.this, EditPlaylistActivity.class);
                    i.putExtra("playlist", playlist);
                    startActivity(i);
                    finish();
                }
            }
        });

        reorderPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PreferenceManager.getLogedUser(getBaseContext()) != null && PreferenceManager.getLogedUser(getBaseContext()).getId().equals(playlist.getCreated_by().getId())){
                    Intent i= new Intent(PlaylistOptionsActivity.this, PlaylistReorderActivity.class);
                    i.putExtra("playlist", playlist);
                    startActivity(i);
                    finish();
                }
            }
        });

        deletePlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PreferenceManager.getLogedUser(getBaseContext()) != null && PreferenceManager.getLogedUser(getBaseContext()).getId().equals(playlist.getCreated_by().getId())){
                    showDeleteDialog();
                }
            }
        });

        if (getIntent().hasExtra("playlist")){
            playlist = (Playlist) getIntent().getSerializableExtra("playlist");
        }

        playlistGrid = (GridView) findViewById(R.id.playlist_grid);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void deletePlaylist () {
        APIManager.getPlaylistManager(getApplicationContext()).deletePlaylist(playlist.getId()).onSuccess(new Continuation<SimpleResponse, Void>() {

            @Override
            public Void then(final Task<SimpleResponse> task) throws Exception {
                final String message = task.getResult().getMessage();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        Toast.makeText(PlaylistOptionsActivity.this, message, Toast.LENGTH_LONG).show();
                        APIManager.getUserManager(getApplicationContext()).saveUserPlaylists(PreferenceManager.getLogedUser(getApplicationContext()).getId());
                        finish();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(PlaylistOptionsActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    /**
     * Shows a dialog to confirm playlist delete
     */
    private void showDeleteDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.delete_dialog_text));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                deletePlaylist();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}
