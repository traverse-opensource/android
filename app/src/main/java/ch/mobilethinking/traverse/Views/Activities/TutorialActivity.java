package ch.mobilethinking.traverse.Views.Activities;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Views.Fragments.TutorialFiveFragment;
import ch.mobilethinking.traverse.Views.Fragments.TutorialFourFragment;
import ch.mobilethinking.traverse.Views.Fragments.TutorialOneFragment;
import ch.mobilethinking.traverse.Views.Fragments.TutorialThreeFragment;
import ch.mobilethinking.traverse.Views.Fragments.TutorialTwoFragment;

public class TutorialActivity extends AppCompatActivity {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mPager, true);

    }

    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

        private ArrayList<Fragment> slides;

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
            slides = new ArrayList<>();
            slides.add(new TutorialOneFragment());
            slides.add(new TutorialTwoFragment());
            slides.add(new TutorialThreeFragment());
            slides.add(new TutorialFourFragment());
            slides.add(new TutorialFiveFragment());
        }

        @Override
        public Fragment getItem(int position) {
            return slides.get(position);
        }

        @Override
        public int getCount() {
            return slides.size();
        }
    }

}
