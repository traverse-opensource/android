package ch.mobilethinking.traverse.Views.Fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.bumptech.glide.Glide;
import com.github.florent37.fiftyshadesof.FiftyShadesOf;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.DiscoveryHorizontalAdapter;
import ch.mobilethinking.traverse.Adapters.ImageHorizontalAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.LinkSpan;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Utils.URLParamGenerator;
import ch.mobilethinking.traverse.Views.Activities.AddToPlaylistActivity;
import ch.mobilethinking.traverse.Views.Activities.LoginActivity;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.R.attr.fragment;

public class FicheDetailFragment extends BaseFragment {

    public static final String TAG = FicheDetailFragment.class.getSimpleName();
    public static int TAB = R.id.tab_discovery;
    private Fiche fiche;
    private NestedScrollView nestedScroll;
    private CircleImageView contributorImage;
    private TextView contributorName;
    private LinearLayout cardContentLayout;
    private TextView title;
    private HorizontalScrollView scrollHashtags;
    private LinearLayout layoutHashtags;
    public TextView eventTime;
    private TextView description;
    private TextView longDescription;
    private TextView bibliographyTitle;
    private TextView bibliography;
    private TextView readMore;
    private ImageView likeAction;
    private TextView likeNumber;
    private RelativeLayout likeActionLayout;
    private ImageView shareAction;
    private RelativeLayout shareActionLayout;
    private RelativeLayout favouriteActionLayout;
    private ImageView favouriteAction;
    private TextView labelsTitle;
    private ChipCloud labelGrid;
    private TextView linkedFichesTitle;
    private RecyclerView linkedFichesList;
    private RecyclerView imageList;
    private View imagesSeparator;
    private View longDescriptionSeparator;
    private View bibliographySeparator;
    private View labelsSeparator;
    private FiftyShadesOf fiftyShadesOf;
    private boolean liked = false;

    public FicheDetailFragment() {
        // Required empty public constructor
    }

    public static FicheDetailFragment newInstance(Bundle args) {
        FicheDetailFragment fragment = new FicheDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_fiche_detail, container, false);
        nestedScroll = (NestedScrollView) rootView.findViewById(R.id.detail_fiche_nested_scroll);
        contributorImage = (CircleImageView) rootView.findViewById(R.id.contributor_image);
        contributorName = (TextView) rootView.findViewById(R.id.contributor_name);
        cardContentLayout = (LinearLayout) rootView.findViewById(R.id.card_content_layout);
        title = (TextView) rootView.findViewById(R.id.title);
        scrollHashtags = (HorizontalScrollView) rootView.findViewById(R.id.scroll_hashtags);
        layoutHashtags = (LinearLayout) rootView.findViewById(R.id.layout_hashtags);
        eventTime = (TextView) rootView.findViewById(R.id.event_time);
        description = (TextView) rootView.findViewById(R.id.description);
        longDescription = (TextView) rootView.findViewById(R.id.long_description);
        bibliographyTitle = (TextView) rootView.findViewById(R.id.bibliography_title);
        bibliography = (TextView) rootView.findViewById(R.id.bibliography);
        readMore = (TextView) rootView.findViewById(R.id.long_description_read_more);
        likeAction = (ImageView) rootView.findViewById(R.id.action_like_icon);
        likeNumber = (TextView) rootView.findViewById(R.id.action_like_label);
        likeActionLayout = (RelativeLayout) rootView.findViewById(R.id.action_like_layout);
        shareAction = (ImageView) rootView.findViewById(R.id.action_share_icon);
        shareActionLayout = (RelativeLayout) rootView.findViewById(R.id.action_share_layout);
        favouriteActionLayout = (RelativeLayout) rootView.findViewById(R.id.action_favourite_layout);
        favouriteAction = (ImageView) rootView.findViewById(R.id.action_favourite_icon);
        labelsTitle = (TextView) rootView.findViewById(R.id.labels_title);
        labelGrid = (ChipCloud) rootView.findViewById(R.id.labels_grid);
        linkedFichesTitle = (TextView) rootView.findViewById(R.id.linked_fiches_title);
        linkedFichesList = (RecyclerView) rootView.findViewById(R.id.linked_fiches_list);
        imageList = (RecyclerView) rootView.findViewById(R.id.fiche_detail_image_list);
        imagesSeparator = rootView.findViewById(R.id.images_separator);
        longDescriptionSeparator = rootView.findViewById(R.id.long_description_separator);
        bibliographySeparator = rootView.findViewById(R.id.bibliography_separator);
        labelsSeparator = rootView.findViewById(R.id.labels_separator);

        fiche = (Fiche) getArguments().getSerializable("fiche");

        favouriteActionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PreferenceManager.getLogedUser(getActivity()) == null) {
                    showLoginDialog();
                } else {
                    Intent i = new Intent(getActivity(), AddToPlaylistActivity.class);
                    i.putExtra("fiche", fiche);
                    startActivity(i);
                }
            }
        });

        likeActionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PreferenceManager.getLogedUser(getContext()) != null) {
                    if (liked) {
                        dislike();
                        liked = false;
                        likeAction.setImageResource(R.drawable.ic_like);
                    } else {
                        like();
                        liked = true;
                        likeAction.setImageResource(R.drawable.ic_like_selected);
                    }
                } else {
                    showLoginDialog();
                }
            }
        });

        shareActionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
                myIntent.putExtra(Intent.EXTRA_TEXT, URLParamGenerator.generateShareUrl(fiche));
                startActivity(Intent.createChooser(myIntent, getString(R.string.share)));
            }
        });

        fiftyShadesOf = FiftyShadesOf.with(getContext())
                .on(title, layoutHashtags, description, longDescription)
                .start();



        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        getFiche();
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        TAB = tab;
    }

    private void getFiche () {
        APIManager.getFicheManager(getActivity().getApplicationContext()).getFiche(fiche.getId()).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                final Fiche fiche = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# FICHE #################\n";
                        toWrite += fiche.toString();
                        Log.d(TAG, toWrite);

                        setContributorName(fiche.getCreated_by().getName());
                        setContributorImage(fiche.getCreated_by().getProfile().getPicture());
                        String color = fiche.getMainTheme().getHexColor();
                        setTitle(fiche.getName());
                        setLikeNumber(fiche.getUserIds().length);
                        setHashtags(fiche.getTags());
                        if(fiche instanceof FicheEvent) {
                            eventTime.setVisibility(View.VISIBLE);
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSS'Z'", Locale.getDefault());
                            Calendar start = Calendar.getInstance(Locale.getDefault());
                            Calendar end = Calendar.getInstance(Locale.getDefault());
                            try {
                                if(!TextUtils.isEmpty(((FicheEvent) fiche).getStart_date())) {
                                    start.setTime(sdf.parse(((FicheEvent) fiche).getStart_date()));
                                }
                                if (!TextUtils.isEmpty(((FicheEvent) fiche).getEnd_date())) {
                                    end.setTime(sdf.parse(((FicheEvent) fiche).getEnd_date()));
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String startDate = start.get(Calendar.DAY_OF_MONTH) + "/" + (start.get(Calendar.MONTH) + 1) + "/" + start.get(Calendar.YEAR);
                            String endDate = end.get(Calendar.DAY_OF_MONTH) + "/" + (end.get(Calendar.MONTH) + 1) + "/" + end.get(Calendar.YEAR);
                            eventTime.setText(startDate + " - " + endDate);
                        } else {
                            eventTime.setVisibility(View.GONE);
                        }
                        setDescription(fiche);
                        setLongDescription(fiche);
                        setBiblio(fiche.getReferences());
                        setLabelGrid(fiche.getCategories(), fiche.getSubCategories(), color);
                        setLinkedFiches(fiche.getRelatedFiches());
                        setLinkedImages(fiche.getRelatedFiches());
                        fiftyShadesOf.stop();

                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void setContributorName(String name) {
        if(!TextUtils.isEmpty(name)) {
            this.contributorName.setText(name);
        }
    }

    public void setContributorImage(String imageUrl) {
        if(!TextUtils.isEmpty(imageUrl)){
            Glide.with(this).load(imageUrl).dontAnimate().into(contributorImage);
        } else {
            Glide.with(this).load(R.drawable.default_contributor).dontAnimate().into(contributorImage);
        }
    }

    public String getThemeColor(Theme[] themes) {
        if(themes == null || themes.length == 0) {
            return null;
        } else {
            int ponderation = 0;
            Theme finalTheme = new Theme();
            for (Theme t: themes
                    ) {
                if(t.getPonderation() >= ponderation){
                    finalTheme = t;
                }
            }
            return finalTheme.getHexColor();
        }
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }

    public void setHashtags(Tag[] tags) {
        layoutHashtags.removeAllViews();
        if(tags == null || tags.length < 1){
            scrollHashtags.setVisibility(View.GONE);
            layoutHashtags.setVisibility(View.GONE);
        } else {
            for (Tag t : tags
                    ) {
                TextView tv = new TextView(getContext());
                tv.setText(getString(R.string.hashtag, t.getName()));
                tv.setTextColor(getResources().getColor(R.color.colorHashtag));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0,0,16,0);
                tv.setLayoutParams(lp);
                layoutHashtags.addView(tv);
            }
        }
    }

    public void setBiblio(String biblio) {
        if(!TextUtils.isEmpty(biblio)){
            Spannable spannableDescription = new SpannableString(Html.fromHtml(biblio));

            // Replace each URLSpan by a LinkSpan
            URLSpan[] spans = spannableDescription.getSpans(0, spannableDescription.length(), URLSpan.class);
            for (URLSpan urlSpan : spans) {
                Log.d(TAG, urlSpan.getURL());
                LinkSpan linkSpan = new LinkSpan(getActivity(), urlSpan.getURL());
                int spanStart = spannableDescription.getSpanStart(urlSpan);
                int spanEnd = spannableDescription.getSpanEnd(urlSpan);
                spannableDescription.setSpan(linkSpan, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                spannableDescription.removeSpan(urlSpan);
            }

            // Make sure the TextView supports clicking on Links
            bibliography.setMovementMethod(LinkMovementMethod.getInstance());
            bibliography.setText(spannableDescription, TextView.BufferType.SPANNABLE);
        } else {
            bibliographyTitle.setVisibility(View.GONE);
            bibliography.setVisibility(View.GONE);
            bibliographySeparator.setVisibility(View.GONE);
        }
    }

    public void setDescription(Fiche fiche) {
        if(!TextUtils.isEmpty(fiche.getShort_description())) {
            this.description.setText(Html.fromHtml(fiche.getShort_description()));
        } else {
            this.description.setVisibility(View.GONE);
        }
    }

    public void setLongDescription(Fiche fiche) {
        this.readMore.setVisibility(View.VISIBLE);
        String stringDescription = "";
        if(!TextUtils.isEmpty(fiche.getLong_description())) {
            stringDescription = fiche.getLong_description();
        } else {
            this.longDescription.setVisibility(View.GONE);
            this.readMore.setVisibility(View.GONE);
            this.longDescriptionSeparator.setVisibility(View.GONE);
        }

        Spannable spannableDescription = new SpannableString(Html.fromHtml(stringDescription));

        // Replace each URLSpan by a LinkSpan
        URLSpan[] spans = spannableDescription.getSpans(0, spannableDescription.length(), URLSpan.class);
        for (URLSpan urlSpan : spans) {
            Log.d(TAG, urlSpan.getURL());
            LinkSpan linkSpan = new LinkSpan(getActivity(), urlSpan.getURL());
            int spanStart = spannableDescription.getSpanStart(urlSpan);
            int spanEnd = spannableDescription.getSpanEnd(urlSpan);
            spannableDescription.setSpan(linkSpan, spanStart, spanEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            spannableDescription.removeSpan(urlSpan);
        }

        // Make sure the TextView supports clicking on Links
        this.longDescription.setMovementMethod(LinkMovementMethod.getInstance());
        this.longDescription.setText(spannableDescription, TextView.BufferType.SPANNABLE);

        //Hide the excess text if needed, show it on click more
        if(this.longDescription.getText().length() <= 500){
            this.readMore.setVisibility(View.GONE);
        } else if(this.longDescription.getText().length() > 500) {
            this.longDescription.setMaxLines(9);
            this.longDescription.setEllipsize(TextUtils.TruncateAt.END);
            this.readMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    longDescription.setMaxLines(1000);
                    readMore.setVisibility(View.GONE);
                }
            });
        }

    }

    public void setLabelGrid(Category[] categories, String[] subcategories, String color) {
        labelGrid.removeAllViews();
        if(categories != null && categories.length > 0) {
            if(!TextUtils.isEmpty(color)){
                String[] s = new String[0];
                new ChipCloud.Configure()
                        .chipCloud(labelGrid)
                        .deselectedColor(Color.parseColor(color))
                        .deselectedFontColor(getResources().getColor(R.color.colorLightWhite))
                        .selectedFontColor(getResources().getColor(R.color.colorLightWhite))
                        .labels(s)
                        .build();
            }
            for (Category c : categories
                    ) {
                labelGrid.addChip(c.getName());
            }
            if(subcategories != null && subcategories.length > 0) {
                for (String s : subcategories
                        ) {
                    labelGrid.addChip(s);
                }
            }
        } else{
            labelsSeparator.setVisibility(View.GONE);
            labelsTitle.setVisibility(View.GONE);
            labelGrid.setVisibility(View.GONE);
        }
    }

    public void setLikeNumber(int likeNumber) {
        this.likeNumber.setText(String.valueOf(likeNumber));
        if(PreferenceManager.getLogedUser(getActivity().getApplicationContext()) != null && Arrays.asList(fiche.getUserIds()).contains(PreferenceManager.getLogedUser(getContext()).getId())){
            liked = true;
            likeAction.setImageResource(R.drawable.ic_like_selected);
        } else {
            liked = false;
            likeAction.setImageResource(R.drawable.ic_like);
        }
    }

    public void setLinkedFiches (Fiche[] fiches) {
        if(fiches != null && fiches.length > 0) {
            DiscoveryHorizontalAdapter linkedFichesAdapter = new DiscoveryHorizontalAdapter(getActivity(), this, ListCombinator.arrayToList(fiches));
            linkedFichesList.setAdapter(linkedFichesAdapter);
        } else {
            linkedFichesTitle.setVisibility(View.GONE);
            linkedFichesList.setVisibility(View.GONE);
        }
    }

    public void setLinkedImages (Fiche[] fiches) {
        List<Object> images = new ArrayList<>();
        if(fiches != null && fiches.length > 0) {
            for (Fiche f : fiches
                    ) {
                if (f instanceof FicheMedia) {
                    FicheMedia fm = (FicheMedia) f;
                    if(!TextUtils.isEmpty(fm.getCoverPath()) && !fm.getCoverPath().contains("defaultImage") && (fm.getPath() == null || TextUtils.isEmpty(fm.getPath()))) {
                        Log.d(TAG, "Cover path: " + fm.getCoverPath());
                        images.add(fm);
                    }
                }
            }
            if(images.size() > 0) {
                ImageHorizontalAdapter linkedImagesAdapter = new ImageHorizontalAdapter(getActivity(), this, images);
                imageList.setAdapter(linkedImagesAdapter);
            } else {
                imageList.setVisibility(View.GONE);
                imagesSeparator.setVisibility(View.GONE);
            }
        } else {
            imageList.setVisibility(View.GONE);
            imagesSeparator.setVisibility(View.GONE);
        }
    }

    /**
     * Shows a dialog to allow log in
     */
    private void showLoginDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.login_dialog_text));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void like() {
        APIManager.getFicheManager(getActivity().getApplicationContext()).likeFiche(fiche.getId(), PreferenceManager.getLogedUser(getActivity().getApplicationContext()).getId()).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                Fiche f = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        fiche = f;
                        setLikeNumber(f.getUserIds().length);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    public void dislike() {
        APIManager.getFicheManager(getActivity().getApplicationContext()).dislikeFiche(fiche.getId(), PreferenceManager.getLogedUser(getActivity().getApplicationContext()).getId()).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                Fiche f = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        fiche = f;
                        setLikeNumber(f.getUserIds().length);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

}
