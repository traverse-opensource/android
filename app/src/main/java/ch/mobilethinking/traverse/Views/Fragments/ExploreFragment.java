package ch.mobilethinking.traverse.Views.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.DiscoveryVerticalAdapter;
import ch.mobilethinking.traverse.Adapters.ExploreVerticalAdapter;
import ch.mobilethinking.traverse.Adapters.PlaceAutocompleteAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Traverse;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MyClusterItem;
import ch.mobilethinking.traverse.Utils.RecyclerViewScrollListener;
import ch.mobilethinking.traverse.Views.Activities.FilterActivity;
import ch.mobilethinking.traverse.Views.Activities.MainActivity;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class ExploreFragment extends BaseFragment {

    public static final String TAG = ExploreFragment.class.getSimpleName();
    //The tab this fragment is linked to
    public static int TAB = R.id.tab_explore;
    private RecyclerView verticalRecycler;
    private ExploreVerticalAdapter verticalAdapter;
    private CardView locationCard;
    private ImageView deleteSearch;
    private ProgressBar progressSearch;
    private List<MyClusterItem> suggestions;
    private LatLng location;
    private List<Object> adapterItems;
    private RecyclerViewScrollListener scrollListener;
    private ChipCloud activeFilters;

    protected GoogleApiClient mGoogleApiClient;

    private PlaceAutocompleteAdapter mAdapter;

    private AutoCompleteTextView mAutocompleteView;

    //LatLng bounds set to include Switzerland/France covered regions in Traverse
    private static final LatLngBounds BOUNDS = new LatLngBounds(
            new LatLng(45.55252525134013, 5.55908203125), new LatLng(47.36115300722623, 8.404541015625));

    public ExploreFragment() {
        // Required empty public constructor
    }

    public static ExploreFragment newInstance(Bundle args) {
        ExploreFragment fragment = new ExploreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        suggestions = new ArrayList<>();
        mGoogleApiClient = getGoogleApiClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_explore, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);
        loadingLayout = (RelativeLayout) rootView.findViewById(R.id.loading_layout);
        mainLayout = (RelativeLayout) rootView.findViewById(R.id.main_layout);
        activeFilters = (ChipCloud) rootView.findViewById(R.id.filters_grid);
        verticalRecycler = (RecyclerView) rootView.findViewById(R.id.explore_vertical_list);
        locationCard = (CardView) rootView.findViewById(R.id.location_card);
        deleteSearch = (ImageView) rootView.findViewById(R.id.delete_search);
        progressSearch = (ProgressBar) rootView.findViewById(R.id.progress_search);
        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutocompleteView = (AutoCompleteTextView) rootView.findViewById(R.id.autocomplete_places);
        TextWatcher completeWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0){
                    progressSearch.setVisibility(View.VISIBLE);
                } else if (s.length() == 0) {
                    progressSearch.setVisibility(View.GONE);
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    if(inputMethodManager.isActive()) {
                        inputMethodManager.hideSoftInputFromWindow(mAutocompleteView.getWindowToken(), 0);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        mAutocompleteView.addTextChangedListener(completeWatcher);
        // Register a listener that receives callbacks when a suggestion has been selected
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        mAdapter = new PlaceAutocompleteAdapter(getContext(), mGoogleApiClient, BOUNDS,
                null);
        mAutocompleteView.setAdapter(mAdapter);

        ViewCompat.setNestedScrollingEnabled(verticalRecycler, false);

        if(getArguments() != null && getArguments().containsKey("location")){
            location = getArguments().getParcelable("location");
        } else {
            location = getLocation();
        }

        deleteSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mAutocompleteView.getText().length() > 0) {
                    mAutocompleteView.setText("");
                    getResults(ExploreFragment.this, getLocation().latitude, getLocation().longitude, Traverse.getFilter());
                    location = getLocation();
                }
                deleteSearch.setVisibility(View.GONE);
            }
        });

        locationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAutocompleteView.setText("");
                deleteSearch.setVisibility(View.GONE);
                progressSearch.setVisibility(View.GONE);
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                if(inputMethodManager.isActive()) {
                    inputMethodManager.hideSoftInputFromWindow(mAutocompleteView.getWindowToken(), 0);
                }
                getResults(ExploreFragment.this, getLocation().latitude, getLocation().longitude, Traverse.getFilter());
                location = getLocation();
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapterItems = new ArrayList<Object>();
        showProgress();

        //show active filters
        initFilters();

        getResults(this, location.latitude, location.longitude, Traverse.getFilter());
    }

    private void initFilters () {
        activeFilters.removeAllViews();
        activeFilters.setChipListener(null);
        int count = 0;
        final ArrayList<String> filterStatus = new ArrayList<>();
        if(Traverse.getFilter() != null && !Traverse.getFilter().isEmpty()) {
            activeFilters.setVisibility(View.VISIBLE);
            String[] empty = new String[0];
            new ChipCloud.Configure()
                    .chipCloud(activeFilters)
                    .labels(empty)
                    .build();

            if(!Traverse.getFilter().getFicheTypes().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.type_count, Traverse.getFilter().getFicheTypes().size(), Traverse.getFilter().getFicheTypes().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Types");
                count++;
            }
            if(!Traverse.getFilter().getThemeNames().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.theme_count, Traverse.getFilter().getThemeNames().size(), Traverse.getFilter().getThemeNames().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Themes");
                count++;
            }
            if(!Traverse.getFilter().getCategoryIds().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.category_count, Traverse.getFilter().getCategoryIds().size(), Traverse.getFilter().getCategoryIds().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Categories");
                count++;
            }
            if(!Traverse.getFilter().getTagIds().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.tag_count, Traverse.getFilter().getTagIds().size(), Traverse.getFilter().getTagIds().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Tags");
            }
            activeFilters.setChipListener(new ChipListener() {
                @Override
                public void chipSelected(int index) {

                }
                @Override
                public void chipDeselected(int index) {
                    String remove = filterStatus.get(index);
                    FilterCreator f = Traverse.getFilter();
                    switch (remove) {
                        case "Types":
                            f.setFicheTypes(new ArrayList<String>());
                            break;
                        case "Themes":
                            f.setThemeNames(new ArrayList<String>());
                            break;
                        case "Categories":
                            f.setCategoryIds(new ArrayList<String>());
                            break;
                        case "Tags":
                            f.setTagIds(new ArrayList<String>());
                            break;
                        default:
                            break;
                    }
                    if(f.isEmpty()) {
                        Traverse.setFilter(null);
                    } else {
                        Traverse.setFilter(f);
                    }
                    initFilters();
                    showProgress();
                    getResults(ExploreFragment.this, location.latitude, location.longitude, Traverse.getFilter());
                }
            });
        } else {
            activeFilters.setVisibility(View.GONE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_explore_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
                Bundle b = new Bundle();
                b.putSerializable("suggestions", (Serializable) suggestions);
                Log.d(TAG, ""+suggestions.size());
                b.putParcelable("location", (LatLng) location);
                BaseFragment mapFragment = ExploreMapFragment.newInstance(b);
                mapFragment.setTab(getTab());
                switchFragment(mapFragment, ExploreMapFragment.TAG, true);
                return true;
            case R.id.filter:
                Intent i = new Intent(getActivity(), FilterActivity.class);
                startActivity(i);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    private void getResults(final BaseFragment fragment, final Double lat, final Double lng, @Nullable final FilterCreator filter) {

        showProgress();

        APIManager.getFilterManager(getActivity().getApplicationContext()).getPaginatedResultsNoRadius(lat, lng, 1, 10, filter).onSuccess(new Continuation<MixinItem[], Void>() {

            @Override
            public Void then(final Task<MixinItem[]> task) throws Exception {
                final MixinItem[] itemList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "Cancelled onSuccess");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "Faulted onSuccess");
                        String stackTrace = "";
                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }
                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        hideProgress();
                        adapterItems = ListCombinator.arrayToList(itemList);
                        verticalAdapter = new ExploreVerticalAdapter(getActivity(), fragment, adapterItems);
                        verticalRecycler.setAdapter(verticalAdapter);

                        scrollListener = new RecyclerViewScrollListener((LinearLayoutManager)verticalRecycler.getLayoutManager()) {
                            @Override
                            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                                // Triggered only when new data needs to be appended to the list
                                // Add whatever code is needed to append new items to the bottom of the list
                                loadMore(fragment, lat, lng, page, Traverse.getFilter());
                            }
                        };
                        scrollListener.resetState();
                        verticalRecycler.addOnScrollListener(scrollListener);

                        //recover
                        List <Fiche> ficheList = ListCombinator.arrayToList(adapterItems.toArray(), Fiche.class);
                        Fiche[] fiches = ficheList.toArray(new Fiche[ficheList.size()]);


                        suggestions = ListCombinator.generateClusterItems(fiches);
                        if(fiches.length == 0) {
                            Toast.makeText(getContext(), getString(R.string.no_results), Toast.LENGTH_LONG).show();
                        }
                    }
                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {
                if (task.isCancelled()) {
                    Log.d(TAG, "Cancelled on continuation");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Faulted on continuation");
                    Log.d(TAG, task.getError().getMessage());
                }else {
                    Log.d(TAG, "OK on continuation");
                }
                return null;
            }
        });
    }

    private void loadMore(final BaseFragment fragment, Double lat, Double lng, int page, @Nullable FilterCreator filter) {
        APIManager.getFilterManager(getActivity().getApplicationContext()).getPaginatedResultsNoRadius(lat, lng, page, 10, filter).onSuccess(new Continuation<MixinItem[], Void>() {

            @Override
            public Void then(final Task<MixinItem[]> task) throws Exception {
                final MixinItem[] itemList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "Cancelled onSuccess");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "Faulted onSuccess");
                        String stackTrace = "";
                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }
                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        //now the resulting json holds fiches and playlists within the same json array
                        List<Object> newItems = ListCombinator.arrayToList(itemList);
                        adapterItems.addAll(newItems);
                        int currSize = verticalAdapter.getItemCount();
                        verticalAdapter.notifyItemRangeInserted(currSize, adapterItems.size() - 1);

                        List <Fiche> ficheList = ListCombinator.arrayToList(adapterItems.toArray(), Fiche.class);
                        Fiche[] fiches = ficheList.toArray(new Fiche[ficheList.size()]);

                        suggestions = ListCombinator.generateClusterItems(fiches);
                    }
                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {
                if (task.isCancelled()) {
                    Log.d(TAG, "Cancelled on continuation");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Faulted on continuation");
                    Log.wtf(TAG, task.getError().getMessage());
                    Log.d(TAG, task.getError().getMessage());
                }else {
                    Log.d(TAG, "OK on continuation");
                }
                return null;
            }
        });
    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            Log.i(TAG, "Place details received: " + place.getName());
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
            if(inputMethodManager.isActive()) {
                inputMethodManager.hideSoftInputFromWindow(mAutocompleteView.getWindowToken(), 0);
            }
            progressSearch.setVisibility(View.GONE);
            deleteSearch.setVisibility(View.VISIBLE);
            getResults(ExploreFragment.this, place.getLatLng().latitude, place.getLatLng().longitude, Traverse.getFilter());
            location = place.getLatLng();
            places.release();
        }
    };

}
