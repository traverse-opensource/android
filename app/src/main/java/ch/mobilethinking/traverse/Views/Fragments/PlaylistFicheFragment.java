package ch.mobilethinking.traverse.Views.Fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.io.Serializable;
import java.util.HashMap;

import ch.mobilethinking.traverse.Adapters.FicheViewPagerAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Activities.VideoPlayerActivity;
import ch.mobilethinking.traverse.Views.Activities.WebViewActivity;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import de.hdodenhof.circleimageview.CircleImageView;

public class PlaylistFicheFragment extends BaseFragment {

    public static final String TAG = PlaylistFicheFragment.class.getSimpleName();
    //The tab this fragment is linked to
    public static int TAB = R.id.tab_discovery;
    private Fiche fiche;
    private String playlistId;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ProgressBar imageProgress;
    private ImageView videoPlay;
    private ImageView coverImage;
    private View themeLine;
    private CircleImageView themeIconColor;
    private ImageView themeIcon;
    private TextView themeName;

    public PlaylistFicheFragment() {
        // Required empty public constructor
    }

    public static PlaylistFicheFragment newInstance(Bundle args) {
        PlaylistFicheFragment fragment = new PlaylistFicheFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_playlist_fiche, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);
        getActionBar().setHomeButtonEnabled(true);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        fiche = (Fiche) getArguments().getSerializable("fiche");
        playlistId = getArguments().getString("playlistId");

        HashMap<String, Boolean> viewedFiches = PreferenceManager.getViewedFiches(getContext());
        viewedFiches.put(fiche.getId(), true);
        PreferenceManager.setViewedFiches(getContext(), viewedFiches);

        viewPager = (ViewPager) rootView.findViewById(R.id.htab_viewpager);
        tabLayout = (TabLayout) rootView.findViewById(R.id.htab_tabs);

        imageProgress = (ProgressBar) rootView.findViewById(R.id.image_progress);
        videoPlay = (ImageView) rootView.findViewById(R.id.video_play);
        coverImage = (ImageView) rootView.findViewById(R.id.cover_image);
        themeLine = (View) rootView.findViewById(R.id.theme_line);
        themeIconColor = (CircleImageView) rootView.findViewById(R.id.theme_icon_color);
        themeIcon = (ImageView) rootView.findViewById(R.id.theme_icon);
        themeName = (TextView) rootView.findViewById(R.id.theme_name);
        setCoverImage(this, fiche.getCoverPath());
        String color = setThemeSchema(this, fiche.getMainTheme());
        setTypeIcon(this, fiche);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());

                switch (tab.getPosition()) {
                    case 0:
                        break;
                    case 1:
                        break;
                    case 2:
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_playlist_fiche_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
                if(fiche.getLatitude() != null && fiche.getLongitude() != null) {
                    Bundle b = new Bundle();
                    Fiche[] fiches = new Fiche[1];
                    fiches[0] = fiche;
                    b.putSerializable("fiche", (Serializable) ListCombinator.generateClusterItems(fiches));
                    BaseFragment mapFragment = FicheMapFragment.newInstance(b);
                    mapFragment.setTab(getTab());
                    switchFragment(mapFragment, FicheFragment.TAG, true);
                } else {
                    Toast.makeText(getContext(), getString(R.string.no_location_attached), Toast.LENGTH_LONG).show();
                }
                return true;
           /* case R.id.other_actions:
                return true;*/
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        TAB = tab;
    }

    private void setupViewPager(ViewPager viewPager) {
        FicheViewPagerAdapter adapter = new FicheViewPagerAdapter(getChildFragmentManager());
        Bundle b = new Bundle();
        b.putSerializable("fiche", fiche);
        b.putString("playlistId", playlistId);
        PlaylistFicheDetailFragment fdf = PlaylistFicheDetailFragment.newInstance(b);
        fdf.setTab(this.getTab());
        adapter.addFrag(fdf, getString(R.string.tab_fiche_detail));
        Bundle bs = new Bundle();
        bs.putSerializable("fiche", fiche);
        PlaylistFicheSocialFragment fsf = PlaylistFicheSocialFragment.newInstance(bs);
        fsf.setTab(this.getTab());
        adapter.addFrag(fsf, getString(R.string.tab_fiche_social));
        Bundle bp = new Bundle();
        bp.putSerializable("fiche", fiche);
        bp.putString("playlistId", playlistId);
        PlaylistFichePlaylistsFragment fpf = PlaylistFichePlaylistsFragment.newInstance(bp);
        fpf.setTab(this.getTab());
        adapter.addFrag(fpf, getString(R.string.tab_fiche_playlists));
        viewPager.setAdapter(adapter);
    }

    public void setCoverImage(Fragment fragment, Object resource) {
        if(resource != null) {
            Glide.with(fragment).load((String)resource).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).listener(new RequestListener<String, GlideDrawable>() {
                @Override
                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                    imageProgress.setVisibility(View.GONE);
                    return false;
                }

                @Override
                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                    imageProgress.setVisibility(View.GONE);
                    if(fiche instanceof FicheMedia && !TextUtils.isEmpty(((FicheMedia) fiche).getPath())){
                        videoPlay.setVisibility(View.VISIBLE);
                        videoPlay.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent i = new Intent(getActivity(), WebViewActivity.class);
                                i.putExtra("url", ((FicheMedia) fiche).getPath());
                                startActivity(i);
                            }
                        });
                    }
                    return false;
                }
            }).into(this.coverImage);
        } else {
            Glide.with(fragment).load(R.drawable.cover_placeholder).diskCacheStrategy(DiskCacheStrategy.SOURCE).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        }
    }

    public String setThemeSchema(Fragment fragment, Theme theme) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setThemeLine(fragment, null);
            setThemeIconColor(fragment, null);
            setThemeName(fragment, " ");
            return null;
        } else {
            setThemeLine(fragment, theme.getHexColor());
            setThemeIconColor(fragment, theme.getHexColor());
            setThemeName(fragment, theme.getName());
            this.themeName.setTextColor(Color.parseColor(theme.getHexColor()));
            return theme.getHexColor();
        }
    }

    public void setTypeIcon (Fragment fragment, Fiche fiche){
        if (fiche instanceof FichePlace){
            themeIcon.setImageDrawable(fragment.getResources().getDrawable(R.drawable.ic_site));
        } else if (fiche instanceof FichePeople){
            themeIcon.setImageDrawable(fragment.getResources().getDrawable(R.drawable.ic_people));
        } else if (fiche instanceof FicheObject){
            themeIcon.setImageDrawable(fragment.getResources().getDrawable(R.drawable.ic_object));
        } else if (fiche instanceof FicheEvent){
            themeIcon.setImageDrawable(fragment.getResources().getDrawable(R.drawable.ic_events));
        } else if (fiche instanceof FicheMedia){
            themeIcon.setImageDrawable(fragment.getResources().getDrawable(R.drawable.ic_image));
        }
    }

    public void setThemeLine(Fragment fragment, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        }
    }

    public void setThemeIconColor(Fragment fragment, String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIconColor.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
        }
    }

    public void setThemeName(Fragment fragment, String name) {
        if(!TextUtils.isEmpty(name)) {
            this.themeName.setText(name);
        }
    }

}
