package ch.mobilethinking.traverse.Views.Activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ch.mobilethinking.traverse.Adapters.DiaporamaAdapter;
import ch.mobilethinking.traverse.R;

public class DiaporamaActivity extends AppCompatActivity {

    private static final String TAG = DiaporamaActivity.class.getSimpleName();
    private Toolbar toolbar;
    private ImageView grid;
    private TextView progress;
    private ImageView back;
    private ImageView forward;
    private ViewPager mPager;
    private int currentPage = 0;
    private Handler handler;
    private Runnable update;
    private List<Object> images = new ArrayList<Object>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diaporama);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        back = (ImageView) findViewById(R.id.diaporama_back);
        forward = (ImageView) findViewById(R.id.diaporama_forward);
        grid = (ImageView) toolbar.findViewById(R.id.grid);
        progress = (TextView) toolbar.findViewById(R.id.progress);

        images = (List<Object>) getIntent().getSerializableExtra("images");
        currentPage = getIntent().getIntExtra("position", 0);
        Log.d(TAG, "Position: " + currentPage);

        progress.setText(getString(R.string.diaporama_progress, currentPage+1, images.size()));
        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(DiaporamaActivity.this, GridActivity.class);
                i.putExtra("images", (Serializable)images);
                startActivity(i);
                finish();
            }
        });

        mPager = (ViewPager) findViewById(R.id.diaporama_pager);
        mPager.setAdapter(new DiaporamaAdapter(this, images));
        mPager.setCurrentItem(currentPage);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPage = position;
                progress.setText(getString(R.string.diaporama_progress, currentPage+1, images.size()));
                back.setVisibility(View.VISIBLE);
                forward.setVisibility(View.VISIBLE);
                if(position == 0){
                    back.setVisibility(View.GONE);
                }
                if(position == images.size()-1){
                    forward.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // Auto start of viewpager
        handler = new Handler();
        update = new Runnable() {
            public void run() {
                if (currentPage == images.size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };

        //Pass to the next slide after 3 seconds
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(update);
            }
        }, 3000, 3000);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
