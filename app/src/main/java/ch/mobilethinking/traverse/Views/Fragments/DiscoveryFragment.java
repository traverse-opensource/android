package ch.mobilethinking.traverse.Views.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.DiscoveryHorizontalAdapter;
import ch.mobilethinking.traverse.Adapters.DiscoveryVerticalAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.MyClusterItem;
import ch.mobilethinking.traverse.Utils.RecyclerViewScrollListener;
import ch.mobilethinking.traverse.Views.Activities.MainActivity;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class DiscoveryFragment extends BaseFragment {

    public static final String TAG = DiscoveryFragment.class.getSimpleName();
    //The tab this fragment is linked to
    public static int TAB = R.id.tab_discovery;
    private RecyclerView horizontalRecycler;
    private RecyclerView verticalRecycler;
    private DiscoveryHorizontalAdapter horizontalAdapter;
    private DiscoveryVerticalAdapter verticalAdapter;
    private List<MyClusterItem> heartstrokes;
    private List<MyClusterItem> suggestions;
    private List<Object> adapterItems;
    private RecyclerViewScrollListener scrollListener;
    private TextView discoveryEmpty;
    private String ficheId;
    private String playlistId;

    public DiscoveryFragment() {
        // Required empty public constructor
    }

    public static DiscoveryFragment newInstance(Bundle args) {
        DiscoveryFragment fragment = new DiscoveryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        heartstrokes = new ArrayList<>();
        suggestions = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_discovery, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);
        loadingLayout = (RelativeLayout) rootView.findViewById(R.id.loading_layout);
        mainLayout = (AppBarLayout) rootView.findViewById(R.id.main_layout);
        horizontalRecycler = (RecyclerView) rootView.findViewById(R.id.discovery_horizontal_list);
        verticalRecycler = (RecyclerView) rootView.findViewById(R.id.playlists_saved_playlists_list);
        discoveryEmpty = (TextView) rootView.findViewById(R.id.discovery_empty);
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapterItems = new ArrayList<Object>();
        showProgress();
        verticalRecycler.setVisibility(View.GONE);
        discoveryEmpty.setVisibility(View.GONE);
        ficheId = null;
        playlistId = null;
        if(getArguments() != null && getArguments().containsKey("ficheId")) {
            ficheId = getArguments().getString("ficheId");
            getFiche(ficheId);
        } else if(getArguments() != null && getArguments().containsKey("playlistId")) {
            playlistId = getArguments().getString("playlistId");
            getPlaylist(playlistId);
        } else {
            getHeartStroke(this);
            getSuggestions(this, null);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_discovery_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.map:
                Bundle b = new Bundle();
                //TODO set fiches and playlists
                BaseFragment mapFragment = DiscoveryMapFragment.newInstance(b);
                mapFragment.setTab(getTab());
                switchFragment(mapFragment, DiscoveryMapFragment.TAG, true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    private void getFiche (String ficheId) {
        APIManager.getFicheManager(getActivity().getApplicationContext()).getFiche(ficheId).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                final Fiche fiche = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# FICHE #################\n";
                        toWrite += fiche.toString();
                        Log.d(TAG, toWrite);
                        Bundle b = new Bundle();
                        fiche.setRelatedFiches(null);
                        fiche.setLong_description(null);
                        fiche.setCategories(null);
                        fiche.setSubCategories(null);
                        fiche.setTags(null);
                        b.putSerializable("fiche", fiche);
                        b.putBoolean("deeplink", true);
                        BaseFragment ficheFragment = FicheFragment.newInstance(b);
                        ficheFragment.setTab(DiscoveryFragment.TAB);
                        getArguments().remove("ficheId");
                        switchFragment(ficheFragment, FicheFragment.TAG, true);
                    }

                    hideProgress();

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                    hideProgress();
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void getPlaylist (String playlistId) {

        APIManager.getPlaylistManager(getActivity().getApplicationContext()).getPlaylist(playlistId).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                Playlist p = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# PLAYLIST #################\n";
                        toWrite += p.toString();
                        Log.d(TAG, toWrite);
                        Bundle b = new Bundle();
                        LinkedFiche[] lfs = new LinkedFiche[p.getLinkedFiches().length];
                        int i = 0;
                        for (LinkedFiche lf: p.getLinkedFiches()
                             ) {
                            LinkedFiche nlf = new LinkedFiche();
                            nlf.setId(lf.getId());
                            lfs[i] = nlf;
                            i++;
                        }
                        p.setLinkedFiches(lfs);
                        p.setLabels(null);
                        b.putSerializable("playlist", p);
                        b.putBoolean("deeplink", true);
                        BaseFragment playlistFragment = PlaylistFragment.newInstance(b);
                        playlistFragment.setTab(DiscoveryFragment.TAB);
                        getArguments().remove("playlistId");
                        switchFragment(playlistFragment, PlaylistFragment.TAG, true);
                    }

                    hideProgress();

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                    hideProgress();
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void getHeartStroke (final BaseFragment fragment) {
        APIManager.getMixinManager(getActivity().getApplicationContext()).getHeartStrokes().onSuccess(new Continuation<MixinItem[], Void>() {

            @Override
            public Void then(final Task<MixinItem[]> task) throws Exception {
                final MixinItem[] itemList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        List <Fiche> ficheList = ListCombinator.arrayToList(itemList, Fiche.class);
                        Fiche[] fiches = ficheList.toArray(new Fiche[ficheList.size()]);

                        //ListCombinator.arrayToList(itemList)
                        horizontalAdapter = new DiscoveryHorizontalAdapter(getActivity(), fragment, ListCombinator.arrayToList(itemList));
                        //horizontalAdapter = new DiscoveryHorizontalAdapter(getActivity(), fragment, ListCombinator.combineLists(fiches, playlists));
                        horizontalRecycler.setAdapter(horizontalAdapter);
                        heartstrokes = ListCombinator.generateClusterItems(fiches);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().toString());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void getSuggestions(final BaseFragment fragment, @Nullable HashMap<String, ArrayList<Object>> arguments) {
        APIManager.getFilterManager(getActivity().getApplicationContext()).getPaginatedResults(getLocation().latitude, getLocation().longitude, 1, 10, null).onSuccess(new Continuation<MixinItem[], Void>() {

            @Override
            public Void then(final Task<MixinItem[]> task) throws Exception {
                final MixinItem[] itemList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "Cancelled onSuccess");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "Faulted onSuccess");
                        String stackTrace = "";
                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }
                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        hideProgress();
                        if(itemList.length == 0) {
                            discoveryEmpty.setVisibility(View.VISIBLE);
                        } else {
                            verticalRecycler.setVisibility(View.VISIBLE);
                            //now the resulting json holds fiches and playlists within the same json array
                            adapterItems = ListCombinator.arrayToList(itemList);
                            verticalAdapter = new DiscoveryVerticalAdapter(getActivity(), fragment, adapterItems);
                            verticalRecycler.setAdapter(verticalAdapter);

                            scrollListener = new RecyclerViewScrollListener((LinearLayoutManager) verticalRecycler.getLayoutManager()) {
                                @Override
                                public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                                    // Triggered only when new data needs to be appended to the list
                                    // Add whatever code is needed to append new items to the bottom of the list
                                    showLoading();
                                    loadMore(fragment, page, null);
                                }
                            };
                            scrollListener.resetState();
                            verticalRecycler.addOnScrollListener(scrollListener);

                            List<Fiche> ficheList = ListCombinator.arrayToList(adapterItems.toArray(), Fiche.class);
                            Fiche[] fiches = ficheList.toArray(new Fiche[ficheList.size()]);

                            suggestions = ListCombinator.generateClusterItems(fiches);
                        }
                    }
                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {
                if (task.isCancelled()) {
                    Log.d(TAG, "Cancelled on continuation");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().toString());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "OK on continuation");
                }
                return null;
            }
        });
    }

    private void loadMore(final BaseFragment fragment, int page, @Nullable HashMap<String, ArrayList<Object>> arguments) {
        APIManager.getFilterManager(getActivity().getApplicationContext()).getPaginatedResults(getLocation().latitude, getLocation().longitude, page, 10, null).onSuccess(new Continuation<MixinItem[], Void>() {

            @Override
            public Void then(final Task<MixinItem[]> task) throws Exception {
                final MixinItem[] itemList = task.getResult();
                hideLoading();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "Cancelled onSuccess");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "Faulted onSuccess");
                        String stackTrace = "";
                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }
                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {

                        //now the resulting json holds fiches and playlists within the same json array
                        List<Object> newItems = ListCombinator.arrayToList(itemList);
                        adapterItems.addAll(newItems);
                        int currSize = verticalAdapter.getItemCount();
                        verticalAdapter.notifyItemRangeInserted(currSize, adapterItems.size() - 1);

                        List <Fiche> ficheList = ListCombinator.arrayToList(adapterItems.toArray(), Fiche.class);
                        Fiche[] fiches = ficheList.toArray(new Fiche[ficheList.size()]);

                        suggestions = ListCombinator.generateClusterItems(fiches);
                    }
                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {
                if (task.isCancelled()) {
                    Log.d(TAG, "Cancelled on continuation");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Faulted on continuation");
                    Log.wtf(TAG, task.getError().getMessage());
                    Log.d(TAG, task.getError().getMessage());
                }else {
                    Log.d(TAG, "OK on continuation");
                }
                return null;
            }
        });
    }

}
