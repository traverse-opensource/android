package ch.mobilethinking.traverse.Views.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.GridPlaylistAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import retrofit2.Call;

public class AddToPlaylistActivity extends AppCompatActivity {

    private final static String TAG = AddToPlaylistActivity.class.getSimpleName();
    private Toolbar toolbar;
    private GridView playlistGrid;
    private Fiche fiche;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_to_playlist);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.add_to_playlist));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel);

        if (getIntent().hasExtra("fiche")){
            fiche = (Fiche) getIntent().getSerializableExtra("fiche");
        }
        playlistGrid = (GridView) findViewById(R.id.playlist_grid);

        getUserPlaylists();
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void getUserPlaylists () {
        APIManager.getUserManager(getApplicationContext()).getUserPlaylists(PreferenceManager.getLogedUser(getApplicationContext()).getId()).onSuccess(new Continuation<Playlist.PlaylistList, Void>() {

            @Override
            public Void then(final Task<Playlist.PlaylistList> task) throws Exception {
                final Playlist.PlaylistList playlists = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        //Show playlists if any
                        List<Playlist> list = playlists.getPlaylists();
                        list.add(0, new Playlist());
                        final GridPlaylistAdapter adapter = new GridPlaylistAdapter(AddToPlaylistActivity.this, list);
                        playlistGrid.setAdapter(adapter);
                        playlistGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view,
                                                    int position, long id) {
                                if(position == 0) {
                                    Playlist playlist = new Playlist();
                                    LinkedFiche ln = new LinkedFiche();
                                    ln.setFiche(fiche);
                                    LinkedFiche [] lnf = {ln};
                                    playlist.setLinkedFiches(lnf);
                                    playlist.setStatus(0);
                                    Intent i= new Intent(AddToPlaylistActivity.this, EditPlaylistActivity.class);
                                    i.putExtra("playlist", playlist);
                                    startActivity(i);
                                    finish();
                                } else {
                                    boolean alreadyInPlaylist = false;
                                    for (LinkedFiche ln: ((Playlist) adapter.getItem(position)).getLinkedFiches()
                                         ) {
                                       if(ln.getFiche() != null && ln.getFiche().getId().equals(fiche.getId())){
                                           alreadyInPlaylist = true;
                                       }
                                    }
                                    if(alreadyInPlaylist) {
                                        Toast.makeText(getBaseContext(), getString(R.string.already_in_playlist), Toast.LENGTH_SHORT).show();
                                    } else {
                                        addFicheToPlaylist(((Playlist) adapter.getItem(position)).getId());
                                    }
                                }
                            }
                        });
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(AddToPlaylistActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void addFicheToPlaylist (String playlistId) {
        if (!TextUtils.isEmpty(fiche.getId())) {
            APIManager.getPlaylistManager(getApplicationContext()).addLinkedFiche(playlistId, fiche.getId(), null).onSuccess(new Continuation<Playlist, Void>() {

                @Override
                public Void then(final Task<Playlist> task) throws Exception {
                    final Playlist playlist = task.getResult();
                    try {

                        if (task.isCancelled()) {
                            Log.d(TAG, "onSuccess cancelled");
                        } else if (task.isFaulted()) {
                            Log.d(TAG, "onSuccess faulted");
                            String stackTrace = "";

                            for (StackTraceElement trace : task.getError().getStackTrace()) {
                                stackTrace += trace.toString() + "\n";
                            }

                            String toWrite =
                                    String.format(
                                            "%s\n\n%s\n\n%s",
                                            task.getError().getMessage(),
                                            task.getError().getCause(),
                                            stackTrace);
                            Log.d(TAG, toWrite);
                        } else {
                            Toast.makeText(getBaseContext(), getString(R.string.added_to_playlist), Toast.LENGTH_SHORT).show();
                            APIManager.getUserManager(getApplicationContext()).saveUserPlaylists(PreferenceManager.getLogedUser(getApplicationContext()).getId());
                            finish();
                        }

                    } catch (Exception e) {
                        Log.d(TAG, "Exception: " + e.getMessage());
                    }
                    return null;
                }
            }).continueWith(new Continuation<Void, Void>() {
                @Override
                public Void then(final Task<Void> task) throws Exception {

                    if (task.isCancelled()) {
                        Log.d(TAG, "Continuation cancelled");
                    } else if (task.isFaulted()) {
                        Log.d(TAG, "Continuation faulted");
                        Log.d(TAG, "Error: " + task.getError().getMessage());
                        MessageUtil.genericError(AddToPlaylistActivity.this);
                    } else {
                        Log.d(TAG, "Continuation OK");
                    }

                    return null;
                }
            });
        }
    }

}
