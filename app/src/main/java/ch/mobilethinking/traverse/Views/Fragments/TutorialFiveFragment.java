package ch.mobilethinking.traverse.Views.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Activities.MainActivity;
import ch.mobilethinking.traverse.Views.Activities.WelcomeActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TutorialFiveFragment extends Fragment {


    public TutorialFiveFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_tutorial_5, container, false);

        TextView start = rootView.findViewById(R.id.tutorial_end_button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.setTutorialDone(getActivity().getApplicationContext(), true);
                Intent i = new Intent(getActivity().getApplicationContext(), MainActivity.class);
                startActivity(i);
                getActivity().finish();
            }
        });

        return rootView;
    }

}
