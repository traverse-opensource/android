package ch.mobilethinking.traverse.Views.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ch.mobilethinking.traverse.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TutorialThreeFragment extends Fragment {


    public TutorialThreeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_tutorial_3, container, false);
        return rootView;
    }

}
