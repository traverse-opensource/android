package ch.mobilethinking.traverse.Views.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.Helpers.OnStartDragListener;
import ch.mobilethinking.traverse.Adapters.Helpers.SimpleItemTouchHelperCallback;
import ch.mobilethinking.traverse.Adapters.ReorderPlaylistAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class PlaylistReorderActivity extends AppCompatActivity implements OnStartDragListener {

    private final static String TAG = PlaylistReorderActivity.class.getSimpleName();
    private Toolbar toolbar;
    private RecyclerView recyclerFicheList;
    private Playlist playlist;
    private TextView headerTitle;
    private CardView themeCard;
    private ReorderPlaylistAdapter adapter;
    private ItemTouchHelper itemTouchHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reorder_playlist);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.playlist_order));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);

        headerTitle = (TextView) findViewById(R.id.header_title);
        themeCard = (CardView) findViewById(R.id.header_themecard);

        if (getIntent().hasExtra("playlist")){
            playlist = (Playlist) getIntent().getSerializableExtra("playlist");
        }

        recyclerFicheList = (RecyclerView) findViewById(R.id.playlist_fiche_list);
        headerTitle.setText(playlist.getName());
        themeCard.setCardBackgroundColor(Color.parseColor(playlist.getMainTheme().getHexColor()));

        adapter = new ReorderPlaylistAdapter(this, this, playlist.getId(), ListCombinator.arrayToList(playlist.getLinkedFiches(), LinkedFiche.class));
        recyclerFicheList.setAdapter(adapter);

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
        itemTouchHelper = new ItemTouchHelper(callback);
        itemTouchHelper.attachToRecyclerView(recyclerFicheList);

    }


    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        itemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_playlist_reorder_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save:
                swapFiches();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void swapFiches () {
        HashMap<String, String[]> json = new HashMap<>();
        json.put("ficheIds", adapter.getLinkedFiches());
        APIManager.getPlaylistManager(getApplicationContext()).swapFiches(playlist.getId(), new JSONObject(json)).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                playlist = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        APIManager.getUserManager(getApplicationContext()).saveUserPlaylists(PreferenceManager.getLogedUser(getApplicationContext()).getId());
                        Intent i = new Intent(PlaylistReorderActivity.this, PlaylistOptionsActivity.class);
                        i.putExtra("playlist", playlist);
                        startActivity(i);
                        finish();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(PlaylistReorderActivity.this);
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

}
