package ch.mobilethinking.traverse.Views.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.adroitandroid.chipcloud.ChipCloud;
import com.adroitandroid.chipcloud.ChipListener;
import com.bumptech.glide.Glide;
import com.github.florent37.fiftyshadesof.FiftyShadesOf;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.SphericalUtil;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.ExploreVerticalAdapter;
import ch.mobilethinking.traverse.Adapters.PlaceAutocompleteAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Traverse;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.MyClusterItem;
import ch.mobilethinking.traverse.Utils.RecyclerViewScrollListener;
import ch.mobilethinking.traverse.Views.Activities.MainActivity;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import de.hdodenhof.circleimageview.CircleImageView;

public class ExploreMapFragment extends BaseFragment implements OnMapReadyCallback, ClusterManager.OnClusterClickListener<MyClusterItem>, ClusterManager.OnClusterInfoWindowClickListener<MyClusterItem>, ClusterManager.OnClusterItemClickListener<MyClusterItem>, ClusterManager.OnClusterItemInfoWindowClickListener<MyClusterItem> {

    public static final String TAG = ExploreMapFragment.class.getSimpleName();
    //The tab this fragment is linked to
    public static int TAB = R.id.tab_explore;
    private static final int DEFAULT_ZOOM = 10;
    private GoogleMap mMap;
    private ClusterManager<MyClusterItem> clusterManager;
    private List<MyClusterItem> suggestions;
    private ImageView zoomIn;
    private ImageView zoomOut;
    private ImageView mapSearch;
    private ImageView myLocation;
    private CardView infoCard;
    private ImageView coverImage;
    private View themeLine;
    private CircleImageView themeIconColor;
    private ImageView themeIcon;
    private LinearLayout cardContentLayout;
    private TextView title;
    private TextView address;
    private TextView distance;
    private FiftyShadesOf fiftyShadesOf;
    private SupportMapFragment mapFragment;
    private Cluster<MyClusterItem> clickedCluster = null;
    private LinearLayout clusterItemsList;
    private ScrollView clusterItemsScroll;
    private ChipCloud activeFilters;
    private LatLng location;

    protected GoogleApiClient mGoogleApiClient;

    private PlaceAutocompleteAdapter mAdapter;

    private AutoCompleteTextView mAutocompleteView;
    private LinearLayout mAutocompleteLayout;

    private boolean isSearchOpen = false;

    //LatLng bounds set to include Switzerland/France covered regions in Traverse
    private static final LatLngBounds BOUNDS = new LatLngBounds(
            new LatLng(45.55252525134013, 5.55908203125), new LatLng(47.36115300722623, 8.404541015625));

    public ExploreMapFragment() {
        // Required empty public constructor
    }

    public static ExploreMapFragment newInstance(Bundle args) {
        ExploreMapFragment fragment = new ExploreMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        mGoogleApiClient = getGoogleApiClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        loadingLayout = (RelativeLayout) rootView.findViewById(R.id.loading_layout);
        zoomIn = (ImageView)  rootView.findViewById(R.id.zoom_in);
        zoomOut = (ImageView)  rootView.findViewById(R.id.zoom_out);
        mapSearch = (ImageView)  rootView.findViewById(R.id.map_search_button);
        // Retrieve the AutoCompleteTextView that will display Place suggestions.
        mAutocompleteView = (AutoCompleteTextView) rootView.findViewById(R.id.autocomplete_places);
        mAutocompleteLayout = (LinearLayout) rootView.findViewById(R.id.autocomplete_layout);
        mAutocompleteLayout.setVisibility(View.GONE);
        isSearchOpen = false;
        myLocation = (ImageView)  rootView.findViewById(R.id.my_location);
        infoCard = (CardView) rootView.findViewById(R.id.map_info_card);
        coverImage = (ImageView) rootView.findViewById(R.id.cover_image);
        themeLine = (View) rootView.findViewById(R.id.theme_line);
        themeIconColor = (CircleImageView) rootView.findViewById(R.id.theme_icon_color);
        themeIcon = (ImageView) rootView.findViewById(R.id.theme_icon);
        cardContentLayout = (LinearLayout) rootView.findViewById(R.id.card_content_layout);
        title = (TextView) rootView.findViewById(R.id.title);
        address = (TextView) rootView.findViewById(R.id.address);
        distance= (TextView) rootView.findViewById(R.id.distance);
        activeFilters = (ChipCloud) rootView.findViewById(R.id.filters_grid);
        clusterItemsList = (LinearLayout) rootView.findViewById(R.id.cluster_items_list);
        clusterItemsScroll = (ScrollView) rootView.findViewById(R.id.cluster_items_scroll);

        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);

        // Register a listener that receives callbacks when a suggestion has been selected
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);

        mAdapter = new PlaceAutocompleteAdapter(getContext(), mGoogleApiClient, BOUNDS,
                null);
        mAutocompleteView.setAdapter(mAdapter);

        if(getArguments().containsKey("location")) {
            location = (LatLng) getArguments().getParcelable("location");
        } else {
            location = getLocation();
        }

        showProgress();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentByTag("exploreMapFragment");
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(R.id.map_container, mapFragment, "exploreMapFragment");
            ft.commit();
            fm.executePendingTransactions();
        }
        //show active filters
        initFilters();
        getResults(this, location.latitude, location.longitude, Traverse.getFilter());
    }

    private void initFilters () {
        activeFilters.removeAllViews();
        activeFilters.setChipListener(null);
        int count = 0;
        final ArrayList<String> filterStatus = new ArrayList<>();
        if(Traverse.getFilter() != null && !Traverse.getFilter().isEmpty()) {
            activeFilters.setVisibility(View.VISIBLE);
            String[] empty = new String[0];
            new ChipCloud.Configure()
                    .chipCloud(activeFilters)
                    .labels(empty)
                    .build();

            if(!Traverse.getFilter().getFicheTypes().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.type_count, Traverse.getFilter().getFicheTypes().size(), Traverse.getFilter().getFicheTypes().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Types");
                count++;
            }
            if(!Traverse.getFilter().getThemeNames().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.theme_count, Traverse.getFilter().getThemeNames().size(), Traverse.getFilter().getThemeNames().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Themes");
                count++;
            }
            if(!Traverse.getFilter().getCategoryIds().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.category_count, Traverse.getFilter().getCategoryIds().size(), Traverse.getFilter().getCategoryIds().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Categories");
                count++;
            }
            if(!Traverse.getFilter().getTagIds().isEmpty()){
                activeFilters.addChip(getResources().getQuantityString(R.plurals.tag_count, Traverse.getFilter().getTagIds().size(), Traverse.getFilter().getTagIds().size()) +
                        Html.fromHtml("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&#9421;"));
                activeFilters.setSelectedChip(count);
                filterStatus.add("Tags");
            }
            activeFilters.setChipListener(new ChipListener() {
                @Override
                public void chipSelected(int index) {

                }
                @Override
                public void chipDeselected(int index) {
                    String remove = filterStatus.get(index);
                    FilterCreator f = Traverse.getFilter();
                    switch (remove) {
                        case "Types":
                            f.setFicheTypes(new ArrayList<String>());
                            break;
                        case "Themes":
                            f.setThemeNames(new ArrayList<String>());
                            break;
                        case "Categories":
                            f.setCategoryIds(new ArrayList<String>());
                            break;
                        case "Tags":
                            f.setTagIds(new ArrayList<String>());
                            break;
                        default:
                            break;
                    }
                    if(f.isEmpty()) {
                        Traverse.setFilter(null);
                    } else {
                        Traverse.setFilter(f);
                    }
                    initFilters();
                    showProgress();
                    getResults(ExploreMapFragment.this, location.latitude, location.longitude, Traverse.getFilter());
                }
            });
        } else {
            activeFilters.setVisibility(View.GONE);
        }
    }

    private void getResults(final BaseFragment fragment, final Double lat, final Double lng, @Nullable final FilterCreator filter) {

        showProgress();

        APIManager.getFilterManager(getActivity().getApplicationContext()).getAllResults(lat, lng, filter).onSuccess(new Continuation<MixinItem[], Void>() {

            @Override
            public Void then(final Task<MixinItem[]> task) throws Exception {
                final MixinItem[] itemList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "Cancelled onSuccess");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "Faulted onSuccess");
                        String stackTrace = "";
                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }
                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        List <Fiche> ficheList = ListCombinator.arrayToList(itemList, Fiche.class);
                        Fiche[] fiches = ficheList.toArray(new Fiche[ficheList.size()]);
                        suggestions = ListCombinator.generateClusterItems(fiches);
                        if(fiches.length == 0) {
                            Toast.makeText(getContext(), getString(R.string.no_results), Toast.LENGTH_LONG).show();
                        }
                        mapFragment.getMapAsync(ExploreMapFragment.this);
                    }
                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {
                if (task.isCancelled()) {
                    Log.d(TAG, "Cancelled on continuation");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Faulted on continuation");
                    Log.d(TAG, task.getError().getMessage());
                }else {
                    Log.d(TAG, "OK on continuation");
                }
                return null;
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "Map is READY");
        mMap = googleMap;
        mMap.clear();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            } else {
                Log.d(TAG, "Permissions OK");
            }
        }

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);

        //Reposition google map toolbar on top-left
        if (mapFragment != null && mapFragment.getView() != null) {
            View toolbar = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).
                    getParent()).findViewById(Integer.parseInt("4"));
            RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) toolbar.getLayoutParams();
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
            rlp.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
            rlp.setMargins(60, 30, 0, 0);
            Log.d(TAG, "Modified map toolbar");
        }

        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.style_json));

            if (!success) {
                Log.e(TAG, "Style parsing failed.");
            } else {
                Log.d(TAG, "Map styled");
            }
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }

        hideProgress();

        Log.d(TAG, "Progress hidden");

        clusterManager = new ClusterManager<MyClusterItem>(getContext(), mMap);
        clusterManager.setRenderer(new MyClusterRenderer());

        // Point the map's listeners at the listeners implemented by the cluster
        // manager.
        mMap.setOnCameraIdleListener(clusterManager);
        mMap.setOnMarkerClickListener(clusterManager);
        mMap.setOnInfoWindowClickListener(clusterManager);

        clusterManager.setOnClusterClickListener(this);
        clusterManager.setOnClusterInfoWindowClickListener(this);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setOnClusterItemInfoWindowClickListener(this);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(location);

        Log.d(TAG, ""+suggestions.size());

        for (MyClusterItem item: suggestions
                ) {
            if(item != null && item.getPosition() != null) {
                clusterManager.addItem(item);
                builder.include(item.getPosition());
            }
        }

        clusterManager.cluster();
        LatLngBounds bounds = builder.build();

        // Get the last known location and move the camera
        if(suggestions.size() > 0) {
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(
                    bounds, 100));
        } else {
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    location, DEFAULT_ZOOM));
            Toast.makeText(getContext(), getString(R.string.no_results), Toast.LENGTH_LONG).show();
        }

        myLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoCard.setVisibility(View.GONE);
                clusterItemsScroll.setVisibility(View.GONE);
                if(isSearchOpen) {
                    mAutocompleteLayout.setVisibility(View.GONE);
                    mAutocompleteView.setText("");
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    if(inputMethodManager.isActive()) {
                        inputMethodManager.hideSoftInputFromWindow(mAutocompleteView.getWindowToken(), 0);
                    }
                    isSearchOpen = false;
                }
                location = getLocation();
                getSuggestions(ExploreMapFragment.this, location.latitude, location.longitude, Traverse.getFilter());
            }
        });

        zoomIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float currentZoom = mMap.getCameraPosition().zoom;
                mMap.animateCamera(CameraUpdateFactory.zoomTo(currentZoom+1));
            }
        });

        zoomOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float currentZoom = mMap.getCameraPosition().zoom;
                mMap.animateCamera(CameraUpdateFactory.zoomTo(currentZoom-1));
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                infoCard.setVisibility(View.GONE);
                clusterItemsScroll.setVisibility(View.GONE);
                if(isSearchOpen) {
                    mAutocompleteLayout.setVisibility(View.GONE);
                    mAutocompleteView.setText("");
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    if(inputMethodManager.isActive()) {
                        inputMethodManager.hideSoftInputFromWindow(mAutocompleteView.getWindowToken(), 0);
                    }
                    isSearchOpen = false;
                }
            }
        });

        mapSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isSearchOpen) {
                    mAutocompleteLayout.setVisibility(View.VISIBLE);
                    mAutocompleteView.requestFocus();
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInput(mAutocompleteView, 0);
                    isSearchOpen = true;
                } else {
                    mAutocompleteLayout.setVisibility(View.GONE);
                    mAutocompleteView.setText("");
                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    if(inputMethodManager.isActive()) {
                        inputMethodManager.hideSoftInputFromWindow(mAutocompleteView.getWindowToken(), 0);
                    }
                    isSearchOpen = false;
                }
            }
        });

        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                clusterManager.onCameraIdle();
                if(SphericalUtil.computeDistanceBetween(location, mMap.getCameraPosition().target) > 15000) {
                    location = mMap.getCameraPosition().target;
                    getSuggestions(ExploreMapFragment.this, location.latitude, location.longitude, Traverse.getFilter());
                }
            }
        });

    }

    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final AutocompletePrediction item = mAdapter.getItem(position);
            final String placeId = item.getPlaceId();
            final CharSequence primaryText = item.getPrimaryText(null);

            Log.i(TAG, "Autocomplete item selected: " + primaryText);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
             details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Log.i(TAG, "Called getPlaceById to get Place details for " + placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

            // Format details of the place for display and show it in a TextView.
            /*mPlaceDetailsText.setText(formatPlaceDetails(getResources(), place.getName(),
                    place.getId(), place.getAddress(), place.getPhoneNumber(),
                    place.getWebsiteUri()));

            // Display the third party attributions if set.
            final CharSequence thirdPartyAttribution = places.getAttributions();
            if (thirdPartyAttribution == null) {
                mPlaceDetailsAttribution.setVisibility(View.GONE);
            } else {
                mPlaceDetailsAttribution.setVisibility(View.VISIBLE);
                mPlaceDetailsAttribution.setText(Html.fromHtml(thirdPartyAttribution.toString()));
            }*/

            Log.i(TAG, "Place details received: " + place.getName());
            clusterItemsScroll.setVisibility(View.GONE);
            infoCard.setVisibility(View.GONE);
            if(isSearchOpen) {
                InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                if(inputMethodManager.isActive()) {
                    inputMethodManager.hideSoftInputFromWindow(mAutocompleteView.getWindowToken(), 0);
                }
                mAutocompleteLayout.setVisibility(View.GONE);
                mAutocompleteView.setText("");
                isSearchOpen = false;
            }
            getSuggestions(ExploreMapFragment.this, place.getLatLng().latitude, place.getLatLng().longitude, null);
            location = place.getLatLng();
            places.release();
        }
    };

    private void getSuggestions(final BaseFragment fragment, Double lat, Double lng, @Nullable FilterCreator filter) {

        showProgress();

        APIManager.getFilterManager(getActivity().getApplicationContext()).getAllResults(lat, lng, filter).onSuccess(new Continuation<MixinItem[], Void>() {

            @Override
            public Void then(final Task<MixinItem[]> task) throws Exception {
                final MixinItem[] itemList = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "Cancelled onSuccess");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "Faulted onSuccess");
                        String stackTrace = "";
                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }
                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# SUGGESTIONS #################\n################# FICHES #################\n";

                        List <Fiche> ficheList = ListCombinator.arrayToList(itemList, Fiche.class);
                        Fiche[] fiches = ficheList.toArray(new Fiche[ficheList.size()]);
                        //Playlist[] playlists = itemList.getPlalistList();

                        for (Fiche fiche: fiches)
                            toWrite += String.format("Fiche: %s\n\n", fiche.getName());
                        //for (User item: itemList.getUsers())

                        toWrite += "\n\n################# PLAYLISTS #################\n";
                        /*for (Playlist playlist: playlists)
                            toWrite += String.format("Playlist: %s\n\n", playlist.getName());
                        Log.d(TAG, toWrite);*/
                        suggestions = ListCombinator.generateClusterItems(fiches);
                        if(fiches.length == 0) {
                            Toast.makeText(getContext(), getString(R.string.no_results), Toast.LENGTH_LONG).show();
                        }
                        onMapReady(mMap);
                    }
                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {
                if (task.isCancelled()) {
                    Log.d(TAG, "Cancelled on continuation");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Faulted on continuation");
                    Log.d(TAG, task.getError().getMessage());
                }else {
                    Log.d(TAG, "OK on continuation");
                }
                return null;
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_discovery_map_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.list:
                Bundle b = new Bundle();
                b.putParcelable("location", location);
                BaseFragment exploreFragment = ExploreFragment.newInstance(b);
                exploreFragment.setTab(getTab());
                switchFragment(exploreFragment, ExploreFragment.TAG, true);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    @Override
    public boolean onClusterClick(Cluster<MyClusterItem> cluster) {

        clusterItemsScroll.setVisibility(View.VISIBLE);
        infoCard.setVisibility(View.GONE);
        clusterItemsList.removeAllViews();

        int i = 0;
        for (MyClusterItem item: cluster.getItems()
                ) {
            View clusterListItem = getActivity().getLayoutInflater().inflate(R.layout.cluster_items_list_item, clusterItemsList, false);
            TextView title = (TextView) clusterListItem.findViewById(R.id.title);
            TextView address = (TextView) clusterListItem.findViewById(R.id.address);
            TextView distance = (TextView) clusterListItem.findViewById(R.id.distance);
            clusterItemsList.addView(clusterListItem);
            final FiftyShadesOf fiftyShadesOfCluster = FiftyShadesOf.with(getContext())
                    .on(title, address, distance)
                    .start();
            getFicheForCluster(item.getId(), clusterListItem, fiftyShadesOfCluster);
            if(i == 0) {
                focusOnView(clusterItemsScroll, clusterListItem);
            }
            i++;
        }
        return false;
    }

    private final void focusOnView(final ScrollView scroll, final View view) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                int vTop = view.getTop();
                int vBottom = view.getBottom();
                int sHeight = scroll.getHeight();
                scroll.smoothScrollTo(((vTop + vBottom - sHeight) / 2), 0);
            }
        });
    }

    @Override
    public void onClusterInfoWindowClick(Cluster<MyClusterItem> cluster) {

    }

    @Override
    public boolean onClusterItemClick(MyClusterItem item) {

        clusterItemsScroll.setVisibility(View.GONE);
        // Loads the info card for a clicked marker
        fiftyShadesOf = FiftyShadesOf.with(getContext())
                .on(title, address, distance)
                .start();
        infoCard.setVisibility(View.VISIBLE);
        getFiche(item.getId());
        return false;
    }

    @Override
    public void onClusterItemInfoWindowClick(MyClusterItem item) {
        // Does nothing, but you could go into the user's profile page, for example.
    }

    private class MyClusterRenderer extends DefaultClusterRenderer<MyClusterItem> {

        private final IconGenerator iconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final IconGenerator clusterIconGenerator = new IconGenerator(getActivity().getApplicationContext());
        private final ImageView itemImage;

        public MyClusterRenderer() {
            super(getActivity().getApplicationContext(), mMap, clusterManager);
            View clusterView = getActivity().getLayoutInflater().inflate(R.layout.cluster_layout, null);
            FrameLayout clusterFrame = (FrameLayout) clusterView.findViewById(R.id.cluster_frame);
            Drawable background = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_cluster, null);
            clusterFrame.setBackground(background);
            clusterIconGenerator.setContentView(clusterView);

            View itemView = getActivity().getLayoutInflater().inflate(R.layout.fiche_map_marker, null);
            iconGenerator.setContentView(itemView);
            itemImage = (ImageView) itemView.findViewById(R.id.type_icon);

        }

        @Override
        protected void onBeforeClusterItemRendered(MyClusterItem item, MarkerOptions markerOptions) {
            Drawable background = ContextCompat.getDrawable(getContext(), R.drawable.fiche_map_marker_shape);
            Log.d("itemcolor", item.getThemeColor());
            if(!TextUtils.isEmpty(item.getThemeColor())) {
                background.setColorFilter(Color.parseColor(item.getThemeColor()), PorterDuff.Mode.SRC_OVER);
            }
            iconGenerator.setBackground(
                    background);
            Log.d("itemicon", "Icon: " + item.getIcon());
            itemImage.setImageDrawable(getResources().getDrawable(item.getIcon()));
            Bitmap icon = iconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected void onBeforeClusterRendered(Cluster<MyClusterItem> cluster, MarkerOptions markerOptions) {
            clusterIconGenerator.setBackground(
                    ContextCompat.getDrawable(getContext(), R.drawable.ic_cluster));
            int size = cluster.getSize();
            Bitmap icon = clusterIconGenerator.makeIcon(String.valueOf(size));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            // Always render clusters.
            return cluster.getSize() > 1;
        }
    }

    /*private class CustomClusterInfoWindow implements GoogleMap.InfoWindowAdapter {

        private final View clusterInfoWindow;

        CustomClusterInfoWindow() {
            clusterInfoWindow = getActivity().getLayoutInflater().inflate(
                    R.layout.cluster_info_window, null);
            Drawable background = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_cluster, null);
            background.setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            clusterInfoWindow.setBackground(background);
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            // TODO Auto-generated method stub

            TextView title = ((TextView) clusterInfoWindow
                    .findViewById(R.id.cluster_title));

            if (clickedCluster != null) {
               title.setText(getString(R.string.cluster_items, clickedCluster.getItems().size()));
            }
            return clusterInfoWindow;
        }
    }*/

    private void getFiche (String id) {
        APIManager.getFicheManager(getActivity().getApplicationContext()).getFiche(id).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                final Fiche fiche = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# FICHE #################\n";
                        toWrite += fiche.toString();
                        Log.d(TAG, toWrite);

                        fiftyShadesOf.stop();

                        setCoverImage(fiche.getCoverPath());
                        setThemeSchema(fiche.getMainTheme());
                        setTypeIcon(fiche);
                        setTitle(fiche.getName());
                        setDescription(fiche);
                        setDistance(new LatLng(fiche.getLatitude(), fiche.getLongitude()), ((MainActivity)getActivity()).getLastLocation());
                        infoCard.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle b = new Bundle();
                                fiche.setRelatedFiches(null);
                                fiche.setLong_description(null);
                                fiche.setCategories(null);
                                fiche.setTags(null);
                                fiche.setSubCategories(null);
                                b.putSerializable("fiche", fiche);
                                BaseFragment ficheFragment = FicheFragment.newInstance(b);
                                ficheFragment.setTab(getTab());
                                switchFragment(ficheFragment, FicheFragment.TAG, true);
                            }
                        });

                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void getFicheForCluster (String id, final View v, final FiftyShadesOf fiftyShadesOfCluster) {
        APIManager.getFicheManager(getActivity().getApplicationContext()).getFiche(id).onSuccess(new Continuation<Fiche, Void>() {

            @Override
            public Void then(final Task<Fiche> task) throws Exception {
                final Fiche fiche = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# FICHE #################\n";
                        toWrite += fiche.toString();
                        Log.d(TAG, toWrite);

                        fiftyShadesOfCluster.stop();

                        setClusterCoverImage(fiche.getCoverPath(), v);
                        setClusterThemeSchema(fiche.getMainTheme(), v);
                        setClusterTypeIcon(fiche, v);
                        setClusterTitle(fiche.getName(), v);
                        setClusterDescription(fiche, v);
                        setClusterDistance(new LatLng(fiche.getLatitude(), fiche.getLongitude()), ((MainActivity)getActivity()).getLastLocation(), v);
                        v.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Bundle b = new Bundle();
                                fiche.setRelatedFiches(null);
                                fiche.setLong_description(null);
                                fiche.setCategories(null);
                                fiche.setTags(null);
                                fiche.setSubCategories(null);
                                b.putSerializable("fiche", fiche);
                                BaseFragment ficheFragment = FicheFragment.newInstance(b);
                                ficheFragment.setTab(getTab());
                                switchFragment(ficheFragment, FicheFragment.TAG, true);
                            }
                        });

                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    //Methods for single fiche
    public void setCoverImage(Object resource) {
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Log.d("Image URL", imageUrl);
            Glide.with(this).load(imageUrl).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        } else {
            Glide.with(this).load(R.drawable.cover_placeholder).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(this.coverImage);
        }
    }

    public void setThemeLine(String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            this.themeLine.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    public void setThemeIconColor(String color) {
        if(!TextUtils.isEmpty(color)) {
            this.themeIconColor.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
        } else {
            this.themeIconColor.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));
        }
    }

    public void setThemeSchema(Theme theme) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setThemeLine(null);
            setThemeIconColor(null);
        } else {
            setThemeLine(theme.getHexColor());
            setThemeIconColor(theme.getHexColor());
        }
    }

    public void setTypeIcon (Fiche fiche){
        if (fiche instanceof FichePlace){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_site));
        } else if (fiche instanceof FichePeople){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_people));
        } else if (fiche instanceof FicheObject){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_object));
        } else if (fiche instanceof FicheEvent){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_events));
        } else if (fiche instanceof FicheMedia){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
        }
    }

    public void setTitle(String title) {
        if(!TextUtils.isEmpty(title)) {
            this.title.setText(title);
        }
    }


    public void setDescription(Fiche fiche) {
        if(!TextUtils.isEmpty(fiche.getShort_description())) {
            this.address.setText(Html.fromHtml(fiche.getShort_description()));
        } else {
            this.address.setVisibility(View.GONE);
        }
    }

    public void setAddress(String address) {

        if(!TextUtils.isEmpty(address)) {
            this.address.setText(address);
        }
    }

    public void setDistance(LatLng fiche, LatLng me) {
        double distance = SphericalUtil.computeDistanceBetween(fiche, me);
        if(!TextUtils.isEmpty(String.valueOf(distance))) {
            this.distance.setText("À " + String.format(Locale.getDefault(), "%.2f", distance/1000) + " km");
        }
    }

    //Methods for cluster lists
    public void setClusterCoverImage(Object resource, final View v) {
        ImageView coverImage = (ImageView) v.findViewById(R.id.cover_image);
        if(resource != null && !((String)resource).contains("defaultImage")) {
            String imageUrl = "";
            if(((String)resource).split("http").length > 2){
                imageUrl = "http" + ((String)resource).split("http")[2];
            } else {
                imageUrl = ((String)resource);
            }
            Log.d("Image URL", imageUrl);
            Glide.with(this).load(imageUrl).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(coverImage);
        } else {
            Glide.with(this).load(R.drawable.cover_placeholder).placeholder(R.drawable.cover_placeholder).error(R.drawable.cover_placeholder).into(coverImage);
        }
    }

    public void setClusterThemeLine(String color, final View v) {
        View themeLine = v.findViewById(R.id.theme_line);
        if(!TextUtils.isEmpty(color)) {
            themeLine.setBackgroundColor(Color.parseColor(color));
        } else {
            themeLine.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        }
    }

    public void setClusterThemeIconColor(String color, final View v) {
        CircleImageView themeIconColor = (CircleImageView) v.findViewById(R.id.theme_icon_color);
        if(!TextUtils.isEmpty(color)) {
            themeIconColor.setImageDrawable(new ColorDrawable(Color.parseColor(color)));
        } else {
            themeIconColor.setImageDrawable(new ColorDrawable(getResources().getColor(R.color.colorAccent)));
        }
    }

    public void setClusterThemeSchema(Theme theme, final View v) {
        if(theme.getName().equalsIgnoreCase("default")) {
            setClusterThemeLine(null, v);
            setClusterThemeIconColor(null, v);
        } else {
            setClusterThemeLine(theme.getHexColor(), v);
            setClusterThemeIconColor(theme.getHexColor(), v);
        }
    }

    public void setClusterTypeIcon (Fiche fiche, final View v){
        ImageView themeIcon = (ImageView) v.findViewById(R.id.theme_icon);
        if (fiche instanceof FichePlace){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_site));
        } else if (fiche instanceof FichePeople){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_people));
        } else if (fiche instanceof FicheObject){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_object));
        } else if (fiche instanceof FicheEvent){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_events));
        } else if (fiche instanceof FicheMedia){
            themeIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_image));
        }
    }

    public void setClusterTitle(String title, final View v) {
        TextView clusterTitle = (TextView) v.findViewById(R.id.title);
        if(!TextUtils.isEmpty(title)) {
            clusterTitle.setText(title);
        }
    }

    public void setClusterDescription(Fiche fiche, final View v) {
        TextView clusterDescription = (TextView) v.findViewById(R.id.address);
        if(!TextUtils.isEmpty(fiche.getShort_description())) {
            clusterDescription.setText(Html.fromHtml(fiche.getShort_description()));
        } else {
            clusterDescription.setVisibility(View.GONE);
        }
    }

    public void setClusterAddress(String address, final View v) {
        TextView clusterAddress = (TextView) v.findViewById(R.id.address);
        if(!TextUtils.isEmpty(address)) {
            clusterAddress.setText(address);
        }
    }

    public void setClusterDistance(LatLng fiche, LatLng me, final View v) {
        TextView clusterDistance = (TextView) v.findViewById(R.id.distance);
        double distance = SphericalUtil.computeDistanceBetween(fiche, me);
        if(!TextUtils.isEmpty(String.valueOf(distance))) {
            clusterDistance.setText("À " + String.format(Locale.getDefault(), "%.2f", distance/1000) + " km");
        }
    }

}
