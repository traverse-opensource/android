package ch.mobilethinking.traverse.Views.Fragments;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.SocialFeedAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.ListCombinator;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Activities.LoginActivity;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.SocialFeed;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class FicheSocialFragment extends BaseFragment {

    public static final String TAG = FicheSocialFragment.class.getSimpleName();
    public static int TAB = R.id.tab_discovery;
    static final int REQUEST_IMAGE_CAPTURE = 6969;
    private Fiche fiche;
    private RecyclerView ficheSocialFeed;
    private SocialFeedAdapter sfa;
    private List<SocialFeed> ficheFeed = new ArrayList<>();;
    private TextView noFeed;
    private TextView socialHashtag;
    private ImageView socialCamera;
    private String photoPath;
    private Uri photoUri;
    private File photoFile;
    private ProgressBar feedProgress;


    public FicheSocialFragment() {
        // Required empty public constructor
    }

    public static FicheSocialFragment newInstance(Bundle args) {
        FicheSocialFragment fragment = new FicheSocialFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_fiche_social, container, false);

        fiche = (Fiche) getArguments().getSerializable("fiche");

        ficheSocialFeed = (RecyclerView) rootView.findViewById(R.id.fiche_social_feed);
        noFeed = (TextView) rootView.findViewById(R.id.text_no_social_feed);
        feedProgress = (ProgressBar) rootView.findViewById(R.id.feed_progress);

        socialHashtag = (TextView) rootView.findViewById(R.id.social_hashtag);
        socialHashtag.setText(fiche.getSlug());
        socialHashtag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                myIntent.setType("text/plain");
                myIntent.putExtra(Intent.EXTRA_TITLE, getString(R.string.app_name));
                myIntent.putExtra(Intent.EXTRA_TEXT, fiche.getSlug());
                startActivity(Intent.createChooser(myIntent, getString(R.string.share)));
                copyToClipboard(fiche.getSlug());
            }
        });

        socialCamera = (ImageView) rootView.findViewById(R.id.social_camera);
        if(!getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            socialCamera.setClickable(false);
            socialCamera.setAlpha(0.6f);
        }

        socialCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(PreferenceManager.getLogedUser(getActivity().getApplicationContext()) != null) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                        // Create the File where the photo should go
                        photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            photoUri = FileProvider.getUriForFile(getActivity(),
                                    "com.example.android.fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                            Intent i = new Intent(Intent.ACTION_PICK);
                            // i.addCategory(Intent.CATEGORY_OPENABLE);
                            i.setType("image/*");

                            // Create file chooser intent
                            Intent chooserIntent = Intent.createChooser(i, getString(R.string.share));

                            // Set camera intent to file chooser
                            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS
                                    , new Parcelable[] { takePictureIntent });
                            startActivityForResult(chooserIntent, REQUEST_IMAGE_CAPTURE);
                        }
                    } else {
                        // Create the File where the photo should go
                        photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            ex.printStackTrace();
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            photoUri = FileProvider.getUriForFile(getActivity(),
                                    "com.example.android.fileprovider",
                                    photoFile);
                            Intent i = new Intent(Intent.ACTION_PICK);
                            // i.addCategory(Intent.CATEGORY_OPENABLE);
                            i.setType("image/*");

                            // Create file chooser intent
                            Intent chooserIntent = Intent.createChooser(i, getString(R.string.share));
                            startActivityForResult(chooserIntent, REQUEST_IMAGE_CAPTURE);
                        }
                    }
                } else {
                    showLoginDialog();
                }
            }
        });

        sfa = new SocialFeedAdapter(getActivity(), ficheFeed);
        getSocialFeed(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    private void getSocialFeed (final BaseFragment fragment) {
        feedProgress.setVisibility(View.VISIBLE);
        APIManager.getFicheManager(getActivity().getApplicationContext()).getFicheFeed(fiche.getId()).onSuccess(new Continuation<SocialFeed[], Void>() {

            @Override
            public Void then(final Task<SocialFeed[]> task) throws Exception {
                final SocialFeed[] feed = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# FEED #################\n";
                        for (SocialFeed f: feed
                                ) {
                            toWrite += f.toString();
                        }
                        Log.d(TAG, toWrite);

                        if(feed.length > 0) {
                            ficheSocialFeed.setVisibility(View.VISIBLE);
                            noFeed.setVisibility(View.GONE);
                            ficheFeed = ListCombinator.arrayToList(feed, SocialFeed.class);
                            sfa = new SocialFeedAdapter(getActivity(), ficheFeed);
                            ficheSocialFeed.setAdapter(sfa);
                            sfa.notifyDataSetChanged();
                        } else {
                            ficheSocialFeed.setVisibility(View.GONE);
                            noFeed.setVisibility(View.VISIBLE);
                        }

                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                feedProgress.setVisibility(View.GONE);
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    //MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void addFeed (final BaseFragment fragment, JSONObject params, File imageFile) {
        feedProgress.setVisibility(View.VISIBLE);
        APIManager.getFicheManager(getActivity().getApplicationContext()).addFeed(fiche.getId(), params, imageFile).onSuccess(new Continuation<SocialFeed, Void>() {

            @Override
            public Void then(final Task<SocialFeed> task) throws Exception {
                final SocialFeed feed = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        String toWrite = "################# INDIVIDUAL FEED #################\n";
                        toWrite += feed.toString();
                        Log.d(TAG, toWrite);
                        ficheFeed.add(0, feed);
                        sfa = new SocialFeedAdapter(getActivity(), ficheFeed);
                        ficheSocialFeed.setAdapter(sfa);
                        sfa.notifyDataSetChanged();
                        ficheSocialFeed.setVisibility(View.VISIBLE);
                        noFeed.setVisibility(View.GONE);
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                feedProgress.setVisibility(View.GONE);
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    //MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //Old code to share with app picker
        /*if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
        Log.d("INTENT_DATA", "Data is: " + (data != null ? data.toString() + " Action is: " + data.getAction() : "Data is NULL"));
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("image/jpeg");
            myIntent.putExtra(Intent.EXTRA_STREAM, photoUri);
            startActivity(Intent.createChooser(myIntent, getString(R.string.share)));
            copyToClipboard(fiche.getSlug());
        }*/

        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                final boolean isCamera;
                if (data == null) {
                    isCamera = true;
                } else {
                    final String action = data.getAction();
                    if (action == null) {
                        isCamera = false;
                    } else {
                        isCamera = action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
                    }
                }

                if (isCamera) {
                    //Bitmap factory
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image as it throws OutOfMemory Exception for larger
                    // images
                    options.inSampleSize = 8;
                    final Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getPath(), options);
                    showCommentDialog(bitmap);
                } else {
                    Uri selectedImageUri = data == null ? null : data.getData();
                    Log.d("ImageURI", selectedImageUri.getLastPathSegment());
                    // /Bitmap factory
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image as it throws OutOfMemory Exception for larger
                    // images
                    options.inSampleSize = 8;
                    try {
                        InputStream input = getActivity().getContentResolver().openInputStream(selectedImageUri);
                        final Bitmap bitmap = BitmapFactory.decodeStream(input);
                        InputStream in = getActivity().getContentResolver().openInputStream(selectedImageUri);
                        OutputStream out = new FileOutputStream(photoFile);
                        byte[] buf = new byte[1024];
                        int len;
                        while((len=in.read(buf))>0){
                            out.write(buf,0,len);
                        }
                        out.close();
                        in.close();
                        showCommentDialog(bitmap);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException ie){
                        ie.printStackTrace();
                    }
                }
            }
        } else if (resultCode == RESULT_CANCELED){
            // user cancelled Image capture
            Toast.makeText(getActivity().getApplicationContext(),
                    "Partage d'image annulé", Toast.LENGTH_SHORT)
                    .show();
        } else {
            // failed to capture image
           MessageUtil.genericError(getActivity());
        }

    }

    /**
     * Shows a dialog to allow log in
     */
    private void showLoginDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.login_dialog_text));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    private void  showCommentDialog(Bitmap bitmap) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getContext());
        LayoutInflater inflater = this.getLayoutInflater();
        View dialog_layout = inflater.inflate(R.layout.dialog_feed_text_input, null);
        builder.setView(dialog_layout);
        final EditText comment = (EditText) dialog_layout.findViewById(R.id.edit_comment);
        ImageView preview = (ImageView) dialog_layout.findViewById(R.id.preview_image);
        preview.setImageBitmap(bitmap);
        comment.setError(null);
        comment.append(fiche.getSlug() + " ");
        builder.setPositiveButton(R.string.publish, null);
        builder.setNegativeButton(R.string.cancel, null);
        final AlertDialog d = builder.create();
        d.show();
        final Button publish = d.getButton(AlertDialog.BUTTON_POSITIVE);
        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(comment.getText().length() <= 200) {
                    JSONObject params = new JSONObject();
                    try {
                        params.put("description", comment.getText().toString());
                        params.put("created_by", PreferenceManager.getLogedUser(getActivity().getApplicationContext()).getId());
                        Log.d(TAG, params.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.d(TAG, params.toString());
                    addFeed(FicheSocialFragment.this, params, photoFile);
                    d.dismiss();
                } else {
                   Toast.makeText(getContext(), getString(R.string.text_too_long), Toast.LENGTH_LONG).show();
                }
            }
        });
        Button cancel = d.getButton(AlertDialog.BUTTON_NEGATIVE);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        photoPath = image.getAbsolutePath();
        return image;
    }

    private void copyToClipboard (String s) {
        ClipboardManager clipboard = (ClipboardManager)
                getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("hashtag", s);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(getContext(), getString(R.string.hashtag_copied), Toast.LENGTH_LONG).show();
    }

}
