package ch.mobilethinking.traverse.Views.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Adapters.MyPlaylistsHorizontalAdapter;
import ch.mobilethinking.traverse.Adapters.SavedPlaylistsHorizontalAdapter;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Activities.LoginActivity;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class PlaylistsFragment extends BaseFragment {

    public static final String TAG = PlaylistsFragment.class.getSimpleName();
    //The tab this fragment is linked to
    public static int TAB = R.id.tab_playlists;
    private RecyclerView horizontalRecyclerMyPlaylists;
    private RecyclerView horizontalRecyclerSavedPlaylists;
    private MyPlaylistsHorizontalAdapter myPlaylistsHorizontalAdapter;
    private SavedPlaylistsHorizontalAdapter savedPlaylistsHorizontalAdapter;
    private ArrayList<Playlist> savedPlaylists;
    private ArrayList<Playlist> myPlaylists;
    private TextView loginText;
    private TextView loginButton;
    private TextView noPlaylists;
    private int savedPlaylist = 0;

    public PlaylistsFragment() {
        // Required empty public constructor
    }

    public static PlaylistsFragment newInstance(Bundle args) {
        PlaylistsFragment fragment = new PlaylistsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_playlists, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);
        loadingLayout = (RelativeLayout) rootView.findViewById(R.id.loading_layout);
        mainLayout = (ScrollView) rootView.findViewById(R.id.main_layout);
        horizontalRecyclerMyPlaylists = (RecyclerView) rootView.findViewById(R.id.playlists_myplaylists_list);
        horizontalRecyclerSavedPlaylists = (RecyclerView) rootView.findViewById(R.id.playlists_saved_playlists_list);
        loginText = (TextView) rootView.findViewById(R.id.login_text);
        loginButton = (TextView) rootView.findViewById(R.id.login_button);
        noPlaylists = (TextView) rootView.findViewById(R.id.playlists_empty);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), LoginActivity.class);
                startActivity(i);
            }
        });
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        myPlaylists = new ArrayList<>();
        savedPlaylists = new ArrayList<>();
        savedPlaylistsHorizontalAdapter = new SavedPlaylistsHorizontalAdapter(getActivity(), this, savedPlaylists);
        horizontalRecyclerSavedPlaylists.setAdapter(savedPlaylistsHorizontalAdapter);
        HashMap<String,Boolean> uPlaylists = PreferenceManager.getSavedPlaylists(getContext());
        if(PreferenceManager.getLogedUser(getContext()) != null) {
            //APIManager.getUserManager(getActivity().getApplicationContext()).saveUserPlaylists(PreferenceManager.getLogedUser(getActivity().getApplicationContext()).getId());
            horizontalRecyclerMyPlaylists.setVisibility(View.VISIBLE);
            loginText.setVisibility(View.GONE);
            loginButton.setVisibility(View.GONE);
            showProgress();
            getUserPlaylists();
        } else {
            horizontalRecyclerMyPlaylists.setVisibility(View.GONE);
            loginText.setVisibility(View.VISIBLE);
            loginButton.setVisibility(View.VISIBLE);
        }
        if(uPlaylists.isEmpty()){
            Toast.makeText(getContext(), getString(R.string.no_saved_playlists), Toast.LENGTH_LONG).show();
        } else {
            //APIManager.getPlaylistManager(getActivity().getApplicationContext()).saveFavouritePlaylists();
            showProgress();
            for (String s: uPlaylists.keySet()
                    ) {
                getPlaylist(s);
            }
        }
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    private void getUserPlaylists () {
        APIManager.getUserManager(getActivity().getApplicationContext()).getUserPlaylists(PreferenceManager.getLogedUser(getActivity().getApplicationContext()).getId()).onSuccess(new Continuation<Playlist.PlaylistList, Void>() {

            @Override
            public Void then(final Task<Playlist.PlaylistList> task) throws Exception {
                final Playlist.PlaylistList playlists = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        //Show playlists if any
                        List<Playlist> list = playlists.getPlaylists();
                        if(list.isEmpty()){
                            horizontalRecyclerMyPlaylists.setVisibility(View.GONE);
                            noPlaylists.setVisibility(View.VISIBLE);
                        } else {
                            horizontalRecyclerMyPlaylists.setVisibility(View.VISIBLE);
                            noPlaylists.setVisibility(View.GONE);
                            myPlaylists.addAll(list);
                            myPlaylistsHorizontalAdapter = new MyPlaylistsHorizontalAdapter(getActivity(), PlaylistsFragment.this, myPlaylists);
                            horizontalRecyclerMyPlaylists.setAdapter(myPlaylistsHorizontalAdapter);
                        }
                        hideProgress();
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

    private void getPlaylist (String playlistId) {
        APIManager.getPlaylistManager(getActivity().getApplicationContext()).getPlaylist(playlistId).onSuccess(new Continuation<Playlist, Void>() {

            @Override
            public Void then(final Task<Playlist> task) throws Exception {
                final Playlist p = task.getResult();
                try{

                    if (task.isCancelled()){
                        Log.d(TAG, "onSuccess cancelled");
                    }else if (task.isFaulted()){
                        Log.d(TAG, "onSuccess faulted");
                        String stackTrace = "";

                        for (StackTraceElement trace: task.getError().getStackTrace()){
                            stackTrace +=  trace.toString() + "\n";
                        }

                        String toWrite =
                                String.format(
                                        "%s\n\n%s\n\n%s",
                                        task.getError().getMessage(),
                                        task.getError().getCause(),
                                        stackTrace);
                        Log.d(TAG, toWrite);
                    }else {
                        savedPlaylists.add(p);
                        savedPlaylistsHorizontalAdapter.notifyDataSetChanged();
                        savedPlaylist++;
                        if(savedPlaylist >= savedPlaylists.size() - 1){
                            hideProgress();
                        }
                    }

                }catch(Exception e){
                    Log.d(TAG, "Exception: " + e.getMessage());
                }
                return null;
            }
        }).continueWith(new Continuation<Void, Void>() {
            @Override
            public Void then(final Task<Void> task) throws Exception {

                if (task.isCancelled()) {
                    Log.d(TAG, "Continuation cancelled");
                }else if (task.isFaulted()) {
                    Log.d(TAG, "Continuation faulted");
                    Log.d(TAG, "Error: " + task.getError().getMessage());
                    MessageUtil.genericError(getActivity());
                }else {
                    Log.d(TAG, "Continuation OK");
                }

                return null;
            }
        });
    }

}
