package ch.mobilethinking.traverse.Views.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Traverse;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Fragments.BaseFragment;
import ch.mobilethinking.traverse.Views.Fragments.DiscoveryFragment;
import ch.mobilethinking.traverse.Views.Fragments.ExploreFragment;
import ch.mobilethinking.traverse.Views.Fragments.FicheFragment;
import ch.mobilethinking.traverse.Views.Fragments.PlaylistFragment;
import ch.mobilethinking.traverse.Views.Fragments.PlaylistsFragment;
import ch.mobilethinking.traverse.Views.Fragments.ProfileFragment;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String ROOT_DISCOVERY_TAB = "ROOT_DISCOVERY_TAB";
    public static final String ROOT_EXPLORE_TAB = "ROOT_EXPLORE_TAB";
    public static final String ROOT_PLAYLISTS_TAB = "ROOT_PLAYLISTS_TAB";
    public static final String ROOT_PROFILE_TAB = "ROOT_PROFILE_TAB";
    private boolean goesToMainTab = true;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1337;
    // Keys for storing activity state.
    private static final String KEY_LOCATION = "location";
    private Fragment currentFragment = null;
    // The entry point to Google Play services, used by the Places API and Fused Location Provider.
    private GoogleApiClient mGoogleApiClient;
    private boolean mLocationPermissionGranted;
    // The geographical location where the device is currently located. That is, the last-known
    // location retrieved by the Fused Location Provider.
    private LocationRequest mLocationRequest;
    private int fragmentNumber = 0;
    public ProgressBar loadingProgress;
    public BottomNavigationView bottomTabs;
    private boolean shouldTriggerSwitch = true;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            if(shouldTriggerSwitch) {
                switch (item.getItemId()) {
                    case R.id.tab_discovery:
                        BaseFragment discoveryFragment = DiscoveryFragment.newInstance(null);
                        switchFragment(discoveryFragment, DiscoveryFragment.TAG, true, ROOT_DISCOVERY_TAB);
                        setTitle(R.string.bottom_tab_discovery);
                        return true;
                    case R.id.tab_explore:
                        BaseFragment exploreFragment = ExploreFragment.newInstance(null);
                        switchFragment(exploreFragment, ExploreFragment.TAG, true, ROOT_EXPLORE_TAB);
                        setTitle(R.string.bottom_tab_explore);
                        return true;
                    case R.id.tab_playlists:
                        BaseFragment playlistsFragment = PlaylistsFragment.newInstance(null);
                        switchFragment(playlistsFragment, PlaylistsFragment.TAG, true, ROOT_PLAYLISTS_TAB);
                        setTitle(R.string.bottom_tab_playlists);
                        return true;
                    case R.id.tab_profile:
                        if(PreferenceManager.getLogedUser(getBaseContext()) == null) {
                            showLoginDialog();
                            return false;
                        } else {
                            BaseFragment profileFragment = ProfileFragment.newInstance(null);
                            switchFragment(profileFragment, ProfileFragment.TAG, true, ROOT_PROFILE_TAB);
                            setTitle(R.string.bottom_tab_profile);
                            return true;
                        }
                }
                return false;
            }else{
                shouldTriggerSwitch = true;
                switch (item.getItemId()) {
                    case R.id.tab_discovery:
                        setTitle(R.string.bottom_tab_discovery);
                        return true;
                    case R.id.tab_explore:
                        setTitle(R.string.bottom_tab_explore);
                        return true;
                    case R.id.tab_playlists:
                        setTitle(R.string.bottom_tab_playlists);
                        return true;
                    case R.id.tab_profile:
                        setTitle(R.string.bottom_tab_profile);
                        return true;
                }
                return false;
            }
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        setTitle(R.string.bottom_tab_discovery);
        loadingProgress = (ProgressBar) findViewById(R.id.discovery_loading);
        bottomTabs = (BottomNavigationView) findViewById(R.id.bottom_tabs);

        bottomTabs.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        // Retrieve location from saved instance state.
        if (savedInstanceState != null && savedInstanceState.containsKey(KEY_LOCATION) && savedInstanceState.getParcelable(KEY_LOCATION) != null ) {
            LatLng ln = savedInstanceState.getParcelable(KEY_LOCATION);
            Location l = new Location("Traverse");
            l.setLatitude(ln.latitude);
            l.setLongitude(ln.longitude);
            Traverse.setLastKnownLocation(l);
        }

        // Build the Play services client for use by the Fused Location Provider and the Places API.
        // Use the addApi() method to request the Google Places API and the Fused Location Provider.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();
        mGoogleApiClient.connect();

        switchFragment(DiscoveryFragment.newInstance(null), DiscoveryFragment.TAG, false, ROOT_DISCOVERY_TAB);

        if(getIntent() != null && getIntent().getData() != null) {
            Intent intent = getIntent();
            String action = intent.getAction();
            Uri data = intent.getData();

            Log.d(TAG, "Intent action: " + action + " - Intent data: " + data.getPathSegments().get(1) + " - " + data.getPathSegments().get(2));

            if(data.getPathSegments().get(1).equalsIgnoreCase("fiche")) {
                Bundle b = new Bundle();
                b.putString("ficheId", data.getPathSegments().get(2));
                switchFragment(DiscoveryFragment.newInstance(b), DiscoveryFragment.TAG, false, ROOT_DISCOVERY_TAB);
            } else if (data.getPathSegments().get(1).equalsIgnoreCase("playlist")) {
                Bundle b = new Bundle();
                b.putString("playlistId", data.getPathSegments().get(2));
                switchFragment(DiscoveryFragment.newInstance(b), DiscoveryFragment.TAG, false, ROOT_DISCOVERY_TAB);
            }
        } else {
            switchFragment(DiscoveryFragment.newInstance(null), DiscoveryFragment.TAG, false, ROOT_DISCOVERY_TAB);
        }

    }

    /**
     * Saves the location when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(KEY_LOCATION, Traverse.getLastKnownLocation());
        super.onSaveInstanceState(outState);
    }

    /**
     * Alerts when the Google Play services client is successfully connected.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        // Get the current location of the device.
        getDeviceLocation();
    }

    /**
     * Handles failure to connect to the Google Play services client.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // Refer to the reference doc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    /**
     * Handles suspension of the connection to the Google Play services client.
     */
    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Play services connection suspended");
    }

    /**
     * Gets the current location of the device.
     */
    public LatLng getDeviceLocation() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        if (mLocationPermissionGranted) {
            Traverse.setLastKnownLocation(LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient));
            mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000); //5 seconds
            mLocationRequest.setFastestInterval(5000); //3 seconds
            mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
            return Traverse.getLastKnownLocation();
        } else{
            return Traverse.getLastKnownLocation();
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                    getDeviceLocation();
                }
            }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        Traverse.setLastKnownLocation(location);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount() == 1){
            showCloseDialog();
        }else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    /**
     * Main navigation and backstack management
     *
     * @param fragment the fragment to be shown
     * @param tag the unique tag of the fragment taken from Fragment#class.getSimpleName()
     * @param addToBackStack whether to add the fragment to the backstack or not
     * @param rootTab defines whether the frgment is the main view of a main tab @null for secondary fragments
     *
     */

    public void switchFragment(BaseFragment fragment, String tag, boolean addToBackStack, String rootTab) {
        FragmentManager manager = getSupportFragmentManager();
        //Manages the clicks between two main tabs. It cleans the backstack up to the root screen of the clicked tab
        if(rootTab != null && goesToMainTab){
            //Pops the stack down to the clean state of a main tab, first fragment on the stack inclusive
            manager.popBackStack(rootTab, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            // Adds the new tab fragment
            manager.beginTransaction()
                    .replace(R.id.fragment_container, fragment, String.valueOf(fragmentNumber))
                    .addToBackStack(rootTab)
                    .commit();
            goesToMainTab = true;
            //Manages navigation between a secondary fragment and a main tab
        } else if(rootTab != null && !goesToMainTab) {
            // Add the new tab fragment
            manager.beginTransaction()
                    .replace(R.id.fragment_container, fragment, String.valueOf(fragmentNumber))
                    .addToBackStack(rootTab)
                    .commit();
            goesToMainTab = true;
        } else {
            //Manages navigation between secondary fragments
            if (addToBackStack) {
                manager.beginTransaction()
                        .replace(R.id.fragment_container, fragment, String.valueOf(fragmentNumber))
                        .addToBackStack(null)
                        .commit();
                goesToMainTab = false;
            } else {
                    //Does a fragment replace without adding to backstack
                    manager.beginTransaction()
                            .replace(R.id.fragment_container, fragment, String.valueOf(fragmentNumber))
                            .commit();
                }
            }
        //Sets the current fragment for tracking and access purposes
        fragmentNumber++;
        setCurrentFragment(fragment);
    }

    public void setSelectedTab (int tab) {
        shouldTriggerSwitch = false;
        bottomTabs.setSelectedItemId(tab);
    }

    public void showLoading () {
        loadingProgress.setVisibility(View.VISIBLE);
    }

    public void hideLoading () {
        loadingProgress.setVisibility(View.GONE);
    }

    public Fragment getCurrentFragment() {
        if (currentFragment != null) {
            return currentFragment;
        } else {
            return new DiscoveryFragment();
        }
    }

    public void setCurrentFragment(Fragment currentFragment) {
        this.currentFragment = currentFragment;
    }

    public LatLng getLastLocation() {
        return getDeviceLocation();
    }

    public GoogleApiClient getGoogleApiClient () {
        return mGoogleApiClient;
    }

    /**
     * Shows a dialog to confirm quitting the app
     */
    private void showCloseDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.quit_dialog_text));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    /**
     * Shows a dialog to allow log in
     */
    private void showLoginDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.login_dialog_text));
        builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(getBaseContext(), LoginActivity.class));
            }
        });
        builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

}
