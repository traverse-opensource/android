package ch.mobilethinking.traverse.Views.Activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.VideoView;

import ch.mobilethinking.traverse.R;

public class VideoPlayerActivity extends AppCompatActivity {

    private static final String TAG = VideoPlayerActivity.class.getSimpleName();
    private ImageView exitVideo;
    private ProgressBar progressVideo;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        exitVideo = (ImageView) findViewById(R.id.exit_video);
        progressVideo = (ProgressBar) findViewById(R.id.video_progress);
        videoView = (VideoView) findViewById(R.id.video_view);

        if(getIntent().hasExtra("video_url")){
            videoView.setVideoPath(getIntent().getStringExtra("video_url"));
            videoView.start();
            progressVideo.setVisibility(View.GONE);
        }

        exitVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
