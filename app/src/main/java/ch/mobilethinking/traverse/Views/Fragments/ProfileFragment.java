package ch.mobilethinking.traverse.Views.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.twitter.sdk.android.core.Twitter;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import ch.mobilethinking.traverse.R;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Views.Activities.WebViewActivity;
import ch.mobilethinking.traverse.api.model.User;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends BaseFragment {

    public static final String TAG = ProfileFragment.class.getSimpleName();
    //The tab this fragment is linked to
    public static int TAB = R.id.tab_profile;
    private CircleImageView userPicture;
    private TextView userName;
    private TextView usage;
    private TextView privacy;
    private TextView bug;
    private TextView about;
    private TextView logOut;
    private GoogleApiClient mGoogleApiClient;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(Bundle args) {
        ProfileFragment fragment = new ProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = getGoogleApiClient();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        Toolbar toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        setActionBar(toolbar);
        userPicture = (CircleImageView) rootView.findViewById(R.id.user_picture);
        userName = (TextView) rootView.findViewById(R.id.user_name);
        logOut = (TextView) rootView.findViewById(R.id.profile_logout);
        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                }

                TwitterSession twitterSession = TwitterCore.getInstance().getSessionManager().getActiveSession();
                if (twitterSession != null) {
                    TwitterCore.getInstance().getSessionManager().clearActiveSession();
                }

                Auth.GoogleSignInApi.signOut(mGoogleApiClient);

                PreferenceManager.logOut(getContext());
                BaseFragment discoveryFragment = DiscoveryFragment.newInstance(null);
                discoveryFragment.setTab(DiscoveryFragment.TAB);
                switchFragment(discoveryFragment, DiscoveryFragment.TAG, true);
            }
        });
        User user = PreferenceManager.getLogedUser(getContext());
        setUserImage(user.getProfile().getPicture());
        Log.d(TAG, "User picture: " + user.getProfile().getPicture());
        setUserName(user.getName());

        usage = (TextView) rootView.findViewById(R.id.profile_usage);
        privacy = (TextView) rootView.findViewById(R.id.profile_privacy);
        bug = (TextView) rootView.findViewById(R.id.profile_bug);
        about = (TextView) rootView.findViewById(R.id.profile_about);

        usage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), WebViewActivity.class);
                i.putExtra("url", "https://traverse-patrimoines.com/policies/utilisation.pdf");
                startActivity(i);
            }
        });

        privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), WebViewActivity.class);
                i.putExtra("url", "https://traverse-patrimoines.com/policies/confidentialite.pdf");
                startActivity(i);
            }
        });

        bug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), WebViewActivity.class);
                i.putExtra("url", "https://traversepep.typeform.com/to/lDJQYk");
                startActivity(i);
            }
        });

        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getActivity(), WebViewActivity.class);
                i.putExtra("url", "https://www.traverse-patrimoines.com/partenaires/");
                startActivity(i);
            }
        });

        return rootView;
    }

    @Override
    public int getTab() {
        return TAB;
    }

    @Override
    public void setTab(int tab) {
        this.TAB = tab;
    }

    public void setUserImage(String imageUrl) {
        if(!TextUtils.isEmpty(imageUrl)){
            Glide.with(this).load(imageUrl).dontAnimate().into(userPicture);
        } else {
            Glide.with(this).load(R.drawable.default_contributor).dontAnimate().into(userPicture);
        }
    }

    public void setUserName(String name) {
        if(!TextUtils.isEmpty(name)) {
            this.userName.setText(name);
        }
    }

}
