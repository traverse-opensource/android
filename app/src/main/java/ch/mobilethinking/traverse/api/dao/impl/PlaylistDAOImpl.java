package ch.mobilethinking.traverse.api.dao.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.BaseTarget;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import bolts.TaskCompletionSource;
import ch.mobilethinking.traverse.BuildConfig;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.api.dao.FicheDAO;
import ch.mobilethinking.traverse.api.dao.PlaylistDAO;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class PlaylistDAOImpl extends AbstractAPIManagerImpl implements PlaylistDAO {
    private static final String PLAYLIST_CACHE = "playlist_cache";
    private final DualCache<Playlist> playlistCache;

    public PlaylistDAOImpl(Context context) {
        super(context);

        final CacheSerializer<Playlist> playlistJsonSerializer =
                new JsonSerializer<>(Playlist.class);

        playlistCache = new Builder<>(PLAYLIST_CACHE, BuildConfig.VERSION_CODE, Playlist.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, playlistJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, playlistJsonSerializer, context)
                .build();

        Log.d("FDI", "playlist cache location> " +  playlistCache.getDiskCacheFolder().getAbsolutePath());
    }

    @Override
    public Task<Playlist> getRemotePlaylist(String playlistId) {
        String args = "";
        String requestPath = String.format("%s/%s",
                CONTROLLER,
                playlistId
        );

        if(playlistCache.get(playlistId) != null && !MessageUtil.checkConnection(context)) {
            final TaskCompletionSource<Playlist> tcs = new TaskCompletionSource<>();
            tcs.setResult(playlistCache.get(playlistId));
            return tcs.getTask();
        } else {

            return addRequest(requestPath, args, Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {


                @Override
                public Task<Playlist> then(final Task<Playlist> task) throws Exception {
                    //save fiches in cache

                    //ficheCache.put(PLAYLISTS_RESULT_CACHE, task.getResult());
                    return task;
                }
            });
        }
    }

    @Override
    public Task<Playlist> getPlaylist(String playlistId) {
        return getRemotePlaylist(playlistId);
    }

    @Override
    public Task<Playlist.PlaylistList> getRemotePlaylists() {
        return getRemotePlaylists(1, 20, 0);
    }

    public Task<Playlist.PlaylistList> getRemotePlaylists(int status, int limit, int page) {
        String args = String.format("status=%d&limit=%d&page=%d", status, limit, page);

        return addRequest(CONTROLLER, args, Playlist.PlaylistList.class).onSuccessTask(new Continuation<Playlist.PlaylistList, Task<Playlist.PlaylistList>>() {



            @Override
            public Task<Playlist.PlaylistList> then(final Task<Playlist.PlaylistList> task) throws Exception {
                //save fiches in cache

                //ficheCache.put(PLAYLISTS_RESULT_CACHE, task.getResult());
                return task;
            }
        });
    }

    @Override
    public Task<Playlist.PlaylistList> getPlaylists() {
        return getRemotePlaylists();
    }

    @Override
    public Task<Playlist.PlaylistList> getRemoteAssociatedPlaylists(String ficheId) {
        String args = "";

        String requestPath = String.format("%s/%s/%s",
                FicheDAO.CONTROLLER,
                ficheId,
                CONTROLLER_ASSOCIATED_PLAYLISTS
        );

        return addRequest(requestPath, args, Playlist.PlaylistList.class).onSuccessTask(new Continuation<Playlist.PlaylistList, Task<Playlist.PlaylistList>>() {



            @Override
            public Task<Playlist.PlaylistList> then(final Task<Playlist.PlaylistList> task) throws Exception {
                //save fiches in cache

                //ficheCache.put(PLAYLISTS_RESULT_CACHE, task.getResult());
                return task;
            }
        });
    }

    @Override
    public Task<Playlist.PlaylistList> getAssociatedPlaylists(String ficheId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        /*if (!prefs.getBoolean(TraverseApplication.PREF_MODE_SELECTION_STREAMING, true)) {

            final Playlist.PlaylistList ficheList = ficheCache.get(PLAYLISTS_RESULT_CACHE);
            if (ficheList != null) {
                final TaskCompletionSource<Playlist.PlaylistList> tcs = new TaskCompletionSource<>();
                tcs.setResult(ficheList);

                return tcs.getTask();
            } else {
                return getRemoteAssociatedPlaylists(ficheId);
            }
        } else {*/
        return getRemoteAssociatedPlaylists(ficheId);
        //}
    }

    @Override
    public Task<Playlist> addLinkedFiche(String playlistId, String ficheId, @Nullable String linkValue) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, playlistId, CONTROLLER_ADD_LINK),
                args = "";

        HashMap<String, String> linkedFiche = new HashMap<>();
        linkedFiche.put("ficheId", ficheId);
        linkedFiche.put("linkValue", (linkValue == null ? "" : linkValue));

        return addPostRequest(requestPath, args, new JSONObject(linkedFiche), Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {
            @Override
            public Task<Playlist> then(Task<Playlist> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Playlist> addPlaylist(JSONObject playlistInfo) {
        String requestPath = String.format(Locale.ENGLISH, "%s", CONTROLLER),
                args = "";

        return addPostRequest(requestPath, args,playlistInfo, Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {
            @Override
            public Task<Playlist> then(Task<Playlist> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Playlist> updatePlaylist(JSONObject playlistInfo, String playlistId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, playlistId, CONTROLLER_FULL_UPDATE),
                args = "";

        return addPostRequest(requestPath, args,playlistInfo, Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {
            @Override
            public Task<Playlist> then(Task<Playlist> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<SimpleResponse> deletePlaylist(String playlistId) {
        String args = "";

        String requestPath = String.format("%s/%s", CONTROLLER, playlistId
        );

        return addDeleteRequest(requestPath, args, SimpleResponse.class).onSuccessTask(new Continuation<SimpleResponse, Task<SimpleResponse>>() {



            @Override
            public Task<SimpleResponse> then(final Task<SimpleResponse> task) throws Exception {
                //save fiches in cache

                //ficheCache.put(PLAYLISTS_RESULT_CACHE, task.getResult());
                return task;
            }
        });
    }

    @Override
    public Task<Playlist> swapFiches(String playlistId, JSONObject linkedFiches) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, playlistId, CONTROLLER_SWAP),
                args = "";

        return addPostRequest(requestPath, args, linkedFiches, Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {
            @Override
            public Task<Playlist> then(Task<Playlist> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Playlist> deleteLinkedFiche(String playlistId, String linkedFicheId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, playlistId, CONTROLLER_DELETE_LINK),
                args = "";

        HashMap<String, String> linkedFiche = new HashMap<>();
        linkedFiche.put("linkedFicheId", linkedFicheId);

        return addPostRequest(requestPath, args, new JSONObject(linkedFiche), Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {
            @Override
            public Task<Playlist> then(Task<Playlist> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Playlist> likePlaylist(String playlistId, String userId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, playlistId, CONTROLLER_LIKE),
                args = "";

        HashMap<String, String> like = new HashMap<>();
        like.put("objectId", userId);

        return addPutRequest(requestPath, args, new JSONObject(like), Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {
            @Override
            public Task<Playlist> then(Task<Playlist> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Playlist> dislikePlaylist(String playlistId, String userId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, playlistId, CONTROLLER_DISLIKE),
                args = "";

        HashMap<String, String> dislike = new HashMap<>();
        dislike.put("objectId", userId);

        return addPutRequest(requestPath, args, new JSONObject(dislike), Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {
            @Override
            public Task<Playlist> then(Task<Playlist> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Boolean saveFavouritePlaylists() {
        HashMap<String, Boolean> uPlaylists = ch.mobilethinking.traverse.Utils.PreferenceManager.getSavedPlaylists(getContext());
        if (!uPlaylists.isEmpty()) {
            for (String s: uPlaylists.keySet()
                 ) {
                savePlaylist(s);
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public Boolean savePlaylist(final String playlistId) {
        final String args = "";
        final String requestPath = String.format("%s/%s",
                CONTROLLER,
                playlistId
        );

                Task<Playlist> t = addRequestAsync(requestPath, args, Playlist.class).onSuccessTask(new Continuation<Playlist, Task<Playlist>>() {

                    @Override
                    public Task<Playlist> then(final Task<Playlist> task) throws Exception {
                                Playlist p = task.getResult();
                                playlistCache.put(playlistId, p);
                                SimpleTarget<File> t = new SimpleTarget<File>() {
                                    @Override
                                    public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {

                                    }
                                };
                                Glide.with(context)
                                        .load(p.getCoverPath())
                                        .downloadOnly(t);
                                for (LinkedFiche lf : p.getLinkedFiches()
                                        ) {
                                    Fiche f = lf.getFiche();
                                    APIManager.getFicheManager(context.getApplicationContext()).saveFiche(f.getId());
                                }
                        return task;
                    }
                });
                return t.isCompleted();
    }


}
