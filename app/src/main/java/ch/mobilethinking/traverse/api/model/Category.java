package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

/**
 * Created by valentinkim on 09.05.17.
 */

public class Category extends MongoEntity {

    @JsonProperty("name")
    String name;

    @JsonProperty("sousCategories")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    String[] subCategories;

    public Category() {}

    //same reason as for user, we will need to fectch in remote nor local data source to retrieve the full object
    public Category (String categoriyId) {
        setId(categoriyId);
    }

    public Category(String id, String name, String[] subCategories) {
        setId(id);
        setName(name);
        setSubCategories(subCategories);
    }

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public void setSubCategories(String[] subCategories) { this.subCategories = subCategories; }

    public String[] getSubCategories() { return subCategories; }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                subCategories != null ?
                    ", subCategories=" + Arrays.toString(subCategories):
                    ", subCategories=<no subcategories found>" +
                '}';
    }
}
