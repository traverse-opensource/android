package ch.mobilethinking.traverse.api.manager;

import android.content.Context;
import android.support.annotation.Nullable;

import org.json.JSONObject;

import bolts.Task;
import ch.mobilethinking.traverse.api.dao.ApiDAO;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 11.05.17.
 */

public class PlaylistManager {

    private final Context context;

    PlaylistManager(Context context) {
        this.context = context;
    }

    public Task<Playlist> getPlaylist(String playlistId) {
        return ApiDAO.getPlaylistDAO(context).getPlaylist(playlistId);
    }

    public Task<Playlist.PlaylistList> getPlaylists() {
        return ApiDAO.getPlaylistDAO(context).getPlaylists();
    }

    public Task<Playlist.PlaylistList> getAssociatedPlaylists(String ficheId) {
        return ApiDAO.getPlaylistDAO(context).getAssociatedPlaylists(ficheId);
    }

    public Task<Playlist> addLinkedFiche(String playlistId, String ficheId, @Nullable String linkValue) {
        return ApiDAO.getPlaylistDAO(context).addLinkedFiche(playlistId, ficheId, linkValue);
    }

    public Task<Playlist> addPlaylist(JSONObject playlistInfo) {
        return ApiDAO.getPlaylistDAO(context).addPlaylist(playlistInfo);
    }

    public Task<Playlist> updatePlaylist(JSONObject playlistInfo, String playlistId) {
        return ApiDAO.getPlaylistDAO(context).updatePlaylist(playlistInfo, playlistId);
    }

    public Task<SimpleResponse> deletePlaylist(String playlistId) {
        return ApiDAO.getPlaylistDAO(context).deletePlaylist(playlistId);
    }

    public Task<Playlist> swapFiches(String playlistId, JSONObject linkedFiches) {
        return ApiDAO.getPlaylistDAO(context).swapFiches(playlistId, linkedFiches);
    }

    public Task<Playlist> deleteLinkedFiche(String playlistId, String ficheId) {
        return ApiDAO.getPlaylistDAO(context).deleteLinkedFiche(playlistId, ficheId);
    }

    public Task<Playlist> likePlaylist(String playlistId, String userId) {
        return ApiDAO.getPlaylistDAO(context).likePlaylist(playlistId, userId);
    }

    public Task<Playlist> dislikePlaylist(String playlistId, String userId) {
        return ApiDAO.getPlaylistDAO(context).dislikePlaylist(playlistId, userId);
    }

    public Boolean saveFavouritePlaylists() {
        return ApiDAO.getPlaylistDAO(context).saveFavouritePlaylists();
    }

    public Boolean savePlaylist(String playlistId) {
        return ApiDAO.getPlaylistDAO(context).savePlaylist(playlistId);
    }

}