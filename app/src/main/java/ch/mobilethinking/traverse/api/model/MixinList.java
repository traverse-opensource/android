package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 16.05.17.
 */

public class MixinList implements APIEntity {

    @JsonProperty("fiches")
    Fiche[] fichelist;

    @JsonProperty("playlists")
    Playlist[] plalistList;

    public MixinList() {}

    public MixinList(Fiche[] fiches, Playlist[] playlists) {
        setFichelist(fiches);
        setPlalistList(playlists);
    }

    public Fiche[] getFichelist() { return fichelist; }

    public void setFichelist(Fiche[] fichelist) { this.fichelist = fichelist; }

    public Playlist[] getPlalistList() { return plalistList; }

    public void setPlalistList(Playlist[] plalistList) { this.plalistList = plalistList; }
}
