package ch.mobilethinking.traverse.api.net;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonSyntaxException;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ch.mobilethinking.traverse.BuildConfig;

/**
 * Created by jerome on 12.10.16.
 * Mobilethinking.ch
 */

public class MultipartRequest<T> extends Request<T> {


    private static final String FILE_PART_NAME = "file";

    private final ObjectMapper objectMapper;
    private final Response.Listener<T> listener;
    private final Class<T> clazz;
    private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
    private File mImageFile = null;

    public MultipartRequest(final String action, final String arguments, final Class<T> clazz, final JSONObject params, final File imageFile,
                            final Response.Listener<T> listener, final Response.ErrorListener errorListener) {
        super(
                Method.POST,
                BuildConfig.API_HOST + (TextUtils.isEmpty(BuildConfig.API_PORT ) ? "" : ":" + BuildConfig.API_PORT) + BuildConfig.API_VERSION + "/" + action + ((arguments!=null&&arguments!="")?"?" + (arguments):""),
                errorListener);

        this.objectMapper = new ObjectMapper();
        this.listener = listener;
        this.clazz = clazz;

        JSONArray keys = params.names();
        for (int i = 0; i < keys.length(); i++) {
            try {
                mBuilder.addTextBody(String.valueOf(keys.get(i)), params.get(String.valueOf(keys.get(i))).toString(), ContentType.TEXT_PLAIN.withCharset("UTF-8"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (imageFile != null) {
            mImageFile = imageFile;
            //buildMultipartEntity();
            mBuilder.addBinaryBody(FILE_PART_NAME, mImageFile, ContentType.create("image/jpg"), mImageFile.getName());
        }

        Log.d("MULTI_REQUEST", mImageFile.getName());
        mBuilder.setLaxMode().setBoundary("xx");
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<>();
        }

        headers.put("Accept", "application/json");
        headers.put("x-mtsk", BuildConfig.API_KEY);
        headers.put("accept-encoding", "gzip, deflate, br");
        headers.put("accept-language", "fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4");

        return headers;
    }

    private void buildMultipartEntity(){
        mBuilder.addBinaryBody(FILE_PART_NAME, mImageFile, ContentType.create("image/jpg"), mImageFile.getName());
    }

    @Override
    public String getBodyContentType(){
        return mBuilder.build().getContentType().getValue();
    }

    @Override
    public byte[] getBody() {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mBuilder.build().writeTo(bos);
        } catch (IOException e) {
            e.printStackTrace();
            VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
        }

        return bos.toByteArray();
    }

    @Override
    public Priority getPriority() {
        return Priority.HIGH;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            final T objResponse = (T) objectMapper.readValue(json, clazz);
            Response<T> resp = Response.success(
                    objResponse,
                    HttpHeaderParser.parseCacheHeaders(response));
            return resp;
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        } catch (JsonParseException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (JsonMappingException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        } catch (IOException e) {
            e.printStackTrace();
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        Log.d("MULTI_RESPONSE", response.toString());
        listener.onResponse(response);
    }
}
