package ch.mobilethinking.traverse.api.manager;

import android.content.Context;

import org.json.JSONObject;

import bolts.Task;
import ch.mobilethinking.traverse.api.dao.ApiDAO;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 11.05.17.
 */

public class UserManager {

    private final Context context;

    UserManager(Context context) {
        this.context = context;
    }

    public Task<User> getUser(String userId) {
        return ApiDAO.getUserDAO(context).getUser(userId);
    }

    public Task<User.UserList> getUsers() {
        return ApiDAO.getUserDAO(context).getUsers();
    }

    public Task<User> signUp(JSONObject user) {
        return ApiDAO.getUserDAO(context).signUp(user);
    }

    public Task<Playlist.PlaylistList> getUserPlaylists(String userId) {
        return ApiDAO.getUserDAO(context).getUserPlaylists(userId);
    }

    public Task<User> login(JSONObject user) {
        return ApiDAO.getUserDAO(context).login(user);
    }

    public Task<SimpleResponse> resetPassword(String email) {
        return ApiDAO.getUserDAO(context).resetPassword(email);
    }

    public Boolean saveUserPlaylists(String userId) {
        return ApiDAO.getUserDAO(context).saveUserPlaylists(userId);
    }

}