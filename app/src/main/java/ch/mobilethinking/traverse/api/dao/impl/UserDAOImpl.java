package ch.mobilethinking.traverse.api.dao.impl;

import android.content.Context;
import android.util.Log;

import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import bolts.TaskCompletionSource;
import ch.mobilethinking.traverse.BuildConfig;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.api.dao.UserDAO;
import ch.mobilethinking.traverse.api.manager.APIManager;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;
import ch.mobilethinking.traverse.api.model.User;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class UserDAOImpl extends AbstractAPIManagerImpl implements UserDAO {
    private static final String MY_PLAYLISTS_CACHE = "my_playlists_cache";

    private final DualCache<Playlist.PlaylistList> myPlaylistsCache;

    public UserDAOImpl(Context context) {
        super(context);

        final CacheSerializer<Playlist.PlaylistList> myPlaylistsJsonSerializer =
                new JsonSerializer<>(Playlist.PlaylistList.class);

        myPlaylistsCache = new Builder<>(MY_PLAYLISTS_CACHE, BuildConfig.VERSION_CODE, Playlist.PlaylistList.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, myPlaylistsJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, myPlaylistsJsonSerializer, context)
                .build();

        Log.d("FDI", "myPlaylists cache location> " +  myPlaylistsCache.getDiskCacheFolder().getAbsolutePath());
    }

    @Override
    public Task<User> getRemoteUser(String userId) {

        String requestPath = String.format(Locale.ENGLISH, "%s/%s", CONTROLLER, userId),
                args = "";

        return addRequest(requestPath, args, User.class).onSuccessTask(new Continuation<User, Task<User>>() {
            @Override
            public Task<User> then(Task<User> task) throws Exception {
                return task;
            }
        });
    }


    @Override
    public Task<User> getUser(String userId) {
        return getRemoteUser(userId);
    }

    @Override
    public Task<User.UserList> getRemoteUsers() {

        return getRemoteUsers(0, 0, 15);
    }

    public Task<User.UserList> getRemoteUsers(int status, int page, int limit) {
        String requestPath = CONTROLLER,
                args = String.format(Locale.ENGLISH, "status=%d&page=%d&limit=%d",
                    status,
                    page,
                    limit
                );

        return addRequest(requestPath, args, User.UserList.class).onSuccessTask(new Continuation<User.UserList, Task<User.UserList>>() {
            @Override
            public Task<User.UserList> then(final Task<User.UserList> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<User.UserList> getUsers() {
        return getRemoteUsers();
    }

    @Override
    public Task<User> signUp(JSONObject user) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, CONTROLLER_SINGLE_USER, CONTROLLER_SIGNUP),
                args = "";

        return addPostRequest(requestPath, args, user, User.class).onSuccessTask(new Continuation<User, Task<User>>() {
            @Override
            public Task<User> then(Task<User> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<User> login(JSONObject user) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, CONTROLLER_SINGLE_USER, CONTROLLER_LOGIN),
                args = "";

        return addPostRequest(requestPath, args, user, User.class).onSuccessTask(new Continuation<User, Task<User>>() {
            @Override
            public Task<User> then(Task<User> task) throws Exception {
                return task;
            }
        });
    }

    public Task<Playlist.PlaylistList> getUserPlaylists(String userId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, userId, CONTROLLER_PLAYLISTS),
                args = "";

        if(myPlaylistsCache.get(userId) != null && !MessageUtil.checkConnection(context)) {
            final TaskCompletionSource<Playlist.PlaylistList> tcs = new TaskCompletionSource<>();
            tcs.setResult(myPlaylistsCache.get(userId));
            return tcs.getTask();
        } else {

            return addRequest(requestPath, args, Playlist.PlaylistList.class).onSuccessTask(new Continuation<Playlist.PlaylistList, Task<Playlist.PlaylistList>>() {
                @Override
                public Task<Playlist.PlaylistList> then(final Task<Playlist.PlaylistList> task) throws Exception {
                    return task;
                }
            });
        }
    }

    @Override
    public Task<SimpleResponse> resetPassword(String email) {
        String args = "";

        String requestPath = String.format("%s/%s/%s", CONTROLLER, CONTROLLER_SINGLE_USER, CONTROLLER_RESET_PASSSWORD
        );

        HashMap<String, String> user = new HashMap<>();
        user.put("email", email);

        return addPostRequest(requestPath, args, new JSONObject(user), SimpleResponse.class).onSuccessTask(new Continuation<SimpleResponse, Task<SimpleResponse>>() {

            @Override
            public Task<SimpleResponse> then(final Task<SimpleResponse> task) throws Exception {
                //save fiches in cache

                //ficheCache.put(PLAYLISTS_RESULT_CACHE, task.getResult());
                return task;
            }
        });
    }

    @Override
    public Boolean saveUserPlaylists(final String userId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, userId, CONTROLLER_PLAYLISTS),
                args = "";
        Task<Playlist.PlaylistList> t = addRequest(requestPath, args, Playlist.PlaylistList.class).onSuccessTask(new Continuation<Playlist.PlaylistList, Task<Playlist.PlaylistList>>() {
            @Override
            public Task<Playlist.PlaylistList> then(final Task<Playlist.PlaylistList> task) throws Exception {
                myPlaylistsCache.put(userId, task.getResult());
                for (Playlist p : task.getResult().getPlaylists()
                     ) {
                    APIManager.getPlaylistManager(context.getApplicationContext()).savePlaylist(p.getId());
                }
                return task;
            }
        });
        Log.d("CACHE", "Saving user playlists...");
        return t.isCompleted();
    }

}
