package ch.mobilethinking.traverse.api.dao.impl;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import bolts.TaskCompletionSource;
import ch.mobilethinking.traverse.BuildConfig;
import ch.mobilethinking.traverse.Utils.MessageUtil;
import ch.mobilethinking.traverse.Utils.MyJsonSerializer;
import ch.mobilethinking.traverse.api.dao.FicheDAO;
import ch.mobilethinking.traverse.api.model.SocialFeed;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class FicheDAOImpl extends AbstractAPIManagerImpl implements FicheDAO {
    private static final String FICHE_EVENT_CACHE = "fiche_event_cache";
    private static final String FICHE_MEDIA_CACHE = "fiche_media_cache";
    private static final String FICHE_OBJECT_CACHE = "fiche_object_cache";
    private static final String FICHE_PEOPLE_CACHE = "fiche_people_cache";
    private static final String FICHE_PLACE_CACHE = "fiche_place_cache";
    private static final String FICHE_CACHE = "fiche_cache";

    private final DualCache<Fiche> ficheCache;
    private final DualCache<FicheEvent> ficheEventCache;
    private final DualCache<FicheMedia> ficheMediaCache;
    private final DualCache<FicheObject> ficheObjectCache;
    private final DualCache<FichePeople> fichePeopleCache;
    private final DualCache<FichePlace> fichePlaceCache;

    public FicheDAOImpl(Context context) {
        super(context);

        final CacheSerializer<Fiche> ficheJsonSerializer =
                new JsonSerializer<>(Fiche.class);

        final CacheSerializer<FicheEvent> ficheEventJsonSerializer =
                new MyJsonSerializer<>(Fiche.class, FicheEvent.class);

        final CacheSerializer<FicheMedia> ficheMediaJsonSerializer =
                new MyJsonSerializer<>(Fiche.class, FicheMedia.class);

        final CacheSerializer<FicheObject> ficheObjectJsonSerializer =
                new MyJsonSerializer<>(Fiche.class, FicheObject.class);

        final CacheSerializer<FichePeople> fichePeopleJsonSerializer =
                new MyJsonSerializer<>(Fiche.class, FichePeople.class);

        final CacheSerializer<FichePlace> fichePlaceJsonSerializer =
                new MyJsonSerializer<>(Fiche.class, FichePlace.class);

        ficheCache = new Builder<>(FICHE_CACHE, BuildConfig.VERSION_CODE, Fiche.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, ficheJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, ficheJsonSerializer, context)
                .build();

        ficheEventCache = new Builder<>(FICHE_EVENT_CACHE, BuildConfig.VERSION_CODE, FicheEvent.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, ficheEventJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, ficheEventJsonSerializer, context)
                .build();

        ficheMediaCache = new Builder<>(FICHE_MEDIA_CACHE, BuildConfig.VERSION_CODE, FicheMedia.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, ficheMediaJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, ficheMediaJsonSerializer, context)
                .build();

        ficheObjectCache = new Builder<>(FICHE_OBJECT_CACHE, BuildConfig.VERSION_CODE, FicheObject.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, ficheObjectJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, ficheObjectJsonSerializer, context)
                .build();

        fichePeopleCache = new Builder<>(FICHE_PEOPLE_CACHE, BuildConfig.VERSION_CODE, FichePeople.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, fichePeopleJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, fichePeopleJsonSerializer, context)
                .build();

        fichePlaceCache = new Builder<>(FICHE_PLACE_CACHE, BuildConfig.VERSION_CODE, FichePlace.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, fichePlaceJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, fichePlaceJsonSerializer, context)
                .build();

        Log.d("FDI", "file cache location> " + ficheCache.getDiskCacheFolder().getAbsolutePath());
    }

    @Override
    public Task<Fiche> getRemoteFiche(String ficheId) {
        String requestPath = String.format("%s/%s", CONTROLLER, ficheId),
                args = "";

        if(ficheCache.get(ficheId) != null && !MessageUtil.checkConnection(context)) {
            final TaskCompletionSource<Fiche> tcs = new TaskCompletionSource<>();
            tcs.setResult(getFicheFromSpecificCache(ficheId));
            return tcs.getTask();
        } else {
            return addRequest(requestPath, args, Fiche.class).onSuccessTask(new Continuation<Fiche, Task<Fiche>>() {
                @Override
                public Task<Fiche> then(final Task<Fiche> task) throws Exception {
                    return task;
                }
            });
        }
    }

    @Override
    public Task<Fiche> getFiche(String ficheId) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        /*if (!prefs.getBoolean(TraverseApplication.PREF_MODE_SELECTION_STREAMING, true)) {

            final Fiche.FicheList ficheList = ficheCache.get(FICHES_RESULT_CACHE);
            final Fiche ficheItem = ficheList.getFiches().get(0);
            if (ficheList != null) {
                final TaskCompletionSource<Fiche> tcs = new TaskCompletionSource<>();
                tcs.setResult(ficheItem);

                return tcs.getTask();
            } else {
                return getRemoteFiche(ficheId);
            }
        } else {*/
            return getRemoteFiche(ficheId);
        //}
    }

    @Override
    public Task<Playlist.PlaylistList> getFichePlaylists(String ficheId) {
        String requestPath = String.format("%s/%s/%s", CONTROLLER, ficheId, CONTROLLER_FICHE_PLAYLISTS),
                args = "";

        return addRequest(requestPath, args, Playlist.PlaylistList.class).onSuccessTask(new Continuation<Playlist.PlaylistList, Task<Playlist.PlaylistList>>() {

            @Override
            public Task<Playlist.PlaylistList> then(final Task<Playlist.PlaylistList> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Fiche.FicheList> getRemoteFiches() {

        Log.e("FDI", "Before getRemoteFiche return");

        return getRemoteFiches(1, 20, 0);
    }

    public Task<Fiche.FicheList> getRemoteFiches(int status, int limit, int page) {

        String args = String.format("status=%d&limit=%d&page=%d", status, limit, page);

        return addRequest(CONTROLLER + "", args, Fiche.FicheList.class).onSuccessTask(new Continuation<Fiche.FicheList, Task<Fiche.FicheList>>() {



            @Override
            public Task<Fiche.FicheList> then(final Task<Fiche.FicheList> task) throws Exception {
                //save fiches in cache
                return task;
            }
        });
    }

    @Override
    public Task<Fiche.FicheList> getFiches() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

        /*if (!prefs.getBoolean(TraverseApplication.PREF_MODE_SELECTION_STREAMING, true)) {

            final Fiche.FicheList ficheList = ficheCache.get(FICHES_RESULT_CACHE);
            if (ficheList != null) {
                final TaskCompletionSource<Fiche.FicheList> tcs = new TaskCompletionSource<>();
                tcs.setResult(ficheList);

                return tcs.getTask();
            } else {
                return getRemoteFiches();
            }
        } else {*/
            return getRemoteFiches();
        //}
    }

    @Override
    public Task<Fiche> likeFiche(String ficheId, String userId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, ficheId, CONTROLLER_LIKE),
                args = "";

        HashMap<String, String> like = new HashMap<>();
        like.put("objectId", userId);

        return addPutRequest(requestPath, args, new JSONObject(like), Fiche.class).onSuccessTask(new Continuation<Fiche, Task<Fiche>>() {
            @Override
            public Task<Fiche> then(Task<Fiche> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Fiche> dislikeFiche(String ficheId, String userId) {
        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s", CONTROLLER, ficheId, CONTROLLER_DISLIKE),
                args = "";

        HashMap<String, String> dislike = new HashMap<>();
        dislike.put("objectId", userId);

        return addPutRequest(requestPath, args, new JSONObject(dislike), Fiche.class).onSuccessTask(new Continuation<Fiche, Task<Fiche>>() {
            @Override
            public Task<Fiche> then(Task<Fiche> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Boolean saveFiche(final String ficheId) {
        String requestPath = String.format("%s/%s", CONTROLLER, ficheId),
                args = "";

           Task<Fiche> t = addRequestAsync(requestPath, args, Fiche.class).onSuccessTask(new Continuation<Fiche, Task<Fiche>>() {

                @Override
                public Task<Fiche> then(final Task<Fiche> task) throws Exception {
                            Fiche f = task.getResult();
                            ficheCache.put(ficheId, f);
                            putFicheInSpecificCache(ficheId, f);
                            SimpleTarget<File> t = new SimpleTarget<File>() {
                                @Override
                                public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {

                                }
                            };
                            Glide.with(context)
                                    .load(f.getCoverPath())
                                    .downloadOnly(t);
                            for (Fiche lf: f.getRelatedFiches()
                                    ) {
                                SimpleTarget<File> tf = new SimpleTarget<File>() {
                                    @Override
                                    public void onResourceReady(File resource, GlideAnimation<? super File> glideAnimation) {

                                    }
                                };
                                Glide.with(context)
                                        .load(lf.getCoverPath())
                                        .downloadOnly(tf);
                            }
                    return task;
                }
            });
        return t.isCompleted();
    }

    @Override
    public Task<SocialFeed> addFeed(String ficheId, JSONObject params, File imageFile) {

        String requestPath = String.format("%s/%s/%s", CONTROLLER, ficheId, CONTROLLER_SOCIAL_FEED);
        Log.d("ADD_FEED", imageFile.toURI().toString());

        return addMultipartRequest(requestPath, null, SocialFeed.class, params, imageFile).onSuccessTask(new Continuation<SocialFeed, Task<SocialFeed>>() {

            @Override
            public Task<SocialFeed> then(final Task<SocialFeed> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<String> flagFeed(String feedId) {

        String requestPath = String.format("%s/%s/%s", CONTROLLER_SOCIAL_FEED, feedId, CONTROLLER_FLAG);

        return addPutGenericRequest(requestPath, String.class).onSuccessTask(new Continuation<String, Task<String>>() {

            @Override
            public Task<String> then(final Task<String> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<SocialFeed[]> getFicheFeed(String ficheId) {
        String requestPath = String.format("%s/%s/%s", CONTROLLER, ficheId, CONTROLLER_SOCIAL_FEED);

        return addRequestArray(requestPath, SocialFeed[].class).onSuccessTask(new Continuation<SocialFeed[], Task<SocialFeed[]>>() {

            @Override
            public Task<SocialFeed[]> then(final Task<SocialFeed[]> task) throws Exception {
                return task;
            }
        });
    }

    private void putFicheInSpecificCache(String ficheId, Fiche fiche) {
        if (fiche instanceof FichePlace){
            fichePlaceCache.put(ficheId, (FichePlace)fiche);
        } else if (fiche instanceof FichePeople){
            fichePeopleCache.put(ficheId, (FichePeople)fiche);
        } else if (fiche instanceof FicheObject){
            ficheObjectCache.put(ficheId, (FicheObject)fiche);
        } else if (fiche instanceof FicheEvent){
            ficheEventCache.put(ficheId, (FicheEvent)fiche);
        } else if (fiche instanceof FicheMedia){
            ficheMediaCache.put(ficheId, (FicheMedia)fiche);
        }
    }

    private Fiche getFicheFromSpecificCache(String ficheId) {
        if(fichePlaceCache.get(ficheId) != null) {
            return (fichePlaceCache.get(ficheId));
        } else if (fichePeopleCache.get(ficheId) != null) {
            return fichePeopleCache.get(ficheId);
        } else if (ficheObjectCache.get(ficheId) != null) {
            return ficheObjectCache.get(ficheId);
        } else if (ficheEventCache.get(ficheId) != null) {
            return ficheEventCache.get(ficheId);
        } else if (ficheMediaCache.get(ficheId) != null) {
            return ficheMediaCache.get(ficheId);
        } else {
            return ficheCache.get(ficheId);
        }
    }

    private Fiche castFicheToSubclass(Fiche fiche) {
        if (fiche instanceof FichePlace){
            return (FichePlace)fiche;
        } else if (fiche instanceof FichePeople){
            return (FichePeople)fiche;
        } else if (fiche instanceof FicheObject){
            return (FicheObject)fiche;
        } else if (fiche instanceof FicheEvent){
            return (FicheEvent)fiche;
        } else {
            return (FicheMedia)fiche;
        }
    }

}
