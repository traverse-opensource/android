package ch.mobilethinking.traverse.api.listener;

import ch.mobilethinking.traverse.api.model.APIEntity;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public interface APIEntityListener<T extends APIEntity> {

    public void onSuccess(final T results);

    public void onFailure();
}