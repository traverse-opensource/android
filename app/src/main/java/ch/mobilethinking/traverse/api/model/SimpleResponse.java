package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by valentinkim on 11.05.17.
 */

@JsonIgnoreProperties({ "__v", "user"})
public class SimpleResponse extends MongoEntity {

    @JsonProperty("message")
    String message;

    public SimpleResponse() {}

    public SimpleResponse(String message) {
        setMessage(message);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return message;
    }
}
