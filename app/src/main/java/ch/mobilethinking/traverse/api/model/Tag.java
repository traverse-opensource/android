package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by valentinkim on 11.05.17.
 */

@JsonIgnoreProperties({ "__v" })
public class Tag extends MongoEntity {

    @JsonProperty("name")
    String name;

    @JsonProperty("count")
    Integer count;

    public Tag() {}

    //Same as for the user Model, since sometimes we only send the objectid from the backend
    public Tag(String tagId) {
        setId(tagId);
    }


    public Tag(String tagId, String name, Integer count) {
        setId(tagId);
        setName(name);
        setCount(count);
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Integer getCount() { return count; }

    public void setCount(Integer count) { this.count = count; }

    @Override
    public String toString() {
        return name;
    }
}
