package ch.mobilethinking.traverse.api.net;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.*;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ch.mobilethinking.traverse.BuildConfig;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class APIEntityRequestArray extends JsonArrayRequest {
    private static final String OUTPUT_FORMAT = ".json";

    /**
     * Creates a APIEntityRequest instance ready to be added to the request queue.
     * @param action - the method to call in the API
     * @param arguments - the arguments to the method formatted like <argument_value>/<argument_value>...
     * @param listener - the listener for the complete request
     * @param errorListener - the listener for errors during request
     */
    public APIEntityRequestArray(final String action, final String arguments,
                                 final Response.Listener<JSONArray> listener, final Response.ErrorListener errorListener) {
        super(
                Method.GET,
                BuildConfig.API_HOST + (TextUtils.isEmpty(BuildConfig.API_PORT ) ? "" : ":" + BuildConfig.API_PORT) + BuildConfig.API_VERSION + "/" + action + ((arguments!=null&&arguments!="")?"?" + (arguments):""),
                null,
                listener,
                errorListener);

    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }

        headers.put("x-mtsk", BuildConfig.API_KEY);

        return headers;
    }
}
