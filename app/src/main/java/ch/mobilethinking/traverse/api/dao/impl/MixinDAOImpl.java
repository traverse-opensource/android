package ch.mobilethinking.traverse.api.dao.impl;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.Utils.URLParamGenerator;
import ch.mobilethinking.traverse.api.dao.MixinDAO;
import ch.mobilethinking.traverse.api.dao.UserDAO;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MixinList;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class MixinDAOImpl extends AbstractAPIManagerImpl implements MixinDAO {
    private static final String MAP_MIXINS_CACHE = "map_mixins_cache";
    private static final String MIXINS_RESULT_CACHE = "mixins_result_cache";

    private static final String ITEM_LIST_CACHE = "item_list_cache";
    private static final String ITEM_LIST_RESULT_CACHE = "item_list_result_cache";

    private final DualCache<MixinList> ficheCache;

    public MixinDAOImpl(Context context) {
        super(context);

        //init map zone cache
        final CacheSerializer<MixinList> fichesJsonSerializer =
                new JsonSerializer<>(MixinList.class);

        ficheCache = new Builder<>(MAP_MIXINS_CACHE, FIX_APP_VERSION, MixinList.class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, fichesJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, fichesJsonSerializer, context)
                .build();

        Log.wtf("FDI", "file cache location> " + ficheCache.getDiskCacheFolder().getAbsolutePath());
    }

    @Override
    public Task<MixinItem[]> getRemoteHeartStrokes() {
        String requestPath = CONTROLLER_HEARTSTROKES;

        return addRequestArray(requestPath, MixinItem[].class).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<MixinItem[]> getHeartStrokes() {
        return getRemoteHeartStrokes();
    }

    @Override
    public Task<MixinItem[]> getRemoteSuggestions(String userId) {

        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s",
                UserDAO.CONTROLLER_SINGLE_USER, userId, CONTROLLER_SINGLE_USER_SUGGESTIONS);

        return addRequestArray(requestPath, MixinItem[].class).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {
                return task;
            }
        });

        /*return addRequest(requestPath, MixinList.class).onSuccessTask(new Continuation<MixinList, Task<MixinList>>() {
            @Override
            public Task<MixinList> then(Task<MixinList> task) throws Exception {
                Log.d("DAOImplUSer", "reached!");
                return task;
            }
        });*/
    }

    @Override
    public Task<MixinItem[]> getSuggestion(String userId) {
        return getRemoteSuggestions(userId);
    }

    @Override
    public Task<MixinItem[]> getRemoteSuggestions(Double latitude, Double longitude, @Nullable HashMap<String, ArrayList<Object>> arguments) {

        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s",
                UserDAO.CONTROLLER, UserDAO.CONTROLLER_SINGLE_USER, CONTROLLER_SINGLE_USER_SUGGESTIONS),
                args = "";
        String params = URLParamGenerator.generate(arguments);

        int radius = PreferenceManager.getSearchRadius(getContext());

        args = String.format(Locale.ENGLISH, "maxDistance=%d&lat=%f&lng=%f&page=1&limit=10000%s", radius, latitude, longitude, params);

        return addRequestArray(requestPath + "?" + args, MixinItem[].class).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {
                return task;
            }
        });

        /*return addRequest(requestPath, args, MixinList.class).onSuccessTask(new Continuation<MixinList, Task<MixinList>>() {
            @Override
            public Task<MixinList> then(Task<MixinList> task) throws Exception {
                Log.d("DAOImplUSer", "reached!");
                return task;
            }
        });*/
    }

    @Override
    public Task<MixinItem[]> getPaginatedSuggestions(Double latitude, Double longitude, int page, int limit, @Nullable HashMap<String, ArrayList<Object>> arguments) {

        String requestPath = String.format(Locale.ENGLISH, "%s/%s/%s",
                UserDAO.CONTROLLER, UserDAO.CONTROLLER_SINGLE_USER, CONTROLLER_SINGLE_USER_SUGGESTIONS),
                args = "";
        String params = URLParamGenerator.generate(arguments);

        int radius = PreferenceManager.getSearchRadius(getContext());

        args = String.format(Locale.ENGLISH, "maxDistance=%d&lat=%f&lng=%f&page=%d&limit=%d%s", radius, latitude, longitude, page, limit, params);

        return addRequestArray(requestPath + "?" + args, MixinItem[].class).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {
                return task;
            }
        });

        /*return addRequest(requestPath, args, MixinList.class).onSuccessTask(new Continuation<MixinList, Task<MixinList>>() {
            @Override
            public Task<MixinList> then(Task<MixinList> task) throws Exception {
                Log.d("DAOImplUSer", "reached!");
                return task;
            }
        });*/
    }

    @Override
    public Task<MixinItem[]> getSuggestion(Double latitude, Double longitude, @Nullable HashMap<String, ArrayList<Object>> arguments) { return getRemoteSuggestions(latitude, longitude, arguments); }
}
