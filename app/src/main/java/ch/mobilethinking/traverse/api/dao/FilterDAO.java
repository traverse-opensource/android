package ch.mobilethinking.traverse.api.dao;

import android.support.annotation.Nullable;

import bolts.Task;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;

/**
 * Created by carlosballester on 20.06.17.
 */

public interface FilterDAO {

    //String CONTROLLER = "";
    String CONTROLLER_FILTERS_TAGS = "tags";
    String CONTROLLER_FILTERS_CATEGORIES = "categories";
    String CONTROLLER_FILTERS_THEMES = "themes";
    String CONTROLLER_FILTERS_ALL = "mixins";

    String[] HASH_ARGUMENTS = {
        //list of tag ids
        "tags",
        //list of theme names
        "themes",
        //list of category ids
        "categories",
        //list of types (string can be found on Contants inside models), only those for fiche types
        "types",
        //boolean 0 for skip and 1 to include them
        "includesPlaylist"
    };

    Task<Tag[]> getTags();
    Task<Category[]> getCategories();
    Task<Theme[]> getThemes();

    Task<MixinItem[]> getAllResults(Double latitude, Double longitude, @Nullable FilterCreator filter);
    Task<MixinItem[]> getPaginatedResults(Double latitude, Double longitude, int page, int limit, @Nullable FilterCreator filter);
    Task<MixinItem[]> getPaginatedResultsNoRadius(Double latitude, Double longitude, int page, int limit, @Nullable FilterCreator filter);
}
