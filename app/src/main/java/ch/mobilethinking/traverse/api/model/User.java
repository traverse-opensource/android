package ch.mobilethinking.traverse.api.model;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ch.mobilethinking.traverse.api.RemoteHelper;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;

/**
 * Created by valentinkim on 11.05.17.
 */

@JsonIgnoreProperties({ "__v", "message" })
public class User extends MongoEntity {

    @JsonProperty("email")
    String email;

    //@JsonProperty("password")
    //String password;

    @JsonProperty("role")
    Role role;

    @JsonProperty("groups")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Group[] groups;

    //@JsonProperty("tokens")
    //String[] tokens;

    @JsonProperty("profile")
    UserProfile profile;

    @JsonProperty("social")
    UserSocial social;

    @JsonProperty("resetPasswordToken")
    String resetPasswordToken;

    @JsonProperty("resetPasswordExpires")
    Date resetPasswordExpires;

    @JsonProperty("favorites")
    String[] favorites;

    public User(){}

    //in case we only send the reference id from the backend
    public User(String id){
        setId(id);
    }

    public User(String id, String email, Role role, Group[] groups, UserProfile profile, String resetPasswordToken, Date resetPasswordExpires, String[] favorites){
        setId(id);
        setEmail(email);
        //setPassword(password);
        setRole(role);
        setGroups(groups);
        //setTokens(tokens);
        setProfile(profile);
        setResetPasswordToken(resetPasswordToken);
        setResetPasswordExpires(resetPasswordExpires);
        setFavorites(favorites);
    }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    /*public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }*/

    public Role getRole() { return role; }

    public void setRole(Role role) { this.role = role; }

    public Group[] getGroups() { return groups; }

    public void setGroups(Group[] groups) { this.groups = groups; }

    /*public String[] getTokens() { return tokens; }

    public void setTokens(String[] tokens) { this.tokens = tokens; }*/

    public UserProfile getProfile() { return profile; }

    public void setProfile(UserProfile profile) { this.profile = profile; }

    public String getResetPasswordToken() { return resetPasswordToken; }

    public void setResetPasswordToken(String resetPasswordToken) { this.resetPasswordToken = resetPasswordToken; }

    public Date getResetPasswordExpires() { return resetPasswordExpires; }

    public void setResetPasswordExpires(Date resetPasswordExpires) { this.resetPasswordExpires = resetPasswordExpires; }

    //method to seek attribute from inner classes
    public String getName(){
        return profile != null ?
                profile.getName(): null;
    }

    public String[] getFavorites() { return favorites; }

    public void setFavorites(String[] favorites) { this.favorites = favorites; }


    //don't knoy yet the type of google props, will ignore it
    @JsonIgnoreProperties({ "lastKnownLocation" })
    public static class UserProfile implements APIEntity {

        @JsonProperty("name")
        String name;

        //maybe an integer here
        @JsonProperty("gender")
        String gender;

        @JsonProperty("location")
        String location;

        @JsonProperty("website")
        String website;

        @JsonProperty("picture")
        String picture;

        public UserProfile() {
            super();
        }

        public UserProfile (String name, String gender, String location, String website, String picture) {
            setName(name);
            setGender(gender);
            setLocation(location);
            setWebsite(website);
            setPicture(picture);
        }

        public String getName() { return name; }

        public void setName(String name) { this.name = name; }

        public String getGender() { return gender; }

        public void setGender(String gender) { this.gender = gender; }

        public String getLocation() { return location; }

        public void setLocation(String location) { this.location = location; }

        public String getWebsite() { return website; }

        public void setWebsite(String website) { this.website = website; }

        public String getPicture() {
            return (picture != null && !TextUtils.isEmpty(picture)) ?
                    RemoteHelper.getRemotePath(picture):
                    null;
        }

        public void setPicture(String picture) { this.picture = picture; }

        @Override
        public String toString() {
            return "UserProfile{" +
                    "name='" + name + '\'' +
                    ", gender='" + gender + '\'' +
                    ", location='" + location + '\'' +
                    ", website='" + website + '\'' +
                    ", picture='" + picture + '\'' +
                    '}';
        }
    }

    public static class UserSocial implements APIEntity {

        @JsonProperty("userName")
        String userName;

        @JsonProperty("value")
        String value;

        @JsonProperty("kind")
        String kind;

        public UserSocial() {
        }

        public UserSocial(String userName, String value, String kind) {
            this.userName = userName;
            this.value = value;
            this.kind = kind;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getKind() {
            return kind;
        }

        public void setKind(String kind) {
            this.kind = kind;
        }

        @Override
        public String toString() {
            return "UserSocial{" +
                    "userName='" + userName + '\'' +
                    ", value='" + value + '\'' +
                    ", kind='" + kind + '\'' +
                    '}';
        }
    }

    @JsonIgnoreProperties({"index", "type"})
    class UserKnownLocation extends MongoEntity {

        @JsonProperty("coordinates")
        Double[] coordinates;

        public Double[] getCoordinates() { return coordinates; }

        public void setCoordinates(Double[] coordinates) { this.coordinates = coordinates; }
    }

    public static class UserList extends MongoEntity {

        @JsonProperty("users")
        List<User> users;

        public UserList(){ users = new ArrayList<>(); }

        public List<User> getUsers() {
            return users;
        }

        public void setUsers(List<User> fiches) {
            this.users = fiches;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                ", id='" + getId() + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                ", groups=" + Arrays.toString(groups) +
                ", profile=" + profile +
                ", social=" + social +
                ", resetPasswordToken='" + resetPasswordToken + '\'' +
                ", resetPasswordExpires=" + resetPasswordExpires +
                '}';
    }
}
