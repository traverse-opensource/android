package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Locale;

/**
 * Created by valentinkim on 26.06.17.
 */

@JsonIgnoreProperties({ "__v" })
public class Group extends MongoEntity{

    @JsonProperty("name")
    String name;
    @JsonProperty("description")
    String description;

    public Group() {
    }

    public Group(String id) {
        setId(id);
    }

    public Group(String id, String name, String description) {
        setId(id);
        setName(name);
        setDescription(description);
    }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String toString(){
        return String.format(Locale.FRENCH, "(%s, %s)", name, description);
    }
}
