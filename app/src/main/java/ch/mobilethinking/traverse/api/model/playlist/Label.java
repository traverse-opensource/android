package ch.mobilethinking.traverse.api.model.playlist;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

import ch.mobilethinking.traverse.api.model.APIEntity;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.MongoEntity;
import ch.mobilethinking.traverse.api.model.Tag;

/**
 * Created by valentinkim on 26.05.17.
 * Helper that handle labels on playlist deserialization
 */

public class Label extends MongoEntity{

    @JsonProperty("categories")
    Category[] categories;

    @JsonProperty("sousCategories")
    String[] sousCategories;

    @JsonProperty("tags")
    Tag[] tags;

    public Label() {}

    public Category[] getCategories() { return categories; }

    public void setCategories(Category[] categories) { this.categories = categories; }

    public String[] getSousCategories() { return sousCategories; }

    public void setSousCategories(String[] sousCategories) { this.sousCategories = sousCategories; }

    public Tag[] getTags() { return tags; }

    public void setTags(Tag[] tags) { this.tags = tags; }

    @Override
    public String toString() {
        return "Label{" +
                "categories=" + Arrays.toString(categories) +
                ", sousCategories=" + Arrays.toString(sousCategories) +
                ", tags=" + Arrays.toString(tags) +
                '}';
    }
}
