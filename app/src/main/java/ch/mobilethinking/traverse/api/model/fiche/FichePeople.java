package ch.mobilethinking.traverse.api.model.fiche;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.HashMap;

import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.SocialHolder;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.fiche.helper.FicheMap;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FichePeople extends Fiche {

    @JsonProperty("surname")
    String surname;

    @JsonProperty("birthday")
    String birthday;

    @JsonProperty("deathday")
    String deathday;

    @JsonProperty("place_of_birth")
    String place_of_birth;

    @JsonProperty("date")
    //FicheDate date;
    Date date;

    @JsonProperty("delta_start")
    Integer deltaStart;

    @JsonProperty("delta_end")
    Integer deltaEnd;

    public FichePeople(){ super(); }

    public FichePeople(String id) {
        super(id);
    }

    @JsonCreator
    public FichePeople(
            @JsonProperty("type") String type, @JsonProperty("_id") String id, @JsonProperty("name") String name,
            @JsonProperty("created_at") Date created_at, @JsonProperty("last_updated_at") Date last_updated_at, @JsonProperty("status") Integer status,
            @JsonProperty("cover") HashMap<String, String> cover, @JsonProperty("created_by") User created_by, @JsonProperty("last_update_by") User last_update_by,
            @JsonProperty("theme") Theme mainTheme, @JsonProperty("slug") String slug, @JsonProperty("userIds") String[] userIds,
            @JsonProperty("__t") String __t, @JsonProperty("short_description") String short_description, @JsonProperty("long_description") String long_description,
            @JsonProperty("related_fiches") Fiche[] relatedFiches, @JsonProperty("categories") Category[] categories,
            @JsonProperty("themes") Theme[] themes, @JsonProperty("sousCategories") String[] subCategories, @JsonProperty("tags") Tag[] tags,
            @JsonProperty("references") String references, @JsonProperty("social") SocialHolder social, @JsonProperty("city") String city,
            @JsonProperty("country") String country, @JsonProperty("latitude") Double latitude, @JsonProperty("longitude") Double longitude,
            @JsonProperty("location") HashMap<String, Object> location, @JsonProperty("map") FicheMap map,
            //custom
            @JsonProperty("surname") String surname, @JsonProperty("birthday") String birthday, @JsonProperty("deathday") String deathday,
            @JsonProperty("place_of_birth") String place_of_birth, @JsonProperty("date") Date date, @JsonProperty("delta_start") Integer deltaStart, @JsonProperty("delta_end") Integer deltaEnd
    ) {
        super(type, id, name, created_at, last_updated_at, status, cover, created_by, last_update_by, mainTheme, slug, userIds, __t, short_description, long_description, relatedFiches, categories,
                themes, subCategories, tags, references, social, city, country, latitude, longitude, location, map);
        setSurname(surname);
        setBirthday(birthday);
        setDeathday(deathday);
        setPlace_of_birth(place_of_birth);
        setDate(date);
        setDeltaStart(deltaStart);
        setDeltaEnd(deltaEnd);
    }

    public String getSurname() { return surname; }

    public void setSurname(String surname) { this.surname = surname; }

    public String getBirthday() { return birthday; }

    public void setBirthday(String birthday) { this.birthday = birthday; }

    public String getDeathday() { return deathday; }

    public void setDeathday(String deathday) { this.deathday = deathday; }

    public String getPlace_of_birth() { return place_of_birth; }

    public void setPlace_of_birth(String place_of_birth) { this.place_of_birth = place_of_birth; }

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public Integer getDeltaStart() { return deltaStart; }

    public void setDeltaStart(Integer deltaStart) { this.deltaStart = deltaStart; }

    public Integer getDeltaEnd() { return deltaEnd; }

    public void setDeltaEnd(Integer deltaEnd) { this.deltaEnd = deltaEnd; }

    @Override
    public String toString() {
        return super.toString() + " ### FichePeople{" +
                "surname='" + surname + '\'' +
                ", birthday='" + birthday + '\'' +
                ", deathday='" + deathday + '\'' +
                ", place_of_birth='" + place_of_birth + '\'' +
                ", date=" + date +
                '}';
    }
}
