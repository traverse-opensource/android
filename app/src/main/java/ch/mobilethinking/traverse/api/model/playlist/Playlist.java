package ch.mobilethinking.traverse.api.model.playlist;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ch.mobilethinking.traverse.api.model.LinkedFiche;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MongoEntity;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.User;

/**
 * Created by valentinkim on 11.05.17.
 */

@JsonIgnoreProperties({ "__v"})
public class Playlist extends MixinItem {

    @JsonProperty("description")
    String description;

    @JsonProperty("linkedFiches")
    LinkedFiche[] linkedFiches;

    @JsonProperty("labels")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    Label labels;

    public Playlist() { super(); }

    public Playlist(String id) { super(id); }

    @JsonCreator
    public Playlist(
            @JsonProperty("type") String type, @JsonProperty("_id") String id, @JsonProperty("name") String name,
            @JsonProperty("created_at") Date created_at, @JsonProperty("last_updated_at") Date last_updated_at, @JsonProperty("status") Integer status,
            @JsonProperty("cover") HashMap<String, String> cover, @JsonProperty("created_by") User created_by, @JsonProperty("last_update_by") User last_update_by,
            @JsonProperty("theme") Theme mainTheme, @JsonProperty("slug") String slug, @JsonProperty("userIds") String[] userIds,
            //custom
            @JsonProperty("description") String description, @JsonProperty("linkedFiches") LinkedFiche[] linkedFiches, @JsonProperty("labels") Label labels) {
        super(type, id, name, created_at, last_updated_at, status, cover, created_by, last_update_by, mainTheme, slug, userIds);
        setDescription(description);
        setLinkedFiches(linkedFiches);
        setLabels(labels);
    }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public LinkedFiche[] getLinkedFiches() { return linkedFiches; }

    public void setLinkedFiches(LinkedFiche[] linkedFiches) { this.linkedFiches = linkedFiches; }

    public Label getLabels() { return labels; }

    public void setLabels(Label label) { this.labels = label; }

    public static class PlaylistList extends MongoEntity {

        @JsonProperty("playlists")
        List<Playlist> playlists;

        public List<Playlist> getPlaylists() {
            return playlists;
        }

        public void setPlaylists(List<Playlist> fiches) {
            this.playlists = fiches;
        }
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "name='" + getName() + '\'' +
                ", description='" + description + '\'' +
                ", created_at=" + getCreated_at() +
                ", last_updated_at='" + getLast_updated_at() + '\'' +
                ", status=" + getStatus() +
                ", cover=" + getCoverPath() +
                ", created_by=" + getCreated_by() +
                ", last_update_by=" + getLast_update_by() +
                ", linkedFiches=" + Arrays.toString(linkedFiches) +
                '}';
    }
}
