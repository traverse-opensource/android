package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by valentinkim on 11.05.17.
 */

public class SocialHolder extends MongoEntity {

    @JsonProperty("web")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    WebHolder web;

    @JsonProperty("facebook")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    TagAndWebHolder facebook;

    @JsonProperty("twitter")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    TagAndWebHolder twitter;

    @JsonProperty("instagram")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    TagAndWebHolder instagram;

    public WebHolder getWeb() { return web; }

    public void setWeb(WebHolder web) { this.web = web; }

    public TagAndWebHolder getFacebook() { return facebook; }

    public void setFacebook(TagAndWebHolder facebook) { this.facebook = facebook; }

    public TagAndWebHolder getTwitter() { return twitter; }

    public void setTwitter(TagAndWebHolder twitter) { this.twitter = twitter; }

    public TagAndWebHolder getInstagram() { return instagram; }

    public void setInstagram(TagAndWebHolder instagram) { this.instagram = instagram; }

    public String toString(){
        return String.format(
                "{web: %s, facebook: %s, twitter: %s, instagram: %s}",
                web != null ? web.toString(): null,
                facebook != null ? facebook.toString(): null,
                twitter != null ? twitter.toString(): null,
                instagram != null ? instagram.toString(): null);
    }

    public static class WebHolder extends MongoEntity {

        @JsonProperty("link")
        String link;

        public WebHolder() {}

        public WebHolder (String link) {
            setLink(link);
        }

        public String getLink() { return link; }

        public String toString() {
            return String.format("<link: %s>", link);
        }

        public void setLink(String link) { this.link = link; }
    }

    public static class TagAndWebHolder extends WebHolder {

        @JsonProperty("tags")
        SocialTag[] tags;

        public TagAndWebHolder() {}

        public TagAndWebHolder(String link, SocialTag[] tags) {
            setLink(link);
            setTags(tags);
        }

        public SocialTag[] getTags() { return tags; }

        public String getTagsAsString() {
            String toReturn = "";
            if (tags != null){
                for (int i = 0, len = tags.length; i < len; ++i) {
                    toReturn += tags[i] + (i == len - 1 ? "": ", ");
                }
            }

            return toReturn;
        }

        public String toString() {
            return String.format("<link: %s; tags: %s>", link, getTagsAsString());
        }

        public void setTags(SocialTag[] tags) { this.tags = tags; }
    }

    //Innner classes should be defined as static ... -_- stupid mistake
    @JsonIgnoreProperties({ "label",  "className"})
    static class SocialTag implements APIEntity {

        @JsonProperty("value")
        String value;

        public SocialTag() {}

        public SocialTag(String value) {
            setValue(value);
        }

        public String getValue() { return value; }

        public String toString() {
            return getValue();
        }

        public void setValue(String value) { this.value = value; }
    }
}
