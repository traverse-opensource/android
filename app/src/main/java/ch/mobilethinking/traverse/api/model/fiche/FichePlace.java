package ch.mobilethinking.traverse.api.model.fiche;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.HashMap;

import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.SocialHolder;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.fiche.helper.FicheMap;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FichePlace extends Fiche {

    @JsonProperty("technical_information")
    String technical_information;

    @JsonProperty("more_information")
    String more_information;

    @JsonProperty("schedule")
    String schedule;

    @JsonProperty("accessibility")
    String accessibility;

    @JsonProperty("contact")
    String contact;

    public FichePlace() {
        super();
    }

    public FichePlace(String id) {
        super(id);
    }

    @JsonCreator
    public FichePlace(
            @JsonProperty("type") String type, @JsonProperty("_id") String id, @JsonProperty("name") String name,
            @JsonProperty("created_at") Date created_at, @JsonProperty("last_updated_at") Date last_updated_at, @JsonProperty("status") Integer status,
            @JsonProperty("cover") HashMap<String, String> cover, @JsonProperty("created_by") User created_by, @JsonProperty("last_update_by") User last_update_by,
            @JsonProperty("theme") Theme mainTheme, @JsonProperty("slug") String slug, @JsonProperty("userIds") String[] userIds,
            @JsonProperty("__t") String __t, @JsonProperty("short_description") String short_description, @JsonProperty("long_description") String long_description,
            @JsonProperty("related_fiches") Fiche[] relatedFiches, @JsonProperty("categories") Category[] categories,
            @JsonProperty("themes") Theme[] themes, @JsonProperty("sousCategories") String[] subCategories, @JsonProperty("tags") Tag[] tags,
            @JsonProperty("references") String references, @JsonProperty("social") SocialHolder social, @JsonProperty("city") String city,
            @JsonProperty("country") String country, @JsonProperty("latitude") Double latitude, @JsonProperty("longitude") Double longitude,
            @JsonProperty("location") HashMap<String, Object> location, @JsonProperty("map") FicheMap map,
            //custom
            @JsonProperty("technical_information") String technical_information, @JsonProperty("more_information") String more_information, @JsonProperty("schedule") String schedule,
            @JsonProperty("accessibility") String accessibility, @JsonProperty("contact") String contact
    ) {
        super(type, id, name, created_at, last_updated_at, status, cover, created_by, last_update_by, mainTheme, slug, userIds, __t, short_description, long_description, relatedFiches, categories,
                themes, subCategories, tags, references, social, city, country, latitude, longitude, location, map);
        setTechnical_information(technical_information);
        setMore_information(more_information);
        setSchedule(schedule);
        setAccessibility(accessibility);
        setContact(contact);
    }

    public String getTechnical_information() { return technical_information; }

    public void setTechnical_information(String technical_information) { this.technical_information = technical_information; }

    public String getMore_information() { return more_information; }

    public void setMore_information(String more_information) { this.more_information = more_information; }

    public String getSchedule() { return schedule; }

    public void setSchedule(String schedule) { this.schedule = schedule; }

    public String getAccessibility() { return accessibility; }

    public void setAccessibility(String accessibility) { this.accessibility = accessibility; }

    public String getContact() { return contact; }

    public void setContact(String contact) { this.contact = contact; }

    @Override
    public String toString() {
        return super.toString() + " ### FichePlace{" +
                ", technical_information='" + technical_information + '\'' +
                ", more_information='" + more_information + '\'' +
                ", schedule='" + schedule + '\'' +
                ", accessibility='" + accessibility + '\'' +
                ", contact='" + contact + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", location=" + location +
                ", map=" + map +
                '}';
    }
}
