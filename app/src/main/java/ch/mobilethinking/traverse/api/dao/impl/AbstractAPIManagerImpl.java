package ch.mobilethinking.traverse.api.dao.impl;

import android.content.Context;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import bolts.Task;
import bolts.TaskCompletionSource;
import ch.mobilethinking.traverse.api.model.APIEntity;
import ch.mobilethinking.traverse.api.net.APIEntityRequest;
import ch.mobilethinking.traverse.api.net.APIEntityRequestArray;
import ch.mobilethinking.traverse.api.net.APIEntityRequestAsync;
import ch.mobilethinking.traverse.api.net.GenericRequestAsync;
import ch.mobilethinking.traverse.api.net.MultipartRequest;
import ch.mobilethinking.traverse.api.net.RequestQueueFactory;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
abstract class AbstractAPIManagerImpl {
    static final int FIX_APP_VERSION = 0;
    static final int RAM_MAX_SIZE = 50 * 1024 * 1024;
    static final int DISK_MAX_SIZE = 50 * 1024 * 1024;
    protected final Context context;
    private final RequestQueue requestQueue;
    private final ObjectMapper objectMapper;

    AbstractAPIManagerImpl(final Context context) {
        this.context = context;
        this.requestQueue = RequestQueueFactory.getRequestQueue(context);
        this.objectMapper = new ObjectMapper();
    }

    //json object handling
    <T> Task<T> addRequest(final String url, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final APIEntityRequest req =
                new APIEntityRequest(Request.Method.GET, url, null, null, createSuccessListener(tcs, resultClass), createErrorListener(tcs));
        Log.d("Volley", ""+req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }

    //json object handling
    <T> Task<T> addPutGenericRequest(final String url, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final GenericRequestAsync req =
                new GenericRequestAsync<>(Request.Method.PUT, url, null, resultClass, createSuccessListenerAsync(tcs), createErrorListener(tcs));
        Log.d("Volley", ""+req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }

    //json array handling
    <T> Task<T> addRequestArray(final String url, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final APIEntityRequestArray req = new APIEntityRequestArray(url, null, createSuccessListenerForArray(tcs, resultClass), createErrorListener(tcs));
        Log.d("Volley", ""+req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }


    <T extends APIEntity> Task<T> addPostRequest(final String action, final String arguments, final JSONObject body, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final APIEntityRequest req =
                new APIEntityRequest(Request.Method.POST, action, arguments, body, createSuccessListener(tcs, resultClass), createErrorListener(tcs));
        Log.d("Volley", req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }

    <T extends APIEntity> Task<T> addRequest(final String action, final String arguments, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final APIEntityRequest req =
                new APIEntityRequest(Request.Method.GET, action, arguments, null, createSuccessListener(tcs, resultClass), createErrorListener(tcs));
        Log.d("Volley", req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }

    <T extends APIEntity> Task<T> addDeleteRequest(final String action, final String arguments, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final APIEntityRequest req =
                new APIEntityRequest(Request.Method.DELETE, action, arguments, null, createSuccessListener(tcs, resultClass), createErrorListener(tcs));
        Log.d("Volley", req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }

    <T extends APIEntity> Task<T> addPutRequest(final String action, final String arguments, final JSONObject body, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final APIEntityRequest req =
                new APIEntityRequest(Request.Method.PUT, action, arguments, body, createSuccessListener(tcs, resultClass), createErrorListener(tcs));
        Log.d("Volley", req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }

    <T extends APIEntity> Task<T> addMultipartRequest(final String action, final String arguments, final Class<T> resultClass, final JSONObject params, final File imageFile) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final MultipartRequest req = new MultipartRequest(action, arguments, resultClass, params, imageFile, createSuccessListenerAsync(tcs), createErrorListener(tcs));
        Log.d("Volley", req.toString());
        req.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(req);
        return tcs.getTask();
    }

    <T extends APIEntity> Task<T> addRequestAsync(final String action, final String arguments, final Class<T> resultClass) {
        final TaskCompletionSource<T> tcs = new TaskCompletionSource<>();
        final APIEntityRequestAsync req =
                new APIEntityRequestAsync<>(Request.Method.GET, action, arguments, resultClass, createSuccessListenerAsync(tcs), createErrorListener(tcs));
        Log.d("Volley", req.toString());
        requestQueue.add(req);
        return tcs.getTask();
    }

    private <T> Response.Listener<JSONObject> createSuccessListener(final TaskCompletionSource<T> tcs, final Class<T> resultClass) {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {
                try {
                    final T objResponse = (T) objectMapper.readValue(response.toString(), resultClass);
                    tcs.setResult(objResponse);
                } catch (final IOException e) {
                    tcs.setError(e);
                }
            }
        };
    }

    private <T> Response.Listener<T> createSuccessListenerAsync(final TaskCompletionSource<T> tcs) {
        return new Response.Listener<T>() {
            @Override
            public void onResponse(final T response) {
                tcs.trySetResult(response);
            }
        };
    }

    private <T> Response.Listener<JSONArray> createSuccessListenerForArray(final TaskCompletionSource<T> tcs, final Class<T> resultClass) {
        return new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(final JSONArray response) {
                try {
                    final T objResponse = (T) objectMapper.readValue(response.toString(), resultClass);
                    tcs.setResult(objResponse);
                } catch (final IOException e) {
                    tcs.setError(e);
                }
            }
        };
    }

    private Response.Listener<String> createSuccessListener(final TaskCompletionSource<String> tcs) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                tcs.setResult(response.toString());
            }
        };
    }

    private <T> Response.ErrorListener createErrorListener(final TaskCompletionSource<T> tcs) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {

                tcs.setError(error);
            }
        };
    }

    Context getContext () {
        return this.context;
    }
}
