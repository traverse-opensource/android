package ch.mobilethinking.traverse.api.model.deserializer;

import android.util.Log;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;

import java.io.IOException;

import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.fiche.FicheEvent;
import ch.mobilethinking.traverse.api.model.fiche.FicheMedia;
import ch.mobilethinking.traverse.api.model.fiche.FicheObject;
import ch.mobilethinking.traverse.api.model.fiche.FichePeople;
import ch.mobilethinking.traverse.api.model.fiche.FichePlace;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FicheDeserializer extends StdDeserializer<Fiche> {

    //these types will be use to instanciate the Fiche child on runtime based on its __t prop

    //cannot use switch statement since I cannot ensure in my computer which jdk is used....
    static final class TYPES{
        static final String PLACES = "Places";
        static final String PEOPLE = "People";
        static final String MEDIA = "Media";
        static final String OBJECTS = "Objects";
        static final String EVENTS = "Events";
    }

    public FicheDeserializer() {
        this(null);
    }

    public FicheDeserializer(Class<?> vc) {
        super(vc);
    }

    public String test(String yolo){
        String aux = yolo.substring(0, 3);
        return String.format("%c%c", aux.charAt(0), aux.charAt(1));
    }

    @Override
    public Fiche deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {
        JsonNode node = jp.getCodec().readTree(jp);

        //Log.wtf("FD", node.get("__t").toString());

        //String type = node.get("__t").toString();

        //String type = new ObjectMapper().writeValueAsString(node.get("__t"));
        String type = node.get("__t").textValue();

        Fiche toReturn = null;

        try {

            Log.wtf("Fd0", type);

            //Log.wtf("Fd", "test" + (type == TYPES.PLACE) );

            //This fucking string switch does not fucking work!!!!!

            Log.wtf("Fd1", String.valueOf(type.charAt(0) + type.charAt(1)));
            Log.wtf("Fd2", "test> " + String.valueOf(type));
            Log.wtf("Fd3", "test> " + test(type));

            /*if (type.charAt(0) == 'P' && type.charAt(1) == 'l') {
                Log.wtf("Fd", "PL");
                return handlePlace(jp);
            }else if (type.charAt(0) == 'P' && type.charAt(1) == 'e') {
                Log.wtf("Fd", "PE");
                return handlePeople(jp);
            }else if (type.charAt(0) == 'M' && type.charAt(1) == 'e') {
                Log.wtf("Fd", "ME");
                return handleMedia(jp);
            }else if (type.charAt(0) == 'O' && type.charAt(1) == 'b') {
                Log.wtf("Fd", "OB");
                return handleObject(jp);
            }else if (type.charAt(0) == 'E' && type.charAt(1) == 'v') {
                Log.wtf("Fd", "EV");
                return handleEvent(jp);
            }

            return null;*/

            String jsonString = node.toString();

            Log.wtf("FDJ> ", jsonString);

            switch (type) {
                case TYPES.PLACES:
                    Log.wtf("Fd", "PL");
                    toReturn = handlePlace(jsonString);
                case TYPES.PEOPLE:
                    Log.wtf("Fd", "PE");
                    toReturn = handlePeople(jsonString);
                case TYPES.MEDIA:
                    Log.wtf("Fd", "ME");
                    toReturn = handleMedia(jsonString);
                case TYPES.OBJECTS:
                    Log.wtf("Fd", "OB");
                    toReturn = handleObject(jsonString);
                case TYPES.EVENTS:
                    Log.wtf("Fd", "EV");
                    toReturn = handleEvent(jsonString);
                default :
                    toReturn = null;
            }

            Log.wtf("finally", "ret> " + toReturn);
        }catch(Exception e){
            Log.w("FD-EX", e.getMessage());
            return toReturn;
            //throw  e;
        }

        return toReturn;

        /*int id = (Integer) ((IntNode) node.get("id")).numberValue();
        String itemName = node.get("itemName").asText();
        int userId = (Integer) ((IntNode) node.get("createdBy")).numberValue();



        return new Item(id, itemName, new User(userId, null));*/
    }

    private FichePlace handlePlace(String json) throws IOException {
        return new ObjectMapper().readValue(json, FichePlace.class);
    }

    private FichePeople handlePeople(String json) throws IOException {
        return new ObjectMapper().readValue(json, FichePeople.class);
    }

    private FicheMedia handleMedia(String json) throws IOException {
        return new ObjectMapper().readValue(json, FicheMedia.class);
    }

    private FicheObject handleObject(String json) throws IOException {
        return new ObjectMapper().readValue(json, FicheObject.class);
    }

    private FicheEvent handleEvent(String json) throws IOException {
        return new ObjectMapper().readValue(json, FicheEvent.class);
    }
}
