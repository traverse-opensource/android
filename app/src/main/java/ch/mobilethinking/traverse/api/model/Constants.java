package ch.mobilethinking.traverse.api.model;

/**
 * Created by valentinkim on 30.06.17.
 * Holds constants for models
 */

public class Constants {

    public static final String TYPE_FICHE = "fiche";
    public static final String TYPE_PLAYLIST = "playlist";
    //sub types
    public static final String TYPE_FICHE_EVENT = "Events";
    public static final String TYPE_FICHE_MEDIA = "Media";
    public static final String TYPE_FICHE_OBJECT = "Objects";
    public static final String TYPE_FICHE_PLACE = "Places";
    public static final String TYPE_FICHE_PERSON = "People";
}
