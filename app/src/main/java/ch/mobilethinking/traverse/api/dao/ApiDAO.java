package ch.mobilethinking.traverse.api.dao;

import android.content.Context;

import ch.mobilethinking.traverse.api.dao.impl.FicheDAOImpl;
import ch.mobilethinking.traverse.api.dao.impl.FilterDAOImpl;
import ch.mobilethinking.traverse.api.dao.impl.MixinDAOImpl;
import ch.mobilethinking.traverse.api.dao.impl.PlaylistDAOImpl;
import ch.mobilethinking.traverse.api.dao.impl.UserDAOImpl;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public final class ApiDAO {

    private static FicheDAO ficheDAO;
    private static PlaylistDAO playlistDAO;
    private static UserDAO userDAO;
    private static MixinDAO mixinDAO;
    private static FilterDAO filterDAO;


    public static FicheDAO getFicheDAO(Context context) {
        if (ficheDAO == null) {
            ficheDAO = new FicheDAOImpl(context);
        }

        return  ficheDAO;
    }

    public static PlaylistDAO getPlaylistDAO(Context context) {
        if (playlistDAO == null) {
            playlistDAO = new PlaylistDAOImpl(context);
        }

        return  playlistDAO;
    }
    public static UserDAO getUserDAO(Context context) {
        if (userDAO == null) {
            userDAO = new UserDAOImpl(context);
        }

        return  userDAO;
    }

    public static MixinDAO getMixinDAO(Context context) {
        if (mixinDAO == null) {
            mixinDAO = new MixinDAOImpl(context);
        }

        return mixinDAO;
    }

    public static FilterDAO getFilterDAO(Context context) {
        if (filterDAO == null) {
            filterDAO = new FilterDAOImpl(context);
        }

        return filterDAO;
    }


    /*private static ProjectDAO projectDAO;
    private static TourDAO tourDAO;
    private static ZoneDAO zoneDAO;
    private static StaticPageDAO staticPageDAO;
    private static AgendaDAO agendaDAO;
    private static GuestbookDAO guestbookDAO;*/

    /*public static ProjectDAO getProjectDAO(Context context) {
        return ApiDAO.getProjectDAO(context, false);
    }
    public static TourDAO getTourDAO(Context context) {
        return ApiDAO.getTourDAO(context, false);
    }
    public static ZoneDAO getZoneDAO(Context context) {
        return ApiDAO.getZoneDAO(context, false);
    }
    public static StaticPageDAO getStaticPageDAO(Context context) {
        return ApiDAO.getStaticPageDAO(context, false);
    }*/
}

