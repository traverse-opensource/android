package ch.mobilethinking.traverse.api.dao;

import android.support.annotation.Nullable;

import org.json.JSONObject;

import bolts.Task;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 11.05.17.
 */

public interface PlaylistDAO {

    String CONTROLLER = "playlists";
    String CONTROLLER_ADD_LINK = "addLink";
    String CONTROLLER_FULL_UPDATE = "fullUpdate";
    String CONTROLLER_ASSOCIATED_PLAYLISTS = "playlists";
    String CONTROLLER_DELETE_LINK = "deleteFiche";
    String CONTROLLER_SWAP = "swap";
    String CONTROLLER_LIKE = "like";
    String CONTROLLER_DISLIKE = "dislike";

    Task<Playlist> getRemotePlaylist(String playlistId);
    Task<Playlist> getPlaylist(String playlistId);
    Task<Playlist.PlaylistList> getRemotePlaylists();
    Task<Playlist.PlaylistList> getPlaylists();
    Task<Playlist.PlaylistList> getRemoteAssociatedPlaylists(String ficheId);
    Task<Playlist.PlaylistList> getAssociatedPlaylists(String ficheId);
    Task<Playlist> addLinkedFiche(String playlistId, String ficheId, @Nullable String linkValue);
    Task<Playlist> addPlaylist(JSONObject playlistInfo);
    Task<Playlist> updatePlaylist(JSONObject playlistInfo, String playlistId);
    Task<SimpleResponse> deletePlaylist(String playlistId);
    Task<Playlist> swapFiches(String playlistId, JSONObject linkedFiches);
    Task<Playlist> deleteLinkedFiche(String playlistId, String ficheId);
    Task<Playlist> likePlaylist(String playlistId, String userId);
    Task<Playlist> dislikePlaylist(String playlistId, String userId);
    Boolean saveFavouritePlaylists();
    Boolean savePlaylist(String playlistId);
}
