package ch.mobilethinking.traverse.api.manager;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;

import bolts.Task;
import ch.mobilethinking.traverse.api.dao.ApiDAO;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.User;

/**
 * Created by valentinkim on 11.05.17.
 */

public class MixinManager {

    private final Context context;

    MixinManager(Context context) {
        this.context = context;
    }

    public Task<MixinItem[]> getHeartStrokes() {
        return ApiDAO.getMixinDAO(context).getHeartStrokes();
    }

    public Task<MixinItem[]> getSuggestion(String userId) {
        return ApiDAO.getMixinDAO(context).getSuggestion(userId);
    }

    public Task<MixinItem[]> getSuggestion(Double latitude, Double longitude, @Nullable HashMap<String, ArrayList<Object>> arguments) {
        return ApiDAO.getMixinDAO(context).getSuggestion(latitude, longitude, arguments);
    }

    public Task<MixinItem[]> getPaginatedSuggestions(Double latitude, Double longitude, int page, int limit, @Nullable HashMap<String, ArrayList<Object>> arguments) {
        return ApiDAO.getMixinDAO(context).getPaginatedSuggestions(latitude, longitude, page, limit, arguments);
    }
}