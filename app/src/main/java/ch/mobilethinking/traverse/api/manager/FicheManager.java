package ch.mobilethinking.traverse.api.manager;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import bolts.Task;
import ch.mobilethinking.traverse.api.dao.ApiDAO;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.SocialFeed;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FicheManager {

    private final Context context;

    FicheManager(Context context) {
        this.context = context;
    }

    public Task<Fiche> getFiche(String ficheId) { return ApiDAO.getFicheDAO(context).getFiche(ficheId); }

    public Task<Fiche.FicheList> getFiches() {
        return ApiDAO.getFicheDAO(context).getFiches();
    }

    public Task<Playlist.PlaylistList> getFichePlaylists(String ficheId) {
        return ApiDAO.getFicheDAO(context).getFichePlaylists(ficheId);
    }

    public Task<Fiche> likeFiche(String ficheId, String userId) {
        return ApiDAO.getFicheDAO(context).likeFiche(ficheId, userId);
    }

    public Task<Fiche> dislikeFiche(String ficheId, String userId) {
        return ApiDAO.getFicheDAO(context).dislikeFiche(ficheId, userId);
    }

    public Boolean saveFiche(String ficheId) {
        return ApiDAO.getFicheDAO(context).saveFiche(ficheId);
    }

    public Task<SocialFeed[]> getFicheFeed (String ficheId) {
        return ApiDAO.getFicheDAO(context).getFicheFeed(ficheId);
    }

    public Task<SocialFeed> addFeed (String ficheId, JSONObject params, File imageFile) {
        return ApiDAO.getFicheDAO(context).addFeed(ficheId, params, imageFile);
    }

    public Task<String> flagFeed (String feedId) {
        return ApiDAO.getFicheDAO(context).flagFeed(feedId);
    }

}