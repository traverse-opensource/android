package ch.mobilethinking.traverse.api.model.fiche.helper;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

import ch.mobilethinking.traverse.api.model.MongoEntity;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FicheMap extends MongoEntity {

    @JsonProperty("latLng")
    LatLng latLng;

    @JsonProperty("country")
    String country;

    @JsonProperty("fullAddrStr")
    String fullAddrStr;

    //used to communicated with google apis, unique placeId
    @JsonProperty("placeId")
    String placeId;

    public LatLng getLatLng() { return latLng; }

    public void setLatLng(LatLng latLng) { this.latLng = latLng; }

    public String getCountry() { return country; }

    public void setCountry(String country) { this.country = country; }

    public String getFullAddrStr() { return fullAddrStr; }

    public void setFullAddrStr(String fullAddrStr) { this.fullAddrStr = fullAddrStr; }

    public String getPlaceId() { return placeId; }

    public void setPlaceId(String placeId) { this.placeId = placeId; }

    //Fixes serializable exception when pausing fragments and saving the Fiche to the bundle
    public static class LatLng implements Serializable {

        @JsonProperty("lng")
        Double lng;

        @JsonProperty("lat")
        Double lat;

        public LatLng () {

        }

        public LatLng (Double lat, Double lng) {
            this.lat = lat;
            this.lng = lng;
        }

        public Double getLng() {
            return lng;
        }

        public void setLng(Double lng) {
            this.lng = lng;
        }

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }
    }
}
