package ch.mobilethinking.traverse.api.dao;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;

import bolts.Task;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;

/**
 * Created by valentinkim on 11.05.17.
 */
//since there will be a lot of mixin list, it is easier to find them if we split it into a new manager
public interface MixinDAO {

    //String CONTROLLER = "";
    String CONTROLLER_HEARTSTROKES = "heartstrokes";
    String CONTROLLER_SINGLE_USER_SUGGESTIONS = "suggestions";

    String PARAM_DISTANCE = "distances";
    String PARAM_DISTANCE_MIN = "min";
    String PARAM_DISTANCE_MAX = "max";
    String PARAM_POSITION = "position";
    String PARAM_POSITION_LATITUDE = "lat";
    String PARAM_POSITION_LONGITUDE = "lng";

    Task<MixinItem[]> getRemoteHeartStrokes();
    Task<MixinItem[]> getHeartStrokes();
    Task<MixinItem[]> getRemoteSuggestions(String userId);
    Task<MixinItem[]> getSuggestion(String userId);
    Task<MixinItem[]> getRemoteSuggestions(Double latitude, Double longitude, @Nullable HashMap<String, ArrayList<Object>> arguments);
    Task<MixinItem[]> getPaginatedSuggestions(Double latitude, Double longitude, int page, int limit, @Nullable HashMap<String, ArrayList<Object>> arguments);
    Task<MixinItem[]> getSuggestion(Double latitude, Double longitude, @Nullable HashMap<String, ArrayList<Object>> arguments);
}
