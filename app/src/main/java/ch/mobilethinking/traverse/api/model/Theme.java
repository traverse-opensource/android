package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by valentinkim on 09.05.17.
 */

public class Theme extends MongoEntity{

    @JsonProperty("color")
    String hexColor;

    @JsonProperty("ponderation")
    Integer ponderation;

    @JsonProperty("description")
    String description;

    @JsonProperty("name")
    String name;

    public String getHexColor() { return hexColor; }

    public int getColor() {
        //since we want to get rid of the first # character that comes from the database
        String hex = hexColor.substring(1);
        return Integer.parseInt(hex, 16);
    }

    public void setHexColor(String hexColor) { this.hexColor = hexColor; }

    public Integer getPonderation() { return ponderation; }

    public void setPonderation(Integer ponderation) { this.ponderation = ponderation; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    @Override
    public String toString() {
        return name;
    }
}
