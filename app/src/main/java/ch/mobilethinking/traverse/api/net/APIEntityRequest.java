package ch.mobilethinking.traverse.api.net;

import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import ch.mobilethinking.traverse.BuildConfig;
import ch.mobilethinking.traverse.api.manager.APIManager;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class APIEntityRequest extends JsonObjectRequest {
    private static final String OUTPUT_FORMAT = ".json";
    private final ObjectMapper objectMapper;

    /**
     * Creates a APIEntityRequest instance ready to be added to the request queue.
     * @param action - the method to call in the API
     * @param arguments - the arguments to the method formatted like <argument_value>/<argument_value>...
     * @param listener - the listener for the complete request
     * @param errorListener - the listener for errors during request
     */
    public APIEntityRequest(final int method, final String action, final String arguments, @Nullable final JSONObject body,
                            final Response.Listener<JSONObject> listener, final Response.ErrorListener errorListener) {
        super(
                method,
                BuildConfig.API_HOST + (TextUtils.isEmpty(BuildConfig.API_PORT ) ? "" : ":" + BuildConfig.API_PORT) + BuildConfig.API_VERSION + "/" + action + ((arguments!=null&&arguments!="")?"?" + (arguments):""),
                body,
                listener,
                errorListener);
        this.objectMapper = new ObjectMapper();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }

        headers.put("x-mtsk", BuildConfig.API_KEY);

        return headers;
    }

    @Override
    public Priority getPriority() {
        return Priority.HIGH;
    }

}
