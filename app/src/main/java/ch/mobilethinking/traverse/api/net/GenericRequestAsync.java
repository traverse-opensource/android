package ch.mobilethinking.traverse.api.net;

import android.text.TextUtils;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import ch.mobilethinking.traverse.BuildConfig;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class GenericRequestAsync<T> extends Request<T> {
    private static final String OUTPUT_FORMAT = ".json";
    private final Gson gson = new Gson();
    private final ObjectMapper objectMapper;
    final Response.Listener<T> listener;
    final Class<T> clazz;

    /**
     * Creates a APIEntityRequest instance ready to be added to the request queue.
     * @param action - the method to call in the API
     * @param arguments - the arguments to the method formatted like <argument_value>/<argument_value>...
     * @param listener - the listener for the complete request
     * @param errorListener - the listener for errors during request
     */
    public GenericRequestAsync(final int method, final String action, final String arguments, final Class<T> clazz,
                               final Response.Listener<T> listener, final Response.ErrorListener errorListener) {
        super(
                method,
                BuildConfig.API_HOST + (TextUtils.isEmpty(BuildConfig.API_PORT ) ? "" : ":" + BuildConfig.API_PORT) + BuildConfig.API_VERSION + "/" + action + ((arguments!=null&&arguments!="")?"?" + (arguments):""),
                errorListener);
        this.objectMapper = new ObjectMapper();
        this.listener = listener;
        this.clazz = clazz;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }

        headers.put("x-mtsk", BuildConfig.API_KEY);

        return headers;
    }

    @Override
    public Priority getPriority() {
        return Priority.HIGH;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String json = new String(
                    response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            //final T objResponse = (T) objectMapper.readValue(json, clazz);
            Response<T> resp = Response.success(
                    (T)json,
                    HttpHeaderParser.parseCacheHeaders(response));
            return resp;
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }
}
