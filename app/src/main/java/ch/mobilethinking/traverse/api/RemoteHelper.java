package ch.mobilethinking.traverse.api;

import ch.mobilethinking.traverse.BuildConfig;

/**
 * Created by valentinkim on 09.05.17.
 * The aim of this class is to provide static computation to remote resources
 */

public class RemoteHelper {

    /*public static String getRootRemotePath(){
        return String.format("%s:%s", BuildConfig.HOST, BuildConfig.PORT);
    }*/

    public static String getRootRemotePath(){
        return String.format("%s", BuildConfig.HOST);
    }

    public static String getRemotePath(String trailingPath){
        if (trailingPath.contains("http")){
            //sometimes we will have directly remote URL within cover path since we can have youtube thumbnails
            return trailingPath;
        }
        return String.format("%s/%s", getRootRemotePath(), trailingPath);
    }
}
