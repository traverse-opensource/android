package ch.mobilethinking.traverse.api.manager;

import android.content.Context;

import java.util.Locale;


/**
 * Created by jerome on 06.07.16.
 * Mobilethinking.ch
 */
public class APIManager {

    private static FicheManager ficheManager;
    private static PlaylistManager playlistManager;
    private static UserManager userManager;
    private static MixinManager mixinManager;
    private static FilterManager filterManager;

    private static Locale tempLanguage = null;

    public static FicheManager getFicheManager(Context context) {

        if (ficheManager == null) {
            ficheManager = new FicheManager(context);
        }

        return ficheManager;
    }

    public static PlaylistManager getPlaylistManager(Context context) {

        if (playlistManager == null) {
            playlistManager = new PlaylistManager(context);
        }

        return playlistManager;
    }
    public static UserManager getUserManager(Context context) {

        if (userManager == null) {
            userManager = new UserManager(context);
        }

        return userManager;
    }

    public static MixinManager getMixinManager(Context context) {

        if (mixinManager == null) {
            mixinManager = new MixinManager(context);
        }

        return mixinManager;
    }

    public static FilterManager getFilterManager(Context context) {

        if (filterManager == null) {
            filterManager = new FilterManager(context);
        }

        return filterManager;
    }



    /*public static ProjectManager getProjectManager(Context context) {

        if (projectManager == null) {
            projectManager = new ProjectManager(context);
        }

        return projectManager;
    }

    public static TourManager getTourManager(Context context) {

        if (tourManager == null) {
            tourManager = new TourManager(context);
        }

        return tourManager;
    }


    public static ZoneManager getZoneManager(Context context) {

        if (zoneManager == null) {
            zoneManager = new ZoneManager(context);
        }

        return zoneManager;
    }

    public static MediaManager getMediaManager(Context context) {

        if (mediaManager == null) {
            mediaManager = new MediaManager(context);
        }

        return mediaManager;
    }

    public static StaticPageManager getStaticPageManager(Context context) {

        if (staticPageManager == null) {
            staticPageManager = new StaticPageManager(context);
        }

        return  staticPageManager;
    }

    public static AgendaManager getAgendaManager(Context context) {

        if (agendaManager == null) {
            agendaManager = new AgendaManager(context);
        }

        return  agendaManager;
    }

    public static GuestbookManager getGuestbookManager(Context context) {

        if (guestbookManager == null) {
            guestbookManager = new GuestbookManager(context);
        }

        return  guestbookManager;
    }
    */
    public static void setTempLanguage(Locale locale) {
        tempLanguage = locale;
    }

    public static Locale getTempLanguage() {
        return tempLanguage;
    }
}

