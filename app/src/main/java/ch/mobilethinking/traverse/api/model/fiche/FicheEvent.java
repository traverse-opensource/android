package ch.mobilethinking.traverse.api.model.fiche;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;
import java.util.HashMap;

import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.SocialHolder;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.fiche.helper.FicheMap;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FicheEvent extends Fiche {

    @JsonProperty("start_date")
    String start_date;

    @JsonProperty("end_date")
    String end_date;

    @JsonProperty("delta_start")
    Integer deltaStart;

    @JsonProperty("delta_end")
    Integer deltaEnd;

    public FicheEvent(){ super(); }

    public FicheEvent(String id) {
        super(id);
    }

    @JsonCreator
    public FicheEvent(
            @JsonProperty("type") String type, @JsonProperty("_id") String id, @JsonProperty("name") String name,
            @JsonProperty("created_at") Date created_at, @JsonProperty("last_updated_at") Date last_updated_at, @JsonProperty("status") Integer status,
            @JsonProperty("cover") HashMap<String, String> cover, @JsonProperty("created_by") User created_by, @JsonProperty("last_update_by") User last_update_by,
            @JsonProperty("theme") Theme mainTheme, @JsonProperty("slug") String slug, @JsonProperty("userIds") String[] userIds,
            @JsonProperty("__t") String __t, @JsonProperty("short_description") String short_description, @JsonProperty("long_description") String long_description,
            @JsonProperty("related_fiches") Fiche[] relatedFiches, @JsonProperty("categories") Category[] categories,
            @JsonProperty("themes") Theme[] themes, @JsonProperty("sousCategories") String[] subCategories, @JsonProperty("tags") Tag[] tags,
            @JsonProperty("references") String references, @JsonProperty("social") SocialHolder social, @JsonProperty("city") String city,
            @JsonProperty("country") String country, @JsonProperty("latitude") Double latitude, @JsonProperty("longitude") Double longitude,
            @JsonProperty("location") HashMap<String, Object> location, @JsonProperty("map") FicheMap map,
            //custom properties
            @JsonProperty("start_date") String start_date, @JsonProperty("end_date") String end_date, @JsonProperty("delta_start") Integer deltaStart, @JsonProperty("delta_end") Integer deltaEnd){
        super(type, id, name, created_at, last_updated_at, status, cover, created_by, last_update_by, mainTheme, slug, userIds, __t, short_description, long_description, relatedFiches, categories,
                themes, subCategories, tags, references, social, city, country, latitude, longitude, location, map);
        setStart_date(start_date);
        setEnd_date(end_date);
        setDeltaStart(deltaStart);
        setDeltaEnd(deltaEnd);
    }

    public String getStart_date() { return start_date; }

    public void setStart_date(String start_date) { this.start_date = start_date; }

    public String getEnd_date() { return end_date; }

    public void setEnd_date(String end_date) { this.end_date = end_date; }

    public Integer getDeltaStart() { return deltaStart; }

    public void setDeltaStart(Integer deltaStart) { this.deltaStart = deltaStart; }

    public Integer getDeltaEnd() { return deltaEnd; }

    public void setDeltaEnd(Integer deltaEnd) { this.deltaEnd = deltaEnd; }

    @Override
    public String toString() {
        return super.toString() + " ### FicheEvent{" +
                "start_date='" + start_date + '\'' +
                ", end_date='" + end_date + '\'' +
                '}';
    }
}
