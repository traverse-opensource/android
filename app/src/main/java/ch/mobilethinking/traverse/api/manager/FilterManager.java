package ch.mobilethinking.traverse.api.manager;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;

import bolts.Task;
import ch.mobilethinking.traverse.api.dao.ApiDAO;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FilterManager {

    private final Context context;

    FilterManager(Context context) {
        this.context = context;
    }

    public Task<Tag[]> getTags() {
        return ApiDAO.getFilterDAO(context).getTags();
    }

    public Task<Category[]> getCategories() {
        return ApiDAO.getFilterDAO(context).getCategories();
    }

    public Task<Theme[]> getThemes() {
        return ApiDAO.getFilterDAO(context).getThemes();
    }

    public Task<MixinItem[]> getAllResults(Double latitude, Double longitude, @Nullable FilterCreator filter) {
        return ApiDAO.getFilterDAO(context).getAllResults(latitude,longitude, filter);
    }

    public Task<MixinItem[]> getPaginatedResults(Double latitude, Double longitude, int page, int limit, @Nullable FilterCreator filter) {
        return ApiDAO.getFilterDAO(context).getPaginatedResults(latitude,longitude, page, limit, filter);
    }

    public Task<MixinItem[]> getPaginatedResultsNoRadius(Double latitude, Double longitude, int page, int limit, @Nullable FilterCreator filter) {
        return ApiDAO.getFilterDAO(context).getPaginatedResultsNoRadius(latitude,longitude, page, limit, filter);
    }
}