package ch.mobilethinking.traverse.api.dao.argument;

import android.net.Uri;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by valentinkim on 03.07.17.
 * This class aims to play with filter parameters using chainable method
 *
 * If we want to filter on categories and themes we need to pass to MixinDao the  below argument
 *
 * FilterCreator filter = new FilterCreator()
 *  .addThemeName("Sites naturels ou paysagers")
 *  .addTagId("591ea33efda54c285ef9a0cf")
 *  .addCategoryId("58d54043a071ddd0d1129a65")
 *  .addFicheType("Places")
 *  .usesPlaylist();
 *
 *  If you need to check the resulting uri argument you can call the method .build() at the end of the chain
 */

public class FilterCreator implements Cloneable{

    private static final String TAG = FilterCreator.class.getSimpleName();
    private List<String> tagIds;
    private List<String> themeNames;
    private List<String> categoryIds;
    private List<String> ficheTypes;
    private boolean includesPlaylist;
    private boolean isFirstArg;
    private long date;

    public FilterCreator(){
        resetFilters();
    }

    public FilterCreator resetFilters() {
        tagIds = new ArrayList<>();
        themeNames = new ArrayList<>();
        categoryIds = new ArrayList<>();
        ficheTypes = new ArrayList<>();
        includesPlaylist = false;
        isFirstArg = true;
        return this;
    }

    public List<String> getTagIds() {
        return tagIds;
    }

    public void setTagIds(List<String> tagIds) {
        this.tagIds = tagIds;
    }

    public List<String> getThemeNames() {
        return themeNames;
    }

    public void setThemeNames(List<String> themeNames) {
        this.themeNames = themeNames;
    }

    public List<String> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<String> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<String> getFicheTypes() {
        return ficheTypes;
    }

    public void setFicheTypes(List<String> ficheTypes) {
        this.ficheTypes = ficheTypes;
    }

    public boolean getUsesPlaylist () {
        return includesPlaylist;
    }

    public FilterCreator addTagId(String tagId) {
        tagIds.add(tagId);
        return this;
    }

    public FilterCreator removeTagId(String tagId) {
        boolean res = tagIds.remove(tagId);
        return this;
    }

    public FilterCreator addThemeName(String themeName) {
        themeNames.add(themeName);
        return this;
    }

    public FilterCreator removeThemeName(String themeName) {
        boolean res = themeNames.remove(themeName);
        return this;
    }

    public FilterCreator addCategoryId(String categoryId) {
        categoryIds.add(categoryId);
        return this;
    }

    public FilterCreator removeCategoryId(String categoryId) {
        boolean res = categoryIds.remove(categoryId);
        return this;
    }

    public FilterCreator addFicheType(String ficheType){
        ficheTypes.add(ficheType);
        return this;
    }

    public FilterCreator removeFicheType(String ficheType){
        boolean res = ficheTypes.remove(ficheType);
        Log.d(TAG, "The type to remove is " + ficheType + " and the result was " + res);
        return this;
    }

    public FilterCreator usesPlaylist(boolean b){
        includesPlaylist = b;
        return this;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    private String createURIArgument(String key, String value) {
        String toReturn = String.format(Locale.ENGLISH, "&"+key+"=%s", Uri.encode(value));
        isFirstArg = false;
        return toReturn;
    }

    //This is called automatically when the request will be built. But you can call this method if you need to debug the request path
    public String build(){
        String toReturn = "";

        if (tagIds.size() > 0) {
            for (String tag: tagIds) {
                toReturn += createURIArgument("tags", tag);
            }
        }

        if (themeNames.size() > 0) {
            for (String theme: themeNames) {
                toReturn += createURIArgument("themes", theme);
            }
        }

        if (categoryIds.size() > 0) {
            for (String category: categoryIds) {
                toReturn += createURIArgument("categories", category);
            }
        }

        if (ficheTypes.size() > 0) {
            for (String type: ficheTypes) {
                toReturn += createURIArgument("types", type);
            }
        }

        if (includesPlaylist) {
            toReturn += createURIArgument("includesPlaylist", "1");
        } else {
            toReturn += createURIArgument("includesPlaylist", "0");
        }

        return toReturn;
    }

    public boolean isEmpty () {
        if(tagIds.isEmpty() && categoryIds.isEmpty() && themeNames.isEmpty() && ficheTypes.isEmpty()){
          return true;
        }
        return false;
    }

}
