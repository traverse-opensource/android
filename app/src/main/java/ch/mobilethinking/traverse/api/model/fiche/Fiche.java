package ch.mobilethinking.traverse.api.model.fiche;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.Constants;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.MongoEntity;
import ch.mobilethinking.traverse.api.model.SocialHolder;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.fiche.helper.FicheMap;

/**
 * Created by valentinkim on 09.05.17.
 * Base Fiche, to get a fiche list, we need to instanciate the children class on runtime
 */

//shall use concrete and abstract class here

@JsonIgnoreProperties({ "playlists", "types", "__v"
})
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXTERNAL_PROPERTY,
        property = "type",
        defaultImpl = Fiche.class)
@JsonSubTypes({
        @Type(value = FicheEvent.class, name = Constants.TYPE_FICHE_EVENT),
        @Type(value = FicheMedia.class, name = Constants.TYPE_FICHE_MEDIA),
        @Type(value = FicheObject.class, name = Constants.TYPE_FICHE_OBJECT),
        @Type(value = FichePeople.class, name = Constants.TYPE_FICHE_PERSON),
        @Type(value = FichePlace.class, name = Constants.TYPE_FICHE_PLACE)
})
public class Fiche extends MixinItem{

    @JsonProperty("related_fiches")
    Fiche[] relatedFiches;

    @JsonProperty("categories")
    Category[] categories;

    @JsonProperty("themes")
    Theme[] themes;

    @JsonProperty("sousCategories")
    String[] subCategories;

    @JsonProperty("tags")
    Tag[] tags;

    @JsonProperty("references")
    String references;

    @JsonProperty("social")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    SocialHolder social;

    @JsonProperty("__t")
    String __t;

    @JsonProperty("short_description")
    String short_description;

    @JsonProperty("long_description")
    String long_description;

    @JsonProperty("city")
    String city;

    @JsonProperty("country")
    String country;

    @JsonProperty("latitude")
    Double latitude;

    @JsonProperty("longitude")
    Double longitude;

    @JsonProperty("location")
    HashMap<String, Object> location;

    @JsonProperty("map")
    FicheMap map;

    public Fiche(){ super(); }

    @JsonCreator
    public Fiche(String id) {
        super(id);
    }

    public Fiche(
            @JsonProperty("type") String type, @JsonProperty("_id") String id, @JsonProperty("name") String name,
            @JsonProperty("created_at") Date created_at, @JsonProperty("last_updated_at") Date last_updated_at, @JsonProperty("status") Integer status,
            @JsonProperty("cover") HashMap<String, String> cover, @JsonProperty("created_by") User created_by, @JsonProperty("last_update_by") User last_update_by,
            @JsonProperty("theme") Theme mainTheme, @JsonProperty("slug") String slug, @JsonProperty("userIds") String[] userIds,
            //custom
            @JsonProperty("__t") String __t, @JsonProperty("short_description") String short_description, @JsonProperty("long_description") String long_description,
            @JsonProperty("related_fiches") Fiche[] relatedFiches, @JsonProperty("categories") Category[] categories,
            @JsonProperty("themes") Theme[] themes, @JsonProperty("sousCategories") String[] subCategories, @JsonProperty("tags") Tag[] tags,
            @JsonProperty("references") String references, @JsonProperty("social") SocialHolder social, @JsonProperty("city") String city,
            @JsonProperty("country") String country, @JsonProperty("latitude") Double latitude, @JsonProperty("longitude") Double longitude,
            @JsonProperty("location") HashMap<String, Object> location, @JsonProperty("map") FicheMap map){
        super(type, id, name, created_at, last_updated_at, status, cover, created_by, last_update_by, mainTheme, slug, userIds);
        set__t(__t);
        setShort_description(short_description);
        setLong_description(long_description);
        setRelatedFiches(relatedFiches);
        setCategories(categories);
        setThemes(themes);
        setSubCategories(subCategories);
        setTags(tags);
        setReferences(references);
        setSocial(social);
        setCity(city);
        setCountry(country);
        setLatitude(latitude);
        setLongitude(longitude);
        setLocation(location);
        setMap(map);
    }

    public Fiche[] getRelatedFiches() { return relatedFiches; }

    public void setRelatedFiches(Fiche[] relatedFiches) { this.relatedFiches = relatedFiches; }

    public Category[] getCategories() { return categories; }

    public void setCategories(Category[] categories) { this.categories = categories; }

    public Theme[] getThemes() { return themes; }

    public void setThemes(Theme[] themes) { this.themes = themes; }

    public String[] getSubCategories() { return subCategories; }

    public void setSubCategories(String[] subCategories) { this.subCategories = subCategories; }

    public Tag[] getTags() { return tags; }

    public void setTags(Tag[] tags) { this.tags = tags; }

    public String getReferences() { return references; }

    public void setReferences(String references) { this.references = references; }

    public SocialHolder getSocial() { return social; }

    public void setSocial(SocialHolder social) { this.social = social; }

    public String get__t() { return __t; }

    public void set__t(String __t) { this.__t = __t; }

    public String getShort_description() { return short_description; }

    public void setShort_description(String short_description) { this.short_description = short_description; }

    public String getLong_description() { return long_description; }

    public void setLong_description(String long_description) { this.long_description = long_description; }

    public String getType() { return getClass().getName().toString(); }

    public String getCity() { return city; }

    public void setCity(String city) { this.city = city; }

    public String getCountry() { return country; }

    public void setCountry(String country) { this.country = country; }

    public Double getLatitude() { return latitude; }

    public void setLatitude(Double latitude) { this.latitude = latitude; }

    public Double getLongitude() { return longitude; }

    public void setLongitude(Double longitude) { this.longitude = longitude; }

    public HashMap<String, Object> getLocation() { return location; }

    public void setLocation(HashMap<String, Object> location) { this.location = location; }

    public Double[] getCoordinates() { return (Double[]) location.get("coordinates"); }

    public FicheMap getMap() { return map; }

    public void setMap(FicheMap map) { this.map = map; }

    public static class FicheList extends MongoEntity {

        @JsonProperty("fiches")
        List<Fiche> fiches;

        public FicheList(){
            fiches = new ArrayList<>();
        }

        public List<Fiche> getFiches() {
            return fiches;
        }

        public void setFiches(List<Fiche> fiches) {
            this.fiches = fiches;
        }
    }

    @Override
    public String toString() {
        return "Fiche{" +
                "name='" + getName() + '\'' +
                ", createdAt=" + getCreated_at() +
                ", lastUpdatedAt=" + getLast_updated_at() +
                ", status=" + getStatus() +
                ", created_by=" + getCreated_by() +
                ", last_update_by=" + getLast_updated_at() +
                ", relatedFiches=" + Arrays.toString(relatedFiches) +
                ", cover=" + getCoverPath() +
                ", categories=" + Arrays.toString(categories) +
                ", themes=" + Arrays.toString(themes) +
                ", subCategories=" + Arrays.toString(subCategories) +
                ", tags=" + Arrays.toString(tags) +
                ", references='" + references + '\'' +
                ", social=" + social +
                ", __t='" + __t + '\'' +
                ", short_description='" + short_description + '\'' +
                ", long_description='" + long_description + '\'' +
                '}';
    }
    
}
