package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by valentinkim on 11.05.17.
 * Base class that has at least mapping between the base props from the mongo database
 */

public abstract class MongoEntity implements APIEntity {

    public static class STATUS {
        public static Integer DRAFT = 0;
        public static Integer PUBLISHED = 1;
        public static Integer DELETED = 2;
    }

    @JsonProperty("_id")
    String id;

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }
}
