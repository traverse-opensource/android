package ch.mobilethinking.traverse.api.dao;

import org.json.JSONObject;

import bolts.Task;
import ch.mobilethinking.traverse.api.model.MixinList;
import ch.mobilethinking.traverse.api.model.SimpleResponse;
import ch.mobilethinking.traverse.api.model.User;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 11.05.17.
 */

public interface UserDAO {

    String CONTROLLER = "users";
    String CONTROLLER_SINGLE_USER = "one";
    String CONTROLLER_SIGNUP = "signup";
    String CONTROLLER_LOGIN = "login";
    String CONTROLLER_PLAYLISTS = "playlists";
    String CONTROLLER_RESET_PASSSWORD = "forgotPassword";

    Task<User> getRemoteUser(String userId);
    Task<User> getUser(String userId);
    Task<User.UserList> getRemoteUsers();
    Task<User.UserList> getUsers();
    Task<User> signUp(JSONObject user);
    Task<User> login(JSONObject user);
    Task<Playlist.PlaylistList> getUserPlaylists(String userId);
    Task<SimpleResponse> resetPassword(String email);
    Boolean saveUserPlaylists(String userId);
}
