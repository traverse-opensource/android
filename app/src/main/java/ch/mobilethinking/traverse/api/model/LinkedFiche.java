package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import ch.mobilethinking.traverse.api.model.fiche.Fiche;

/**
 * Created by valentinkim on 11.05.17.
 */

@JsonIgnoreProperties({ "__v" })
public class LinkedFiche extends MongoEntity {

    @JsonProperty("fiche")
    Fiche ficheId;

    @JsonProperty("value")
    String value;

    @JsonProperty("next")
    LinkedFiche next;

    public LinkedFiche() {}

    public LinkedFiche(String objectId) {
        setId(objectId);
    }

    public LinkedFiche(String objectId, Fiche fiche, String value, LinkedFiche next) {
        setId(objectId);
        setFiche(fiche);
        setValue(value);
        setNext(next);
    }

    public Fiche getFiche() { return ficheId; }

    public void setFiche(Fiche fiche) { this.ficheId = fiche; }

    public String getValue() { return value; }

    public void setValue(String value) { this.value = value; }

    public LinkedFiche getNext() { return next; }

    public void setNext(LinkedFiche next) { this.next = next; }
}
