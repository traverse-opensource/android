package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by valentinkim on 26.06.17.
 */

@JsonIgnoreProperties({ "__v" })
public class Role extends MongoEntity {

    @JsonProperty("title")
    String title;
    @JsonProperty("description")
    String description;

    public Role() {
    }

    public Role (String id) {
        setId(id);
    }

    public Role(String id, String title, String description) {
        setId(id);
        setTitle(title);
        setDescription(description);
    }

    public String getTitle() { return title; }

    public void setTitle(String title) { this.title = title; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
