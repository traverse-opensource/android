package ch.mobilethinking.traverse.api.dao;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.List;

import bolts.Task;
import ch.mobilethinking.traverse.api.model.SocialFeed;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 11.05.17.
 */

public interface FicheDAO {

    String CONTROLLER = "fiches";
    String CONTROLLER_FICHE_PLAYLISTS = "playlists";
    String CONTROLLER_SOCIAL_FEED = "feeds";
    String CONTROLLER_FLAG = "flag";
    String CONTROLLER_LIKE = "like";
    String CONTROLLER_DISLIKE = "dislike";

    Task<Fiche> getRemoteFiche(String ficheId);
    Task<Fiche> getFiche(String ficheId);
    Task<Playlist.PlaylistList> getFichePlaylists(String ficheId);
    Task<Fiche.FicheList> getRemoteFiches();
    Task<Fiche.FicheList> getFiches();
    Task<Fiche> likeFiche(String ficheId, String userId);
    Task<Fiche> dislikeFiche(String ficheId, String userId);
    Boolean saveFiche(String ficheId);
    Task<SocialFeed[]> getFicheFeed(String ficheId);
    Task<SocialFeed> addFeed(String ficheId, JSONObject params, File imageFile);
    Task<String> flagFeed(String feedId);

}
