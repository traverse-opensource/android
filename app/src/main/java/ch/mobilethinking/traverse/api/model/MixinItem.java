package ch.mobilethinking.traverse.api.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.Date;
import java.util.HashMap;

import ch.mobilethinking.traverse.api.RemoteHelper;
import ch.mobilethinking.traverse.api.model.fiche.Fiche;
import ch.mobilethinking.traverse.api.model.playlist.Playlist;

/**
 * Created by valentinkim on 26.06.17.
 * This class aims to be able to call a list of a mix between fiche and playlist so that we
 * can interpret directly the list without checking the json property fiches nor playlists
 *
 * So this class is the father class of Fiche and Playlist model which holds common attributes
 */

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type"
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Fiche.class, name = Constants.TYPE_FICHE),
        @JsonSubTypes.Type(value = Playlist.class, name = Constants.TYPE_PLAYLIST)
})
public abstract class MixinItem extends MongoEntity {

    @JsonProperty("type")
    String type;

    @JsonProperty("name")
    String name;

    @JsonProperty("created_at")
    Date created_at;

    @JsonProperty("last_updated_at")
    Date last_updated_at;

    @JsonProperty("status")
    Integer status;

    @JsonProperty("cover")
    HashMap<String, String> cover;

    @JsonProperty("created_by")
    User created_by;

    @JsonProperty("last_update_by")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    User last_update_by;

    @JsonProperty("theme")
    Theme mainTheme;

    @JsonProperty("slug")
    String slug;

    @JsonProperty("userIds")
    String[] userIds;

    public MixinItem() { }

    public MixinItem(@JsonProperty("_id") String id) { setId(id); }

    public MixinItem(
            @JsonProperty("type") String type, @JsonProperty("_id") String id, @JsonProperty("name") String name,
            @JsonProperty("created_at") Date created_at, @JsonProperty("last_updated_at") Date last_updated_at, @JsonProperty("status") Integer status,
            @JsonProperty("cover") HashMap<String, String> cover, @JsonProperty("created_by") User created_by, @JsonProperty("last_update_by") User last_update_by,
            @JsonProperty("theme") Theme mainTheme, @JsonProperty("slug") String slug, @JsonProperty("userIds") String[] userIds){
        setType(type);
        setId(id);
        setName(name);
        setCreated_at(created_at);
        setLast_updated_at(last_updated_at);
        setStatus(status);
        setCover(cover);
        setCreated_by(created_by);
        setLast_update_by(last_update_by);
        setMainTheme(mainTheme);
        setSlug(slug);
        setUserIds(userIds);
    }

    /*
     * Convenient methods
     */

    public boolean isDraft() { return status == STATUS.DRAFT; }

    public boolean isPublished() { return status == STATUS.PUBLISHED; }

    public boolean isDeleted() { return status == STATUS.DELETED; }

    /*
     * end Convenient
     */

    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

    public Date getCreated_at() { return created_at; }

    public void setCreated_at(Date created_at) { this.created_at = created_at; }

    public Integer getStatus() { return status; }

    public void setStatus(Integer status) { this.status = status; }

    public HashMap<String, String> getCover() { return cover; }

    public void setCover(HashMap<String, String> cover) { this.cover = cover; }

    public String getCoverPath() {
        return cover != null ?
                RemoteHelper.getRemotePath(cover.get("path")):
                null;
    }

    public User getCreated_by() { return created_by; }

    public void setCreated_by(User created_by) { this.created_by = created_by; }

    public Date getLast_updated_at() { return last_updated_at; }

    public void setLast_updated_at(Date last_updated_at) { this.last_updated_at = last_updated_at; }

    public User getLast_update_by() { return last_update_by; }

    public void setLast_update_by(User last_update_by) { this.last_update_by = last_update_by; }

    public Theme getMainTheme() { return mainTheme; }

    public void setMainTheme(Theme mainTheme) { this.mainTheme = mainTheme; }

    public String getSlug() { return slug; }

    public void setSlug(String slug) { this.slug = slug; }

    public String[] getUserIds() { return userIds; }

    public void setUserIds(String[] userIds) { this.userIds = userIds; }
}
