package ch.mobilethinking.traverse.api.dao.impl;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;
import com.vincentbrison.openlibraries.android.dualcache.JsonSerializer;

import java.util.Locale;

import bolts.Continuation;
import bolts.Task;
import ch.mobilethinking.traverse.Utils.PreferenceManager;
import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.dao.FilterDAO;
import ch.mobilethinking.traverse.api.model.Category;
import ch.mobilethinking.traverse.api.model.MixinItem;
import ch.mobilethinking.traverse.api.model.Tag;
import ch.mobilethinking.traverse.api.model.Theme;

/**
 * Created by jerome on 05.07.16.
 * Mobilethinking.ch
 */
public class FilterDAOImpl extends AbstractAPIManagerImpl implements FilterDAO {

    private static final String MAP_FILTER_CACHE = "map_filter_cache";
    private static final String FILTER_RESULT_CACHE = "filter_result_cache";

    private static final String TAG_LIST_CACHE = "tag_list_cache";
    private static final String TAG_LIST_RESULT_CACHE = "tag_list_result_cache";

    private static final String CATEGORY_LIST_CACHE = "category_list_cache";
    private static final String CATEGORY_LIST_RESULT_CACHE = "category_list_result_cache";

    private static final String THEME_LIST_CACHE = "theme_list_cache";
    private static final String THEME_LIST_RESULT_CACHE = "theme_list_result_cache";

    private final DualCache<Tag[]> tagCache;
    private final DualCache<Category[]> categoryCache;
    private final DualCache<Theme[]> themeCache;

    public FilterDAOImpl(Context context) {
        super(context);

        //TODO check this later on
        final CacheSerializer<Tag[]> tagJsonSerializer =
                new JsonSerializer<>(Tag[].class);

        tagCache = new Builder<>(TAG_LIST_CACHE, FIX_APP_VERSION, Tag[].class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, tagJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, tagJsonSerializer, context)
                .build();

        Log.wtf("FDI", "file cache location> " + tagCache.getDiskCacheFolder().getAbsolutePath());

        final CacheSerializer<Category[]> categoryJsonSerializer =
                new JsonSerializer<>(Category[].class);

        categoryCache = new Builder<>(CATEGORY_LIST_CACHE, FIX_APP_VERSION, Category[].class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, categoryJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, categoryJsonSerializer, context)
                .build();

        Log.wtf("FDI", "file cache location> " + categoryCache.getDiskCacheFolder().getAbsolutePath());

        final CacheSerializer<Theme[]> themeJsonSerializer =
                new JsonSerializer<>(Theme[].class);

        themeCache = new Builder<>(THEME_LIST_CACHE, FIX_APP_VERSION, Theme[].class)
                .enableLog()
                .useSerializerInRam(RAM_MAX_SIZE, themeJsonSerializer)
                .useSerializerInDisk(DISK_MAX_SIZE, true, themeJsonSerializer, context)
                .build();

        Log.wtf("FDI", "file cache location> " + themeCache.getDiskCacheFolder().getAbsolutePath());
    }

    @Override
    public Task<Tag[]> getTags() {
        String requestPath = CONTROLLER_FILTERS_TAGS;

        return addRequestArray(requestPath, Tag[].class).onSuccessTask(new Continuation<Tag[], Task<Tag[]>>() {
            @Override
            public Task<Tag[]> then(Task<Tag[]> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Category[]> getCategories() {
        String requestPath = CONTROLLER_FILTERS_CATEGORIES;

        return addRequestArray(requestPath, Category[].class).onSuccessTask(new Continuation<Category[], Task<Category[]>>() {
            @Override
            public Task<Category[]> then(Task<Category[]> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<Theme[]> getThemes() {
        String requestPath = CONTROLLER_FILTERS_THEMES;

        return addRequestArray(requestPath, Theme[].class).onSuccessTask(new Continuation<Theme[], Task<Theme[]>>() {
            @Override
            public Task<Theme[]> then(Task<Theme[]> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<MixinItem[]> getAllResults(Double latitude, Double longitude, @Nullable FilterCreator filter) {

        String requestPath;
        int radius = PreferenceManager.getSearchRadius(getContext());

        if(filter != null) {
            requestPath = String.format(Locale.ENGLISH, "%s?maxDistance=%d&lat=%f&lng=%f&page=1&limit=10000%s", CONTROLLER_FILTERS_ALL, radius, latitude, longitude, filter.build());
        } else {
            requestPath = String.format(Locale.ENGLISH, "%s?maxDistance=%d&lat=%f&lng=%f&page=1&limit=10000", CONTROLLER_FILTERS_ALL, radius, latitude, longitude);
        }

        return addRequestArray(requestPath, MixinItem[].class).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<MixinItem[]> getPaginatedResults(Double latitude, Double longitude, int page, int limit, @Nullable FilterCreator filter) {

        String requestPath;
        int radius = PreferenceManager.getSearchRadius(getContext());

        if(filter != null) {
            requestPath = String.format(Locale.ENGLISH, "%s?maxDistance=%d&lat=%f&lng=%f&page=%d&limit=%d%s", CONTROLLER_FILTERS_ALL, radius, latitude, longitude, page, limit, filter.build());
        } else {
            requestPath = String.format(Locale.ENGLISH, "%s?maxDistance=%d&lat=%f&lng=%f&page=%d&limit=%d", CONTROLLER_FILTERS_ALL, radius, latitude, longitude, page, limit);
        }

        return addRequestArray(requestPath, MixinItem[].class).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {
                return task;
            }
        });
    }

    @Override
    public Task<MixinItem[]> getPaginatedResultsNoRadius(Double latitude, Double longitude, int page, int limit, @Nullable FilterCreator filter) {

        String requestPath;
        int radius = 30000000;

        if(filter != null) {
            requestPath = String.format(Locale.ENGLISH, "%s?maxDistance=%d&lat=%f&lng=%f&page=%d&limit=%d%s", CONTROLLER_FILTERS_ALL, radius, latitude, longitude, page, limit, filter.build());
        } else {
            requestPath = String.format(Locale.ENGLISH, "%s?maxDistance=%d&lat=%f&lng=%f&page=%d&limit=%d", CONTROLLER_FILTERS_ALL, radius, latitude, longitude, page, limit);
        }

        return addRequestArray(requestPath, MixinItem[].class).onSuccessTask(new Continuation<MixinItem[], Task<MixinItem[]>>() {
            @Override
            public Task<MixinItem[]> then(Task<MixinItem[]> task) throws Exception {
                return task;
            }
        });
    }

}
