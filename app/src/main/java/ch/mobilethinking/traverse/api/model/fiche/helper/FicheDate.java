package ch.mobilethinking.traverse.api.model.fiche.helper;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

import ch.mobilethinking.traverse.api.model.MongoEntity;

/**
 * Created by valentinkim on 11.05.17.
 */

public class FicheDate extends MongoEntity{

    @JsonProperty("date")
    Date date;

    @JsonProperty("description")
    String description;

    public Date getDate() { return date; }

    public void setDate(Date date) { this.date = date; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }
}
