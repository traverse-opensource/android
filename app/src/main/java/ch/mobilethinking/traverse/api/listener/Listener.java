package ch.mobilethinking.traverse.api.listener;

/**
 * Created by jerome on 06.07.16.
 * Mobilethinking.ch
 */
public interface Listener {

    public void onCompleted();

}
