package ch.mobilethinking.traverse.api.model;

import android.provider.MediaStore;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

import ch.mobilethinking.traverse.api.model.fiche.Fiche;

/**
 * Created by carlosballester on 11.05.17.
 */

public class SocialFeed extends MongoEntity {

    @JsonProperty("type")
    String type;

    @JsonProperty("name")
    String name;

    @JsonProperty("description")
    String description;

    @JsonProperty("permalink")
    String permalink;

    @JsonProperty("kind")
    String kind;

    @JsonProperty("created_at")
    String created_at;

    @JsonProperty("cover")
    SocialCover cover;

    @JsonProperty("video")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    SocialVideo video;

    @JsonProperty("created_by")
    SocialUser created_by;

    public SocialFeed() {}

    public SocialFeed(String id){ setId(id); }

    public SocialFeed(@JsonProperty("_id") String id, @JsonProperty("type") String type, @JsonProperty("name") String name,
                      @JsonProperty("description") String description, @JsonProperty("permalink") String permalink, @JsonProperty("kind") String kind,
                      @JsonProperty("created_at") String created_at, @JsonProperty("cover") SocialCover cover, @JsonProperty("video") SocialVideo video,
                      @JsonProperty("created_by") SocialUser created_by) {
        setId(id);
        setType(type);
        setName(name);
        setDescription(description);
        setPermalink(permalink);
        setKind(kind);
        setCreated_at(created_at);
        setCover(cover);
        setVideo(video);
        setCreated_by(created_by);

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPermalink() {
        return permalink;
    }

    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public SocialCover getCover() {
        return cover;
    }

    public void setCover(SocialCover cover) {
        this.cover = cover;
    }

    public SocialVideo getVideo() {
        return video;
    }

    public void setVideo(SocialVideo video) {
        this.video = video;
    }

    public SocialUser getCreated_by() {
        return created_by;
    }

    public void setCreated_by(SocialUser created_by) {
        this.created_by = created_by;
    }

    @Override
    public String toString() {
        return "SocialFeed{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", permalink='" + permalink + '\'' +
                ", kind='" + kind + '\'' +
                ", created_at='" + created_at + '\'' +
                ", cover=" + cover +
                ", video=" + video +
                ", created_by=" + created_by +
                '}';
    }

    public static class SocialCover implements APIEntity {

        @JsonProperty("big")
        String big;

        @JsonProperty("path")
        String path;

        @JsonProperty("thumb")
        String thumb;

        public SocialCover() {
        }

        public SocialCover(String big, String path, String thumb) {
            setBig(big);
            setPath(path);
            setThumb(thumb);
        }

        public String getBig() {
            return big;
        }

        public void setBig(String big) {
            this.big = big;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        @Override
        public String toString() {
            return "SocialCover{" +
                    "big='" + big + '\'' +
                    ", path='" + path + '\'' +
                    ", thumb='" + thumb + '\'' +
                    '}';
        }
    }

    public static class SocialVideo implements APIEntity {
        @JsonProperty("big")
        String big;

        @JsonProperty("path")
        String path;

        @JsonProperty("thumb")
        String thumb;

        public SocialVideo() {
        }

        public SocialVideo(String big, String path, String thumb) {
            setBig(big);
            setPath(path);
            setThumb(thumb);
        }

        public String getBig() {
            return big;
        }

        public void setBig(String big) {
            this.big = big;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        @Override
        public String toString() {
            return "SocialVideo{" +
                    "big='" + big + '\'' +
                    ", path='" + path + '\'' +
                    ", thumb='" + thumb + '\'' +
                    '}';
        }
    }

    public static class SocialUser implements APIEntity {
        @JsonProperty("name")
        String name;

        @JsonProperty("full_name")
        String full_name;

        @JsonProperty("photo_url")
        String photo_url;

        public SocialUser() {
        }

        public SocialUser(String name, String full_name, String photo_url) {
            setName(name);
            setFull_name(full_name);
            setPhoto_url(photo_url);
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getFull_name() {
            return full_name;
        }

        public void setFull_name(String full_name) {
            this.full_name = full_name;
        }

        public String getPhoto_url() {
            return photo_url;
        }

        public void setPhoto_url(String photo_url) {
            this.photo_url = photo_url;
        }

        @Override
        public String toString() {
            return "SocialUser{" +
                    "name='" + name + '\'' +
                    ", full_name='" + full_name + '\'' +
                    ", photo_url='" + photo_url + '\'' +
                    '}';
        }
    }

    public static class SocialFeedList extends MongoEntity {

        @JsonProperty("feed")
        List<SocialFeed> feed;

        public SocialFeedList(){
            feed = new ArrayList<>();
        }

        public List<SocialFeed> getFeed() {
            return feed;
        }

        public void setFeed(List<SocialFeed> feed) {
            this.feed = feed;
        }

        @Override
        public String toString() {
            return "SocialFeedList{" +
                    "feed=" + feed +
                    '}';
        }
    }

}
