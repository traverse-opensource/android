package ch.mobilethinking.traverse;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.location.Location;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDexApplication;
import android.util.DisplayMetrics;
import android.util.Log;

import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import ch.mobilethinking.traverse.api.dao.argument.FilterCreator;
import ch.mobilethinking.traverse.api.manager.APIManager;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.twitter.sdk.android.core.Twitter;

/**
 * Created by Carlos Ballester on 19-Apr-17.
 */

public class Traverse extends MultiDexApplication {

    public static final String TAG = Traverse.class.getSimpleName();
    public static final String PREF_MODE_SELECTION_STREAMING = "PREF_MODE_SELECTION_STREAMING";
    public static final String ONBOARDING_STEP = "ONBOARDING_STEP";;
    public static final int STEP_DISCLAIMER_HEADSET = 0;
    public static final int STEP_DISCLAIMER_MODE_SELECTION = 1;
    public static final int STEP_DISCLAIMER_BLUETOOTH = 2;
    public static final int STEP_DISCLAIMER_MAIN = 3;
    private Tracker mTracker;
    private static final LatLng defaultLocation = new LatLng(46.2050242, 6.1090692);
    private static Location lastKnownLocation = null;
    private static FilterCreator filter = null;

    private static Context context;

    @Override
    public void onCreate() {

        super.onCreate();
        Log.d("ON APP START", "Starting app class");

        /*Locale lo = new Locale(PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString(getString(R.string.lang_settings_select_lang_key), getString(R.string.lang_settings_selected_lang_default)));

        //set the locale for dea api calls
        APIManager.setTempLanguage(lo);*/

        Twitter.initialize(this);
        AppEventsLogger.activateApp(this);

        Resources res = getResources();
        // Change locale settings in the app.
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        //check ow to do this on api level 16
        //conf.setLocale(lo);

        res.updateConfiguration(conf, dm);
//        Locale.setDefault(lo);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(BuildConfig.DEFAULT_FONT_PATH)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }

    /*synchronized public Tracker getDefaultTracker() {

        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker(R.xml.global_tracker);
            //comment the line above and uncomment the next line to be able to compile in debug mode
            //mTracker = analytics.newTracker("ABCDE"); //fake tracker object to make the app run in debug mode
        }
        return mTracker;
    }*/

    public static Context getContext() {
        return Traverse.context;
    }

    public static LatLng getDefaultLocation() {
        return defaultLocation;
    }

    public static LatLng getLastKnownLocation() {
        if(lastKnownLocation != null) {
            Log.d(TAG, "Last known location retrieved");
            return new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
        } else {
            Log.d(TAG, "Default location retrieved");
            return defaultLocation;
        }
    }

    public static void setLastKnownLocation(Location lastKnownLocation) {
        Traverse.lastKnownLocation = lastKnownLocation;
    }

    public static FilterCreator getFilter() {
        return filter;
    }

    public static void setFilter(FilterCreator filter) {
        Traverse.filter = filter;
    }
}
