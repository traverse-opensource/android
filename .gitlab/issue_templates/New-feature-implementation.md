# NEW FEATURE


## Description

(Describe on a high level your new feature)

---
## Scenario

(By default we only list the possible problematic scenarios.)

```gherkin
Feature: (This is a possible new feature)

   Scenario: (This would be the scenario)
      Given (That we accept this)
         Then  (Do that and that)
         Then  (Create something new)
```
---
## Screenshot

(Place the feature screenshot here)

---
## Ressources

### Assets

(Link to any assets here)

---
### Texts

(Provide any available text here)

---