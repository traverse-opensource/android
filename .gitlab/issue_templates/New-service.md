# NEW SERVICE

## Description

(Describe on a high level your new feature)

---
## Scenario

(By default we only list the possible problematic scenarios.)

```gherkin
Feature: (This is a possible new feature)

   Scenario: (This would be the scenario)
      Given (That we accept this)
         Then  (Do that and that)
         Then  (Create something new)
```
---
## Screenshot

(Place the feature screenshot here)

---
## Resources

### Links

(Link to other issues or external links)

---
### End points

(List and describe all the end points)

---
### Request type and description

(List and describe what you call and any extra parameters ex: POST param1 = xxx)

---

### Response type and description

(List and describe what you get back ex: Json + format)

---